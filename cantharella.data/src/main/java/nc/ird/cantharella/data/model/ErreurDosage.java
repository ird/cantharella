package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import org.apache.commons.beanutils.BeanComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "nom"))
public class ErreurDosage  extends AbstractModel implements Comparable<ErreurDosage> {

    /** Id de l'erreur */
    @Id
    @GeneratedValue
    private Integer idErreurDosage;

    /** Nom */
    @Length(max = LENGTH_MEDIUM_TEXT)
    @NotEmpty
    private String nom;

    /** Description */
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    // see HHH-6105
    @NotEmpty
    private String description;

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return nom;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(ErreurDosage erreurDosage) {
        return new BeanComparator<ErreurDosage>("nom").compare(this, erreurDosage);
    }

    public Integer getIdErreurDosage() {
        return idErreurDosage;
    }

    public void setIdErreurDosage(Integer idErreurDosage) {
        this.idErreurDosage = idErreurDosage;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
