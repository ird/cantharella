/*
 * #%L
 * Cantharella :: Data
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.config.DataContext;
import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.data.model.utils.DocumentAttachable;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * Modèle : Résultat issue d'un dosage
 * 
 * Custom indexing of polymorphic produit is based on example at:
 * http://blog.pfa-labs.com/2009/03/building-custom-entity-bridge-with.html
 * 
 * @author Jean Couteau Viney
 */
@Entity
@Indexed
public class ResultatDosage extends AbstractModel implements Cloneable, Comparable<ResultatDosage>, DocumentAttachable {

    /** Id du produit */
    @Id
    @GeneratedValue
    private Integer id;

    /** Numéro d'analyse */
    @NotNull
    private Integer analyseNb;

    /** Produit utilisé obtenir le résultat **/
    @ManyToOne(fetch = FetchType.EAGER)
    @IndexedEmbedded
    private Produit produit;

    /** Lot utilisé obtenir le résultat **/
    @ManyToOne(fetch = FetchType.EAGER)
    @IndexedEmbedded
    private Lot lot;

    /** Concentration/masse **/
    @Min(value = 0)
    @Max(value = DataContext.DECIMAL_MAX)
    @Column(precision = DataContext.DECIMAL_PRECISION, scale = DataContext.DECIMAL_SCALE)
    private BigDecimal concMasse;

    /** Unité utilisé pour la concentration/masse */
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private UniteConcMasse uniteConcMasse;

    /** Composé dosé */
    @Length(max = LENGTH_BIG_TEXT)
    @FullTextField(projectable = Projectable.YES)
    private String composeDose;

    /** Molécule dosée */
    @ManyToOne(fetch = FetchType.EAGER)
    @IndexedEmbedded
    private Molecule moleculeDosee;

    /** Valeur du résultat **/
    @Max(value = DataContext.DECIMAL_MAX)
    @Column(precision = DataContext.DECIMAL_PRECISION, scale = DataContext.DECIMAL_SCALE)
    private BigDecimal valeur;

    @Max(value = DataContext.DECIMAL_MAX)
    @Column(precision = DataContext.DECIMAL_PRECISION, scale = DataContext.DECIMAL_SCALE)
    private BigDecimal SD;

    /** True si le dosage est supérieur au seuil **/
    private Boolean supSeuil;

    /** Erreur (pas de valeur si renseigné) */
    @ManyToOne(fetch = FetchType.EAGER)
    private ErreurDosage erreur;

    /** Dosage dont fait partie le resultat */
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @IndexedEmbedded
    private Dosage dosage;

    /**
     * Constructor
     */
    public ResultatDosage() {
        super();
    }

    /** {@inheritDoc} */
    @Override
    public ResultatDosage clone() throws CloneNotSupportedException {
        ResultatDosage clone = (ResultatDosage) super.clone();
        clone.id = id;
        clone.analyseNb = analyseNb;
        clone.lot = lot;
        clone.produit = produit;
        clone.concMasse = concMasse;
        clone.uniteConcMasse = uniteConcMasse;
        clone.composeDose = composeDose;
        clone.moleculeDosee = moleculeDosee;
        clone.valeur = valeur;
        clone.SD = SD;
        clone.supSeuil = supSeuil;
        clone.erreur = erreur;
        clone.dosage = dosage;
        return clone;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "(" + this.getProduit() + this.getLot() + ", " + this.getComposeDose() + ", "
                + this.getMoleculeDosee() + ")";
    }

    /** {@inheritDoc} */
    @Override
    public final int compareTo(ResultatDosage resultat) {
        ComparatorChain comparatorChain = new ComparatorChain();
        comparatorChain.addComparator(new BeanComparator<ResultatDosage>("dosage.ref"));
        comparatorChain.addComparator(new BeanComparator<ResultatDosage>("analyseNb"));
        return comparatorChain.compare(this, resultat);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnalyseNb() {
        return analyseNb;
    }

    public void setAnalyseNb(Integer analyseNb) {
        this.analyseNb = analyseNb;
    }

    public BigDecimal getConcMasse() {
        return concMasse;
    }

    public void setConcMasse(BigDecimal concMasse) {
        this.concMasse = concMasse;
    }

    public UniteConcMasse getUniteConcMasse() {
        return uniteConcMasse;
    }

    public void setUniteConcMasse(UniteConcMasse uniteConcMasse) {
        this.uniteConcMasse = uniteConcMasse;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }

    public BigDecimal getSD() {
        return SD;
    }

    public void setSD(BigDecimal SD) {
        this.SD = SD;
    }

    public Boolean getSupSeuil() {
        return supSeuil;
    }

    public void setSupSeuil(Boolean supSeuil) {
        this.supSeuil = supSeuil;
    }

    public ErreurDosage getErreur() {
        return erreur;
    }

    public void setErreur(ErreurDosage erreur) {
        this.erreur = erreur;
    }

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    public String getComposeDose() {
        return composeDose;
    }

    public void setComposeDose(String composeDose) {
        this.composeDose = composeDose;
    }

    public Molecule getMoleculeDosee() {
        return moleculeDosee;
    }

    public void setMoleculeDosee(Molecule moleculeDosee) {
        this.moleculeDosee = moleculeDosee;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Lot getLot() {
        return lot;
    }

    public void setLot(Lot lot) {
        this.lot = lot;
    }

    public Lot getLotSource() {
        if (lot != null) {
            return lot;
        } else {
            if (produit == null) {
                return null;
            }
            if (produit.isExtrait()) {
                Extrait extrait = (Extrait) produit;
                return extrait.getExtraction().getLot();
            }
            // cas où c'est une fraction
            Fraction fraction = (Fraction) produit;
            return fraction.getPurification().getLotSource();
        }
    }

    public String getEchantillon() {
        if (produit != null) {
            return produit.getRef();
        } else if (lot != null){
            return lot.getRef();
        } else return "";
    }

    /** {@inheritDoc} */
    public List<Document> getDocuments() {
        return dosage.getDocuments();
    }

    /** {@inheritDoc} */
    @Override
    public void addDocument(Document document) {
        dosage.addDocument(document);
    }

    /** {@inheritDoc} */
    @Override
    public void removeDocument(Document document) {
        dosage.removeDocument(document);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResultatDosage)) return false;
        ResultatDosage that = (ResultatDosage) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
