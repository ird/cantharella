package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections4.comparators.ComparatorChain;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Objects;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"referentiel", "code"} ))
public class EntreeReferentiel extends AbstractModel implements Comparable<EntreeReferentiel> {


    public enum Referentiel implements Comparable<Referentiel> {
        ICPC3,
        ICD11,
        EBDCS,
    }

    @Id
    @GeneratedValue
    private Integer idEntree;

    private Referentiel referentiel;

    /** Nom */
    @Length(max = LENGTH_TINY_TEXT)
    @NotEmpty
    @KeywordField
    private String code;

    /** Nom */
    @Length(max = LENGTH_BIG_TEXT)
    @NotEmpty
    @FullTextField(projectable = Projectable.YES)
    private String valeur;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "classification")
    private List<Remede> remedes;

    @Override
    public int compareTo(EntreeReferentiel entreeReferentiel) {
        ComparatorChain<EntreeReferentiel> comparatorChain = new ComparatorChain<>( );
        comparatorChain.addComparator( new BeanComparator<>( "referentiel" ) );
        comparatorChain.addComparator( new BeanComparator<>( "code" ) );
        return comparatorChain.compare(this, entreeReferentiel);
    }

    public Referentiel getReferentiel() {
        return referentiel;
    }

    public void setReferentiel(Referentiel referentiel) {
        this.referentiel = referentiel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Integer getIdEntree() {
        return idEntree;
    }

    public void setIdEntree(Integer idEntree) {
        this.idEntree = idEntree;
    }

    @Override
    public String toString() {
        return valeur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntreeReferentiel that = (EntreeReferentiel) o;
        return Objects.equals(getIdEntree(), that.getIdEntree());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdEntree());
    }
}
