package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.data.model.utils.CompositeId;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table
@Embeddable
public class RemedePersonneDroits  extends AbstractModel implements PersonneDroits<Remede>{


    /** Droits */
    @NotNull
    private Droits droits;

    /** ID */
    @EmbeddedId
    @NotNull
    private CompositeId<Remede, Personne> id;

    /**
     * Constructeur
     */
    public RemedePersonneDroits() {
        droits = new Droits();
        id = new CompositeId<>();
    }

    /** {@inheritDoc} */
    @Override
    public final RemedePersonneDroits clone() throws CloneNotSupportedException {
        RemedePersonneDroits clone = new RemedePersonneDroits();
        clone.id = id.clone();
        clone.droits = droits.clone();
        return clone;
    }

    /**
     * droits getter
     *
     * @return droits
     */
    public Droits getDroits() {
        return droits;
    }

    /**
     * droits setter
     *
     * @param droits droits
     */
    public void setDroits(Droits droits) {
        this.droits = droits;
    }

    /**
     * id getter
     *
     * @return id
     */
    @Override
    public CompositeId<Remede, Personne> getId() {
        return id;
    }

    /**
     * id setter
     *
     * @param id id
     */

    public void setId(CompositeId<Remede, Personne> id) {
        this.id = id;
    }
}
