/*
 * #%L
 * Cantharella :: Data
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.data.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.data.model.utils.DocumentAttachable;
import nc.ird.cantharella.data.validation.CountryCode;
import nc.ird.cantharella.data.validation.Latitude;
import nc.ird.cantharella.data.validation.Longitude;
import nc.ird.cantharella.data.validation.Referentiel;

import org.apache.commons.beanutils.BeanComparator;
import org.hibernate.annotations.AttributeAccessor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotEmpty;

/**
 * Modèle : station
 * 
 * @author Mickael Tricot
 * @author Adrien Cheype
 */
@Entity
@Table
@AttributeAccessor("field")
@Indexed
@Embeddable
public class Station extends AbstractModel implements Cloneable, Comparable<Station>, DocumentAttachable {

    /** ID */
    @Id
    @GeneratedValue
    private Integer idStation;

    /** Nom */
    @Column(unique = true)
    @NotEmpty
    @Length(max = LENGTH_MEDIUM_TEXT)
    @FullTextField(projectable = Projectable.YES)
    private String nom;

    /** Code pays */
    @NotNull
    @Length(min = 2, max = 2)
    @CountryCode
    @KeywordField
    private String codePays;

    /** Complément */
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    // see HHH-6105
    private String complement;

    /** Créateur */
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Personne createur;

    /** Localité */
    @Length(max = LENGTH_MEDIUM_TEXT)
    @FullTextField
    private String localite;

    /** Latitude */
    @Latitude
    private String latitude;

    /** Longitude */
    @Longitude
    private String longitude;

    /** Référentiel */
    @Referentiel
    private Integer referentiel;

    /** Lots */
    @NotNull
    @OneToMany(mappedBy = "station", fetch = FetchType.LAZY)
    private List<Lot> lots;

    /** Needed for HibernateSearch indexation but should not be used otherwise (no getter/setters provided)**/
    @OneToMany(mappedBy = "station", fetch = FetchType.LAZY)
    private List<PersonneInterrogee> personneInterrogees;

    /** Needed for HibernateSearch indexation but should not be used otherwise (no getter/setters provided)**/
    @OneToMany(mappedBy = "provenance", fetch = FetchType.LAZY)
    private List<IngredientRemede> ingredients;

    /** Needed for HibernateSearch indexation but should not be used otherwise (no getter/setters provided)**/
    @OneToMany(mappedBy = "station", fetch = FetchType.LAZY)
    private List<Remede> remedes;

    /** Stations */
    @NotNull
    @ManyToMany(mappedBy = "stations", fetch = FetchType.EAGER)
    // FIXME echatellier 20130524 EAGER for hibernate search (no other simple solution)
    @Fetch(value = FetchMode.SUBSELECT)
    // see HHH-1718
    @IndexedEmbedded
    private List<Campagne> campagnes;

    /** Spécimens de référence qui sont rattachés à cette station */
    @OneToMany(mappedBy = "station", fetch = FetchType.LAZY)
    private List<Specimen> specimensRattaches;

    /** Attached documents. */
    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "station")
    @Cascade({ CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SUBSELECT)
    // see HHH-1718
    private List<Document> documents;

    /**
     * Constructeur
     */
    public Station() {
        lots = new ArrayList<>();
        campagnes = new ArrayList<>();
        documents = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    public Station clone() throws CloneNotSupportedException {
        Station clone = (Station) super.clone();
        clone.idStation = idStation;
        clone.nom = nom;
        clone.codePays = codePays;
        clone.complement = complement;
        clone.createur = createur;
        clone.localite = localite;
        clone.latitude = latitude;
        clone.longitude = longitude;
        clone.referentiel = referentiel;
        return clone;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return nom;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(Station station) {
        return new BeanComparator<Station>("nom").compare(this, station);
    }

    /**
     * idStation getter
     * 
     * @return idStation
     */
    public Integer getIdStation() {
        return idStation;
    }

    /**
     * idStation setter
     * 
     * @param idStation idStation
     */
    public void setIdStation(Integer idStation) {
        this.idStation = idStation;
    }

    /**
     * nom getter
     * 
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * nom setter
     * 
     * @param nom nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * codePays getter
     * 
     * @return codePays
     */
    public String getCodePays() {
        return codePays;
    }

    /**
     * codePays setter
     * 
     * @param codePays codePays
     */
    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    /**
     * complement getter
     * 
     * @return complement
     */
    public String getComplement() {
        return complement;
    }

    /**
     * complement setter
     * 
     * @param complement complement
     */
    public void setComplement(String complement) {
        this.complement = complement;
    }

    /**
     * createur getter
     * 
     * @return createur
     */
    public Personne getCreateur() {
        return createur;
    }

    /**
     * createur setter
     * 
     * @param createur createur
     */
    public void setCreateur(Personne createur) {
        this.createur = createur;
    }

    /**
     * localite getter
     * 
     * @return localite
     */
    public String getLocalite() {
        return localite;
    }

    /**
     * localite setter
     * 
     * @param localite localite
     */
    public void setLocalite(String localite) {
        this.localite = localite;
    }

    /**
     * latitude getter
     * 
     * @return latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * latitude setter
     * 
     * @param latitude latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * longitude getter
     * 
     * @return longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * longitude setter
     * 
     * @param longitude longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * referentiel getter
     * 
     * @return referentiel
     */
    public Integer getReferentiel() {
        return referentiel;
    }

    /**
     * referentiel setter
     * 
     * @param referentiel referentiel
     */
    public void setReferentiel(Integer referentiel) {
        this.referentiel = referentiel;
    }

    /**
     * lots getter
     * 
     * @return lots
     */
    public List<Lot> getLots() {
        return lots;
    }

    /**
     * lots setter
     * 
     * @param lots lots
     */
    public void setLots(List<Lot> lots) {
        this.lots = lots;
    }

    /**
     * campagnes getter
     * 
     * @return campagnes
     */
    public List<Campagne> getCampagnes() {
        return campagnes;
    }

    /**
     * campagnes setter
     * 
     * @param campagnes campagnes
     */
    public void setCampagnes(List<Campagne> campagnes) {
        this.campagnes = campagnes;
    }

    /**
     * specimensRattaches getter
     * 
     * @return specimensRattaches
     */
    public List<Specimen> getSpecimensRattaches() {
        return specimensRattaches;
    }

    /**
     * specimensRattaches setter
     * 
     * @param specimensRattaches specimensRattaches
     */
    public void setSpecimensRattaches(List<Specimen> specimensRattaches) {
        this.specimensRattaches = specimensRattaches;
    }

    /** {@inheritDoc} */
    public List<Document> getDocuments() {
        return documents;
    }

    /**
     * Documents setter.
     * 
     * @param documents the documents to set
     */
    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    /** {@inheritDoc} */
    @Override
    public void addDocument(Document document) {
        documents.add(document);
    }

    /** {@inheritDoc} */
    @Override
    public void removeDocument(Document document) {
        documents.remove(document);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Station)) {
            return false;
        }
        Station station = (Station) o;
        return Objects.equals(getIdStation(), station.getIdStation()) && Objects.equals(getNom(), station.getNom());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdStation(), getNom());
    }
}