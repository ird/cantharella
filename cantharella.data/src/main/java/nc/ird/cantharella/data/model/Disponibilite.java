package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import org.apache.commons.beanutils.BeanComparator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "nom"))
public class Disponibilite extends AbstractModel implements Comparable<Disponibilite> {

    /** Id de l'erreur */
    @Id
    @GeneratedValue
    private Integer idDisponibilite;

    /** Nom */
    @Length(max = LENGTH_MEDIUM_TEXT)
    @NotEmpty
    private String nom;

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return nom;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(Disponibilite disponibilite) {
        return new BeanComparator<Disponibilite>("nom").compare(this, disponibilite);
    }

    public Integer getIdDisponibilite() {
        return idDisponibilite;
    }

    public void setIdDisponibilite(Integer idDisponibilite) {
        this.idDisponibilite = idDisponibilite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disponibilite that = (Disponibilite) o;
        return Objects.equals(getNom(), that.getNom());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getNom());
    }
}
