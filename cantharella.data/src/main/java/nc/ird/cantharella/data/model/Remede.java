package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.comparators.IngredientRemedeComp;
import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.data.model.utils.DocumentAttachable;
import nc.ird.cantharella.data.validation.LanguageCode;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections4.comparators.ComparatorChain;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Indexed
public class Remede  extends AbstractModel implements Comparable<Remede>, DocumentAttachable {

    @Id
    @GeneratedValue
    private Integer idRemede;

    /** Créateur */
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Personne createur;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @IndexedEmbedded
    Station station;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @IndexedEmbedded
    Campagne campagne;

    @NotNull
    @Temporal(TemporalType.DATE)
    Date date;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @IndexedEmbedded
    Personne enqueteur;

    @NotEmpty
    @NotNull
    @KeywordField(projectable = Projectable.YES)
    @Column(unique = true, name="reference")
    String ref;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @IndexedEmbedded
    PersonneInterrogee prescripteur;

    //doc;
    @OneToOne
    Document consentement;

    @Length(max = LENGTH_MEDIUM_TEXT)
    @FullTextField(projectable = Projectable.YES)
    String nom;

    @Length(min = 2, max = 2)
    @LanguageCode
    String codeLangue;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "Remede_EntreeReferentiel",
        joinColumns = { @JoinColumn(name = "idRemede") },
        inverseJoinColumns = { @JoinColumn(name = "idEntree") }
    )

    @IndexedEmbedded
    List<EntreeReferentiel> classification;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String indicationBiomedicale;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String indicationVernaculaire;

    Integer nbIngredients;

    @NotNull
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "remede", orphanRemoval = true)
    @Cascade({ CascadeType.SAVE_UPDATE })
    List<IngredientRemede> ingredients;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String preparation;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String conservation;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String administration;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String posologie;

    boolean recommandations;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String recommandationsText;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String effetsIndesirables;

    Integer patientsParAn;

    boolean dispoAnalyse;

    @Length(max = LENGTH_MEDIUM_TEXT)
    String dispoAnalyseLabo;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String complement;

    /** Field to know we need to delete the consentement document on saving the entity **/
    @Transient
    Document consentementToDelete;

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "remede")
    @Cascade({ CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Document> documents;

    /** Droits attribués aux personnes */
    @OneToMany(mappedBy = "id.pk1", fetch = FetchType.LAZY, orphanRemoval = true)
    @Cascade({ CascadeType.SAVE_UPDATE })
    @NotNull
    private List<RemedePersonneDroits> personnesDroits;

    /**
     * Constructor
     */
    public Remede() {
        ingredients = new ArrayList<>();
        documents = new ArrayList<>();
        classification = new ArrayList<>();
        personnesDroits = new ArrayList<>();
    }

    public Integer getIdRemede() {
        return idRemede;
    }

    public void setIdRemede(Integer idRemede) {
        this.idRemede = idRemede;
    }

    public Personne getCreateur() {
        return createur;
    }

    public void setCreateur(Personne createur) {
        this.createur = createur;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Campagne getCampagne() {
        return campagne;
    }

    public void setCampagne(Campagne campagne) {
        this.campagne = campagne;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Personne getEnqueteur() {
        return enqueteur;
    }

    public void setEnqueteur(Personne enqueteur) {
        this.enqueteur = enqueteur;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public PersonneInterrogee getPrescripteur() {
        return prescripteur;
    }

    public void setPrescripteur(PersonneInterrogee prescripteur) {
        this.prescripteur = prescripteur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodeLangue() {
        return codeLangue;
    }

    public void setCodeLangue(String codeLangue) {
        this.codeLangue = codeLangue;
    }

    public String getIndicationBiomedicale() {
        return indicationBiomedicale;
    }

    public void setIndicationBiomedicale(String indicationBiomedicale) {
        this.indicationBiomedicale = indicationBiomedicale;
    }

    public String getIndicationVernaculaire() {
        return indicationVernaculaire;
    }

    public void setIndicationVernaculaire(String indicationVernaculaire) {
        this.indicationVernaculaire = indicationVernaculaire;
    }

    public Integer getNbIngredients() {
        return nbIngredients;
    }

    public void setNbIngredients(Integer nbIngredients) {
        this.nbIngredients = nbIngredients;
    }

    public List<IngredientRemede> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientRemede> ingredients) {
        this.ingredients = ingredients;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public String getConservation() {
        return conservation;
    }

    public void setConservation(String conservation) {
        this.conservation = conservation;
    }

    public String getAdministration() {
        return administration;
    }

    public void setAdministration(String administration) {
        this.administration = administration;
    }

    public String getPosologie() {
        return posologie;
    }

    public void setPosologie(String posologie) {
        this.posologie = posologie;
    }

    public boolean getRecommandations() {
        return recommandations;
    }

    public void setRecommandations(boolean recommandations) {
        this.recommandations = recommandations;
    }

    public String getRecommandationsText() {
        return recommandationsText;
    }

    public void setRecommandationsText(String recommandationsText) {
        this.recommandationsText = recommandationsText;
    }

    public String getEffetsIndesirables() {
        return effetsIndesirables;
    }

    public void setEffetsIndesirables(String effetsIndesirables) {
        this.effetsIndesirables = effetsIndesirables;
    }

    public Integer getPatientsParAn() {
        return patientsParAn;
    }

    public void setPatientsParAn(Integer patientsParAn) {
        this.patientsParAn = patientsParAn;
    }

    public boolean getDispoAnalyse() {
        return dispoAnalyse;
    }

    public void setDispoAnalyse(boolean dispoAnalyse) {
        this.dispoAnalyse = dispoAnalyse;
    }

    public String getDispoAnalyseLabo() {
        return dispoAnalyseLabo;
    }

    public void setDispoAnalyseLabo(String dispoAnalyseLabo) {
        this.dispoAnalyseLabo = dispoAnalyseLabo;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public List<EntreeReferentiel> getClassification() {
        classification.sort(Comparator.naturalOrder());
        return classification;
    }

    public void setClassification(List<EntreeReferentiel> classification) {
        this.classification = classification;
    }

    public Document getConsentement() {
        return consentement;
    }

    public void setConsentement(Document consentement) {
        this.consentement = consentement;
    }

    public Document getConsentementToDelete() {
        return consentementToDelete;
    }

    public void setConsentementToDelete(Document consentementToDelete) {
        this.consentementToDelete = consentementToDelete;
    }

    public @NotNull List<RemedePersonneDroits> getPersonnesDroits() {
        return personnesDroits;
    }

    public void setPersonnesDroits(@NotNull List<RemedePersonneDroits> personnesDroits) {
        this.personnesDroits = personnesDroits;
    }

    /** {@inheritDoc} */
    public List<Document> getDocuments() {

        List<Document> docs = new ArrayList<>(documents);
        if (consentement != null && !documents.contains(consentement)) {
            docs.add(consentement);
        }
        return docs;
    }

    /**
     * Documents setter.
     *
     * @param documents the documents to set
     */
    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    /** {@inheritDoc} */
    @Override
    public void addDocument(Document document) {
        documents.add(document);
    }

    /** {@inheritDoc} */
    @Override
    public void removeDocument(Document document) {
        documents.remove(document);

        if (document.equals(consentement)) {
            setConsentementToDelete(consentement);
            setConsentement(null);
        }
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(Remede remede) {
        ComparatorChain<Remede> comparatorChain = new ComparatorChain<>();
        comparatorChain.addComparator(new BeanComparator<>("ref"));
        comparatorChain.addComparator(new BeanComparator<>("campagne"));
        return comparatorChain.compare(this, remede);
    }

    /**
     * Rend les résultats triés en utilisant le comparateur {@link IngredientRemedeComp}
     *
     * @return resultats
     */
    public List<IngredientRemede> getSortedIngredients() {
        // comme "@Sort(type = SortType.COMPARATOR, comparator = ResultatsOfTestBioComp.class)" ne rend pas une
        // liste triée avec List, tri dans le getter
        Collections.sort(ingredients, new IngredientRemedeComp());
        return ingredients;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return ref;
    }
}
