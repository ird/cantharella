package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import org.apache.commons.beanutils.BeanComparator;
import org.hibernate.validator.constraints.Length;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
@Embeddable
public class UniteConcMasse extends AbstractModel implements Comparable<UniteConcMasse> {

    /** Id  */
    @Id
    @GeneratedValue
    private Integer idUnite;

    /** Référence de la manip */
    @Length(max = LENGTH_MEDIUM_TEXT)
    @Column(unique = true)
    @NotEmpty
    private String code;

    /** Manipulateur */
    @Length(max = LENGTH_MEDIUM_TEXT)
    @Column(unique = true)
    @NotEmpty
    private String valeur;

    @Override
    public int compareTo(UniteConcMasse uniteConcMasse) {
        return new BeanComparator<UniteConcMasse>("code").compare(this, uniteConcMasse);
    }

    public Integer getIdUnite() {
        return idUnite;
    }

    public void setIdUnite(Integer idUnite) {
        this.idUnite = idUnite;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return this.valeur;
    }
}