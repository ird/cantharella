/*
 * #%L
 * Cantharella :: Data
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.data.config;


import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.search.CantharellaAnalysisConfigurer;
import nc.ird.cantharella.utils.CantharellaConfig;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.search.mapper.orm.cfg.HibernateOrmMapperSettings;
import org.nuiton.config.ArgumentsParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;


/**
 * Spring context for the data layer tests
 *
 * Class should be in src/tests but is here to be reused by other modules
 */
// Import the spring context file
@ImportResource(value = "classpath:/dataContext.xml")
// Scans for @Repository, @Service and @Component
@ComponentScan(basePackages = { "nc.ird.cantharella.data.dao", "nc.ird.cantharella.data.validation.utils" })
// Enable @Transactional support -> not work with <aop:aspectj-autoproxy /> in xml context file
@EnableTransactionManagement
// This is a configuration class
@Configuration
public abstract class TestDataContext extends DataContext{

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(TestDataContext.class);

    /**
     * Set the data layer properties for the cantharella configuration
     * 
     * @return The placeholder configurer which get the data layer properties
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer properties() {
        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        try {
            pspc.setProperties(CantharellaConfig.getProperties("data.properties"));
        } catch (ArgumentsParserException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
        pspc.setIgnoreUnresolvablePlaceholders(true);
        return pspc;
    }



    /**
     * @return Session factory (for DB connections)
     */
    @Bean
    public SessionFactory sessionFactory() {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setPackagesToScan("nc.ird.cantharella.data.model");
        Properties hibernateProperties = new Properties();
        // Hibernate: basic
        hibernateProperties.setProperty(Environment.DIALECT, H2Dialect.class.getName());
        //System.setProperty(Environment.BYTECODE_PROVIDER, "cglib");
        hibernateProperties.setProperty(Environment.HBM2DDL_AUTO, hbm2ddl);
        // hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        hibernateProperties.setProperty("hibernate.generate_statistics", String.valueOf(dbDebugProperty));
        hibernateProperties.setProperty("hibernate.cache.use_structured_entries", String.valueOf(dbDebugProperty));
        hibernateProperties.setProperty("hibernate.show_sql", String.valueOf(false));
        hibernateProperties.setProperty("hibernate.format_sql", String.valueOf(false));
        hibernateProperties.setProperty("hibernate.use_sql_comments", String.valueOf(dbDebugProperty));
        // Hibernate: cache
        hibernateProperties.setProperty(Environment.CACHE_REGION_FACTORY, "org.hibernate.cache.jcache.JCacheRegionFactory");
        hibernateProperties.setProperty("hibernate.javax.cache.provider", "org.ehcache.jsr107.EhcacheCachingProvider");
        hibernateProperties.setProperty("hibernate.cache.use_query_cache", String.valueOf(true));
        hibernateProperties.setProperty("hibernate.cache.use_second_level_cache", String.valueOf(true));
        hibernateProperties.setProperty("hibernate.cache.use_structured_entries", String.valueOf(dbDebugProperty));
        // Hibernate: connection
        hibernateProperties.setProperty("hibernate.connection.autocommit", String.valueOf(false));
        hibernateProperties.setProperty("hibernate.connection.release_mode", "on_close");
        // Hibernate search
        //hibernateProperties.setProperty("hibernate.search.backend.directory.provider", "filesystem");
        hibernateProperties.setProperty("hibernate.search.backend.directory.root", hibernateSearchIndexBase);
        hibernateProperties.setProperty("hibernate.search.backend.analysis.configurer", CantharellaAnalysisConfigurer.class.getName());
        hibernateProperties.setProperty(HibernateOrmMapperSettings.AUTOMATIC_INDEXING_ENABLE_DIRTY_CHECK, "false");
        hibernateProperties.setProperty("hibernate.search.backend.lucene_version", "8.8.2");
        // TODO batch_size à ajuster
        hibernateProperties.setProperty("hibernate.default_batch_fetch_size", String.valueOf(20));

        sessionFactoryBean.setHibernateProperties(hibernateProperties);

        try {
            sessionFactoryBean.afterPropertiesSet();
        } catch (Exception e) {
            throw new RuntimeException("SessionFactory misconfiguration", e);
        }
        return sessionFactoryBean.getObject();
    }


}
