package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.data.model.utils.CompositeId;

import java.io.Serializable;

public interface PersonneDroits<A extends AbstractModel> extends Serializable, Cloneable {

    /**
     * droits getter
     *
     * @return droits
     */
    Droits getDroits();

    /**
     * droits setter
     *
     * @param droits droits
     */
    void setDroits(Droits droits);

    /**
     * id getter
     *
     * @return id
     */
    CompositeId<A, Personne> getId();

    /**
     * id setter
     *
     * @param id id
     */
    void setId(CompositeId<A, Personne> id);

    PersonneDroits<A> clone() throws CloneNotSupportedException;
}
