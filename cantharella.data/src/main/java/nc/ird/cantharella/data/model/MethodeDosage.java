package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import org.apache.commons.beanutils.BeanComparator;
import org.hibernate.annotations.Type;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.hibernate.validator.constraints.Length;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.util.List;

@Entity
@Embeddable
public class MethodeDosage  extends AbstractModel implements Comparable<MethodeDosage> {

    /** Id de la méthode */
    @Id
    @GeneratedValue
    private Integer idMethodeDosage;

    /** Nom de la méthode */
    @Length(max = LENGTH_MEDIUM_TEXT)
    @Column(unique = true)
    @NotEmpty
    @FullTextField(projectable = Projectable.YES)
    private String nom;

    /** Acronyme de la méthode, chaîne de caractère. Tout comme nom, chaque cible doit être unique dans la base.*/
    @Length(max = LENGTH_MEDIUM_TEXT)
    @NotEmpty
    @Column(unique = true)
    private String acronyme;

    /** Précise dans quel domaine de recherche se situe ce dosage (biochimie, polluant, toxine, …)*/
    @NotEmpty
    private String domaine;

    /** Défini comment est réalisé le dosage, le mode opératoire.*/
    @NotEmpty
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    // see HHH-6105
    private String description;

    @NotEmpty
    private String valeurMesuree;

    @NotEmpty
    private String unite;

    private String seuil;

    /** Needed for HibernateSearch indexation but should not be used otherwise (no getter/setters provided)**/
    @OneToMany(mappedBy = "methode", fetch = FetchType.LAZY)
    private List<Dosage> dosages;

    /**
     * Constructor
     */
    public MethodeDosage() {
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return nom;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(MethodeDosage methodeDosage) {
        return new BeanComparator<MethodeDosage>("nom").compare(this, methodeDosage);
    }

    public Integer getIdMethodeDosage() {
        return idMethodeDosage;
    }

    public void setIdMethodeDosage(Integer idMethodeDosage) {
        this.idMethodeDosage = idMethodeDosage;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAcronyme() {
        return acronyme;
    }

    public void setAcronyme(String acronyme) {
        this.acronyme = acronyme;
    }

    public String getDomaine() {
        return domaine;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValeurMesuree() {
        return valeurMesuree;
    }

    public void setValeurMesuree(String valeurMesuree) {
        this.valeurMesuree = valeurMesuree;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public String getSeuil() {
        return seuil;
    }

    public void setSeuil(String seuil) {
        this.seuil = seuil;
    }
}