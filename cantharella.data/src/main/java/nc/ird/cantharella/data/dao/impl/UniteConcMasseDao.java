package nc.ird.cantharella.data.dao.impl;

import nc.ird.cantharella.data.dao.AbstractModelDao;
import nc.ird.cantharella.data.model.UniteConcMasse;

public class UniteConcMasseDao extends AbstractModelDao {

    /** Criteria : utilisateur valide par nom et prénom (mis en lower) */
    public static final String HQL_FIND_UNITE_PAR_VALEUR = String.format(
            "FROM %s WHERE LOWER(valeur) LIKE LOWER(:valeur)", UniteConcMasse.class.getSimpleName());

    /**
     * Constructor (empêche l'instantiation)
     */
    private UniteConcMasseDao() {
        //
    }
}
