package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.config.DataContext;
import nc.ird.cantharella.data.model.comparators.ResultatDosageComp;
import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.data.model.utils.DocumentAttachable;
import org.apache.commons.beanutils.BeanComparator;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Modèle : Manipulation de dosage
 *
 * @author Jean Couteau Viney
 */
@Entity
@Embeddable
public class Dosage extends AbstractModel implements Comparable<Dosage>, DocumentAttachable {

    /** Id du test bio */
    @Id
    @GeneratedValue
    private Integer idDosage;

    /** Référence de la manip */
    @Length(max = LENGTH_MEDIUM_TEXT)
    @Column(unique = true)
    @NotEmpty
    @KeywordField(projectable = Projectable.YES)
    private String ref;

    /** Manipulateur */
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Personne manipulateur;

    /** Organisme qui a effectué le dosage **/
    @NotNull
    @Length(max = LENGTH_MEDIUM_TEXT)
    private String organisme;

    /** Date de la manip */
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date date;

    /** Méthode de dosage **/
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @IndexedEmbedded
    private MethodeDosage methode;

    /** Concentration/masse utilisé par défaut pour les résultats **/
    @Min(value = 0)
    @Max(value = DataContext.DECIMAL_MAX)
    @Column(precision = DataContext.DECIMAL_PRECISION, scale = DataContext.DECIMAL_SCALE)
    private BigDecimal concMasseDefaut;

    /** Unité utilisé pour la concentration/masse par défaut */
    @ManyToOne(fetch = FetchType.EAGER)
    private UniteConcMasse uniteConcMasseDefaut;

    /** Commentaire pour la manip */
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    // see HHH-6105
    private String complement;

    /** Créateur */
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Personne createur;

    /** Résultats du dosage */
    @NotNull
    @OneToMany(mappedBy = "dosage", fetch = FetchType.LAZY, orphanRemoval = true)
    @Cascade({ CascadeType.SAVE_UPDATE })
    private List<ResultatDosage> resultats;

    /** Attached documents. */
    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "dosage")
    @Cascade({ CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SUBSELECT)
    // see HHH-1718
    private List<Document> documents;

    /**
     * Constructeur
     */
    public Dosage() {
        documents = new ArrayList<>();
        resultats = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return ref;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(Dosage dosage) {
        return new BeanComparator<Dosage>("ref").compare(this, dosage);
    }

    public Integer getIdDosage() {
        return idDosage;
    }

    public void setIdDosage(Integer idDosage) {
        this.idDosage = idDosage;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Personne getManipulateur() {
        return manipulateur;
    }

    public void setManipulateur(Personne manipulateur) {
        this.manipulateur = manipulateur;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public MethodeDosage getMethode() {
        return methode;
    }

    public void setMethode(MethodeDosage methode) {
        this.methode = methode;
    }

    public BigDecimal getConcMasseDefaut() {
        return concMasseDefaut;
    }

    public void setConcMasseDefaut(BigDecimal concMasseDefaut) {
        this.concMasseDefaut = concMasseDefaut;
    }

    public UniteConcMasse getUniteConcMasseDefaut() {
        return uniteConcMasseDefaut;
    }

    public void setUniteConcMasseDefaut(UniteConcMasse uniteConcMasseDefaut) {
        this.uniteConcMasseDefaut = uniteConcMasseDefaut;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public Personne getCreateur() {
        return createur;
    }

    public void setCreateur(Personne createur) {
        this.createur = createur;
    }

    public List<ResultatDosage> getResultats() {
        return resultats;
    }

    public void setResultats(List<ResultatDosage> resultats) {
        this.resultats = resultats;
    }

    @Override
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    /** {@inheritDoc} */
    @Override
    public void addDocument(Document document) {
        documents.add(document);
    }

    /** {@inheritDoc} */
    @Override
    public void removeDocument(Document document) {
        documents.remove(document);
    }

    /**
     * Rend les résultats triés en utilisant le comparateur {@link ResultatDosageComp}
     *
     * @return resultats
     */
    public List<ResultatDosage> getSortedResultats() {
        // comme "@Sort(type = SortType.COMPARATOR, comparator = ResultatsOfTestBioComp.class)" ne rend pas une
        // liste triée avec List, tri dans le getter
        resultats.sort(new ResultatDosageComp());
        return resultats;
    }
}

//analyses