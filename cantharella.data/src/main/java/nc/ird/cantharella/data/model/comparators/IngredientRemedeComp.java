package nc.ird.cantharella.data.model.comparators;

import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.utils.IntuitiveStringComparator;
import org.apache.commons.beanutils.BeanComparator;

import java.util.Comparator;

public class IngredientRemedeComp  implements Comparator<IngredientRemede> {

    /** {@inheritDoc} */
    @Override
    public int compare(IngredientRemede obj1, IngredientRemede obj2) {
        return new BeanComparator<IngredientRemede>("specimen.ref", new IntuitiveStringComparator<String>()).compare(obj1, obj2);
    }

}
