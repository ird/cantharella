package nc.ird.cantharella.data.model;

public interface ProduitOrLot {

    String getRef();

    Boolean isLot();

    Boolean isProduit();

}
