package nc.ird.cantharella.data.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

public class PastValidator implements ConstraintValidator<Past, Date> {

    /** {@inheritDoc} */
    @Override
    public void initialize(Past annotation) {
        //
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid(Date pasteDate, ConstraintValidatorContext constraintContext) {
        return pasteDate.before(new Date());
    }

}
