package nc.ird.cantharella.data.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Past constraint valid for java.sql.Date coming from base in java.util.Date field
 *
 * @author Jean Couteau
 */
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PastValidator.class)
@Documented
public @interface Past {
    /**
     * @return Groups
     */
    Class<?>[] groups() default {};

    /**
     * @return Message key
     */
    String message() default "{nc.ird.cantharella.data.validation.Past.message}";

    /**
     * @return Payload
     */
    Class<? extends Payload>[] payload() default {};
}