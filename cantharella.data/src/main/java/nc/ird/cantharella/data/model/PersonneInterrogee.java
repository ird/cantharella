package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.data.model.utils.DocumentAttachable;
import nc.ird.cantharella.data.validation.CountryCode;
import nc.ird.cantharella.data.validation.Telephone;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Indexed
public class PersonneInterrogee extends AbstractModel implements Comparable<PersonneInterrogee>, DocumentAttachable {

    public enum Genre implements Comparable<Genre> {
        M,
        F,
        NSP,
    }

    @Id
    @GeneratedValue
    private Integer idPersonne;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @IndexedEmbedded
    Station station;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @IndexedEmbedded
    Campagne campagne;

    @Length(max = LENGTH_MEDIUM_TEXT)
    @NotEmpty
    @FullTextField(projectable = Projectable.YES)
    private String nom;

    @Length(max = LENGTH_MEDIUM_TEXT)
    @NotEmpty
    @FullTextField(projectable = Projectable.YES)
    private String prenom;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private Genre genre;

    Integer anneeNaissance;

    Integer experience;

    @Length(max = LENGTH_MEDIUM_TEXT)
    String profession;

    @Length(max = LENGTH_MEDIUM_TEXT)
    String labellisation;

    @Length(max = LENGTH_MEDIUM_TEXT)
    @Telephone
    private String telephone;

    @Column(unique = true)
    @Length(max = LENGTH_MEDIUM_TEXT)
    @Email
    private String courriel;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String adressePostale;

    @Length(max = LENGTH_TINY_TEXT)
    private String codePostal;

    @Length(max = LENGTH_MEDIUM_TEXT)
    @NotEmpty
    @FullTextField(projectable = Projectable.YES)
    private String ville;

    @NotEmpty
    @Length(min = 2, max = 2)
    @CountryCode
    @KeywordField
    private String codePays;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String complement;

    /** Créateur */
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Personne createur;

    /** Needed for HibernateSearch indexation but should not be used otherwise (no getter/setters provided)**/
    @OneToMany(mappedBy = "prescripteur", fetch = FetchType.LAZY)
    private List<Remede> remedes;

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "personneInterrogee")
    @Cascade({ CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Document> documents;

    public Integer getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(Integer idPersonne) {
        this.idPersonne = idPersonne;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Campagne getCampagne() {
        return campagne;
    }

    public void setCampagne(Campagne campagne) {
        this.campagne = campagne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Integer getAnneeNaissance() {
        return anneeNaissance;
    }

    public void setAnneeNaissance(Integer anneeNaissance) {
        this.anneeNaissance = anneeNaissance;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getLabellisation() {
        return labellisation;
    }

    public void setLabellisation(String labellisation) {
        this.labellisation = labellisation;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCourriel() {
        return courriel;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public String getAdressePostale() {
        return adressePostale;
    }

    public void setAdressePostale(String adressePostale) {
        this.adressePostale = adressePostale;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePays() {
        return codePays;
    }

    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public Personne getCreateur() {
        return createur;
    }

    public void setCreateur(Personne createur) {
        this.createur = createur;
    }

    @Override
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    /** {@inheritDoc} */
    @Override
    public void addDocument(Document document) {
        documents.add(document);
    }

    /** {@inheritDoc} */
    @Override
    public void removeDocument(Document document) {
        documents.remove(document);
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return getNom() + " " + getPrenom();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonneInterrogee)) return false;

        PersonneInterrogee that = (PersonneInterrogee) o;

        // remember to use *getters*
        return getIdPersonne().equals(that.getIdPersonne());
    }

    /**
     * Constructor
     */
    public PersonneInterrogee() {
        documents = new ArrayList<>();
    }

    @Override
    public int hashCode() {
        if (getIdPersonne() != null ) {
            return getIdPersonne().hashCode();
        } else {
            return 1;
        }
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(PersonneInterrogee personne) {
        ComparatorChain comparatorChain = new ComparatorChain();
        comparatorChain.addComparator(new BeanComparator<PersonneInterrogee>("station"));
        comparatorChain.addComparator(new BeanComparator<PersonneInterrogee>("nom"));
        comparatorChain.addComparator(new BeanComparator<PersonneInterrogee>("prenom"));
        return comparatorChain.compare(this, personne);
    }
}
