package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Configuration extends AbstractModel {

    /** Id  */
    @Id
    private Integer idConfiguration;

    @NotNull
    protected Integer maxFileSize;

    protected String messageAccueil;

    protected String droitAcces;

    protected String editeur;

    protected String hebergement;

    protected String droitsReproduction;

    @Basic(fetch = FetchType.LAZY)
    protected byte[] logo;

    public Integer getMaxFileSize() {
        return maxFileSize;
    }

    public void setMaxFileSize(Integer maxFileSize) {
        this.maxFileSize = maxFileSize;
    }

    public String getMessageAccueil() {
        return messageAccueil;
    }

    public void setMessageAccueil(String messageAccueil) {
        this.messageAccueil = messageAccueil;
    }

    public String getDroitAcces() {
        return droitAcces;
    }

    public void setDroitAcces(String droitAcces) {
        this.droitAcces = droitAcces;
    }

    public String getEditeur() {
        return editeur;
    }

    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    public String getHebergement() {
        return hebergement;
    }

    public void setHebergement(String hebergement) {
        this.hebergement = hebergement;
    }

    public String getDroitsReproduction() {
        return droitsReproduction;
    }

    public void setDroitsReproduction(String droitsReproduction) {
        this.droitsReproduction = droitsReproduction;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }
}
