package nc.ird.cantharella.data.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class TelephoneValidator  implements ConstraintValidator<Telephone, String> {

    Pattern allCountryTelephonePattern = Pattern.compile("^(\\+\\d{1,3}( )?)?((\\(\\d{1,3}\\))|\\d{1,3})[- .]?\\d{3,4}[- .]?\\d{4}$");

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(Telephone annotation) {
        //
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(String telephone, ConstraintValidatorContext constraintContext) {
        return telephone == null || telephone.isBlank() || allCountryTelephonePattern.matcher(telephone).matches();
    }
}