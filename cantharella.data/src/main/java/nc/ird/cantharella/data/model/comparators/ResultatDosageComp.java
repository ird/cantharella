/*
 * #%L
 * Cantharella :: Data
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.data.model.comparators;

import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.utils.IntuitiveStringComparator;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.lucene.search.comparators.IntComparator;
import org.apache.lucene.search.comparators.NumericComparator;

import java.util.Comparator;

/**
 * ResultatDosage comparator. Compare on analyseNb with intuitive sort.
 * 
 * @author Adrien Cheype
 */
public class ResultatDosageComp implements Comparator<ResultatDosage> {

    /** {@inheritDoc} */
    @Override
    public int compare(ResultatDosage obj1, ResultatDosage obj2) {
        return Integer.compare(obj1.getAnalyseNb(), obj2.getAnalyseNb());
    }
}
