package nc.ird.cantharella.data.dao.impl;

import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class PersonneInterrogeeDao {

    public static final DetachedCriteria CRITERIA_LIST_RESULTATS = DetachedCriteria.forClass(PersonneInterrogee.class,
            "r").addOrder(Order.asc("r.nom"));

    /** Criteria : utilisateur valide par nom et prénom (mis en lower) */
    public static final String HQL_FIND_PERSONNE_INTERROGEE_PAR_NOM_PRENOM = String.format(
            "FROM %s WHERE LOWER(nom) LIKE LOWER(:nom) and LOWER(prenom) LIKE LOWER(:prenom)",
            PersonneInterrogee.class.getSimpleName());

    /**
     * Criteria : liste les résultats de tests bio qui sont de type "produit". L'ensemble est trié par (cible, produit,
     * id)
     */
    public static DetachedCriteria getCriteriaListPersonneInterrogeeForCampagne(Campagne campagne) {
        return DetachedCriteria.forClass(PersonneInterrogee.class, "r").add(Restrictions.eq("r.campagne", campagne))
                .addOrder(Order.asc("r.nom"));
    }
}
