package nc.ird.cantharella.data.dao.impl;

import nc.ird.cantharella.data.model.MethodeDosage;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

public class DosageDao {

    /** Criteria : valeurs existantes du champ 'domaine' existants pour les méthodes de dosage */
    public static final DetachedCriteria CRITERIA_DISTINCT_DOMAINES_METHODES = DetachedCriteria
            .forClass(MethodeDosage.class).setProjection(Projections.distinct(Projections.property("domaine")))
            .addOrder(Order.asc("domaine"));

    /** Criteria : valeurs existantes du champ 'unite' existants pour les méthodes de dosage */
    public static final DetachedCriteria CRITERIA_DISTINCT_UNITES_METHODES = DetachedCriteria
            .forClass(MethodeDosage.class).setProjection(Projections.distinct(Projections.property("unite")))
            .addOrder(Order.asc("unite"));
}
