package nc.ird.cantharella.data.model.search;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.analysis.util.ElisionFilterFactory;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurationContext;
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurer;

public class CantharellaAnalysisConfigurer implements LuceneAnalysisConfigurer {
    @Override
    public void configure(LuceneAnalysisConfigurationContext context) {
        context.similarity( new ClassicSimilarity() );

        context.analyzer( "french" ).custom()
                .tokenizer( StandardTokenizerFactory.class )
                .tokenFilter(ElisionFilterFactory.class)
                .tokenFilter(LowerCaseFilterFactory.class );
    }
}