package nc.ird.cantharella.data.model;

import java.io.Serializable;

public interface Identifiable<T extends Serializable> {
    T getId();
}