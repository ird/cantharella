package nc.ird.cantharella.data.model.search;

/*
 * #%L
 * Cantharella :: Data
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import nc.ird.cantharella.data.model.Extrait;
import nc.ird.cantharella.data.model.Fraction;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.ResultatTestBio;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.search.engine.backend.document.DocumentElement;
import org.hibernate.search.engine.backend.document.IndexFieldReference;
import org.hibernate.search.mapper.pojo.bridge.TypeBridge;
import org.hibernate.search.mapper.pojo.bridge.binding.TypeBindingContext;
import org.hibernate.search.mapper.pojo.bridge.mapping.programmatic.TypeBinder;
import org.hibernate.search.mapper.pojo.bridge.runtime.TypeBridgeWriteContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ResultatTestBio} class contains a polymorphic entity that can't be properly indexed by hibernate search.
 * 
 * @author Eric Chatellier
 */

public class ProduitBinder implements TypeBinder {

    @Override
    public void bind(TypeBindingContext context) {

        context.dependencies().useRootOnly();
        
        IndexFieldReference<String> refField = context.indexSchemaElement()
                .field("produitLotRef", f -> f.asString().analyzer("keyword"))
                .toReference();

        IndexFieldReference<String> embranchementField = context.indexSchemaElement()
                .field("produitLotSpecimenEmbranchement", f -> f.asString().analyzer("standard"))
                .toReference();

        IndexFieldReference<String> familleField = context.indexSchemaElement()
                .field("produitLotSpecimenFamille", f -> f.asString().analyzer("standard"))
                .toReference();

        IndexFieldReference<String> genreField = context.indexSchemaElement()
                .field("produitLotSpecimenGenre", f -> f.asString().analyzer("standard"))
                .toReference();

        IndexFieldReference<String> especeField = context.indexSchemaElement()
                .field("produitLotSpecimenEspece", f -> f.asString().analyzer("standard"))
                .toReference();

        IndexFieldReference<String> campagneField = context.indexSchemaElement()
                .field("produitLotCampagneNom", f -> f.asString().analyzer("keyword"))
                .toReference();

        IndexFieldReference<String> paysField = context.indexSchemaElement()
                .field("produitLotCampagneCodePays", f -> f.asString().analyzer("keyword"))
                .toReference();

        context.bridge(
                Produit.class,
                new ProduitBridge(
                        refField,
                        embranchementField,
                        familleField,
                        genreField,
                        especeField,
                        campagneField,
                        paysField
                )
        );
    }
}

class ProduitBridge implements TypeBridge<Produit> {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ProduitBridge.class);


    private final IndexFieldReference<String> refField;
    private final IndexFieldReference<String> embranchementField;
    private final IndexFieldReference<String> familleField;
    private final IndexFieldReference<String> genreField;
    private final IndexFieldReference<String> especeField;
    private final IndexFieldReference<String> campagneField;
    private final IndexFieldReference<String> paysField;

    ProduitBridge(IndexFieldReference<String> refField,
                  IndexFieldReference<String> embranchementField,
                  IndexFieldReference<String> familleField,
                  IndexFieldReference<String> genreField,
                  IndexFieldReference<String> especeField,
                  IndexFieldReference<String> campagneField,
                  IndexFieldReference<String> paysField) {
        this.refField = refField;
        this.embranchementField = embranchementField;
        this.familleField = familleField;
        this.genreField = genreField;
        this.especeField = especeField;
        this.campagneField = campagneField;
        this.paysField = paysField;
    }

    @Override
    public void write(DocumentElement target, Produit bridgedElement, TypeBridgeWriteContext context) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("Custom indexing of Produit entity : " + bridgedElement);
        }

        // commons information for all produit
        Lot lot = null;
        if (bridgedElement instanceof Extrait) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("Custom indexing of Extrait entity");
            }
            Extrait extrait = (Extrait) bridgedElement;
            lot = extrait.getExtraction().getLot();
        } else if (bridgedElement instanceof Fraction) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("Custom indexing of Fraction entity");
            }
            Fraction fraction = (Fraction) bridgedElement;
            lot = fraction.getPurification().getLotSource();
        }

        // ref field tokenized with a non fr analyzer
        // to avoid duplicated letter removal (00)
        target.addValue(refField, lot.getRef());

        if (StringUtils.isNotBlank(lot.getSpecimenRef().getEmbranchement())) {
            target.addValue(embranchementField, lot.getSpecimenRef().getEmbranchement());
        }
        if (StringUtils.isNotBlank(lot.getSpecimenRef().getFamille())) {
            target.addValue(familleField, lot.getSpecimenRef().getFamille());
        }
        if (StringUtils.isNotBlank(lot.getSpecimenRef().getGenre())) {
            target.addValue(genreField, lot.getSpecimenRef().getGenre());
        }
        if (StringUtils.isNotBlank(lot.getSpecimenRef().getEspece())) {
            target.addValue(especeField, lot.getSpecimenRef().getEspece());
        }
        target.addValue(campagneField, lot.getCampagne().getNom());
        target.addValue(paysField, lot.getCampagne().getCodePays());
    }
}
