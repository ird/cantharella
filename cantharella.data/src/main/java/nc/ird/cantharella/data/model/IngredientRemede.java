package nc.ird.cantharella.data.model;

import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.data.model.utils.DocumentAttachable;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections4.comparators.ComparatorChain;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Indexed
public class IngredientRemede extends AbstractModel implements Comparable<IngredientRemede>, DocumentAttachable, Cloneable {

    @Id
    @GeneratedValue
    private Integer idIngredient;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @IndexedEmbedded
    Specimen specimen;

    @Length(max = LENGTH_MEDIUM_TEXT)
    @FullTextField(projectable = Projectable.YES)
    String nomVernaculaire;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @IndexedEmbedded
    Partie partie;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @IndexedEmbedded
    Station provenance;

    @Length(max = LENGTH_MEDIUM_TEXT)
    String periode;

    @Length(max = LENGTH_MEDIUM_TEXT)
    String disponibilite;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @NotNull
    @IndexedEmbedded
    private Remede remede;

    public Integer getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(Integer idIngredient) {
        this.idIngredient = idIngredient;
    }

    public Specimen getSpecimen() {
        return specimen;
    }

    public void setSpecimen(Specimen specimen) {
        this.specimen = specimen;
    }

    public String getNomVernaculaire() {
        return nomVernaculaire;
    }

    public void setNomVernaculaire(String nomVernaculaire) {
        this.nomVernaculaire = nomVernaculaire;
    }

    public Partie getPartie() {
        return partie;
    }

    public void setPartie(Partie partie) {
        this.partie = partie;
    }

    public Station getProvenance() {
        return provenance;
    }

    public void setProvenance(Station provenance) {
        this.provenance = provenance;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(String disponibilite) {
        this.disponibilite = disponibilite;
    }

    public Remede getRemede() {
        return remede;
    }

    public void setRemede(Remede remede) {
        this.remede = remede;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(IngredientRemede ingredientRemede) {
        ComparatorChain<IngredientRemede> comparatorChain = new ComparatorChain<>();
        comparatorChain.addComparator(new BeanComparator<>("specimen"));
        comparatorChain.addComparator(new BeanComparator<>("nomVernaculaire"));
        return comparatorChain.compare(this, ingredientRemede);
    }


    @Override
    public List<Document> getDocuments() {
        return remede.getDocuments();
    }

    @Override
    public void addDocument(Document document) {
        remede.addDocument(document);
    }

    @Override
    public void removeDocument(Document document) {
        remede.removeDocument(document);
    }

    @Override
    public IngredientRemede clone() throws CloneNotSupportedException {
        return (IngredientRemede)super.clone();
    }
}
