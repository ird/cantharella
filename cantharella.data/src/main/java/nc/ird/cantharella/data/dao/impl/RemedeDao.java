package nc.ird.cantharella.data.dao.impl;

import nc.ird.cantharella.data.model.EntreeReferentiel;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Remede;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.Date;

public class RemedeDao {

    public static final DetachedCriteria CRITERIA_LIST = DetachedCriteria.forClass(Remede.class, "r").addOrder(
            Order.asc("r.id"));

    public static final DetachedCriteria CRITERIA_LIST_INGREDIENTS = DetachedCriteria.forClass(IngredientRemede.class,
            "r").addOrder(Order.asc("r.id"));

    public static final DetachedCriteria CRITERIA_DISTINCT_NOM_REMEDE = DetachedCriteria.forClass(Remede.class)
            .setProjection(Projections.distinct(Projections.property("nom"))).addOrder(Order.asc("nom"));

    public static final DetachedCriteria CRITERIA_DISTINCT_NOM_VERNACULAIRE_INGREDIENT = DetachedCriteria
            .forClass(IngredientRemede.class)
            .setProjection(Projections.distinct(Projections.property("nomVernaculaire")))
            .addOrder(Order.asc("nomVernaculaire"));

    /**
     * Rend le criteria qui rend le nombre de tests biologiques qui référencent un produit
     * 
     * @param referentiel La référence du produit
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaListEntreeReferentiel(EntreeReferentiel.Referentiel referentiel) {
        return DetachedCriteria.forClass(EntreeReferentiel.class).add(Restrictions.eq("referentiel", referentiel))
                .addOrder(Order.asc("valeur"));
    }

    public static DetachedCriteria getCriteriaEntreeReferentielUsed(EntreeReferentiel entree) {
        return DetachedCriteria.forClass(Remede.class).createAlias("classification", "classification")
                .add(Restrictions.eq("classification.id", entree.getIdEntree()))
                .setProjection(Projections.distinct(Projections.property("code")))
                .setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui compte le nombre d'occurrences d'un prescripteur
     * 
     * @param personneInterrogee Le prescripteur
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountPrescripteur(PersonneInterrogee personneInterrogee) {
        return DetachedCriteria.forClass(IngredientRemede.class).createAlias("remede", "remede", JoinType.FULL_JOIN)
                .add(Restrictions.eq("remede.prescripteur", personneInterrogee)).setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'un codeLangue
     * 
     * @param codeLangue Le codeLangue
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountCodeLangue(String codeLangue) {
        return DetachedCriteria.forClass(IngredientRemede.class).createAlias("remede", "remede", JoinType.FULL_JOIN)
                .add(Restrictions.eq("remede.codeLangue", codeLangue)).setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'une date
     * 
     * @param date La référence du produit
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountDate(Date date) {
        return DetachedCriteria.forClass(IngredientRemede.class).createAlias("remede", "remede", JoinType.FULL_JOIN)
                .add(Restrictions.eq("remede.date", date)).setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'un nom de remède
     * 
     * @param nom Le nom du remède
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountNomRemede(String nom) {
        return DetachedCriteria.forClass(IngredientRemede.class).createAlias("remede", "remede", JoinType.FULL_JOIN)
                .add(Restrictions.eq("remede.nom", nom)).setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'une indication vernaculaire
     * 
     * @param indication L'indication vernaculaire
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountIndicationsVernaculaires(String indication) {
        return DetachedCriteria.forClass(IngredientRemede.class).createAlias("remede", "remede", JoinType.FULL_JOIN)
                .add(Restrictions.eq("remede.indicationVernaculaire", indication))
                .setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'une ref de specimen
     * 
     * @param ref La référence de specimen
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountSpecimenRef(String ref) {
        return DetachedCriteria.forClass(IngredientRemede.class)
                .createAlias("specimen", "specimen", JoinType.INNER_JOIN).add(Restrictions.eq("specimen.ref", ref))
                .setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'une famille de specimen
     * 
     * @param famille La famille de specimen
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountSpecimenFamille(String famille) {
        return DetachedCriteria.forClass(IngredientRemede.class)
                .createAlias("specimen", "specimen", JoinType.INNER_JOIN)
                .add(Restrictions.eq("specimen.famille", famille)).setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'un genre de specimen
     * 
     * @param genre Le genre de specimen
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountSpecimenGenre(String genre) {
        return DetachedCriteria.forClass(IngredientRemede.class)
                .createAlias("specimen", "specimen", JoinType.INNER_JOIN).add(Restrictions.eq("specimen.genre", genre))
                .setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'une espèce de specimen
     * 
     * @param espece L'espece de specimen
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountSpecimenEspece(String espece) {
        return DetachedCriteria.forClass(IngredientRemede.class)
                .createAlias("specimen", "specimen", JoinType.INNER_JOIN)
                .add(Restrictions.eq("specimen.espece", espece)).setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'un nom vernaculaire d'ingrédient
     * 
     * @param nom Le nom vernaculaire de l'ingrédient
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountNomVernaculaire(String nom) {
        return DetachedCriteria.forClass(IngredientRemede.class).add(Restrictions.eq("nomVernaculaire", nom))
                .setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'un codePays
     * 
     * @param codePays Le codePays
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountCodePays(String codePays) {
        return DetachedCriteria.forClass(IngredientRemede.class).createAlias("remede", "remede", JoinType.FULL_JOIN)
                .createAlias("remede.campagne", "campagne", JoinType.INNER_JOIN)
                .add(Restrictions.eq("campagne.codePays", codePays)).setProjection(Projections.rowCount());
    }

    /**
     * Rend le criteria qui rend le nombre d'occurrences d'une camapgne
     * 
     * @param campagne La campagne
     * @return Le criteria
     **/
    public static DetachedCriteria getCriteriaCountCampagne(Campagne campagne) {
        return DetachedCriteria.forClass(IngredientRemede.class).createAlias("remede", "remede", JoinType.FULL_JOIN)
                .add(Restrictions.eq("remede.campagne", campagne)).setProjection(Projections.rowCount());
    }
}
