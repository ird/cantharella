package nc.ird.cantharella.web.utils.data;

import nc.ird.cantharella.service.services.DocumentService;
import nc.ird.cantharella.web.utils.ZipDataExporter;
import org.apache.wicket.Component;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.ExportToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.IDataExporter;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ResourceLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.ResourceStreamResource;
import org.apache.wicket.util.resource.IResourceStream;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ZipTableExportToolbar extends ExportToolbar {

    /**
     * Constructor adding csv export configuration.
     *
     * @param table    table to add export to
     * @param fileName export filename
     */
    public ZipTableExportToolbar(DataTable<?, ?> table, String fileName, DocumentService documentService) {
        super(table);

        // set message model
        setMessageModel(new StringResourceModel("ExportTo", this, null));

        // file name model
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd-HH_mm-");
        String headerFilename = dateFormat.format(new Date());
        setFileNameModel(new Model<>(headerFilename + fileName));

        ZipDataExporter zipDataExporter = new ZipDataExporter(documentService);
        addDataExporter(zipDataExporter);
    }

    /**
     * Creates a new link to the exported data for the provided {@link IDataExporter}.
     *
     * @param componentId  The component of the link.
     * @param dataExporter The data exporter to use to export the data.
     * @return a new link to the exported data for the provided {@link IDataExporter}.
     */
    protected Component createExportLink(String componentId, final IDataExporter dataExporter) {
        IResource resource = new ResourceStreamResource() {
            @Override
            protected IResourceStream getResourceStream(IResource.Attributes attributes) {
                return new DataExportResourceStreamWriter(dataExporter, getTable());
            }
        }.setFileName(getFileNameModel().getObject() + "." + dataExporter.getFileNameExtension());

        return new ZipTableExportToolbar.ImageResourceLink(componentId, resource, dataExporter);
    }

    /**
     * Panel which include a image. Used with the LinkableImagePropertyColumn$LinkablePanel.html file
     */
    public class ImageResourceLink extends Panel {

        /**
         * Constructor
         *
         * @param id           Component id
         * @param resource     link resource
         * @param dataExporter data exporter
         */
        public ImageResourceLink(String id, IResource resource, IDataExporter dataExporter) {
            super(id);

            ResourceLink<Void> link = new ResourceLink<>("link", resource);
            link.setBody(dataExporter.getDataFormatNameModel());
            add(link);

            // add a link on <type>_text.png image
            // for CSV : csv_text.png image
            String type = dataExporter.getDataFormatNameModel().getObject().toLowerCase();
            WebComponent img = new Image("img", new ContextRelativeResource("images/" + type + "_text.png"));
            add(img);
        }
    }
}