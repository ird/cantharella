/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.dosage;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.ErreurDosage;
import nc.ird.cantharella.data.model.MethodeDosage;
import nc.ird.cantharella.data.model.Molecule;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.ProduitOrLot;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.data.validation.utils.ModelValidator;
import nc.ird.cantharella.service.services.ConfigurationService;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.service.services.MoleculeService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.ProduitService;
import nc.ird.cantharella.service.services.TestBioService;
import nc.ird.cantharella.utils.BeanTools.AccessType;
import nc.ird.cantharella.utils.CollectionTools;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.document.panel.ManageListDocumentsPanel;
import nc.ird.cantharella.web.pages.domain.personne.ManagePersonnePage;
import nc.ird.cantharella.web.pages.renderers.PersonneRenderer;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.JSConfirmationBehavior;
import nc.ird.cantharella.web.utils.behaviors.ReplaceEmptyLabelBehavior;
import nc.ird.cantharella.web.utils.forms.AutoCompleteTextFieldString;
import nc.ird.cantharella.web.utils.forms.AutoCompleteTextFieldString.ComparisonMode;
import nc.ird.cantharella.web.utils.forms.SubmittableButton;
import nc.ird.cantharella.web.utils.forms.SubmittableButtonEvents;
import nc.ird.cantharella.web.utils.models.DisplayBooleanPropertyModel;
import nc.ird.cantharella.web.utils.models.DisplayDecimalPropertyModel;
import nc.ird.cantharella.web.utils.models.DisplayDecimalPropertyModel.DecimalDisplFormat;
import nc.ird.cantharella.web.utils.models.DisplayEnumPropertyModel;
import nc.ird.cantharella.web.utils.panels.SimpleTooltipPanel;
import nc.ird.cantharella.web.utils.renderers.EnumChoiceRenderer;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Page for adding/update/delete a new "Test biologique"
 * 
 * @author Adrien Cheype
 */
@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public final class ManageDosagePage extends TemplatePage {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ManageDosagePage.class);

    /** Action : create */
    private static final String ACTION_CREATE = "Create";

    /** Action : update */
    private static final String ACTION_UPDATE = "Update";

    /** Action : delete */
    private static final String ACTION_DELETE = "Delete";

    /** testBio Model */
    private final IModel<Dosage> dosageModel;

    /** model for adding resultat */
    private Model<ResultatDosageForUI> newResultatModel;

    /** Service : dosages */
    @SpringBean
    private DosageService dosageService;

    /** Service : molecules */
    @SpringBean
    private MoleculeService moleculeService;

    /** Service : lots */
    @SpringBean
    private LotService lotService;

    /** Service : produits */
    @SpringBean
    private ProduitService produitService;

    /** Service : personnes */
    @SpringBean
    private PersonneService personneService;

    /** Service : testBios */
    @SpringBean
    private ConfigurationService configurationService;

    /** Liste des personnes existantes */
    private final List<Personne> personnes;

    /** Liste des méthodes de dosage existantes */
    private final List<MethodeDosage> methodes;

    /** Liste des organismes testeurs déjà renseignés pour les personnes */
    private final List<String> organismes;

    /** Liste des molécules existantes */
    private final List<Molecule> molecules;

    /** Liste des molécules existantes */
    private final List<ProduitOrLot> produitsAndLots;

    /** Liste des erreurs de test existantes */
    private final List<ErreurDosage> erreurs;

    /** Liste des unites */
    private final List<UniteConcMasse> unites;

    /** Model validateur */
    @SpringBean(name = "webModelValidator")
    private ModelValidator validator;

    /** Page appelante */
    private final CallerPage callerPage;

    /** Saisie multiple */
    private boolean multipleEntry;

    /** Bouton d'ajout d'un résultat de test bio **/
    Button addResultatButton;

    /** Container pour l'affichage de la description de la méthode **/
    MarkupContainer descriptionMethoContainer;

    /** Container pour l'affichage des paramètres de la méthode **/
    MarkupContainer paramsMethoContainer;

    /** Input du conc./masse par défaut du test bio **/
    TextField<BigDecimal> concMasseDefautInput;

    /** Input pour l'unité de conc./masse par défaut du test bio **/
    DropDownChoice<UniteConcMasse> uniteConcMasseDefautInput;

    /** Input du conc./masse du résultat courant **/
    TextField<BigInteger> concMasseInput;

    /** Input pour l'unité de conc./masse du résultat courant **/
    DropDownChoice<UniteConcMasse> uniteConcMasseInput;

    DropDownChoice<Molecule> moleculeChoice;

    /**
     * Constructeur (mode création)
     * 
     * @param callerPage Page appelante
     * @param multipleEntry Saisie multiple
     */
    public ManageDosagePage(CallerPage callerPage, boolean multipleEntry) {
        this(null, null, callerPage, multipleEntry);
    }

    /**
     * Constructeur (mode édition)
     * 
     * @param idDosage Id de la manip de dosage
     * @param callerPage Page appelante
     */
    public ManageDosagePage(Integer idDosage, CallerPage callerPage) {
        this(idDosage, null, callerPage, false);
    }

    /**
     * Constructeur (mode saisie de la manip suivante)
     * 
     * @param dosage Manip de dosage
     * @param callerPage Page appelante
     */
    public ManageDosagePage(Dosage dosage, CallerPage callerPage) {
        this(null, dosage, callerPage, true);
    }

    /**
     * Constructeur. Si idDosage et dosage sont nuls, on crée un nouveau dosage. Si idDosage est renseignée,
     * on édite le dosage correspondant. Si dosage est renseigné, on crée un nouveau dosage à partir des
     * informations qu'elle contient.
     * 
     * @param idDosage Id du dosage
     * @param dosage dosage
     * @param callerPage Page appelante
     * @param multipleEntry Saisie multiple
     */
    private ManageDosagePage(Integer idDosage, Dosage dosage, final CallerPage callerPage, boolean multipleEntry) {
        super(ManageDosagePage.class);
        assert idDosage == null || dosage == null;
        this.callerPage = callerPage;
        final CallerPage currentPage = new CallerPage(this);
        this.multipleEntry = multipleEntry;

        newResultatModel = new Model<>(new ResultatDosageForUI());

        // Initialisation du modèle
        try {
            dosageModel = new Model<>(idDosage == null && dosage == null ? new Dosage() : dosage != null ? dosage
                    : dosageService.loadDosage(idDosage));
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
        boolean createMode = idDosage == null;
        if (createMode) {
            dosageModel.getObject().setCreateur(getSession().getUtilisateur());
        }

        // Initialisation des listes (pour le dropDownChoice)
        personnes = personneService.listPersonnes();
        methodes = dosageService.listMethodesDosage();
        molecules = moleculeService.listMolecules();
        molecules.removeIf(m -> m.getNomCommun() == null);
        molecules.sort((d1, d2) -> {
            if (d1.getNomCommun() == null) {
                return -1;
            } else if (d2.getNomCommun() == null) {
                return 1;
            } else {
                return d1.getNomCommun().compareToIgnoreCase(d2.getNomCommun());
            }
        });
        erreurs = dosageService.listErreursDosage();
        produitsAndLots = new ArrayList<>();
        produitsAndLots.addAll(lotService.listLots(getSession().getUtilisateur()));
        produitsAndLots.addAll(produitService.listProduits(getSession().getUtilisateur()));
        produitsAndLots.sort((d1, d2) -> d1.getRef().compareToIgnoreCase(d2.getRef()));

        newResultatModel.getObject().setConcMasse(dosageModel.getObject().getConcMasseDefaut());
        newResultatModel.getObject().setUniteConcMasse(dosageModel.getObject().getUniteConcMasseDefaut());

        if (dosage != null) {
            // qd saisie multiple avec préremplissage, hack nécessaire afin d'avoir dans le model le même objet que
            // celui de la liste de choix (sinon comme les objets viennent de sessions hibernate différentes, on n'a pas
            // l'égalité entre les objets)
            dosageModel.getObject().setManipulateur(
                    CollectionTools.findWithValue(personnes, "idPersonne", AccessType.GETTER, dosageModel.getObject()
                            .getManipulateur().getIdPersonne()));
            dosageModel.getObject().setMethode(
                    CollectionTools.findWithValue(methodes, "idMethodeDosage", AccessType.GETTER, dosageModel
                            .getObject().getMethode().getIdMethodeDosage()));
        }

        // liste des organismes suggérés à la saisie
        organismes = personneService.listPersonneOrganismes();

        unites = configurationService.listUnites();

        // bind with markup
        final Form<Void> formView = new Form<>("Form");

        // initialisation du formulaire
        initPrincipalFields(formView);
        initMethodeFields(formView);
        initResultatsFields(formView);

        // add list document panel
        ManageListDocumentsPanel manageListDocumentsPanel = new ManageListDocumentsPanel("ManageListDocumentsPanel",
                dosageModel, currentPage);
        formView.add(manageListDocumentsPanel);

        // Action : create the dosage
        Button createButton = new SubmittableButton(ACTION_CREATE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                dosageService.createDosage(dosageModel.getObject());
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_CREATE);
                redirect();
            }

            @Override
            public void onValidate() {
                validateModel();
            }
        });
        createButton.setVisibilityAllowed(createMode);
        formView.add(createButton);

        // Action : update the dosage
        Button updateButton = new SubmittableButton(ACTION_UPDATE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                dosageService.updateDosage(dosageModel.getObject());
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_UPDATE);
                callerPage.responsePage((TemplatePage) getPage());
            }

            @Override
            public void onValidate() {
                validateModel();
            }
        });
        updateButton.setVisibilityAllowed(!createMode);
        formView.add(updateButton);

        // Action : suppression
        Button deleteButton = new SubmittableButton(ACTION_DELETE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                dosageService.deleteDosage(dosageModel.getObject());
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_DELETE);
                callerPage.responsePage((TemplatePage) getPage());
            }
        });
        deleteButton.setVisibilityAllowed(!createMode);
        deleteButton.setDefaultFormProcessing(false);
        deleteButton.add(new JSConfirmationBehavior(getStringModel("Confirm")));
        formView.add(deleteButton);

        formView.add(new Link<Void>("Cancel") {
            // Cas où le formulaire est annulé
            @Override
            public void onClick() {
                callerPage.responsePage((TemplatePage) getPage());
            }
        });

        formView.setDefaultButton(addResultatButton);
        add(formView);

    }

    /**
     * Initialise les champs principaux
     * 
     * @param formView le formulaire
     */
    private void initPrincipalFields(Form<Void> formView) {
        formView.add(new TextField<String>("Dosage.ref", new PropertyModel<>(dosageModel, "ref")));

        formView.add(new DropDownChoice<>("Dosage.manipulateur", new PropertyModel<>(dosageModel,
                "manipulateur"), personnes, new PersonneRenderer()).setNullValid(false));

        // Action : création d'une nouvelle personne
        // ajaxSubmitLink permet de sauvegarder l'état du formulaire
        formView.add(new AjaxSubmitLink("NewPersonne") {
            @Override
            protected void onSubmit(AjaxRequestTarget arg0) {
                setResponsePage(new ManagePersonnePage(new CallerPage((TemplatePage) getPage()), false));
            }

            // si erreur, le formulaire est également enregistré puis la redirection effectuée
            @Override
            protected void onError(AjaxRequestTarget target) {
                setResponsePage(new ManagePersonnePage(new CallerPage((TemplatePage) getPage()), false));
            }
        });

        formView.add(new AutoCompleteTextFieldString("Dosage.organisme", new PropertyModel<>(
                dosageModel, "organisme"), organismes, ComparisonMode.CONTAINS));

        formView.add(new DateTextField("Dosage.date", new PropertyModel<>(dosageModel, "date"),"yyyy-MM-dd"));

        concMasseDefautInput = new TextField<>("Dosage.concMasseDefaut", new PropertyModel<>(
                dosageModel, "concMasseDefaut"));

        concMasseDefautInput.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                newResultatModel.getObject().setConcMasse(concMasseDefautInput.getModelObject());
                target.add(concMasseInput);
            }
        });
        formView.add(concMasseDefautInput);

        uniteConcMasseDefautInput = new DropDownChoice<>("Dosage.uniteConcMasseDefaut",
                new PropertyModel<>(dosageModel, "uniteConcMasseDefaut"), unites);
        uniteConcMasseDefautInput.setNullValid(true);

        uniteConcMasseDefautInput.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                newResultatModel.getObject().setUniteConcMasse(uniteConcMasseDefautInput.getModelObject());
                target.add(uniteConcMasseInput);
            }
        });
        formView.add(uniteConcMasseDefautInput);

        formView.add(new TextArea<String>("Dosage.complement", new PropertyModel<>(dosageModel, "complement")));
        // Créateur en lecture seule
        formView.add(new TextField<String>("Dosage.createur", new PropertyModel<>(dosageModel, "createur"))
                .setEnabled(false));


    }

    /**
     * Initialise les champs relatifs à la méthode
     * 
     * @param formView le formulaire
     */
    private void initMethodeFields(final Form<Void> formView) {

        final WebMarkupContainer methodeCont = new WebMarkupContainer("Dosage.methode");
        methodeCont.setOutputMarkupId(true);
        formView.add(methodeCont);

        // Champs pour la méthode
        descriptionMethoContainer = new WebMarkupContainer("Dosage.descriptionMethodeCont") {
            @Override
            public boolean isVisible() {
                // description cachée si pas de méthode sélectionnée
                return dosageModel.getObject().getMethode() != null;
            }
        };
        descriptionMethoContainer.setOutputMarkupId(true); // pour l'update Ajax
        descriptionMethoContainer.setOutputMarkupPlaceholderTag(true); // pour accéder à l'élement html qd son état est
        // non visible
        methodeCont.add(descriptionMethoContainer);

        Label methodeAcronyme = new Label("Dosage.acronymeMethode", new PropertyModel<String>(dosageModel, "methode.acronyme"));
        descriptionMethoContainer.add(methodeAcronyme);

        Label methodeDomaine = new Label("Dosage.domaineMethode", new PropertyModel<String>(dosageModel,
                "methode.domaine"));
        descriptionMethoContainer.add(methodeDomaine);

        MultiLineLabel methodeDesc = new MultiLineLabel("Dosage.descriptionMethode", new PropertyModel<String>(
                dosageModel, "methode.description"));
        descriptionMethoContainer.add(methodeDesc);

        Label methodeValeurMesuree = new Label("Dosage.valeurMesureeMethode", new PropertyModel<String>(dosageModel,
                "methode.valeurMesuree"));
        descriptionMethoContainer.add(methodeValeurMesuree);

        Label seuilMethode = new Label("Dosage.seuilMethode", new PropertyModel<String>(
                dosageModel, "methode.seuil"));
        descriptionMethoContainer.add(seuilMethode);

        Label methodeUniteResultat = new Label("Dosage.uniteResultatMethode", new PropertyModel<String>(dosageModel,
                "methode.unite"));
        descriptionMethoContainer.add(methodeUniteResultat);

        final DropDownChoice<MethodeDosage> methodeChoice = new DropDownChoice<>("Dosage.nomMethode",
                new PropertyModel<>(dosageModel, "methode"), methodes);
        methodeChoice.setNullValid(false);
        // mise à jour de la description de la méthode et des fractions lors de la sélection de la méthode
        methodeChoice.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                // mise à jour de la description et des paramètres
                target.add(methodeCont);
            }
        });
        methodeCont.add(methodeChoice);
    }

    /**
     * Initialise les champs relatifs aux résultats de test
     *
     * @param formView Le formulaire
     */
    private void initResultatsFields(final Form<Void> formView) {

        // Déclaration tableau des resultats
        final MarkupContainer dosagesTable = new WebMarkupContainer("Dosage.resultats.Table");
        dosagesTable.setOutputMarkupId(true);

        // Contenu tableaux resultats
        dosagesTable.add(new ListView<ResultatDosageForUI>("Dosage.resultats.List",
                new PropertyModel<>(dosageModel, "sortedResultats")) {
            @Override
            protected void populateItem(ListItem<ResultatDosageForUI> item) {
                if (item.getIndex() % 2 == 1) {
                    item.add(new AttributeModifier("class", "odd"));
                }

                final IModel<ResultatDosageForUI> resultatModel = item.getModel();
                final ResultatDosageForUI resultat = new ResultatDosageForUI(item.getModelObject());

                // Colonnes
                item.add(new Label("Dosage.resultats.List.analyseNb", new PropertyModel<String>(resultat, "analyseNb")));
                item.add(new Label("Dosage.resultats.List.refEchantillon", new PropertyModel<String>(resultat, "reference.ref")));
                item.add(new Label("Dosage.resultats.List.concMasse", new PropertyModel<String>(resultat,"concMasse")));
                item.add(new Label("Dosage.resultats.List.uniteConcMasse", new PropertyModel<String>(resultat, "uniteConcMasse")));
                item.add(new Label("Dosage.resultats.List.compose", new PropertyModel<String>(resultat,"composeDose")));
                item.add(new Label("Dosage.resultats.List.molecule", new PropertyModel<String>(resultat,"moleculeDosee.nomCommun")));
                item.add(new Label("Dosage.resultats.List.valeur", new DisplayDecimalPropertyModel(resultat, "valeur",
                        DecimalDisplFormat.LARGE, getSession().getLocale())));
                item.add(new Label("Dosage.resultats.List.SD", new DisplayDecimalPropertyModel(resultat, "SD",
                        DecimalDisplFormat.LARGE, getSession().getLocale())));
                item.add(new Label("Dosage.resultats.List.supSeuil", new DisplayBooleanPropertyModel(resultatModel,
                        "supSeuil", (TemplatePage) this.getPage())).add(new ReplaceEmptyLabelBehavior()));

                item.add(new Label("Dosage.resultats.List.erreur", new PropertyModel<String>(resultat, "erreur.nom")));
                // info-bulle comprenant la description de l'erreur
                item.add(new SimpleTooltipPanel("Dosage.resultats.List.erreur.info", new PropertyModel<String>(
                        resultat, "erreur.description")) {
                    /** {@inheritDoc} */
                    @Override
                    public boolean isVisible() {
                        return resultat.getErreur() != null;
                    }
                });

                // Action : suppression d'un résultat de test
                Button deleteButton = new AjaxFallbackButton("Dosage.resultats.List.Delete", formView) {
                    @Override
                    protected void onSubmit(Optional<AjaxRequestTarget> target) {
                        // Suppression
                        dosageModel.getObject().getResultats().remove(resultat.toResultatDosage());

                        if (target.isPresent()) {
                            target.get().add(dosagesTable);
                            refreshFeedbackPage(target.get());
                        }
                    }
                };
                deleteButton.setDefaultFormProcessing(false);
                item.add(deleteButton);
            }
        });

        // champs d'input
        dosagesTable.add(new TextField<String>("Dosage.resultats.analyseNb", new PropertyModel<>(newResultatModel,
                "analyseNb")) {
            @Override
            @SuppressWarnings("unchecked")
            public boolean isRequired() {
                // champs requis uniquement qd le bouton d'ajout de résultat est activé
                // de même pour les autres composants ci-dessous
                Form<Void> form = (Form<Void>) findParent(Form.class);
                return form.getRootForm().findSubmitter() == addResultatButton;
            }
        });
        dosagesTable.add(new DropDownChoice<>("Dosage.resultats.refEchantillon",
                new PropertyModel<>(newResultatModel, "reference"), produitsAndLots) {
            @Override
            public boolean isRequired() {
                Form form = findParent(Form.class);
                return form.getRootForm().findSubmitter() == addResultatButton;
            }
        });

        concMasseInput = new TextField<>("Dosage.resultats.concMasse", new PropertyModel<>(
                newResultatModel, "concMasse"));
        concMasseInput.setOutputMarkupId(true);
        concMasseInput.setOutputMarkupPlaceholderTag(true);
        dosagesTable.add(concMasseInput);

        uniteConcMasseInput = new DropDownChoice<>("Dosage.resultats.uniteConcMasse",
                new PropertyModel<>(newResultatModel, "uniteConcMasse"), unites);
        uniteConcMasseInput.setOutputMarkupId(true);
        uniteConcMasseInput.setOutputMarkupPlaceholderTag(true);
        uniteConcMasseInput.setNullValid(true);

        dosagesTable.add(uniteConcMasseInput);

        final Boolean[] composeRequired = {true};
        final Boolean[] moleculeRequired = {true};

        TextField<String> composeField = new TextField<>("Dosage.resultats.compose", new PropertyModel<>(newResultatModel,
                "composeDose")) {
            @Override
            public boolean isRequired() {
                // champs requis uniquement qd le bouton d'ajout de résultat est activé
                // de même pour les autres composants ci-dessous
                Form form = findParent(Form.class);
                return form.getRootForm().findSubmitter() == addResultatButton && composeRequired[0];
            }
        };
        composeField.setOutputMarkupId(true);
        composeField.setOutputMarkupPlaceholderTag(true);
        dosagesTable.add(composeField);

        // comportement dynamique sur les inputs
        composeField.add(new AjaxFormComponentUpdatingBehavior("keyup") {
            protected void onUpdate(AjaxRequestTarget target) {
                if (composeField.getModelObject() != null) {
                    // si une molecule est sélectionnée, le champ compose est désactivé
                    moleculeChoice.setVisibilityAllowed(false);
                    newResultatModel.getObject().setMoleculeDosee(null);
                    composeRequired[0] = true;
                    moleculeRequired[0] = false;
                } else {
                    // si aucune molecule est sélectionnée, le champ compose est activé
                    moleculeChoice.setVisibilityAllowed(true);
                    composeRequired[0] = false;
                    moleculeRequired[0] = true;
                }
                target.add(moleculeChoice);
            }
        });

        moleculeChoice = new DropDownChoice<>("Dosage.resultats.molecule",
                new PropertyModel<>(newResultatModel, "moleculeDosee"), molecules) {
            @Override
            public boolean isRequired() {
                Form form = findParent(Form.class);
                return form.getRootForm().findSubmitter() == addResultatButton
                        && moleculeRequired[0];
            }
        };
        moleculeChoice.setOutputMarkupId(true);
        moleculeChoice.setOutputMarkupPlaceholderTag(true);
        moleculeChoice.setNullValid(true);
        dosagesTable.add(moleculeChoice);

        // comportement dynamique sur les inputs
        moleculeChoice.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                if (moleculeChoice.getModelObject() != null) {
                    // si une molecule est sélectionnée, le champ compose est désactivé
                    composeField.setVisibilityAllowed(false);
                    newResultatModel.getObject().setComposeDose(null);
                    composeRequired[0] = false;
                    moleculeRequired[0] = true;
                } else {
                    // si aucune molecule est sélectionnée, le champ compose est activé
                    composeField.setVisibilityAllowed(true);
                    composeRequired[0] = true;
                    moleculeRequired[0] = false;
                }
                target.add(composeField);
            }
        });

        dosagesTable.add(new CheckBox("Dosage.resultats.supSeuil", new PropertyModel<>(newResultatModel, "supSeuil")));

        final DropDownChoice<ErreurDosage> erreurChoice = new DropDownChoice<>(
                "Dosage.resultats.erreur", new PropertyModel<>(newResultatModel, "erreur"), erreurs);
        erreurChoice.setNullValid(true);
        dosagesTable.add(erreurChoice);

        // info-bulle comprenant la description de l'erreur
        final SimpleTooltipPanel infoBulle = new SimpleTooltipPanel("Dosage.resultats.erreur.info",
                new PropertyModel<String>(newResultatModel, "erreur.description"));
        infoBulle.setVisibilityAllowed(false);
        // permet la mise en visibité ou non en Ajax
        infoBulle.setOutputMarkupId(true);
        infoBulle.setOutputMarkupPlaceholderTag(true);
        dosagesTable.add(infoBulle);

        final TextField<BigDecimal> valeurInput = new TextField<>("Dosage.resultats.valeur",
                new PropertyModel<>(newResultatModel, "valeur")) {
            @Override
            @SuppressWarnings("unchecked")
            public boolean isRequired() {
                Form<Void> form = (Form<Void>) findParent(Form.class);
                return form.getRootForm().findSubmitter() == addResultatButton
                        && erreurChoice.getModelObject() == null;
            }
        };
        valeurInput.setOutputMarkupId(true);
        valeurInput.setOutputMarkupPlaceholderTag(true);
        dosagesTable.add(valeurInput);

        TextField<BigDecimal> sdInput = new TextField<>("Dosage.resultats.SD",
                new PropertyModel<>(newResultatModel, "SD"));
        sdInput.setOutputMarkupId(true);
        sdInput.setOutputMarkupPlaceholderTag(true);
        dosagesTable.add(sdInput);

        erreurChoice.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                if (erreurChoice.getModelObject() != null) {
                    // si une erreur est sélectionnée, le champ valeur est désactivé et l'info-bulle affichée
                    valeurInput.setVisibilityAllowed(false);
                    sdInput.setVisibilityAllowed(false);
                    newResultatModel.getObject().setValeur(null);
                    newResultatModel.getObject().setSD(null);
                    infoBulle.setVisibilityAllowed(true);
                } else {
                    // si une erreur est désélectionnée, le champ valeur est activé et l'info-bulle désactivée
                    valeurInput.setVisibilityAllowed(true);
                    sdInput.setVisibilityAllowed(true);
                    infoBulle.setVisibilityAllowed(false);
                }
                target.add(valeurInput, sdInput, infoBulle);
            }
        });

        // Bouton AJAX pour ajouter un résultat de test
        addResultatButton = new AjaxFallbackButton("Dosage.resultats.Add", formView) {
            @Override
            protected void onSubmit(Optional<AjaxRequestTarget> target) {
                // ajout du dosage
                newResultatModel.getObject().setDosage(dosageModel.getObject());

                List<String> errors = validator.validate(newResultatModel.getObject(), getSession().getLocale());

                //assure que l'échantillon est le même que les précédents pour le même analyseNb;
                for (ResultatDosage result:dosageModel.getObject().getResultats()) {
                    ResultatDosageForUI resultForUI = new ResultatDosageForUI(result);
                    if (Objects.equals(resultForUI.getAnalyseNb(), newResultatModel.getObject().getAnalyseNb()) &&
                            !Objects.equals(resultForUI.getReference().getRef(), newResultatModel.getObject().getReference().getRef())) {
                        errors.add(getString("Dosage.resultats.analyseRefMismatch"));
                        break;
                    }
                }

                //assure qu'il n'y a qu'un composé ou molécule par analyse;
                for (ResultatDosage result:dosageModel.getObject().getResultats()) {
                    ResultatDosageForUI resultForUI = new ResultatDosageForUI(result);
                    if (Objects.equals(resultForUI.getAnalyseNb(), newResultatModel.getObject().getAnalyseNb()) &&
                            resultForUI.getComposeDose() != null &&
                            Objects.equals(resultForUI.getComposeDose(), newResultatModel.getObject().getComposeDose())) {
                        errors.add(getString("Dosage.resultats.composeDose"));
                        break;
                    }
                    if (Objects.equals(resultForUI.getAnalyseNb(), newResultatModel.getObject().getAnalyseNb()) &&
                            resultForUI.getMoleculeDosee() != null &&
                            Objects.equals(resultForUI.getMoleculeDosee(), newResultatModel.getObject().getMoleculeDosee())) {
                        errors.add(getString("Dosage.resultats.moleculeDosee"));
                        break;
                    }
                }

                // si une saisie existe pour concMasse, on s'assure que les deux composantes sont bien renseignées
                if ((newResultatModel.getObject().getConcMasse() != null && newResultatModel.getObject()
                        .getUniteConcMasse() == null)
                        || (newResultatModel.getObject().getConcMasse() == null && newResultatModel.getObject()
                                .getUniteConcMasse() != null)) {
                    errors.add(getString("Dosage.resultats.concMasse.KO"));
                }

                if (errors.isEmpty()) {
                    // ajout à la liste
                    ResultatDosage resultatAdded = newResultatModel.getObject().toResultatDosage();
                    dosageModel.getObject().getResultats().add(resultatAdded);
                    // réinit des champs de la ligne "ajout"
                    newResultatModel.getObject().setAnalyseNb(null);
                    newResultatModel.getObject().setReference(null);
                    // concMasse et uniteConcMasse prennent les valeurs par défaut
                    newResultatModel.getObject().setConcMasse(concMasseDefautInput.getModelObject());
                    newResultatModel.getObject().setUniteConcMasse(uniteConcMasseDefautInput.getModelObject());
                    newResultatModel.getObject().setComposeDose(null);
                    newResultatModel.getObject().setMoleculeDosee(null);
                    newResultatModel.getObject().setValeur(null);
                    newResultatModel.getObject().setSD(null);
                    newResultatModel.getObject().setSupSeuil(null);
                    newResultatModel.getObject().setErreur(null);
                    // réactivation du champ valeur en cas d'ancienne sélection d'erreur
                    valeurInput.setVisibilityAllowed(true);
                    moleculeChoice.setVisibilityAllowed(true);
                    composeField.setVisibilityAllowed(true);
                } else {
                    addValidationErrors(errors);
                }

                if (target.isPresent()) {
                    target.get().add(dosagesTable);
                    refreshFeedbackPage(target.get());
                }
            }

            @Override
            protected void onError(Optional<AjaxRequestTarget> target) {
                target.ifPresent(ajaxRequestTarget -> refreshFeedbackPage(ajaxRequestTarget));
            }

        };
        dosagesTable.add(addResultatButton);
        formView.add(dosagesTable);
    }

    /** {@inheritDoc} */
    @Override
    protected void onBeforeRender() {
        // On rafraichit le modèle lorsque la page est rechargée (par exemple après l'ajout d'une nouvelle entité
        // Personne ou Lot)
        refreshModel();

        super.onBeforeRender();
    }

    /**
     * Redirection vers une autre page. Cas où le formulaire est validé
     */
    private void redirect() {
        if (multipleEntry) {
            // Redirection de nouveau vers l'écran de saisie d'un nouveau dosage
            Dosage nextManip = new Dosage();
            nextManip.setManipulateur(dosageModel.getObject().getManipulateur());
            nextManip.setOrganisme(dosageModel.getObject().getOrganisme());
            nextManip.setMethode(dosageModel.getObject().getMethode());
            setResponsePage(new ManageDosagePage(nextManip, callerPage));
        } else if (callerPage != null) {
            // On passe l'id du dosage associé à cette page, en paramètre de la prochaine page, pour lui permettre de
            // l'exploiter si besoin
            callerPage.addPageParameter(Dosage.class.getSimpleName(), dosageModel.getObject());
            callerPage.responsePage(this);
        }
    }

    /**
     * Refresh model, appelé au rechargement de la page
     */
    private void refreshModel() {

        // Récupère (et supprime) les éventuels nouveaux objets créés dans les paramètres de la page.
        String key = Personne.class.getSimpleName();
        if (getPageParameters().getNamedKeys().contains(key)) {
            CollectionTools.setter(personnes, personneService.listPersonnes());
            try {
                Personne createdPersonne = personneService.loadPersonne(getPageParameters().get(key).toInt());
                dosageModel.getObject().setManipulateur(createdPersonne);
            } catch (DataNotFoundException e) {
                LOG.error(e.getMessage(), e);
                throw new UnexpectedException(e);
            }
            getPageParameters().remove(key);
        }
    }

    /**
     * Validate model
     */
    private void validateModel() {
        addValidationErrors(validator.validate(dosageModel.getObject(), getSession().getLocale()));

        /*if (!CollectionTools.containsWithValue(dosageModel.getObject().getResultats(), "typeResultat",
                AccessType.GETTER, TypeResultat.PRODUIT)) {
            error(getString("Dosage.resultats.noProduit"));
        }*/
        if (!dosageService.isDosageUnique(dosageModel.getObject())) {
            error(getString("Dosage.notUnique"));
        }
    }
}
