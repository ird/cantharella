/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.search;

import java.util.*;
import java.util.stream.Collectors;

import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Molecule;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.ProduitOrLot;
import nc.ird.cantharella.data.model.Purification;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.TypeDocument;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.DocumentSearchResult;
import nc.ird.cantharella.service.model.MoleculeProvenanceBean;
import nc.ird.cantharella.service.model.SearchBean;
import nc.ird.cantharella.service.model.SearchResult;
import nc.ird.cantharella.service.services.DocumentService;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.service.services.ExtractionService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.service.services.MoleculeService;
import nc.ird.cantharella.service.services.PersonneInterrogeeService;
import nc.ird.cantharella.service.services.PurificationService;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.service.services.SearchService;
import nc.ird.cantharella.service.services.SpecimenService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.service.services.TestBioService;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.campagne.ListCampagnesPage;
import nc.ird.cantharella.web.pages.domain.dosage.AnalyseDosage;
import nc.ird.cantharella.web.pages.domain.dosage.ListDosagePage;
import nc.ird.cantharella.web.pages.domain.dosage.ResultatDosageForUI;
import nc.ird.cantharella.web.pages.domain.extraction.ListExtractionsPage;
import nc.ird.cantharella.web.pages.domain.lot.ListLotsPage;
import nc.ird.cantharella.web.pages.domain.molecule.ListMoleculesPage;
import nc.ird.cantharella.web.pages.domain.personneInterrogee.ListPersonnesInterrogeesPage;
import nc.ird.cantharella.web.pages.domain.purification.ListPurificationsPage;
import nc.ird.cantharella.web.pages.domain.remede.ListRemedePage;
import nc.ird.cantharella.web.pages.domain.remede.SortableRemedeDataProvider;
import nc.ird.cantharella.web.pages.domain.specimen.ListSpecimensPage;
import nc.ird.cantharella.web.pages.domain.station.ListStationsPage;
import nc.ird.cantharella.web.pages.domain.testBio.ListTestsBioPage;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.RemedeListBehavior;
import nc.ird.cantharella.web.utils.behaviors.SearchMapViewBehavior;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import nc.ird.cantharella.web.utils.models.SimpleSortableListDataProvider;
import nc.ird.cantharella.web.utils.renderers.MapChoiceRenderer;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;

import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.extensions.markup.html.tabs.TabbedPanel;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.value.ValueMap;

/**
 * Search results page.
 * 
 * @author echatellier
 */
@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public class SearchPage extends TemplatePage {

    /** Form query bean. */
    protected Model<SearchBean> queryModel;

    /** Search service. */
    @SpringBean
    protected SearchService searchService;

    /** Specimen service. */
    @SpringBean
    protected SpecimenService specimenService;

    /** Lot service. */
    @SpringBean
    protected LotService lotService;

    /** Extraction service. */
    @SpringBean
    protected ExtractionService extractionService;

    /** Purification service. */
    @SpringBean
    protected PurificationService purificationService;

    /** Testbio service. */
    @SpringBean
    protected TestBioService testBioService;

    /** Station service. */
    @SpringBean
    protected StationService stationService;

    /** Molecule service. */
    @SpringBean
    protected MoleculeService moleculeService;

    /** Dosage service. */
    @SpringBean
    protected DosageService dosageService;

    /** Personne interrogée service. */
    @SpringBean
    protected PersonneInterrogeeService personneInterrogeeService;

    /** Remede service. */
    @SpringBean
    protected RemedeService remedeService;

    /** Document service. */
    @SpringBean
    protected DocumentService documentService;

    /**
     * Constructor.
     */
    public SearchPage() {
        this(null);
    }

    /**
     * Constructor.
     * 
     * @param queryBean form query bean
     */
    public SearchPage(SearchBean queryBean) {
        super(SearchPage.class);
        final CallerPage currentPage = new CallerPage(this);

        List tabs=new ArrayList<ITab>();
        tabs.add(new AbstractTab(Model.of("Recherche de données")) {
            @Override
            public Panel getPanel(String panelId)
            {
                return new GeneralSearchPanel(panelId, queryBean, currentPage, getSession().getUtilisateur(), (TemplatePage) getPage());
            }
        });
        tabs.add(new AbstractTab(new Model<String>("Recherche de documents")) {
            public Panel getPanel(String panelId)
            {
                return new DocumentSearchPanel(panelId, queryBean, currentPage, getSession().getUtilisateur(), (TemplatePage) getPage());
            }
        });

        TabbedPanel tabPanel = new TabbedPanel<>("tabs", tabs);
        tabPanel.setSelectedTab(queryBean!=null?queryBean.getSearchTab():0);
        add(tabPanel);
    }

    private class GeneralSearchPanel extends Panel {

        public GeneralSearchPanel(String id, SearchBean queryBean, CallerPage currentPage, Utilisateur utilisateur, TemplatePage template) {
            super(id);

            // ca sert a rien, mais wicket est pas content
            ExternalLink link = new ExternalLink("advancedLink",
                    "https://lucene.apache.org/core/8_10_1/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#package.description");
            add(link);

            // init query model
            queryModel = Model.of(queryBean == null ? new SearchBean() : queryBean);

            // search form
            Form<ValueMap> searchForm = new Form<>(getResource() + ".Form") {
                protected void onSubmit() {
                    queryBean.setSearchTab(0);
                    setResponsePage(new SearchPage(queryModel.getObject()));
                }
            };
            searchForm.add(new TextField<>("SearchPage.Query", new PropertyModel<String>(queryModel, "query")));
            searchForm.add(new DropDownChoice<>("SearchPage.Country",
                    new PropertyModel<>(queryModel, "country"), WebContext.COUNTRY_CODES
                    .get(getSession().getLocale()), new MapChoiceRenderer<>(WebContext.COUNTRIES
                    .get(getSession().getLocale()))));

            searchForm.add(new TextField<>("SearchPage.NElatitude", new PropertyModel<String>(queryModel, "NElatitude")));
            searchForm.add(new TextField<>("SearchPage.NElongitude", new PropertyModel<String>(queryModel, "NElongitude")));
            searchForm.add(new TextField<>("SearchPage.SOlatitude", new PropertyModel<String>(queryModel, "SOlatitude")));
            searchForm.add(new TextField<>("SearchPage.SOlongitude", new PropertyModel<String>(queryModel, "SOlongitude")));

            Button button1 = new Button("SearchPage.Clear") {
                public void onSubmit() {
                    queryModel = Model.of(new SearchBean());
                }
            };
            searchForm.add(button1);

            add(searchForm);

            add(new Label("SearchPage.map", new Model<>("")).add(new SearchMapViewBehavior()));

            // search results
            SearchResult searchResult = searchService.search(queryModel.getObject(), utilisateur);

            List<Station> stationsToDisplay = new ArrayList<>(searchResult.getStations());

            List<Station> finalStationsToDisplay = stationsToDisplay;
            searchResult.getLots().forEach(lot -> finalStationsToDisplay.add(lot.getStation()));
            searchResult.getSpecimens().forEach(specimen -> finalStationsToDisplay.add(specimen.getStation()));
            searchResult.getRemedes().forEach(remede -> finalStationsToDisplay.add(remede.getRemede().getStation()));
            searchResult.getPersonnesInterrogees().forEach(personne -> finalStationsToDisplay.add(personne.getStation()));

            stationsToDisplay = finalStationsToDisplay.stream().distinct().collect(Collectors.toList());

            add(new Label("SearchPage.values", new Model<>(generateJSONStringForMap(stationsToDisplay))));

            // Additional transformation for molecule provenances
            List<Molecule> molecules = searchResult.getMolecules();
            List<MoleculeProvenanceBean> moleculeProvenances = moleculeService.listMoleculeProvenances(molecules,
                    utilisateur);

            // specimens table
            DataTable<Specimen, String> specimensDataTable = ListSpecimensPage.initSpecimensDataTable(template,
                    "SearchPage.Specimens.Results", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(searchResult.getSpecimens(), this.getSession().getLocale()),
                    specimenService);
            add(specimensDataTable);

            // lot table
            DataTable<Lot, String> lotsDataTable = ListLotsPage.initLotsDataTable(template, "SearchPage.Lots.Results",
                    currentPage,
                    new LoadableDetachableSortableListDataProvider<>(searchResult.getLots(), this.getSession().getLocale()),
                    lotService);
            add(lotsDataTable);

            // extractions table
            DataTable<Extraction, String> extractionsDataTable = ListExtractionsPage.initExtractionsDataTable(template,
                    "SearchPage.Extractions.Results", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(searchResult.getExtractions(), this.getSession().getLocale()),
                    extractionService);
            add(extractionsDataTable);

            // purification table
            DataTable<Purification, String> purificationsDataTable = ListPurificationsPage.initPurificationsDataTable(template,
                    "SearchPage.Purifications.Results", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(searchResult.getPurifications(), this.getSession().getLocale()),
                    purificationService);
            add(purificationsDataTable);

            // test bio table
            DataTable<ResultatTestBio, String> testBiosDataTable = ListTestsBioPage.initTestsBioDataTable(template,
                    "SearchPage.ResultatTestBios.Results", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(searchResult.getResultatTestBios(), this.getSession().getLocale()),
                    testBioService);
            add(testBiosDataTable);

            // stations table
            DataTable<Station, String> stationsDataTable = ListStationsPage.initStationsDataTable(template,
                    "SearchPage.Stations.Results", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(searchResult.getStations(), this.getSession().getLocale()),
                    stationService);
            add(stationsDataTable);

            // molecule table
            DataTable<MoleculeProvenanceBean, String> moleculesDataTable = ListMoleculesPage.initMoleculesDataTable(template,
                    "SearchPage.Molecules.Results", currentPage,
                    new SimpleSortableListDataProvider<>(moleculeProvenances, getSession().getLocale()));
            add(moleculesDataTable);

            //Transform Dosages
            List<ResultatDosage> dosages = searchResult.getResultatDosages();
            List<ResultatDosageForUI> dosagesForUI = new ArrayList<>();
            Map<String, AnalyseDosage> analyses = new HashMap<>();
            List<String> composes = new ArrayList<>();

            dosages.forEach(res -> dosagesForUI.add(new ResultatDosageForUI(res)));

            dosages.forEach(res -> {
                ProduitOrLot echantillon;
                if (res.getLot() != null) {
                    echantillon = res.getLot();
                } else {
                    echantillon = res.getProduit();
                }
                String ref = res.getDosage().getRef() + "#" + echantillon.getRef() + "#" + res.getAnalyseNb();

                String compose;
                if (res.getMoleculeDosee() != null) {
                    compose = res.getMoleculeDosee().getNomCommun();
                } else {
                    compose = res.getComposeDose();
                }

                AnalyseDosage analyse = analyses.get(ref);
                if (analyse == null) {
                    analyse = new AnalyseDosage();
                    analyse.setAnalyseNb(res.getAnalyseNb());
                    analyse.setDosageRef(res.getDosage().getRef());
                    analyse.setReference(echantillon);
                    analyse.setDosage(res.getDosage());
                }
                analyse.addResultat(res);

                if (!composes.contains(compose) && compose != null) {
                    composes.add(compose);
                }

                analyses.put(ref, analyse);
            });

            composes.sort(Comparator.naturalOrder());

            // dosages table
            DataTable<ResultatDosageForUI, String> dosagesDataTable = ListDosagePage.initDosagesDataTable(template,
                    "SearchPage.ResultatDosages.Results", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(dosagesForUI, this.getSession().getLocale()),
                    dosageService);
            dosagesDataTable.setOutputMarkupId(true); // pour l'update Ajax
            dosagesDataTable.setOutputMarkupPlaceholderTag(true);
            add(dosagesDataTable);

            // analyses table
            DataTable<AnalyseDosage, String> analysesDataTable = ListDosagePage.initAnalyseDosagesDataTable(template,
                    "SearchPage.ResultatDosages.Transpose", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(new ArrayList<>(analyses.values()), this.getSession().getLocale()),
                    dosageService,
                    composes);
            dosagesDataTable.setOutputMarkupId(true); // pour l'update Ajax
            dosagesDataTable.setOutputMarkupPlaceholderTag(true);
            add(analysesDataTable);

            Button transposeButton = new Button("SearchPage.Transpose") {
                public void onSubmit() {
                }
            };
            add(transposeButton);

            // personne interrogee table
            DataTable<PersonneInterrogee, String> personnesInterrogeesDataTable = ListPersonnesInterrogeesPage.initPersonneInterrogeeDataTable(template,
                    "SearchPage.PersonnesInterrogees.Results", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(searchResult.getPersonnesInterrogees(), getSession().getLocale()), personneInterrogeeService);
            add(personnesInterrogeesDataTable);

            // remedes table
            DataTable<IngredientRemede, String> ingredientsRemedesDataTable = ListRemedePage.initRemedeSearchDataTable(template,
                    "SearchPage.Remedes.Results", currentPage,
                    new LoadableDetachableSortableListDataProvider<>(searchResult.getRemedes(), getSession().getLocale()),
                    remedeService);
            ingredientsRemedesDataTable.setOutputMarkupId(true); // pour l'update Ajax
            ingredientsRemedesDataTable.setOutputMarkupPlaceholderTag(true);
            add(ingredientsRemedesDataTable);

            //remedes sorted by citation



            DataTable<IngredientRemede, String> citationRemedesDataTable = ListRemedePage.initRemedeSearchDataTable(template,
                    "SearchPage.Remedes.CitationResults", currentPage,
                    new SortableRemedeDataProvider(searchResult.getRemedes(), this.getSession().getLocale(), remedeService),
                    remedeService);
            citationRemedesDataTable.setOutputMarkupId(true); // pour l'update Ajax
            citationRemedesDataTable.setOutputMarkupPlaceholderTag(true);
            add(citationRemedesDataTable);

            Button citationButton = new Button("ListRemedePage.Citation") {};
            add(citationButton);

            Button alphanumericalButton = new Button("ListRemedePage.Alphanumerique") {};
            add(alphanumericalButton);

            add(new RemedeListBehavior());

        }
    }

    private String generateJSONStringForMap(List<Station> stations) {
        StringBuilder sb =new StringBuilder();
        sb.append("[");
        for (Station station:stations) {
            if (station != null && station.getLatitude() != null && station.getLongitude() != null) {
                sb.append("{");
                sb.append("\"nom\":\"").append(station.getNom()).append("\",");
                sb.append("\"latitude\":\"").append(station.getLatitude().trim()).append("\",");
                sb.append("\"longitude\":\"").append(station.getLongitude().trim()).append("\"");
                sb.append("},");
            }
        }
        if (sb.lastIndexOf(",") != -1) {
            sb.deleteCharAt(sb.lastIndexOf(","));
        }
        sb.append("]");

        return sb.toString();
    }

    private class DocumentSearchPanel extends Panel {

        public DocumentSearchPanel(String id, SearchBean queryBean, CallerPage currentPage, Utilisateur utilisateur, TemplatePage template) {
            super(id);

            // ca sert a rien, mais wicket est pas content
            ExternalLink link = new ExternalLink("advancedLink",
                    "https://lucene.apache.org/core/8_10_1/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#package.description");
            add(link);

            // init query model
            queryModel = Model.of(queryBean == null ? new SearchBean() : queryBean);

            // search form
            Form<ValueMap> searchForm = new Form<>(getResource() + ".Document.Form") {
                protected void onSubmit() {
                    queryBean.setSearchTab(1);
                    setResponsePage(new SearchPage(queryModel.getObject()));
                }
            };
            searchForm.add(new TextField<>("SearchPage.Document.Query", new PropertyModel<String>(queryModel, "query")));

            List<TypeDocument> typesDocument = documentService.listTypeDocuments();

            searchForm.add(new DropDownChoice<>("SearchPage.DocumentType",
                    new PropertyModel<>(queryModel, "DocumentType"), typesDocument));

            searchForm.add(new TextField<>("SearchPage.NElatitude", new PropertyModel<String>(queryModel, "NElatitude")));
            searchForm.add(new TextField<>("SearchPage.NElongitude", new PropertyModel<String>(queryModel, "NElongitude")));
            searchForm.add(new TextField<>("SearchPage.SOlatitude", new PropertyModel<String>(queryModel, "SOlatitude")));
            searchForm.add(new TextField<>("SearchPage.SOlongitude", new PropertyModel<String>(queryModel, "SOlongitude")));

            Button button1 = new Button("SearchPage.Clear") {
                public void onSubmit() {
                    queryModel = Model.of(new SearchBean());
                }
            };
            searchForm.add(button1);

            add(searchForm);


           DocumentSearchResult searchResult = searchService.searchDocs(queryModel.getObject(), utilisateur);

            // Campaigns
            DataTable<Document, String> campagnesDataTable = ListCampagnesPage.initCampagnesDocumentDataTable(template,
                    "SearchPage.Documents.Campagnes.Results", currentPage, searchResult, documentService);
            add(campagnesDataTable);

            // Stations
            DataTable<Document, String> stationsDataTable = ListStationsPage.initStationsDocumentDataTable(template,
                    "SearchPage.Documents.Stations.Results", currentPage, searchResult, documentService);
            add(stationsDataTable);

            // Specimens
            DataTable<Document, String> specimensDataTable = ListSpecimensPage.initSpecimensDocumentDataTable(template,
                    "SearchPage.Documents.Specimens.Results", currentPage, searchResult, documentService);
            add(specimensDataTable);

            //Lots
            DataTable<Document, String> lotsDataTable = ListLotsPage.initLotsDocumentDataTable(template,
                    "SearchPage.Documents.Lots.Results", currentPage, searchResult, documentService);
            add(lotsDataTable);

            // Extractions
            DataTable<Document, String> extractionsDataTable = ListExtractionsPage.initExtractionsDocumentDataTable(template,
                    "SearchPage.Documents.Extractions.Results", currentPage, searchResult, documentService);
            add(extractionsDataTable);

            // Purifications
            DataTable<Document, String> purificationsDataTable = ListPurificationsPage.initPurificationsDocumentDataTable(template,
                    "SearchPage.Documents.Purifications.Results", currentPage, searchResult, documentService);
            add(purificationsDataTable);

            // Tests biologiques
            DataTable<Document, String> testsBioDataTable = ListTestsBioPage.initTestsBioDocumentDataTable(template,
                    "SearchPage.Documents.ResultatTestBios.Results", currentPage, searchResult, documentService);
            add(testsBioDataTable);

            // Molécules
            DataTable<Document, String> moleculesDataTable = ListMoleculesPage.initMoleculesDocumentDataTable(template,
                    "SearchPage.Documents.Molecules.Results", currentPage, searchResult, documentService);
            add(moleculesDataTable);

            // Dosages
            DataTable<Document, String> dosagesDataTable = ListDosagePage.initDosagesDocumentDataTable(template,
                    "SearchPage.Documents.Dosages.Results", currentPage, searchResult, documentService);
            add(dosagesDataTable);

            // Dosages
            DataTable<Document, String> personnesInterrogeesDataTable = ListPersonnesInterrogeesPage.initPersonnesInterrogeesDocumentDataTable(template,
                    "SearchPage.Documents.PersonnesInterrogees.Results", currentPage, searchResult, documentService);
            add(personnesInterrogeesDataTable);

            //remedes
            DataTable<Document, String> remedesDataTable = ListRemedePage.initRemedeDocumentDataTable(template,
                    "SearchPage.Documents.Remedes.Results", currentPage, searchResult, documentService);
            add(remedesDataTable);

        }
    }
}
