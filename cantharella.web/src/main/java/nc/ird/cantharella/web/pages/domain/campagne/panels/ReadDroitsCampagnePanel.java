/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.campagne.panels;

import edu.emory.mathcs.backport.java.util.Collections;
import nc.ird.cantharella.data.model.*;
import nc.ird.cantharella.data.model.comparators.CampagnePersonneDroitsComp;
import nc.ird.cantharella.data.model.comparators.PersonneDroitsComp;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.lot.ReadLotPage;
import nc.ird.cantharella.web.pages.domain.personne.ReadPersonnePage;
import nc.ird.cantharella.web.pages.domain.remede.ReadRemedePage;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.panels.PropertyLabelLinkPanel;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Panneau de consultation des droits des lots d'une campagne
 * 
 */
public final class ReadDroitsCampagnePanel extends Panel {

    /**
     * Constructeur
     * 
     * @param id Id du composant
     * @param campagneModel Modèle de la campagne concernée
     */
    public ReadDroitsCampagnePanel(String id, final IModel<? extends Campagne> campagneModel) {
        super(id, campagneModel);

        // Gestion des utilisateurs et lots autorisés
        final MarkupContainer autorisationsContainer = new WebMarkupContainer("Authorizations.Table") {
            @Override
            public boolean isVisible() {
                return !campagneModel.getObject().getLots().isEmpty() || !campagneModel.getObject().getPersonnesDroits().isEmpty();
            }
        };
        autorisationsContainer.setOutputMarkupId(true);


        // Récupération des droits des personnes sur la campagne
        final LoadableDetachableModel<List<CampagnePersonneDroits>> personnesCampagneDroitsModel = new LoadableDetachableModel<List<CampagnePersonneDroits>>() {
            @Override
            protected List<CampagnePersonneDroits> load() {
                List<CampagnePersonneDroits> listPersonnesDroits = campagneModel.getObject().getPersonnesDroits();

                // tri pour affichage
                Collections.sort(listPersonnesDroits, new CampagnePersonneDroitsComp());
                return listPersonnesDroits;
            }
        };

        autorisationsContainer.add(new ListView<CampagnePersonneDroits>("Authorizations.Campagnes.List", personnesCampagneDroitsModel) {
            @Override
            protected void populateItem(ListItem<CampagnePersonneDroits> item) {
                if (item.getIndex() % 2 == 1) {
                    item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));
                }

                item.add(new PropertyLabelLinkPanel<Personne>(
                        "Authorizations.Campagnes.Personne.nom",
                        new PropertyModel<Personne>(item.getModel(), "id.pk2"),
                        new StringResourceModel("Read", this, null)) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ReadPersonnePage(getModelObject().getIdPersonne(), new CallerPage(
                                (TemplatePage) getPage())));
                    }
                });
            }
        });

        // Récupération des droits des personnes sur les lots de la campagne
        final LoadableDetachableModel<List<PersonneDroits>> personnesDroitsModel = new LoadableDetachableModel<>() {
            @Override
            protected List<PersonneDroits> load() {
                List<PersonneDroits> listPersonnesDroits = campagneModel.getObject().getLots()
                        .stream()
                        .map(lot -> lot.getPersonnesDroits())
                        .collect(Collectors.toList())
                        .stream().flatMap(Collection::stream).collect(Collectors.toList());

                List<PersonneDroits> listRemedePersonnesDroits = campagneModel.getObject().getRemedes()
                        .stream()
                        .map(remede -> remede.getPersonnesDroits())
                        .collect(Collectors.toList())
                        .stream().flatMap(Collection::stream).collect(Collectors.toList());

                listPersonnesDroits.addAll(listRemedePersonnesDroits);

                // tri pour affichage
                Collections.sort(listPersonnesDroits, new PersonneDroitsComp());
                return listPersonnesDroits;
            }
        };

        autorisationsContainer.add(new ListView<>("Authorizations.Lots.List", personnesDroitsModel) {
            @Override
            protected void populateItem(ListItem<PersonneDroits> item) {
                if (item.getIndex() % 2 == 1) {
                    item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));
                }

                item.add(new PropertyLabelLinkPanel<Personne>(
                        "Authorizations.Lots.Personne.nom",
                        new PropertyModel<>(item.getModel(), "id.pk2"),
                        new StringResourceModel("Read", this, null)) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ReadPersonnePage(getModelObject().getIdPersonne(), new CallerPage(
                                (TemplatePage) getPage())));
                    }
                });

                item.add(new PropertyLabelLinkPanel<>(
                        "Authorizations.Lots.Lot.ref",
                        new PropertyModel<>(item.getModel(), "id.pk1"),
                        new StringResourceModel("Read", this, null)) {
                    @Override
                    public void onClick() {
                        if (getModelObject() instanceof Lot) {
                            setResponsePage(new ReadLotPage(((Lot)getModelObject()).getIdLot(), new CallerPage(
                                    (TemplatePage) getPage())));
                        }
                        if (getModelObject() instanceof Remede) {
                            setResponsePage(new ReadRemedePage(((Remede)getModelObject()).getIdRemede(), new CallerPage(
                                    (TemplatePage) getPage())));
                        }
                    }
                });
            }
        });

        add(autorisationsContainer);

        // Selon la non existence d'elements dans la table on affiche le span pour remplacer
        MarkupContainer emptyLotsContainer = new WebMarkupContainer("Authorizations.emptyTable") {
            @Override
            public boolean isVisible() {
                return !autorisationsContainer.isVisible();
            }
        };
        add(emptyLotsContainer);
    }
}
