package nc.ird.cantharella.web.utils.columns;

import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.web.utils.behaviors.SelectAllBehavior;
import nc.ird.cantharella.web.utils.panels.CheckBoxPanel;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.IExportableColumn;
import org.apache.wicket.extensions.model.AbstractCheckBoxModel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class CheckBoxColumn<S> extends PropertyColumn<Document, S> implements IExportableColumn<Document, S> {

    public CheckBoxColumn(final IModel<String> displayModel){
        super(displayModel, "selected");
    }

    @Override
    public Component getHeader(final String componentId) {
        CheckBoxPanel panel = new CheckBoxPanel(componentId, new Model<>());

        panel.add(new AttributeAppender("onclick", new Model<>("selectAll(this);"), ";"));
        panel.add(new SelectAllBehavior());
        panel.setOutputMarkupId(true);
        return panel;
    }


    @Override
    public void populateItem(Item<ICellPopulator<Document>> cellItem, String componentId, IModel<Document> rowModel) {
        cellItem.add(new CheckBoxPanel(componentId, new AbstractCheckBoxModel() {

            final Document doc = rowModel.getObject();

            @Override
            public boolean isSelected() {
                return doc.getSelected() != null ? doc.getSelected() : false;
            }

            @Override
            public void select() {
                doc.setSelected(true);

            }

            @Override
            public void unselect() {
                doc.setSelected(false);
            }
        }));
    }
}
