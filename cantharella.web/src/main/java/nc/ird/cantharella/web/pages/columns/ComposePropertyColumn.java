package nc.ird.cantharella.web.pages.columns;

import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.utils.BeanTools;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.IExportableColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ComposePropertyColumn<T, S> extends PropertyColumn<T, S> implements IExportableColumn<T, S> {

    List<String> composes;

    int index;

    /**
     * Constructor
     *
     * @param displayModel Display model
     * @param sortProperty Sort property
     * @param propertyExpression Property expression
     */
    public ComposePropertyColumn(IModel<String> displayModel, S sortProperty, String propertyExpression, List<String> composes, int initColumn) {
        super(displayModel, sortProperty, propertyExpression);
        this.composes = composes;
        this.index = initColumn;
    }

    /**
     * Constructor
     *
     * @param displayModel Display model
     * @param propertyExpression Property expression
     */
    public ComposePropertyColumn(IModel<String> displayModel, String propertyExpression, List<String> composes, int initColumn) {
        super(displayModel, propertyExpression);
        this.composes = composes;
        this.index = initColumn;
    }

    /** {@inheritDoc} */
    @Override
    public void populateItem(Item<ICellPopulator<T>> item, String componentId, IModel<T> rowModel) {

        String compose = composes.get(index);

        if (rowModel.getObject() != null) {
            // title différent suivant si le produit provient d'une extraction ou d'une purification
            List<ResultatDosage> resultats = (List<ResultatDosage>) BeanTools.getValue(rowModel.getObject(), BeanTools.AccessType.GETTER, "resultats");
            AtomicReference<String> labelString = new AtomicReference<>("");
            if (resultats != null) {
                resultats.forEach(res -> {
                    if (res.getValeur() != null) {
                        if (res.getComposeDose() != null && res.getComposeDose().equals(compose)) {
                            labelString.set(res.getValeur().toPlainString());
                        } else if (res.getMoleculeDosee() != null && res.getMoleculeDosee().getNomCommun() != null && res.getMoleculeDosee().getNomCommun().equals(compose)) {
                            labelString.set(res.getValeur().toPlainString());
                        }
                    }
                });
            }

            Label label = new Label(componentId, labelString);
            item.add(label);
        }
    }

    /** {@inheritDoc} */
    @Override
    public IModel<Serializable> getDataModel(final IModel<T> rowModel) {

        return new Model<>(rowModel) {
            /** {@inheritDoc} */
            @Override
            public String getObject() {

                String compose = composes.get(index);

                if (rowModel.getObject() != null) {
                    // title différent suivant si le produit provient d'une extraction ou d'une purification
                    List<ResultatDosage> resultats = (List<ResultatDosage>) BeanTools.getValue(rowModel.getObject(), BeanTools.AccessType.GETTER, "resultats");
                    AtomicReference<String> labelString = new AtomicReference<>("");
                    if (resultats != null) {
                        resultats.forEach(res -> {
                            if (res.getValeur() != null) {
                                if (res.getComposeDose() != null && res.getComposeDose().equals(compose)) {
                                    labelString.set(res.getValeur().toPlainString());
                                } else if (res.getMoleculeDosee() != null && res.getMoleculeDosee().getNomCommun() != null && res.getMoleculeDosee().getNomCommun().equals(compose)) {
                                    labelString.set(res.getValeur().toPlainString());
                                }
                            }
                        });
                    }

                    return labelString.get();
                } else return "";
            }
        };
    }
}
