/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.config.panels;

import nc.ird.cantharella.data.model.MethodeDosage;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.web.pages.domain.config.ManageMethodeDosagePage;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

/**
 * Panel qui liste les méthodes de dosage
 * 
 * @author Jean Couteau Viney
 */
public class ListMethodeDosagePanel extends Panel {

    /** Service : test */
    @SpringBean
    private DosageService dosageService;

    /**
     * Constructor
     *
     * @param id The panel ID
     */
    public ListMethodeDosagePanel(String id) {
        super(id);

        add(new BookmarkablePageLink<Void>("ListMethodeDosagePanel.NewMethodeDosage", ManageMethodeDosagePage.class));

        // On englobe le "DataView" dans un composant neutre que l'on pourra
        // rafraichir quand la liste évoluera
        final MarkupContainer methodesTestRefresh = new WebMarkupContainer(
                "ListMethodeDosagePanel.MethodesDosage.Refresh");
        methodesTestRefresh.setOutputMarkupId(true);
        add(methodesTestRefresh);

        // Liste des methodesDosage
        final List<MethodeDosage> methodesDosage = dosageService.listMethodesDosage();
        LoadableDetachableSortableListDataProvider<MethodeDosage> methodesDataProvider =
                new LoadableDetachableSortableListDataProvider<>(methodesDosage, getSession().getLocale());

        methodesTestRefresh.add(new DataView<>("ListMethodeDosagePanel.MethodesDosage",
                methodesDataProvider) {
            @Override
            protected void populateItem(Item<MethodeDosage> item) {
                item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));

                MethodeDosage methodeExtraction = item.getModelObject();

                // Colonnes
                item.add(new Label("ListMethodeDosagePanel.MethodesDosage.nom", new PropertyModel<String>(
                        methodeExtraction, "nom")));
                item.add(new Label("ListMethodeDosagePanel.MethodesDosage.acronyme", new PropertyModel<String>(
                        methodeExtraction, "acronyme")));
                item.add(new Label("ListMethodeDosagePanel.MethodesDosage.domaine", new PropertyModel<String>(
                        methodeExtraction, "domaine")));
                item.add(new MultiLineLabel("ListMethodeDosagePanel.MethodesDosage.description",
                        new PropertyModel<String>(methodeExtraction, "description")));
                item.add(new Label("ListMethodeDosagePanel.MethodesDosage.valeurMesuree", new PropertyModel<String>(
                        methodeExtraction, "valeurMesuree")));
                item.add(new Label("ListMethodeDosagePanel.MethodesDosage.unite", new PropertyModel<String>(
                        methodeExtraction, "unite")));
                item.add(new Label("ListMethodeDosagePanel.MethodesDosage.seuil",
                        new PropertyModel<String>(methodeExtraction, "seuil")));

                // Action : mise à jour (redirection vers le formulaire)
                Link<MethodeDosage> updateLink = new Link<>(
                        "ListMethodeDosagePanel.MethodesDosage.Update", new Model<>(methodeExtraction)) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ManageMethodeDosagePage(getModelObject().getIdMethodeDosage()));
                    }
                };
                item.add(updateLink);
            }
        });
    }

}
