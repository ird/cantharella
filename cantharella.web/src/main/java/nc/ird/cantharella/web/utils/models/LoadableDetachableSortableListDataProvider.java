/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.utils.models;

import java.util.*;

import nc.ird.cantharella.data.config.DataContext;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.ProduitOrLot;
import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.utils.AssertTools;
import nc.ird.cantharella.utils.BeanTools;
import nc.ird.cantharella.utils.BeanTools.AccessType;
import nc.ird.cantharella.utils.GenericsTools;

import nc.ird.cantharella.web.pages.domain.dosage.AnalyseDosage;
import org.apache.commons.collections.comparators.NullComparator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;

/**
 * Generic loadable/detachable/sortable data provider. Warning: do not use it in forms. TODO ajouter gestion des filtres
 * avec "implements IFilterStateLocator<FilterMapHomeMade>"
 * 
 * @author Mickael Tricot
 * @author Adrien Cheype
 * @param <M> Model object type
 */
public class LoadableDetachableSortableListDataProvider<M extends AbstractModel> extends
        SortableDataProvider<M, String> {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(LoadableDetachableSortableListDataProvider.class);

    /** Comparator */
    protected final Comparator<Object> comparator;

    /** Data list */
    // TODO penser à remettre "transient" à la liste
    protected List<M> list;

    /** Locale */
    protected final Locale locale;

    /** To recognize a special sort by codePays */
    public final static String CODE_PAYS_PROPERTY = "codePays";

    /** To recognize a special sort by codeLangue */
    public final static String CODE_LANGUE_PROPERTY = "codeLangue";

    /** To recognize a special sort on Dosage transpose table */
    public final static String VALEURFORCOMPOSE_PROPERTY = "valeurForCompose";

    /** To recognize a special sort by reference for ProduitOrLot */
    public final static String REFERENCE_PROPERTY = "reference";

    /** To recognize a special sort by collection size */
    public final static String SIZE_PROPERTY = "size";

    /** Current filter to select results */
    // private FilterMapHomeMade filter;
    /**
     * Constructor
     * 
     * @param list List
     * @param locale Locale
     */
    @SuppressWarnings("unchecked")
    public LoadableDetachableSortableListDataProvider(List<M> list, Locale locale) {
        AssertTools.assertNotNull(list);
        AssertTools.assertIn(locale, DataContext.LOCALES);
        this.list = list;
        this.locale = locale;
        comparator = new NullComparator(true);
    }

    /** {@inheritDoc} */
    @Override
    public Iterator<? extends M> iterator(long first, long count) {

        if (getSort() != null && !StringUtils.isEmpty(getSort().getProperty())) {
            list.sort((o1, o2) -> compareAlphabetically(o1, o2, getSort().getProperty(), getSort().isAscending()));
        }

        return list.subList((int) first, (int) Math.min(first + count, size())).iterator();
    }

    /** {@inheritDoc} */
    @Override
    public GenericLoadableDetachableModel<M> model(M object) {
        // return new Model<M>(object);
        return new GenericLoadableDetachableModel<M>(object);
    }

    /** {@inheritDoc} */
    @Override
    public long size() {
        return list.size();
    }

    /**
     * list getter
     * 
     * @return list
     */
    public List<M> getList() {
        return list;
    }

    /**
     * list setter
     * 
     * @param list list
     */
    public void setList(List<M> list) {
        this.list = list;
    }

    protected int compareAlphabetically(M o1, M o2, String propertySort, boolean isAscending) {
        try {
            Comparable<?> c1;
            Object c2;
            if (propertySort.endsWith(SIZE_PROPERTY)) {
                // get collection size
                String property = StringUtils.substring(propertySort, 0, propertySort.length() - 5);
                c1 = ((Collection)BeanTools.getValueFromPath(o1, AccessType.GETTER, property)).size();
                c2 = ((Collection)BeanTools.getValueFromPath(o2, AccessType.GETTER, property)).size();
            } else if (propertySort.startsWith(VALEURFORCOMPOSE_PROPERTY)) {
                String compose = propertySort.substring(VALEURFORCOMPOSE_PROPERTY.length()+1);
                c1 = ((AnalyseDosage)o1).getValeurForCompose(compose);
                c2 = ((AnalyseDosage)o2).getValeurForCompose(compose);
            }else {
                c1 = GenericsTools.cast(BeanTools.getValueFromPath(o1, AccessType.GETTER, propertySort));

                c2 = GenericsTools.cast(BeanTools.getValueFromPath(o2, AccessType.GETTER, propertySort));
            }

            // Exceptions
            // Countries are sorted by country name, not by country code
            if (propertySort.endsWith(CODE_PAYS_PROPERTY)) {
                if (c1 != null) {
                    c1 = WebContext.COUNTRIES.get(locale).get(c1);
                }
                if (c2 != null) {
                    c2 = WebContext.COUNTRIES.get(locale).get(c2);
                }
            }

            // Languages are sorted by language name, not by language code
            if (propertySort.endsWith(CODE_LANGUE_PROPERTY)) {
                if (c1 != null) {
                    c1 = WebContext.LANGUAGES.get(locale).get(c1);
                }
                if (c2 != null) {
                    c2 = WebContext.LANGUAGES.get(locale).get(c2);
                }
            }

            // ProduirorLot sorting
            if (propertySort.endsWith(REFERENCE_PROPERTY)) {
                if (c1 != null) {
                    c1 = ((ProduitOrLot)c1).getRef();
                }
                if (c2 != null) {
                    c2 = ((ProduitOrLot)c2).getRef();
                }
            }

            return (isAscending ? 1 : -1) * comparator.compare(c1, c2);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

}