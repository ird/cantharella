/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.document;

import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.Remede;
import nc.ird.cantharella.data.model.TypeDocument;
import nc.ird.cantharella.data.model.utils.DocumentAttachable;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.JSConfirmationBehavior;
import nc.ird.cantharella.web.utils.forms.SubmittableButton;
import nc.ird.cantharella.web.utils.forms.SubmittableButtonEvents;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Document management page (creation/edition).
 * 
 * @author Eric Chatellier
 */
public class ManageConsentementDocumentPage extends ManageDocumentPage {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ManageConsentementDocumentPage.class);

    /**
     * Constructeur (mode création)
     *
     * @param callerPage Page appelante
     * @param documentAttachable entity where document is attached to
     * @param multipleEntry Saisie multiple
     */
    public ManageConsentementDocumentPage(CallerPage callerPage, DocumentAttachable documentAttachable, boolean multipleEntry) {
        this(null, documentAttachable, callerPage, multipleEntry, false);
    }

    /**
     * Constructeur (mode édition)
     *
     * @param document document to edit
     * @param documentAttachable document sur le
     * @param callerPage Page appelante
     * @param updateWithService if {@code true} should update entity with service
     */
    public ManageConsentementDocumentPage(Document document, DocumentAttachable documentAttachable, CallerPage callerPage,
                                          boolean updateWithService) {
        this(document, documentAttachable, callerPage, false, updateWithService);
    }

    /**
     * Constructeur. Si idDocument et document sont null, on créée un nouveau Document. Si idDocument est renseigné, on
     * édite le document correspondant. Si document est renseigné, on créée un nouveau document à partir des
     * informations qu'il contient.
     *
     * @param document document
     * @param documentAttachable entity where document is attached to
     * @param callerPage Page appelante
     * @param multipleEntry Saisie multiple
     */
    protected ManageConsentementDocumentPage(Document document, final DocumentAttachable documentAttachable,
                                           final CallerPage callerPage, boolean multipleEntry, final boolean updateWithService) {
        super(document, documentAttachable, callerPage, multipleEntry, updateWithService);

        typeDocumentChoice.setEnabled(false);
        TypeDocument consentement = null;
        try {
            consentement = documentService.loadTypeDocument(TypeDocument.TYPE_CONSENTEMENT);
        } catch (DataNotFoundException e) {
            //should never happen
            LOG.error("Type de document 'Consentement' n'existe pas. Cela ne devrait pas se produire.", e);
        }
        typeDocumentChoice.setModelObject(consentement);

    }

    @Override
    protected void addCreateButton(Form<Void> formView, boolean createMode) {
        // Action : création du document
        Button createButton = new SubmittableButton(ACTION_CREATE, new SubmittableButtonEvents() {
            @Override
            public void onValidate() {
                Document document = documentModel.getObject();
                final FileUpload uploadedFile = fileUploadField.getFileUpload();
                if (uploadedFile != null) {
                    documentService.addDocumentContent(document, uploadedFile.getClientFileName(),
                            uploadedFile.getContentType(), uploadedFile.getBytes());

                    // if no error
                    validateModel();
                } else if (StringUtils.isEmpty(document.getFileName())) {
                    error(getString("ManageDocumentPage.Error.emptyFile"));
                } else {
                    validateModel();
                }
            }

            @Override
            public void onProcess() {
                // document can only be created from an attached entity
                Document document = documentModel.getObject();
                ((Remede)documentAttachable).setConsentement(document);
            }

            @Override
            public void onSuccess() {
                // document can only be created from an attached entity
                successNextPage(ACTION_CREATE_LATER);
                redirect();
            }
        });
        createButton.setVisibilityAllowed(createMode);
        formView.add(createButton);
    }

    @Override
    protected void addDeleteButton(Form<Void> formView, boolean updateWithService, boolean createMode) {
        Button deleteButton = new SubmittableButton(ACTION_DELETE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() {
                ((Remede)documentAttachable).setConsentementToDelete(((Remede)documentAttachable).getConsentement());
                ((Remede)documentAttachable).setConsentement(null);
            }

            @Override
            public void onSuccess() {
                if (updateWithService) {
                    successNextPage(ACTION_DELETE);
                    // first getPage() is read document
                    // it has been deleted so go to previous one
                    callerPage.responsePage((TemplatePage) getPage().getPage());
                } else {
                    successNextPage(ACTION_DELETE_LATER);
                    callerPage.responsePage((TemplatePage) getPage());
                }

            }
        });
        deleteButton.setVisibilityAllowed(!createMode);
        deleteButton.add(new JSConfirmationBehavior(getStringModel("Confirm")));
        deleteButton.setDefaultFormProcessing(false);
        formView.add(deleteButton);
    }
}
