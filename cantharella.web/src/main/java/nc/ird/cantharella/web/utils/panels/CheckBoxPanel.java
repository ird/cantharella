package nc.ird.cantharella.web.utils.panels;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class CheckBoxPanel  extends Panel {

    private AjaxCheckBox field;

    public CheckBoxPanel(String id, IModel<Boolean> model) {
        super(id);
        field = new AjaxCheckBox("checkBox", model) {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
            }
        };
        add(field);
    }

    public CheckBoxPanel(String id) {
        this(id, new Model<>());
    }

    public CheckBox getField() {
        return field;
    }
}