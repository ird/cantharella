package nc.ird.cantharella.web.pages.domain.remede;

import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SortableRemedeDataProvider extends LoadableDetachableSortableListDataProvider<IngredientRemede> {

    private static final Logger LOG = LoggerFactory.getLogger(SortableRemedeDataProvider.class);

    private final RemedeService remedeService;

    /**
     * Constructor
     *
     * @param list   List
     * @param locale Locale
     */
    public SortableRemedeDataProvider(List<IngredientRemede> list, Locale locale, RemedeService service) {
        super(list, locale);
        this.remedeService = service;
    }

	@Override
	public Iterator<IngredientRemede> iterator(long first, long count) {

        if (getSort() != null && !StringUtils.isEmpty(getSort().getProperty())) {

            list.sort((o1, o2) -> {
                try {
                    long c1 = 0;
                    long c2 = 0;

                    String property = getSort().getProperty();
                    boolean isAscending = getSort().isAscending();

                    if (property.equals("remede.prescripteur")) {
                        c1 = remedeService.countPrescripteurOccurence(o1);
                        c2 = remedeService.countPrescripteurOccurence(o2);
                    } else if (property.equals("remede.codeLangue")) {
                        c1 = remedeService.countCodeLangueOccurence(o1);
                        c2 = remedeService.countCodeLangueOccurence(o2);
                    } else if (property.equals("remede.date")) {
                        c1 = remedeService.countDateOccurence(o1);
                        c2 = remedeService.countDateOccurence(o2);
                    } else if (property.equals("remede.nom")) {
                        c1 = remedeService.countNomRemedeOccurence(o1);
                        c2 = remedeService.countNomRemedeOccurence(o2);
                    } else if (property.equals("remede.indicationVernaculaire")) {
                        c1 = remedeService.countIndicationsVernaculairesOccurence(o1);
                        c2 = remedeService.countIndicationsVernaculairesOccurence(o2);
                    } else if (property.equals("specimen.ref")) {
                        c1 = remedeService.countSpecimenRefOccurence(o1);
                        c2 = remedeService.countSpecimenRefOccurence(o2);
                    } else if (property.equals("specimen.famille")) {
                        c1 = remedeService.countSpecimenFamilleOccurence(o1);
                        c2 = remedeService.countSpecimenFamilleOccurence(o2);
                    } else if (property.equals("specimen.genre")) {
                        c1 = remedeService.countSpecimenGenreOccurence(o1);
                        c2 = remedeService.countSpecimenGenreOccurence(o2);
                    } else if (property.equals("specimen.espece")) {
                        c1 = remedeService.countSpecimenEspeceOccurence(o1);
                        c2 = remedeService.countSpecimenEspeceOccurence(o2);
                    } else if (property.equals("nomVernaculaire")) {
                        c1 = remedeService.countNomVernaculaireOccurence(o1);
                        c2 = remedeService.countNomVernaculaireOccurence(o2);
                    } else if (property.equals("remede.campagne")) {
                        c1 = remedeService.countCampagneOccurence(o1);
                        c2 = remedeService.countCampagneOccurence(o2);
                    } else if (property.equals("remede.campagne.codePays")) {
                        c1 = remedeService.countCodePaysOccurence(o1);
                        c2 = remedeService.countCodePaysOccurence(o2);
                    }

                    if (comparator.compare(c1, c2) == 0) {
                        //Si égal, tri alphabétique
                        return compareAlphabetically(o1, o2, property, isAscending);
                    } else {
                        return (isAscending ? 1 : -1) * comparator.compare(c1, c2);
                    }
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                    throw new UnexpectedException(e);
                }

            });
        }

        return list.subList((int) first, (int) Math.min(first + count, size())).iterator();
	}
}
