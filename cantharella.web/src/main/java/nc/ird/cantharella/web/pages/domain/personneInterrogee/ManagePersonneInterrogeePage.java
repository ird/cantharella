package nc.ird.cantharella.web.pages.domain.personneInterrogee;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.data.validation.utils.ModelValidator;
import nc.ird.cantharella.service.services.CampagneService;
import nc.ird.cantharella.service.services.PersonneInterrogeeService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.utils.CollectionTools;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.campagne.ManageCampagnePage;
import nc.ird.cantharella.web.pages.domain.document.panel.ManageListDocumentsPanel;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.JSConfirmationBehavior;
import nc.ird.cantharella.web.utils.forms.SubmittableButton;
import nc.ird.cantharella.web.utils.forms.SubmittableButtonEvents;
import nc.ird.cantharella.web.utils.panels.SimpleTooltipPanel;
import nc.ird.cantharella.web.utils.renderers.EnumChoiceRenderer;
import nc.ird.cantharella.web.utils.renderers.MapChoiceRenderer;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.form.AbstractSingleSelectChoice;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.StringValueConversionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public class ManagePersonneInterrogeePage extends TemplatePage {

    /** Action : create */
    private static final String ACTION_CREATE = "Create";

    /** Action : delete */
    private static final String ACTION_DELETE = "Delete";

    /** Action : update */
    private static final String ACTION_UPDATE = "Update";

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ManagePersonneInterrogeePage.class);

    /** Choix d'une station prospectée */
    private AbstractSingleSelectChoice<Station> availableStations;

    /** Page appelante */
    private final CallerPage callerPage;

    /** Modèle : PersonneInterrogee */
    private final IModel<PersonneInterrogee> personneInterrogeeModel;

    /** Service : PersonneInterrogee */
    @SpringBean
    private PersonneInterrogeeService PersonneInterrogeeService;

    /** Modèle : station */
    private final IModel<Station> stationModel;

    /** Liste des personnes existantes */
    private final List<Personne> personnes;

    /** Liste des utilisateurs existantes */
    private final List<Utilisateur> utilisateurs;

    /** Liste des stations existantes */
    private final List<Station> stations;

    /** Liste des stations existantes */
    private final List<Campagne> campagnes;

    /** Service : personnes */
    @SpringBean
    private PersonneService personneService;

    /** Service : station */
    @SpringBean
    private StationService stationService;

    /** Service : station */
    @SpringBean
    private CampagneService campagneService;

    /** Validateur modèle */
    @SpringBean(name = "webModelValidator")
    private ModelValidator validator;

    /** ComplementView */
    FormComponent<String> complementView;

    /** Saisie multiple */
    private boolean multipleEntry;

    /**
     * Constructeur (mode création)
     *
     * @param callerPage Page appelante
     * @param multipleEntry Saisie multiple
     */
    public ManagePersonneInterrogeePage(CallerPage callerPage, boolean multipleEntry) {
        this(null, null, callerPage, multipleEntry);
    }

    /**
     * Constructeur (mode édition)
     *
     * @param idPersonneInterrogee ID PersonneInterrogee
     * @param callerPage Page appelante
     */
    public ManagePersonneInterrogeePage(Integer idPersonneInterrogee, CallerPage callerPage) {
        this(idPersonneInterrogee, null, callerPage, false);
    }

    /**
     * Constructeur (mode saisie de la PersonneInterrogee suivante)
     *
     * @param PersonneInterrogee PersonneInterrogee
     * @param callerPage Page appelante
     */
    public ManagePersonneInterrogeePage(PersonneInterrogee PersonneInterrogee, CallerPage callerPage) {
        this(null, PersonneInterrogee, callerPage, true);
    }

    /**
     * Constructeur. Si idPersonneInterrogee et PersonneInterrogee sont null, on créée une nouvelle PersonneInterrogee. Si idPersonneInterrogee est renseigné,
     * on édite la PersonneInterrogee correspondante. Si PersonneInterrogee est renseignée, on créée une nouvelle PersonneInterrogee à partir des
     * informations qu'elle contient.
     *
     * @param idPersonneInterrogee ID PersonneInterrogee
     * @param PersonneInterrogee Lot
     * @param callerPage Page appelante
     * @param multipleEntry Saisie multiple
     */
    private ManagePersonneInterrogeePage(Integer idPersonneInterrogee, PersonneInterrogee PersonneInterrogee, final CallerPage callerPage, boolean multipleEntry) {
        super(ManagePersonneInterrogeePage.class);
        assert idPersonneInterrogee == null || PersonneInterrogee == null;
        this.callerPage = callerPage;
        final CallerPage currentPage = new CallerPage(this);
        this.multipleEntry = multipleEntry;

        // Initialisation des modèles
        try {
            personneInterrogeeModel = new Model<>(idPersonneInterrogee == null && PersonneInterrogee == null ? new PersonneInterrogee()
                    : PersonneInterrogee != null ? PersonneInterrogee : PersonneInterrogeeService.loadPersonneInterrogee(idPersonneInterrogee));
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
        boolean createMode = idPersonneInterrogee == null;
        if (createMode) {
            personneInterrogeeModel.getObject().setCreateur(getSession().getUtilisateur());
        }

        stationModel = new Model<>(new Station());

        // Initialisation des listes
        personnes = personneService.listPersonnes();
        utilisateurs = personneService.listUtilisateursValid();
        stations = stationService.listStations(getSession().getUtilisateur());
        campagnes = campagneService.listCampagnes(getSession().getUtilisateur());

        final Form<Void> formView = new Form<Void>("Form");

        DropDownChoice<Campagne> campagnesInput = new DropDownChoice<>("PersonneInterrogee.campagne",
                new PropertyModel<>(personneInterrogeeModel, "campagne"), campagnes);
        campagnesInput.setNullValid(false);
        formView.add(campagnesInput);

        // Action : création d'une nouvelle campagne
        // ajaxSubmitLink permet de sauvegarder l'état du formulaire
        formView.add(new AjaxSubmitLink("NewCampagne") {
            @Override
            protected void onSubmit(AjaxRequestTarget target) {
                setResponsePage(new ManageCampagnePage(currentPage, false));
            }

            // si erreur, le formulaire est également enregistré puis la redirection effectuée
            @Override
            protected void onError(AjaxRequestTarget target) {
                setResponsePage(new ManageCampagnePage(currentPage, false));
            }
        });

        final DropDownChoice<Station> stationsInput = new DropDownChoice<>("PersonneInterrogee.station",
                new PropertyModel<>(personneInterrogeeModel, "station"),
                personneInterrogeeModel.getObject().getCampagne() == null ?
                        new ArrayList<>() :
                        stationService.listStationsForCampagne(personneInterrogeeModel.getObject().getCampagne(), getSession().getUtilisateur()));
        stationsInput.setOutputMarkupId(true);
        stationsInput.setNullValid(false);
        stationsInput.setEnabled(personneInterrogeeModel.getObject().getCampagne() != null);
        formView.add(stationsInput);

        formView.add(new SimpleTooltipPanel("PersonneInterrogee.station.info", getStringModel("PersonneInterrogee.station.info")));

        campagnesInput.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                List<Station> stations = new ArrayList<>();
                if (personneInterrogeeModel.getObject().getCampagne() != null) {
                    campagneService.refreshCampagne(personneInterrogeeModel.getObject().getCampagne());
                    stations = stationService.listStationsForCampagne(personneInterrogeeModel.getObject().getCampagne(), getSession().getUtilisateur());
                }
                stationsInput.setChoices(stations);
                stationsInput.setEnabled(personneInterrogeeModel.getObject().getCampagne() != null);
                personneInterrogeeModel.getObject().setStation(null);
                // refresh the station choices component
                target.add(stationsInput);
            }
        });

        formView.add(new TextField<>("PersonneInterrogee.nom", new PropertyModel<>(personneInterrogeeModel, "nom")));
        formView.add(new TextField<>("PersonneInterrogee.prenom", new PropertyModel<>(personneInterrogeeModel, "prenom")));

        formView.add(new RadioChoice<>("PersonneInterrogee.genre", new PropertyModel<>(personneInterrogeeModel, "genre"),
                Arrays.asList(nc.ird.cantharella.data.model.PersonneInterrogee.Genre.values()), new EnumChoiceRenderer<>(this)));

        formView.add(new TextField<>("PersonneInterrogee.anneeNaissance", new PropertyModel<>(personneInterrogeeModel, "anneeNaissance")));
        formView.add(new TextField<>("PersonneInterrogee.experience", new PropertyModel<>(personneInterrogeeModel, "experience")));
        formView.add(new SimpleTooltipPanel("PersonneInterrogee.experience.info", getStringModel("PersonneInterrogee.experience.info")));
        formView.add(new TextField<>("PersonneInterrogee.profession", new PropertyModel<>(personneInterrogeeModel, "profession")));
        formView.add(new TextField<>("PersonneInterrogee.labellisation", new PropertyModel<>(personneInterrogeeModel, "labellisation")));
        formView.add(new TextField<>("PersonneInterrogee.telephone", new PropertyModel<>(personneInterrogeeModel, "telephone")));
        formView.add(new TextField<>("PersonneInterrogee.courriel", new PropertyModel<>(personneInterrogeeModel, "courriel")));
        formView.add(new TextField<>("PersonneInterrogee.adressePostale", new PropertyModel<>(personneInterrogeeModel, "adressePostale")));
        formView.add(new TextField<>("PersonneInterrogee.codePostal", new PropertyModel<>(personneInterrogeeModel, "codePostal")));
        formView.add(new TextField<>("PersonneInterrogee.ville", new PropertyModel<>(personneInterrogeeModel, "ville")));

        // Choix du code pays
        formView.add(
                new DropDownChoice<>(
                        "PersonneInterrogee.codePays",
                        new PropertyModel<>(personneInterrogeeModel,"codePays"),
                        WebContext.COUNTRY_CODES.get(getSession().getLocale()),
                        new MapChoiceRenderer<>(WebContext.COUNTRIES.get(getSession().getLocale()))));
        if (personneInterrogeeModel.getObject().getCodePays() == null) {
            personneInterrogeeModel.getObject().setCodePays(
                    WebContext.COUNTRIES.get(getSession().getLocale()).entrySet().iterator().next().getKey());
        }

        formView.add(new TextArea<>("PersonneInterrogee.complement", new PropertyModel<>(personneInterrogeeModel, "complement")));

        formView.add(new TextField<>("PersonneInterrogee.createur", new PropertyModel<>(personneInterrogeeModel, "createur"))
                .setEnabled(false));

        // add list document panel
        ManageListDocumentsPanel manageListDocumentsPanel = new ManageListDocumentsPanel("ManageListDocumentsPanel",
                personneInterrogeeModel, currentPage);
        manageListDocumentsPanel.setUpdateOrDeleteEnabled(createMode
                || PersonneInterrogeeService.updateOrdeletePersonneInterrogeeEnabled(personneInterrogeeModel.getObject(), getSession()
                .getUtilisateur()));
        formView.add(manageListDocumentsPanel);

        // Action : création de la PersonneInterrogee
        Button createButton = new SubmittableButton(ACTION_CREATE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                PersonneInterrogeeService.createPersonneInterrogee(personneInterrogeeModel.getObject());
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_CREATE);
                redirect();
            }

            @Override
            public void onValidate() {
                validateModel();
            }
        });
        createButton.setVisibilityAllowed(createMode);
        formView.add(createButton);

        // Action : mise à jour de la PersonneInterrogee
        Button updateButton = new SubmittableButton(ACTION_UPDATE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                PersonneInterrogeeService.updatePersonneInterrogee(personneInterrogeeModel.getObject());
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_UPDATE);
                redirect();
            }

            @Override
            public void onValidate() {
                validateModel();
            }
        });
        updateButton.setVisibilityAllowed(!createMode
                && PersonneInterrogeeService.updateOrdeletePersonneInterrogeeEnabled(personneInterrogeeModel.getObject(), getSession()
                .getUtilisateur()));
        formView.add(updateButton);

        // Action : suppression de la PersonneInterrogee
        Button deleteButton = new SubmittableButton(ACTION_DELETE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                PersonneInterrogeeService.deletePersonneInterrogee(personneInterrogeeModel.getObject());
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_DELETE);
                redirect();
            }
        });
        deleteButton.setVisibilityAllowed(!createMode
                && PersonneInterrogeeService.updateOrdeletePersonneInterrogeeEnabled(personneInterrogeeModel.getObject(), getSession()
                .getUtilisateur()));
        deleteButton.add(new JSConfirmationBehavior(getStringModel("Confirm")));
        deleteButton.setDefaultFormProcessing(false);
        formView.add(deleteButton);

        // Action : annulation (lien)
        formView.add(new Link<Void>("Cancel") {
            // Cas où le formulaire est annulé
            @Override
            public void onClick() {
                callerPage.responsePage((TemplatePage) this.getPage());
            }
        });

        add(formView);
    }

    /**
     * Redirection vers une autre page. Cas où le formulaire est validé
     */
    private void redirect() {
        if (multipleEntry) {
            // Redirection vers l'écran de saisie d'une nouvelle PersonneInterrogee, en fournissant déjà quelques données
            PersonneInterrogee nextPersonneInterrogee = new PersonneInterrogee();

            nextPersonneInterrogee.setCodePays(personneInterrogeeModel.getObject().getCodePays());
            setResponsePage(new ManagePersonneInterrogeePage(nextPersonneInterrogee, callerPage));
        } else if (callerPage != null) {
            // On passe la PersonneInterrogee associée à cette page, en paramètre de la prochaine page, pour lui permettre de
            // l'exploiter si besoin
            callerPage.addPageParameter(PersonneInterrogee.class.getSimpleName(), personneInterrogeeModel.getObject().getIdPersonne());
            callerPage.responsePage(this);
        }
    }



    /**
     * Validate the PersonneInterrogee model (for update & create)
     */
    private void validateModel() {
        if (personneInterrogeeModel.getObject().getCreateur() == null) {
            personneInterrogeeModel.getObject().setCreateur(getSession().getUtilisateur());
        }
        addValidationErrors(validator.validate(personneInterrogeeModel.getObject(), getSession().getLocale()));
    }



    /**
     * Refresh model
     */
    private void refreshModel() {

        // Récupère (et supprime) les éventuels nouveaux objets créés dans les paramètres de la page.
        String key = Campagne.class.getSimpleName();
        if (getPageParameters().getNamedKeys().contains(key)) {
            CollectionTools.setter(campagnes, campagneService.listCampagnes(getSession().getUtilisateur()));
            try {
                Campagne createdCampagne = campagneService.loadCampagne(getPageParameters().get(key).toInt());
                personneInterrogeeModel.getObject().setCampagne(createdCampagne);
            } catch (StringValueConversionException | DataNotFoundException e) {
                LOG.error(e.getMessage(), e);
                throw new UnexpectedException(e);
            }
            getPageParameters().remove(key);
        }

        key = Station.class.getSimpleName();
        if (getPageParameters().getNamedKeys().contains(key)) {
            CollectionTools.setter(stations, stationService.listStations(getSession().getUtilisateur()));
            try {
                Station createdStation = stationService.loadStation(getPageParameters().get(key).toInt());
                personneInterrogeeModel.getObject().setStation(createdStation);
            } catch (StringValueConversionException | DataNotFoundException e) {
                LOG.error(e.getMessage(), e);
                throw new UnexpectedException(e);
            }
            getPageParameters().remove(key);
        }
    }
}
