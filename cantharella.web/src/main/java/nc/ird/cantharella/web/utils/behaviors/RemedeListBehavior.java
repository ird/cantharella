package nc.ird.cantharella.web.utils.behaviors;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnLoadHeaderItem;

public class RemedeListBehavior extends Behavior {

        /**
     * Constructor with default canvas size.
     */
    public RemedeListBehavior() {
        super();
    }

    /** {@inheritDoc} */
    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        response.render(JavaScriptHeaderItem.forUrl("js/remede.js"));
        response.render(OnLoadHeaderItem.forScript("addCitationButton()"));
    }
}
