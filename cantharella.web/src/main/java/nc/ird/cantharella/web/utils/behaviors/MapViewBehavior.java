package nc.ird.cantharella.web.utils.behaviors;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnLoadHeaderItem;

public class MapViewBehavior extends Behavior {

        /**
     * Constructor with default canvas size.
     */
    public MapViewBehavior() {
        super();
    }

    /** {@inheritDoc} */
    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        response.render(JavaScriptHeaderItem.forUrl("leaflet/leaflet.js"));
        response.render(CssHeaderItem.forUrl("leaflet/leaflet.css"));
        response.render(JavaScriptHeaderItem.forUrl("leaflet/leaflet-providers.js"));
        response.render(JavaScriptHeaderItem.forUrl("leaflet/leaflet-image.js"));
        response.render(JavaScriptHeaderItem.forUrl("leaflet/tokml.js"));
        response.render(JavaScriptHeaderItem.forUrl("js/mapviewer.js"));
        response.render(OnLoadHeaderItem.forScript("addMap('smallMap')"));
    }

}
