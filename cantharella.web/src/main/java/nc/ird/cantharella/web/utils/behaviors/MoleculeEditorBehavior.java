/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.utils.behaviors;

import org.apache.wicket.Application;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnLoadHeaderItem;
import org.apache.wicket.model.IModel;

/**
 * Permet d'ajouter la formule dans un attribut formula. L'attribut est ensuite utilise en javascript pour ajouter la
 * visualisation de la formule
 * 
 * @author poussin
 * @version $Revision$
 * 
 *          Last update: $Date$ by : $Author$
 */
public class MoleculeEditorBehavior extends AttributeModifier {

    /**
     * Constructor.
     * 
     * @param replaceModel replace model
     */
    public MoleculeEditorBehavior(IModel<?> replaceModel) {
        super("formula", replaceModel);
    }

    /** {@inheritDoc} */
    @Override
    protected String newValue(String currentValue, String replacementValue) {
        // on ajoute toujours un premier caractere pour oblige l'existance de
        // l'attribut car sinon si formula est vide l'attribut n'est pas ajoute
        String result = ".";
        if (replacementValue != null) {
            result += replacementValue;
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        response.render(JavaScriptHeaderItem.forReference(Application.get().getJavaScriptLibrarySettings()
                .getJQueryReference()));
        response.render(CssHeaderItem.forUrl("ChemDoodleWeb/ChemDoodleWeb.css"));
        response.render(CssHeaderItem.forUrl("ChemDoodleWeb/uis/jquery-ui-1.11.4.css"));
        //response.render(JavaScriptHeaderItem.forUrl("ChemDoodleWeb/js/jquery-ui-1.9.2.custom.min.js"));
        //response.render(JavaScriptHeaderItem.forUrl("ChemDoodleWeb/js/ChemDoodleWeb-libs.js"));
        response.render(JavaScriptHeaderItem.forUrl("ChemDoodleWeb/ChemDoodleWeb.js"));
        response.render(JavaScriptHeaderItem.forUrl("ChemDoodleWeb/uis/ChemDoodleWeb-uis.js"));
        response.render(JavaScriptHeaderItem.forUrl("js/moleditor.js"));

        final String id = component.getMarkupId();
        response.render(OnLoadHeaderItem.forScript("addEditorMolecule('" + id + "');"));
    }

    /** {@inheritDoc} */
    @Override
    public void beforeRender(Component component) {
        // il faut que l'element HTML est forcement un identifiant pour pouvoir
        // travailler avec
        component.setOutputMarkupId(true);
    }
}
