package nc.ird.cantharella.web.pages.domain.dosage;

import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.Extrait;
import nc.ird.cantharella.data.model.Fraction;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.ProduitOrLot;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.utils.AbstractModel;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AnalyseDosage extends AbstractModel {

    @Id
    String id;
    Integer analyseNb;
    String dosageRef;
    ProduitOrLot reference;
    List<ResultatDosage> resultats = new ArrayList<>();

    Dosage dosage;

    protected void computeId() {
        setId(dosageRef + "#" + reference.getRef() + "#" + getAnalyseNb());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getAnalyseNb() {
        return analyseNb;
    }

    public void setAnalyseNb(Integer analyseNb) {
        this.analyseNb = analyseNb;
    }

    public String getDosageRef() {
        return dosageRef;
    }

    public void setDosageRef(String dosageRef) {
        this.dosageRef = dosageRef;
    }

    public ProduitOrLot getReference() {
        return reference;
    }

    public void setReference(ProduitOrLot reference) {
        this.reference = reference;
    }

    public List<ResultatDosage> getResultats() {
        return resultats;
    }

    public void setResultats(List<ResultatDosage> resultats) {
        this.resultats = resultats;
    }

    public void addResultat(ResultatDosage resultat) {
        this.resultats.add(resultat);
    }

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    public Lot getLotSource() {
        if (reference.isLot()) {
            return (Lot) reference;
        } else {
            if (reference == null) {
                return null;
            }
            if (((Produit) reference).isExtrait()) {
                Extrait extrait = (Extrait) reference;
                return extrait.getExtraction().getLot();
            }
            // cas où c'est une fraction
            Fraction fraction = (Fraction) reference;
            return fraction.getPurification().getLotSource();
        }
    }

    public BigDecimal getValeurForCompose(String compose) {
        for (ResultatDosage res:resultats) {
            if ((res != null) &&
                    (compose.equals(res.getComposeDose()) ||
                            (res.getMoleculeDosee() != null && compose.equals(res.getMoleculeDosee().getNomCommun()))
                    )) {
                return res.getValeur();
            }
        }
        return null;
    }
}
