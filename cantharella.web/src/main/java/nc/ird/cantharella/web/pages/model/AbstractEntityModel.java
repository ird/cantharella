package nc.ird.cantharella.web.pages.model;

import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Identifiable;
import org.apache.wicket.model.LoadableDetachableModel;

import java.io.Serializable;

public abstract class AbstractEntityModel<T extends Identifiable<?>> extends LoadableDetachableModel<T> {
    private Class clazz;
    private Serializable id;

    public AbstractEntityModel(T entity)
    {
        clazz = entity.getClass();
        id = entity.getId();
    }

    public AbstractEntityModel(Class< ? extends T> clazz, Serializable id)
    {
        this.clazz = clazz;
        this.id = id;
    }

    @Override
    protected T load() {
        try {
            return load(clazz, id);
        } catch (DataNotFoundException dnfe) {
            throw new UnexpectedException(dnfe);
        }
    }

    protected abstract T load(Class clazz, Serializable id) throws DataNotFoundException;
}