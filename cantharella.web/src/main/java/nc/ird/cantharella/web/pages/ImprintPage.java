/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages;

import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Configuration;
import nc.ird.cantharella.service.services.ConfigurationService;
import nc.ird.cantharella.web.utils.behaviors.ReplaceEmptyLabelBehavior;
import nc.ird.cantharella.web.utils.models.GenericLoadableDetachableModel;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 * Imprint page.
 * 
 * @author Chatellier Eric
 */
public class ImprintPage extends TemplatePage {

    /** Modèle : campagne */
    private final IModel<Configuration> configurationModel;

    /** Service : station */
    @SpringBean
    private ConfigurationService configurationService;

    /**
     * Constructeur
     */
    public ImprintPage() {
        super(ImprintPage.class);

        // Initialisation des modèles
        configurationModel = new GenericLoadableDetachableModel<>(Configuration.class, 1);

        add(new MultiLineLabel("Configuration.droitAcces", new PropertyModel<String>(configurationModel, "droitAcces"))
                .add(new ReplaceEmptyLabelBehavior()));

        add(new MultiLineLabel("Configuration.editeur", new PropertyModel<String>(configurationModel, "editeur"))
                .add(new ReplaceEmptyLabelBehavior()));

        add(new MultiLineLabel("Configuration.hebergement", new PropertyModel<String>(configurationModel, "hebergement"))
                .add(new ReplaceEmptyLabelBehavior()));

        add(new MultiLineLabel("Configuration.droitsReproduction", new PropertyModel<String>(configurationModel, "droitsReproduction"))
                .add(new ReplaceEmptyLabelBehavior()));
    }
}
