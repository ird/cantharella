package nc.ird.cantharella.web.pages.domain.dosage;

import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.ProduitOrLot;
import nc.ird.cantharella.data.model.ResultatDosage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResultatDosageForUI extends ResultatDosage {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ResultatDosageForUI.class);

    ProduitOrLot reference;

    ResultatDosageForUI() {
        super();
    }

    public ResultatDosageForUI(ResultatDosage source) {
        super();
        this.setId(source.getId());
        this.setAnalyseNb(source.getAnalyseNb());
        this.setLot(source.getLot());
        this.setProduit(source.getProduit());
        this.setConcMasse(source.getConcMasse());
        this.setUniteConcMasse(source.getUniteConcMasse());
        this.setComposeDose(source.getComposeDose());
        this.setMoleculeDosee(source.getMoleculeDosee());
        this.setValeur(source.getValeur());
        this.setSD(source.getSD());
        this.setSupSeuil(source.getSupSeuil());
        this.setErreur(source.getErreur());
        this.setDosage(source.getDosage());
        if (source.getLot() != null) {
            this.setReference(source.getLot());
        }
        if (source.getProduit() != null) {
            this.setReference(source.getProduit());
        }
    }

    ResultatDosage toResultatDosage() {

        ResultatDosage clone = new ResultatDosage();

        clone.setId(getId());
        clone.setAnalyseNb(getAnalyseNb());
        clone.setLot(getLot());
        clone.setProduit(getProduit());
        clone.setConcMasse(getConcMasse());
        clone.setUniteConcMasse(getUniteConcMasse());
        clone.setComposeDose(getComposeDose());
        clone.setMoleculeDosee(getMoleculeDosee());
        clone.setValeur(getValeur());
        clone.setSD(getSD());
        clone.setSupSeuil(getSupSeuil());
        clone.setErreur(getErreur());
        clone.setDosage(getDosage());

        if (reference instanceof Produit) {
            clone.setProduit((Produit) reference);
            clone.setLot(null);
        } else if (reference instanceof Lot) {
            clone.setProduit(null);
            clone.setLot((Lot) reference);
        }

        return clone;
    }

    public ProduitOrLot getReference() {
        return reference;
    }

    public void setReference(ProduitOrLot reference) {
        this.reference = reference;
    }
}
