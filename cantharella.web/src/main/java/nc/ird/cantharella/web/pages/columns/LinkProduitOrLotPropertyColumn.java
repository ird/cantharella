/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.columns;

import nc.ird.cantharella.data.model.Extrait;
import nc.ird.cantharella.data.model.Fraction;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.ProduitOrLot;
import nc.ird.cantharella.utils.BeanTools;
import nc.ird.cantharella.utils.BeanTools.AccessType;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.utils.columns.LinkPropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;

import java.util.HashMap;
import java.util.Map;

/**
 * LinkPropertyColumn used to link toward an Extraction a Purification or a Lot depending on the produit type (extrait or
 * fraction). T MUST HAVE a property named "produit" which give a Produit.
 * 
 * @author Adrien Cheype
 * @param <T> Row type
 * @param <S> the type of the sort property
 */
public abstract class LinkProduitOrLotPropertyColumn<T, S> extends LinkPropertyColumn<T, S> {

    /** page used to get messages */
    private final TemplatePage page;

    final Map<Integer, T> datas = new HashMap<>();

    /**
     * Constructor
     *
     * @param displayModel displayModel
     * @param sortProperty sortProperty
     * @param propertyExpression propertyExpression
     * @param page page used to get messages
     */
    public LinkProduitOrLotPropertyColumn(IModel<String> displayModel, S sortProperty, String propertyExpression,
                                          TemplatePage page) {
        super(displayModel, sortProperty, propertyExpression);
        this.page = page;}

    /** {@inheritDoc} */
    @Override
    public void onClick(Item<ICellPopulator<T>> item, String componentId, IModel<T> model) {
        Item rowItem = item.findParent( Item.class );
        int rowIndex = rowItem.getIndex();
        T object = datas.get(rowIndex);
        if (object != null) {
            ProduitOrLot produitOrLot = (ProduitOrLot) BeanTools.getValue(object, AccessType.GETTER, "reference");

            if (produitOrLot.isProduit()) {
                if (((Produit)produitOrLot).isExtrait()) {
                    Extrait extrait = (Extrait) produitOrLot;
                    onClickIfExtrait(extrait);
                } else {
                    // le produit est une fraction
                    Fraction fraction = (Fraction) produitOrLot;
                    onClickIfFraction(fraction);
                }
            } else  if (produitOrLot.isLot()) {
                    Lot lot = (Lot) produitOrLot;
                    onClickIfLot(lot);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void populateItem(Item<ICellPopulator<T>> item, String componentId, IModel<T> model) {
        //init dosageId for onClick
        Item rowItem = item.findParent( Item.class );
        int rowIndex = rowItem.getIndex();
        datas.put(rowIndex, model.getObject());
        if (model.getObject() != null) {
            // title différent suivant si le produit provient d'une extraction ou d'une purification
            ProduitOrLot produitOrLot = (ProduitOrLot) BeanTools.getValue(model.getObject(), AccessType.GETTER, "reference");
            if (produitOrLot != null) {
                if (produitOrLot.isProduit()) {
                    if (((Produit) produitOrLot).isExtrait()) {
                        linkTitle = page.getStringModel("ReadExtraction");
                    } else {
                        linkTitle = page.getStringModel("ReadPurification");
                    }
                } else if (produitOrLot.isLot()) {
                    linkTitle = page.getStringModel("ReadLot");
                }
            }
        }
        super.populateItem(item, componentId, model);
    }

    /**
     * Executed on click evenement when the produis is an extrait
     * 
     * @param extrait extrait
     */
    public abstract void onClickIfExtrait(Extrait extrait);

    /**
     * Executed on click evenement when the produis is a fraction
     * 
     * @param fraction fraction
     */
    public abstract void onClickIfFraction(Fraction fraction);

    /**
     * Executed on click evenement when the produis is a fraction
     *
     * @param lot lot
     */
    public abstract void onClickIfLot(Lot lot);

}
