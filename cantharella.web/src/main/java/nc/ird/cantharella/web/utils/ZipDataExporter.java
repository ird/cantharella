package nc.ird.cantharella.web.utils;

import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.service.services.DocumentService;
import nc.ird.cantharella.service.services.impl.SearchServiceImpl;
import nc.ird.cantharella.web.utils.columns.CheckBoxColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.AbstractDataExporter;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.IExportableColumn;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

public class ZipDataExporter extends AbstractDataExporter {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ZipDataExporter.class);

    protected DocumentService documentService;

    public ZipDataExporter(DocumentService documentService){
		super(Model.of("Zip"), "application/zip", "zip");
        this.documentService = documentService;
	}

    @Override
    public <T> void exportData(IDataProvider<T> dataProvider, List<IExportableColumn<T, ?>> columns, OutputStream outputStream) throws IOException {

        ZipOutputStream out = new ZipOutputStream(outputStream);

        long numberOfRows = dataProvider.size();
		Iterator<? extends T> rowIterator = dataProvider.iterator(0, numberOfRows);
		while (rowIterator.hasNext()) {
            T row = rowIterator.next();

            for (IExportableColumn<T, ?> col : columns) {
                if (col instanceof CheckBoxColumn) {
                    Document doc = (Document)row;

                    if (doc.getSelected() != null ? doc.getSelected() : false) {
                        //Add entry to zip
                        try {
                            ZipEntry e = new ZipEntry(doc.getFileName());
                            out.putNextEntry(e);
                            out.write(documentService.getDocumentContent(doc).getFileContent(), 0, documentService.getDocumentContent(doc).getFileContent().length);
                            out.closeEntry();
                        } catch (ZipException ze) {
                            LOG.error("Could not add zip entry : " + ze.getMessage());
                        }
                    }
                }
            }
        }

        out.close();
    }
}
