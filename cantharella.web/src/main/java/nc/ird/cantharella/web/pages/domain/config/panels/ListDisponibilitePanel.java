package nc.ird.cantharella.web.pages.domain.config.panels;

import nc.ird.cantharella.data.model.Disponibilite;
import nc.ird.cantharella.data.model.ErreurDosage;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.web.pages.domain.config.ManageDisponibilitePage;
import nc.ird.cantharella.web.pages.domain.config.ManageErreurDosagePage;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

public class ListDisponibilitePanel extends Panel {

    /** Service : test */
    @SpringBean
    private RemedeService remedeService;

    /**
     * Constructor
     *
     * @param id The panel ID
     */
    public ListDisponibilitePanel(String id) {
        super(id);

        add(new BookmarkablePageLink<Void>("ListDisponibilitePanel.NewDisponibilite", ManageDisponibilitePage.class));

        // On englobe le "DataView" dans un composant neutre que l'on pourra
        // rafraichir quand la liste évoluera
        final MarkupContainer erreursDosageRefresh = new WebMarkupContainer(
                "ListDisponibilitePanel.Disponibilites.Refresh");
        erreursDosageRefresh.setOutputMarkupId(true);
        add(erreursDosageRefresh);

        // Liste des erreursTest
        final List<Disponibilite> erreursTest = remedeService.listDisponibilites();
        LoadableDetachableSortableListDataProvider<Disponibilite> erreursDataProvider = new LoadableDetachableSortableListDataProvider<>(
                erreursTest, getSession().getLocale());

        erreursDosageRefresh
                .add(new DataView<>("ListDisponibilitePanel.Disponibilites", erreursDataProvider) {
                    @Override
                    protected void populateItem(Item<Disponibilite> item) {
                        item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));

                        Disponibilite disponibilite = item.getModelObject();
                        // Colonnes
                        item.add(new Label("ListDisponibilitePanel.Disponibilites.nom", new PropertyModel<String>(
                                disponibilite, "nom")));

                        // Action : mise à jour (redirection vers le formulaire)
                        Link<Disponibilite> updateLink = new Link<>(
                                "ListDisponibilitePanel.Disponibilites.Update", new Model<>(disponibilite)) {
                            @Override
                            public void onClick() {
                                setResponsePage(new ManageDisponibilitePage(getModelObject().getIdDisponibilite()) );
                            }
                        };
                        item.add(updateLink);
                    }
                });
    }
}
