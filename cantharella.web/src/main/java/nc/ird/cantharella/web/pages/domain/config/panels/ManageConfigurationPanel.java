package nc.ird.cantharella.web.pages.domain.config.panels;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Configuration;
import nc.ird.cantharella.data.validation.utils.ModelValidator;
import nc.ird.cantharella.service.exceptions.InvalidFileExtensionException;
import nc.ird.cantharella.service.services.ConfigurationService;
import nc.ird.cantharella.web.config.WebApplicationImpl;
import nc.ird.cantharella.web.pages.domain.config.ListConfigurationPage;
import nc.ird.cantharella.web.pages.domain.config.ManageErreurDosagePage;
import nc.ird.cantharella.web.utils.forms.SubmittableButton;
import nc.ird.cantharella.web.utils.forms.SubmittableButtonEvents;
import nc.ird.cantharella.web.utils.panels.SimpleTooltipPanel;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class ManageConfigurationPanel extends Panel {

    /** Action : update */
    private static final String ACTION_UPDATE = "Update";

    /** The return page parameter (key and value) */
    final private String[] RETURN_PARAM = { "configuration", "closed" };

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ManageErreurDosagePage.class);

    /** erreurTest Model */
    private final IModel<Configuration> configurationModel;

    /** Model validateur */
    @SpringBean(name = "webModelValidator")
    private ModelValidator validator;

    /** Service : test (for erreurs) */
    @SpringBean
    private ConfigurationService configurationService;

    protected Boolean deleteLogo = false;

    /**
     * Constructor. If idErreurDosage is null, creating a new error, else editing the corresponding error
     */
    public ManageConfigurationPanel(String id) {
        super(id);

        // model initialization
        try {
            configurationModel = new Model<>(configurationService.loadConfiguration());
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }

        // bind with markup
        final Form<Void> formView = new Form<>("Form");

        formView.add(new TextField<>("Configuration.maxFileSize", new PropertyModel<String>(configurationModel, "maxFileSize")));
        formView.add(new TextArea<>("Configuration.messageAccueil", new PropertyModel<String>(configurationModel, "messageAccueil")));
        formView.add(new TextArea<>("Configuration.droitAcces", new PropertyModel<String>(configurationModel, "droitAcces")));
        formView.add(new TextArea<>("Configuration.editeur", new PropertyModel<String>(configurationModel, "editeur")));
        formView.add(new TextArea<>("Configuration.hebergement", new PropertyModel<String>(configurationModel, "hebergement")));
        formView.add(new TextArea<>("Configuration.droitsReproduction", new PropertyModel<String>(configurationModel, "droitsReproduction")));

        // champ fichier
        final FileUploadField fileUploadField = new FileUploadField("Configuration.logo");



        // Action : suppression du logo
        Button deleteButton = new AjaxFallbackButton("Configuration.logo.Delete", formView) {
            @Override
            protected void onSubmit(Optional<AjaxRequestTarget> target) {
                deleteLogo = true;
            }
        };
        deleteButton.setDefaultFormProcessing(false);
        formView.add(deleteButton);


        // get configuration
        long documentMaxUploadSize = ((WebApplicationImpl) getApplication()).getDocumentMaxUploadSize();
        String documentExtensionAllowed = ((WebApplicationImpl) getApplication()).getLogoExtensionAllowed();

        formView.add(new SimpleTooltipPanel("Configuration.logo.info", new Model<>(getString("Document.file.info",
                Model.of(new Object[] { documentMaxUploadSize, documentExtensionAllowed })))));
        formView.add(fileUploadField);

        // Action : update the configuration
        Button updateButton = new SubmittableButton(ACTION_UPDATE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                final FileUpload uploadedFile = fileUploadField.getFileUpload();

                Configuration configuration = configurationModel.getObject();

                if (deleteLogo) {
                    configuration.setLogo(null);
                }

                if (uploadedFile != null) {
                    String extension = FilenameUtils.getExtension(uploadedFile.getClientFileName());
                    extension = extension.toLowerCase(); // check lower case
                    if (!documentExtensionAllowed.contains(extension)) {
                        error(getString("ManageDocumentPage.Error.notAllowedExtension"));
                    } else {
                        configuration.setLogo(uploadedFile.getBytes());
                    }
                }

                validateModel();

                configurationService.updateConfiguration(configuration);
                ((WebApplicationImpl) getApplication()).updateConfig();
            }

            @Override
            public void onSuccess() {
                PageParameters params = new PageParameters();
                params.add(RETURN_PARAM[0], RETURN_PARAM[1]);
                setResponsePage(ListConfigurationPage.class, params);
            }

            @Override
            public void onValidate() {
                validateModel();
            }
        });
        updateButton.setVisibilityAllowed(true);
        formView.add(updateButton);

        formView.add(new Link<Void>("Cancel") {
            @Override
            public void onClick() {
                PageParameters params = new PageParameters();
                params.add(RETURN_PARAM[0], RETURN_PARAM[1]);
                setResponsePage(ListConfigurationPage.class, params);
            }
        });

        add(formView);
    }

    /**
     * Validate model
     */
    private void validateModel() {
        for (String violation : validator.validate(configurationModel.getObject(), getSession().getLocale())) {
            error(violation);
        }
    }

}
