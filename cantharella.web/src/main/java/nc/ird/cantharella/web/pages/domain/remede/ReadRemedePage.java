package nc.ird.cantharella.web.pages.domain.remede;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.EntreeReferentiel;
import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Remede;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.campagne.ManageCampagnePage;
import nc.ird.cantharella.web.pages.domain.campagne.ReadCampagnePage;
import nc.ird.cantharella.web.pages.domain.document.panel.DocumentLinkPanel;
import nc.ird.cantharella.web.pages.domain.document.panel.ReadListDocumentsPanel;
import nc.ird.cantharella.web.pages.domain.personne.ReadPersonnePage;
import nc.ird.cantharella.web.pages.domain.personneInterrogee.ReadPersonneInterrogeePage;
import nc.ird.cantharella.web.pages.domain.specimen.ReadSpecimenPage;
import nc.ird.cantharella.web.pages.domain.station.ReadStationPage;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.JSConfirmationBehavior;
import nc.ird.cantharella.web.utils.behaviors.MapViewBehavior;
import nc.ird.cantharella.web.utils.behaviors.ReplaceEmptyLabelBehavior;
import nc.ird.cantharella.web.utils.forms.SubmittableButton;
import nc.ird.cantharella.web.utils.forms.SubmittableButtonEvents;
import nc.ird.cantharella.web.utils.models.DisplayBooleanPropertyModel;
import nc.ird.cantharella.web.utils.models.DisplayMapValuePropertyModel;
import nc.ird.cantharella.web.utils.models.GenericLoadableDetachableModel;
import nc.ird.cantharella.web.utils.panels.PropertyLabelLinkPanel;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public class ReadRemedePage extends TemplatePage {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ReadRemedePage.class);
    /** Action : delete */
    public static final String ACTION_DELETE = "Delete";

    /** Page appelante */
    private final CallerPage callerPage;

    /** Modèle : campagne */
    private final IModel<Remede> remedeModel;

    /** Service : personneInterrogee */
    @SpringBean
    private RemedeService remedeService;

    /**
     * Constructeur
     *
     * @param idRemede ID campagne
     * @param callerPage Page appelante
     */
    public ReadRemedePage(Integer idRemede, final CallerPage callerPage) {

        super(ReadRemedePage.class);
        final CallerPage currentPage = new CallerPage((TemplatePage) getPage());
        this.callerPage = callerPage;

        // Initialisation des modèles
        remedeModel = new GenericLoadableDetachableModel<>(Remede.class, idRemede);

        final Remede remede = remedeModel.getObject();

        //Map
        add(new Label("Remede.map", new Model<>("")).add(new MapViewBehavior()));
        add(new Label("Remede.latitude", new PropertyModel<String>(remedeModel, "station.latitude"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.longitude", new PropertyModel<String>(remedeModel, "station.longitude"))
                .add(new ReplaceEmptyLabelBehavior()));

        // Mapping des champs du modèle
        add(new PropertyLabelLinkPanel<>("Remede.campagne", new PropertyModel<Campagne>(remedeModel,
                "campagne"), getStringModel("Read")) {
            @Override
            public void onClick() {
                setResponsePage(new ReadCampagnePage(getModelObject().getIdCampagne(), currentPage));
            }
        });
        add(new PropertyLabelLinkPanel<>("Remede.station", new PropertyModel<Station>(remedeModel,
                "station"), getStringModel("Read")) {
            @Override
            public void onClick() {
                setResponsePage(new ReadStationPage(getModelObject().getIdStation(), currentPage));
            }
        });
        add(new Label("Remede.date", new PropertyModel<String>(remedeModel, "date"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new PropertyLabelLinkPanel<>("Remede.enqueteur", new PropertyModel<Personne>(remedeModel,
                "enqueteur"), getStringModel("Read")) {
            @Override
            public void onClick() {
                setResponsePage(new ReadPersonnePage(getModelObject().getIdPersonne(), currentPage));
            }
        });
        add(new Label("Remede.reference", new PropertyModel<String>(remedeModel, "ref"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new PropertyLabelLinkPanel<>("Remede.prescripteur", new PropertyModel<PersonneInterrogee>(remedeModel,
                "prescripteur"), getStringModel("Read")) {
            @Override
            public void onClick() {
                setResponsePage(new ReadPersonneInterrogeePage(getModelObject().getIdPersonne(), currentPage));
            }
        });
        add(new Label("Remede.experience", new PropertyModel<String>(remedeModel, "prescripteur.experience"))
                .add(new ReplaceEmptyLabelBehavior()));
        createConsentementLink();
        add(new Label("Remede.nom", new PropertyModel<String>(remedeModel, "nom"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.langue", new DisplayMapValuePropertyModel<>(remedeModel, "codeLangue",
                WebContext.LANGUAGES.get(getSession().getLocale()))).add(new ReplaceEmptyLabelBehavior()));
        initClassificationFields();
        add(new Label("Remede.indicationBiomedicale", new PropertyModel<String>(remedeModel, "indicationBiomedicale"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.indicationVernaculaire", new PropertyModel<String>(remedeModel, "indicationVernaculaire"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.nbIngredients", new PropertyModel<String>(remedeModel, "ingredients.size"))
                .add(new ReplaceEmptyLabelBehavior()));




        add(new Label("Remede.preparation", new PropertyModel<String>(remedeModel, "preparation"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.conservation", new PropertyModel<String>(remedeModel, "conservation"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.administration", new PropertyModel<String>(remedeModel, "administration"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.posologie", new PropertyModel<String>(remedeModel, "posologie"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.recommandations", new DisplayBooleanPropertyModel(remedeModel, "recommandations", this))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.recommandationsText", new PropertyModel<String>(remedeModel, "recommandationsText"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.effetsIndesirables", new PropertyModel<String>(remedeModel, "effetsIndesirables"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.patientsParAn", new PropertyModel<String>(remedeModel, "patientsParAn"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.dispoAnalyse", new DisplayBooleanPropertyModel(remedeModel, "dispoAnalyse", this))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.dispoAnalyseLabo", new PropertyModel<String>(remedeModel, "dispoAnalyseLabo"))
                .add(new ReplaceEmptyLabelBehavior()));

        add(new MultiLineLabel("Remede.complement", new PropertyModel<String>(remedeModel, "complement"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Remede.createur", new PropertyModel<String>(remedeModel, "createur"))
                .add(new ReplaceEmptyLabelBehavior()));

        initIngredientsFields(currentPage);

        // add list document panel
        ReadListDocumentsPanel readListDocumentsPanel = new ReadListDocumentsPanel("ReadListDocumentsPanel",
                remedeModel, currentPage);
        add(readListDocumentsPanel);

        // Ajout du formulaire pour les actions
        final Form<Void> formView = new Form<>("Form");

        // Action : mise à jour (redirection vers le formulaire)
        Link<Remede> updateLink = new Link<>(getResource() + ".Remede.Update",
                new Model<>(remede)) {
            @Override
            public void onClick() {
                setResponsePage(new ManageRemedePage(getModelObject().getIdRemede(), currentPage));
            }
        };
        updateLink.setVisibilityAllowed(remedeService.updateOrdeleteRemedeEnabled(remede,
                getSession().getUtilisateur()));
        formView.add(updateLink);

        // Action : retour à la page précédente
        formView.add(new Link<Void>(getResource() + ".Remede.Back") {
            @Override
            public void onClick() {
                redirect();
            }
        });

        // Action : suppression de la remede
        Button deleteButton = new SubmittableButton(ACTION_DELETE, ManageCampagnePage.class,
                new SubmittableButtonEvents() {
                    @Override
                    public void onProcess() throws DataConstraintException {
                        remedeService.deleteRemede(remede);
                    }

                    @Override
                    public void onSuccess() {
                        successNextPage(ManageCampagnePage.class, ACTION_DELETE);
                        redirect();
                    }
                });
        deleteButton.setVisibilityAllowed(remedeService.updateOrdeleteRemedeEnabled(remede,
                getSession().getUtilisateur()));
        deleteButton.add(new JSConfirmationBehavior(getStringModel("Confirm")));
        formView.add(deleteButton);
        add(formView);
    }

    /**
     * Redirection vers une autre page
     */
    private void redirect() {
        callerPage.responsePage(this);
    }


    /**
     * Bind fields concerning results
     *
     * @param currentPage currentPage
     */
    private void initIngredientsFields(final CallerPage currentPage) {
        // Déclaration tableau des resultats
        // Pas de possibilité d'avoir le tableau vide car toujours au moins un résultat de type produit
        final MarkupContainer ingredientsTable = new WebMarkupContainer("Remede.ingredients.Table");
        ingredientsTable.setOutputMarkupId(true);
        add(ingredientsTable);

        final WebMarkupContainer ingredientsNotAccessibleCont = new WebMarkupContainer(
                "Remede.ingredients.ingredientsNotAccessibles");
        ingredientsNotAccessibleCont.setOutputMarkupPlaceholderTag(true);
        ingredientsTable.add(ingredientsNotAccessibleCont);

        // Model de liste des résultats
        final LoadableDetachableModel<List<IngredientRemede>> listIngredientsModel = new LoadableDetachableModel<>() {
            @Override
            protected List<IngredientRemede> load() {
                boolean isOneResultNotAccessible = false;

                List<IngredientRemede> listResults = new ArrayList<>();

                for (IngredientRemede ingredient : remedeModel.getObject().getSortedIngredients()) {
                    // les résultats de type blanc ou témoin sont tjr accessibles
                    if (true) {
                        listResults.add(ingredient);
                    } else {
                        isOneResultNotAccessible = true;
                    }
                }
                // si un des résultats non accessible, on rend visible le message d'avertissement
                ingredientsNotAccessibleCont.setVisibilityAllowed(isOneResultNotAccessible);
                return listResults;
            }
        };

        // Contenu tableaux resultats
        ingredientsTable.add(new ListView<>("Remede.ingredients.List", listIngredientsModel) {
            @Override
            protected void populateItem(ListItem<IngredientRemede> item) {
                if (item.getIndex() % 2 == 1) {
                    item.add(new AttributeModifier("class", "odd"));
                }

                final IngredientRemede resultatModel = item.getModelObject();

                // Colonnes
                item.add(new PropertyLabelLinkPanel<>("Remede.ingredients.List.specimen", new PropertyModel<String>(resultatModel, "specimen.ref"),
                        getStringModel("Read")) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ReadSpecimenPage(resultatModel.getSpecimen().getIdSpecimen(), currentPage));
                    }

                }.add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.ingredients.List.type", new PropertyModel<String>(resultatModel, "specimen.typeOrganisme")).add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.ingredients.List.famille", new PropertyModel<String>(resultatModel,"specimen.famille")).add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.ingredients.List.genre", new PropertyModel<String>(resultatModel, "specimen.genre")).add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.ingredients.List.espece", new PropertyModel<String>(resultatModel,"specimen.espece")).add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.ingredients.List.nomVernaculaire", new PropertyModel<String>(resultatModel,"nomVernaculaire")).add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.ingredients.List.partie", new PropertyModel<String>(resultatModel,"partie")).add(new ReplaceEmptyLabelBehavior()));
                item.add(new PropertyLabelLinkPanel<>("Remede.ingredients.List.provenance", new PropertyModel<String>(resultatModel, "provenance"),
                        getStringModel("Read")) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ReadStationPage(resultatModel.getProvenance().getIdStation(), currentPage));
                    }

                }.add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.ingredients.List.periode", new PropertyModel<String>(resultatModel,"periode")).add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.ingredients.List.disponibilite", new PropertyModel<String>(resultatModel,"disponibilite")).add(new ReplaceEmptyLabelBehavior()));
            }
        });
    }


    /**
     * Bind fields concerning classification
     */
    private void initClassificationFields() {
        // Déclaration tableau des resultats
        // Pas de possibilité d'avoir le tableau vide car toujours au moins un résultat de type produit
        final MarkupContainer classificationTable = new WebMarkupContainer("Remede.classification.Table");
        classificationTable.setOutputMarkupId(true);
        add(classificationTable);

        // Model de liste des résultats
        final LoadableDetachableModel<List<EntreeReferentiel>> listClassificationModel = new LoadableDetachableModel<>(remedeModel.getObject().getClassification()) {
            @Override
            protected List<EntreeReferentiel> load() {
                return remedeModel.getObject().getClassification();
            }
        };

        // Contenu tableaux resultats
        classificationTable.add(new ListView<>("Remede.classification.List", listClassificationModel) {
            @Override
            protected void populateItem(ListItem<EntreeReferentiel> item) {
                if (item.getIndex() % 2 == 1) {
                    item.add(new AttributeModifier("class", "odd"));
                }

                final EntreeReferentiel resultatModel = item.getModelObject();

                // Colonnes
                item.add(new Label("Remede.classification.List.referentiel", new PropertyModel<String>(resultatModel, "referentiel")).add(new ReplaceEmptyLabelBehavior()));
                item.add(new Label("Remede.classification.List.valeur", new PropertyModel<String>(resultatModel, "valeur")).add(new ReplaceEmptyLabelBehavior()));
            }
        });

        classificationTable.add(new AttributeModifier("class", Model.of("property")));
    }

    protected void createConsentementLink() {
        Component consentementLink;
        if (remedeModel.getObject().getConsentement() != null) {
            consentementLink = new DocumentLinkPanel("Remede.consentement", new Model<>(remedeModel.getObject().getConsentement())){

                @Override
                public boolean isVisible() {
                    return remedeModel.getObject().getConsentement() != null;
                }

            };
        } else {
            consentementLink = new Label("Remede.consentement");
        }

        consentementLink.setOutputMarkupId(true);
        addOrReplace(consentementLink);
    }

    /** {@inheritDoc} */
    @Override
    protected void onBeforeRender() {
        createConsentementLink();

        super.onBeforeRender();
    }
}
