/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.utilisateur.panels;

import java.util.ArrayList;
import java.util.List;

import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.CampagnePersonneDroits;
import nc.ird.cantharella.data.model.Droits;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Remede;
import nc.ird.cantharella.data.model.PersonneDroits;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.data.model.comparators.CampagnePersonneDroitsComp;
import nc.ird.cantharella.data.model.comparators.PersonneDroitsComp;
import nc.ird.cantharella.data.model.utils.CompositeId;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.campagne.ReadCampagnePage;
import nc.ird.cantharella.web.pages.domain.lot.ReadLotPage;
import nc.ird.cantharella.web.pages.domain.remede.ReadRemedePage;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.panels.PropertyLabelLinkPanel;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * Panneau de consultation des droits d'un utilisateur
 * 
 * @author Adrien Cheype
 */
public final class ReadDroitsUtilisateurPanel extends Panel {

    /**
     * Constructeur
     * 
     * @param id Id du composant
     * @param utilisateurModel Modèle de l'utilisateur concerné
     */
    public ReadDroitsUtilisateurPanel(String id, final IModel<? extends Utilisateur> utilisateurModel) {
        super(id, utilisateurModel);

        // Gestion des campagnes et lots autorisés
        final MarkupContainer autorisationsContainer = new WebMarkupContainer("Authorizations.Table") {
            @Override
            public boolean isVisible() {
                return !utilisateurModel.getObject().getCampagnesDroits().isEmpty()
                        || !utilisateurModel.getObject().getLotsDroits().isEmpty()
                        || !utilisateurModel.getObject().getCampagnesAdministrees().isEmpty();
            }
        };
        autorisationsContainer.setOutputMarkupId(true);

        final LoadableDetachableModel<List<CampagnePersonneDroits>> campagnesModel = new LoadableDetachableModel<>() {
            @Override
            protected List<CampagnePersonneDroits> load() {
                List<CampagnePersonneDroits> listCampagnesDroits = new ArrayList<>(
                        utilisateurModel.getObject().getCampagnesDroits().values());

                //Ajout manuel des campagnes administrées pour visualisation des droits
                List<Campagne> campagnesAdministrees = utilisateurModel.getObject().getCampagnesAdministrees();
                for (Campagne campagne: campagnesAdministrees) {
                    CampagnePersonneDroits testDroit = new CampagnePersonneDroits();
                    testDroit.setDroits(new Droits());
                    CompositeId<Campagne, Personne> idtest = new CompositeId<>();
                    idtest.setPk1(campagne);
                    idtest.setPk2(utilisateurModel.getObject());
                    testDroit.setId(idtest);
                    listCampagnesDroits.add(testDroit);
                }

                // tri pour affichage
                Collections.sort(listCampagnesDroits, new CampagnePersonneDroitsComp());
                return listCampagnesDroits;
            }
        };

        // Liste des campagnes autorisées
        autorisationsContainer.add(new ListView<>("Authorizations.Campagnes.List", campagnesModel) {
                    @Override
                    protected void populateItem(final ListItem<CampagnePersonneDroits> item) {
                        item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));

                        item.add(new PropertyLabelLinkPanel<>("Authorizations.Campagnes.Campagne.nom",
                                new PropertyModel<Campagne>(item.getModel(), "id.pk1"), new StringResourceModel("Read",
                                        this, null)) {
                            @Override
                            public void onClick() {
                                setResponsePage(new ReadCampagnePage(getModelObject().getIdCampagne(), new CallerPage(
                                        (TemplatePage) getPage())));
                            }
                        });
                    }
                });

        // Liste des lots autorisés
        final LoadableDetachableModel<List<PersonneDroits>> lotsModel = new LoadableDetachableModel<>() {
            protected List<PersonneDroits> load() {
                List<PersonneDroits> listLotsDroits = new ArrayList<>(utilisateurModel.getObject()
                        .getLotsDroits().values());
                listLotsDroits.addAll(utilisateurModel.getObject().getRemedesDroits().values());

                // tri pour affichage
                Collections.sort(listLotsDroits, new PersonneDroitsComp());
                return listLotsDroits;
            }
        };
        autorisationsContainer.add(new ListView<>("Authorizations.Lots.List", lotsModel) {
            @Override
            protected void populateItem(ListItem<PersonneDroits> item) {
                item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));

                item.add(new PropertyLabelLinkPanel<>("Authorizations.Lots.Campagne.nom",
                        new PropertyModel<Campagne>(item.getModel(), "id.pk1.campagne"), new StringResourceModel(
                                "Read", this, null)) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ReadCampagnePage(getModelObject().getIdCampagne(), new CallerPage(
                                (TemplatePage) getPage())));
                    }
                });

                item.add(new PropertyLabelLinkPanel<>(
                        "Authorizations.Lots.Lot.ref",
                        new PropertyModel<>(item.getModel(), "id.pk1"),
                        new StringResourceModel("Read", this, null)) {
                    @Override
                    public void onClick() {
                        if (getModelObject() instanceof Lot) {
                            setResponsePage(new ReadLotPage(((Lot)getModelObject()).getIdLot(), new CallerPage(
                                    (TemplatePage) getPage())));
                        }
                        if (getModelObject() instanceof Remede) {
                            setResponsePage(new ReadRemedePage(((Remede)getModelObject()).getIdRemede(), new CallerPage(
                                    (TemplatePage) getPage())));
                        }
                    }
                });
            }
        });
        add(autorisationsContainer);

        // Selon la non existence d'elements dans la table on affiche le span pour remplacer
        MarkupContainer emptyCampagnesContainer = new WebMarkupContainer("Authorizations.emptyTable") {
            @Override
            public boolean isVisible() {
                return !autorisationsContainer.isVisible();
            }
        };
        add(emptyCampagnesContainer);
    }
}
