package nc.ird.cantharella.web.pages.domain;

import java.io.Serializable;

/**
 * Serializable functional interface used to refresh datatables so that Wicket does not complain about serialization
 */
@FunctionalInterface
public interface RefreshAction extends Serializable {
    void refresh();
}
