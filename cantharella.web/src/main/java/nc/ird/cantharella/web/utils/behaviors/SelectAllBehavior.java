package nc.ird.cantharella.web.utils.behaviors;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;

public class SelectAllBehavior extends Behavior {

    public SelectAllBehavior() {
        super();
    }

    /** {@inheritDoc} */
    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        response.render(JavaScriptHeaderItem.forUrl("js/selectAll.js"));
    }
}
