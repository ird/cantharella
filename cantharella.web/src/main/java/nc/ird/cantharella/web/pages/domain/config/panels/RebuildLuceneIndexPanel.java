/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.config.panels;

import nc.ird.cantharella.service.services.SearchService;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Optional;

/**
 * Panel qui permet de reconstruire l'index lucene.
 * 
 * @author Eric Chatellier
 */
public class RebuildLuceneIndexPanel extends Panel {

    /** Service : test */
    @SpringBean
    private SearchService searchService;

    /**
     * Constructor
     * 
     * @param id The panel ID
     */
    public RebuildLuceneIndexPanel(String id) {
        super(id);

        final Form<Void> formView = new Form<>("Form");

        final IModel<String> stringLabel = Model.of("");
        final Label label = new Label("Status", stringLabel);
        label.setOutputMarkupId(true);
        formView.add(label);

        final AjaxFallbackButton addButton = new AjaxFallbackButton("Rebuild", formView) {
            @Override
            protected void onAfterSubmit(Optional<AjaxRequestTarget> target) {
                searchService.reIndex();
                stringLabel.setObject("Done");
                target.ifPresent(ajaxRequestTarget -> ajaxRequestTarget.add(label));
            }

            @Override
            protected void onError(Optional<AjaxRequestTarget> target) {
                stringLabel.setObject("Error");
                target.ifPresent(ajaxRequestTarget -> ajaxRequestTarget.add(label));
            }
        };
        addButton.setOutputMarkupId(true);
        formView.add(addButton);

        add(formView);
    }
}
