package nc.ird.cantharella.web.pages.domain.config.panels;

import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.service.services.ConfigurationService;
import nc.ird.cantharella.web.pages.domain.config.ManageUniteConcMassePage;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

public class ListUniteConcMassePanel extends Panel {

    /**
     * Service : test
     */
    @SpringBean
    private ConfigurationService configurationService;

    /**
     * Constructor
     *
     * @param id The panel ID
     */
    public ListUniteConcMassePanel(String id) {
        super(id);

        add(new BookmarkablePageLink<Void>("ListUniteConcMassePanel.NewUniteConcMasse", ManageUniteConcMassePage.class));

        // On englobe le "DataView" dans un composant neutre que l'on pourra
        // rafraichir quand la liste évoluera
        final MarkupContainer uniteConcMassesRefresh = new WebMarkupContainer(
                "ListUniteConcMassePanel.Unites.Refresh");
        uniteConcMassesRefresh.setOutputMarkupId(true);
        add(uniteConcMassesRefresh);

        // Liste des uniteConcMasse
        final List<UniteConcMasse> unites = configurationService.listUnites();
        LoadableDetachableSortableListDataProvider<UniteConcMasse> methodesDataProvider =
                new LoadableDetachableSortableListDataProvider<>(unites, getSession().getLocale());

        uniteConcMassesRefresh.add(new DataView<>("ListUniteConcMassePanel.Unites",
                methodesDataProvider) {
            @Override
            protected void populateItem(Item<UniteConcMasse> item) {
                item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));

                UniteConcMasse unite = item.getModelObject();

                // Colonnes
                item.add(new Label("ListUniteConcMassePanel.Unites.code", new PropertyModel<String>(
                        unite, "code")));
                item.add(new Label("ListUniteConcMassePanel.Unites.valeur", new PropertyModel<String>(
                        unite, "valeur")));

                // Action : mise à jour (redirection vers le formulaire)
                Link<UniteConcMasse> updateLink = new Link<>(
                        "ListUniteConcMassePanel.Unites.Update", new Model<>(unite)) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ManageUniteConcMassePage(getModelObject().getIdUnite()));
                    }
                };
                item.add(updateLink);
            }
        });
    }
}