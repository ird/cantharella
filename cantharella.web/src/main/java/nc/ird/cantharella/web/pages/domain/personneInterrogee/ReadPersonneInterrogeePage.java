package nc.ird.cantharella.web.pages.domain.personneInterrogee;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.service.services.PersonneInterrogeeService;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.campagne.ManageCampagnePage;
import nc.ird.cantharella.web.pages.domain.campagne.ReadCampagnePage;
import nc.ird.cantharella.web.pages.domain.document.panel.ReadListDocumentsPanel;
import nc.ird.cantharella.web.pages.domain.station.ReadStationPage;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.CampagneMapViewBehavior;
import nc.ird.cantharella.web.utils.behaviors.JSConfirmationBehavior;
import nc.ird.cantharella.web.utils.behaviors.MapViewBehavior;
import nc.ird.cantharella.web.utils.behaviors.ReplaceEmptyLabelBehavior;
import nc.ird.cantharella.web.utils.forms.SubmittableButton;
import nc.ird.cantharella.web.utils.forms.SubmittableButtonEvents;
import nc.ird.cantharella.web.utils.models.DisplayEnumPropertyModel;
import nc.ird.cantharella.web.utils.models.DisplayMapValuePropertyModel;
import nc.ird.cantharella.web.utils.models.GenericLoadableDetachableModel;
import nc.ird.cantharella.web.utils.panels.PropertyLabelLinkPanel;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public class ReadPersonneInterrogeePage extends TemplatePage {

    /** Logger */
    //private static final Logger LOG = LoggerFactory.getLogger(ReadCampagnePage.class);
    /** Action : delete */
    public static final String ACTION_DELETE = "Delete";

    /** Page appelante */
    private final CallerPage callerPage;

    /** Modèle : campagne */
    private final IModel<PersonneInterrogee> personneInterrogeeModel;

    /** Service : personneInterrogee */
    @SpringBean
    private PersonneInterrogeeService personneInterrogeeService;

    /**
     * Constructeur
     *
     * @param idPersonneInterrogee ID campagne
     * @param callerPage Page appelante
     */
    public ReadPersonneInterrogeePage(Integer idPersonneInterrogee, final CallerPage callerPage) {

        super(ReadPersonneInterrogeePage.class);
        final CallerPage currentPage = new CallerPage((TemplatePage) getPage());
        this.callerPage = callerPage;

        // Initialisation des modèles
        personneInterrogeeModel = new GenericLoadableDetachableModel<>(PersonneInterrogee.class, idPersonneInterrogee);

        final PersonneInterrogee personneInterrogee = personneInterrogeeModel.getObject();

        //Map
        add(new Label("PersonneInterrogee.map", new Model<>("")).add(new MapViewBehavior()));
        add(new Label("PersonneInterrogee.latitude", new PropertyModel<String>(personneInterrogeeModel, "station.latitude"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.longitude", new PropertyModel<String>(personneInterrogeeModel, "station.longitude"))
                .add(new ReplaceEmptyLabelBehavior()));

        // Mapping des champs du modèle
        add(new PropertyLabelLinkPanel<>("PersonneInterrogee.campagne", new PropertyModel<Campagne>(personneInterrogeeModel,
                "campagne"), getStringModel("Read")) {
            @Override
            public void onClick() {
                setResponsePage(new ReadCampagnePage(getModelObject().getIdCampagne(), currentPage));
            }
        });
        add(new Label("PersonneInterrogee.nom", new PropertyModel<String>(personneInterrogeeModel, "nom"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.prenom", new PropertyModel<String>(personneInterrogeeModel, "prenom"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new PropertyLabelLinkPanel<>("PersonneInterrogee.station", new PropertyModel<Station>(personneInterrogeeModel,
                "station"), getStringModel("Read")) {
            @Override
            public void onClick() {
                setResponsePage(new ReadStationPage(getModelObject().getIdStation(), currentPage));
            }
        });
        add(new Label("PersonneInterrogee.genre", new DisplayEnumPropertyModel(personneInterrogeeModel, "genre", this)));
        add(new Label("PersonneInterrogee.anneeNaissance", new PropertyModel<String>(personneInterrogeeModel, "anneeNaissance"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.experience", new PropertyModel<String>(personneInterrogeeModel, "experience"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.profession", new PropertyModel<String>(personneInterrogeeModel, "profession"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.labellisation", new PropertyModel<String>(personneInterrogeeModel, "labellisation"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.telephone", new PropertyModel<String>(personneInterrogeeModel, "telephone"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.courriel", new PropertyModel<String>(personneInterrogeeModel, "courriel"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.adressePostale", new PropertyModel<String>(personneInterrogeeModel, "adressePostale"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.codePostal", new PropertyModel<String>(personneInterrogeeModel, "codePostal"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.ville", new PropertyModel<String>(personneInterrogeeModel, "ville"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.codePays", new DisplayMapValuePropertyModel<>(personneInterrogeeModel, "codePays",
                WebContext.COUNTRIES.get(getSession().getLocale()))).add(new ReplaceEmptyLabelBehavior()));
        add(new MultiLineLabel("PersonneInterrogee.complement", new PropertyModel<String>(personneInterrogeeModel, "complement"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("PersonneInterrogee.createur", new PropertyModel<String>(personneInterrogeeModel, "createur"))
                .add(new ReplaceEmptyLabelBehavior()));

        // add list document panel
        ReadListDocumentsPanel readListDocumentsPanel = new ReadListDocumentsPanel("ReadListDocumentsPanel",
                personneInterrogeeModel, currentPage);
        add(readListDocumentsPanel);

        // Ajout du formulaire pour les actions
        final Form<Void> formView = new Form<>("Form");

        // Action : mise à jour (redirection vers le formulaire)
        Link<PersonneInterrogee> updateLink = new Link<>(getResource() + ".PersonneInterrogee.Update",
                new Model<>(personneInterrogee)) {
            @Override
            public void onClick() {
                setResponsePage(new ManagePersonneInterrogeePage(getModelObject().getIdPersonne(), currentPage));
            }
        };
        updateLink.setVisibilityAllowed(personneInterrogeeService.updateOrdeletePersonneInterrogeeEnabled(personneInterrogee,
                getSession().getUtilisateur()));
        formView.add(updateLink);

        // Action : retour à la page précédente
        formView.add(new Link<Void>(getResource() + ".PersonneInterrogee.Back") {
            @Override
            public void onClick() {
                redirect();
            }
        });

        // Action : suppression de la personneInterrogee
        Button deleteButton = new SubmittableButton(ACTION_DELETE, ManageCampagnePage.class,
                new SubmittableButtonEvents() {
                    @Override
                    public void onProcess() throws DataConstraintException {
                        personneInterrogeeService.deletePersonneInterrogee(personneInterrogee);
                    }

                    @Override
                    public void onSuccess() {
                        successNextPage(ManageCampagnePage.class, ACTION_DELETE);
                        redirect();
                    }
                });
        deleteButton.setVisibilityAllowed(personneInterrogeeService.updateOrdeletePersonneInterrogeeEnabled(personneInterrogee,
                getSession().getUtilisateur()));
        deleteButton.add(new JSConfirmationBehavior(getStringModel("Confirm")));
        formView.add(deleteButton);
        add(formView);
    }

    /**
     * Redirection vers une autre page
     */
    private void redirect() {
        callerPage.responsePage(this);
    }
}
