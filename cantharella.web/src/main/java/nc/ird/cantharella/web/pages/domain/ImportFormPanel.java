package nc.ird.cantharella.web.pages.domain;

import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.MoleculeProvenanceBean;
import nc.ird.cantharella.service.model.StationImportModel;
import nc.ird.cantharella.service.services.ImportServiceFactory;
import nc.ird.cantharella.web.utils.models.SimpleSortableListDataProvider;
import nc.ird.cantharella.web.utils.panels.SimpleTooltipPanel;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.DownloadLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.lang.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class ImportFormPanel extends Panel {
    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImportFormPanel.class);

    private FileUploadField fileUpload;

    /**
     * Service : imports
     */
    @SpringBean
    private ImportServiceFactory importService;

    public ImportFormPanel(String id, String ressourceName, Utilisateur utilisateur, RefreshAction action) {
        super(id);

        Form<?> form = new Form<Void>("importForm") {
            @Override
            protected void onSubmit() {

                final FileUpload uploadedFile = fileUpload.getFileUpload();
                if (uploadedFile != null) {

                    // write to a new file
                    try {
                        InputStream inputStream = uploadedFile.getInputStream();

                        ImportResult importResult = importService.importData(ressourceName, inputStream, utilisateur, getSession().getLocale());
                        if (importResult.isValid()) {
                            info(getString("Import.ImportSucess"));
                            action.refresh();

                        } else {
                            error(getString("Import.ImportFailed"));
                            for (String errorLine : importResult.getErrorLines()) {
                                error(errorLine);
                            }
                        }
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        throw new UnexpectedException(e);
                    }
                }

            }

        };
        add(form);

        // Enable multipart mode (need for uploads file) (Wicket now autodetects it, but always good to force)
        form.setMultiPart(true);

        // max upload size, 1Mb
        form.setMaxSize(Bytes.megabytes(1));

        fileUpload = new FileUploadField("fileUpload");
        //Needed so that the datatable refresh when using pagination is not buggy
        fileUpload.setOutputMarkupId(true);
        form.add(fileUpload);

        form.add(new SimpleTooltipPanel("csvInfo",
                new StringResourceModel("Import.csvInfo", this, null)));

        // Download virgin file before upload
        try {
            File file = importService.exportHeader(ressourceName);
            if (file != null) {
                DownloadLink downloadLink = new DownloadLink("csvExample", file, ressourceName + "Headers.csv");
                downloadLink.setDeleteAfterDownload(true);

                form.add(downloadLink);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }




        StringResourceModel labelValue = new StringResourceModel("Import." + ressourceName, this, null);
        Label label = new Label("importLabel", labelValue);
        form.add(label);

    }
}
