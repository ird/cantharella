/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.dosage;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.document.panel.ReadListDocumentsPanel;
import nc.ird.cantharella.web.pages.domain.personne.ReadPersonnePage;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.JSConfirmationBehavior;
import nc.ird.cantharella.web.utils.behaviors.ReplaceEmptyLabelBehavior;
import nc.ird.cantharella.web.utils.forms.SubmittableButton;
import nc.ird.cantharella.web.utils.forms.SubmittableButtonEvents;
import nc.ird.cantharella.web.utils.models.DisplayBooleanPropertyModel;
import nc.ird.cantharella.web.utils.models.DisplayDecimalPropertyModel;
import nc.ird.cantharella.web.utils.models.DisplayDecimalPropertyModel.DecimalDisplFormat;
import nc.ird.cantharella.web.utils.models.DisplayEnumPropertyModel;
import nc.ird.cantharella.web.utils.models.DisplayShortDatePropertyModel;
import nc.ird.cantharella.web.utils.models.GenericLoadableDetachableModel;
import nc.ird.cantharella.web.utils.panels.PropertyLabelLinkPanel;
import nc.ird.cantharella.web.utils.panels.SimpleTooltipPanel;
import nc.ird.cantharella.web.utils.security.AuthSession;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Consultation d'un dosage
 * 
 * @author Jean Couteau Viney
 */
public final class ReadDosagePage extends TemplatePage {

    /** Logger */
    //private static final Logger LOG = LoggerFactory.getLogger(ReadTestBioPage.class);
    /** Action : update */
    private static final String ACTION_DELETE = "Delete";

    /** testBio Model */
    private IModel<Dosage> dosageModel;

    /** Service : testBios */
    @SpringBean
    private DosageService dosageService;

    /** Page appelante */
    private final CallerPage callerPage;

    /** Bouton d'ajout d'un résultat de dosage **/
    Button addResultatButton;

    /** Container pour l'affichage des paramètres de la méthode **/
    MarkupContainer paramsMethoContainer;

    /**
     * Constructeur
     * 
     * @param idDosage identifiant du dosage
     * @param callerPage Page appelante
     */
    public ReadDosagePage(Integer idDosage, CallerPage callerPage) {
        super(ReadDosagePage.class);
        final CallerPage currentPage = new CallerPage((TemplatePage) getPage());
        this.callerPage = callerPage;

        // Initialisation du modèle
        dosageModel = new GenericLoadableDetachableModel<>(Dosage.class, idDosage);

        final Dosage dosage = dosageModel.getObject();

        initPrincipalFields(currentPage);
        initTestMethodFields();
        initResultsFields(currentPage);

        // add list document panel
        ReadListDocumentsPanel readListDocumentsPanel = new ReadListDocumentsPanel("ReadListDocumentsPanel",
                dosageModel, currentPage);
        add(readListDocumentsPanel);

        // Ajout du formulaire pour les actions
        Form<Void> formView = new Form<>("Form");

        // Action : mise à jour (redirection vers le formulaire)
        Link<Dosage> updateLink = new Link<>(getResource() + ".Dosage.Update", new Model<>(dosage)) {
            @Override
            public void onClick() {
                setResponsePage(new ManageDosagePage(getModelObject().getIdDosage(), currentPage));
            }
        };
        updateLink.setVisibilityAllowed(dosageService.updateOrdeleteDosageEnabled(dosage, getSession()
                .getUtilisateur()));
        formView.add(updateLink);

        // Action : retour à la page précédente
        formView.add(new Link<Void>(getResource() + ".Dosage.Back") {
            @Override
            public void onClick() {
                redirect();
            }
        });

        // Action : suppression de la dosage
        Button deleteButton = new SubmittableButton(ACTION_DELETE, ManageDosagePage.class,
                new SubmittableButtonEvents() {

                    @Override
                    public void onProcess() throws DataConstraintException {
                        dosageService.deleteDosage(dosage);
                    }

                    @Override
                    public void onSuccess() {
                        successNextPage(ManageDosagePage.class, ACTION_DELETE);
                        redirect();
                    }
                });
        deleteButton.setVisibilityAllowed(dosageService.updateOrdeleteDosageEnabled(dosage, getSession()
                .getUtilisateur()));
        deleteButton.add(new JSConfirmationBehavior(getStringModel("Confirm")));
        formView.add(deleteButton);
        add(formView);
    }

    /**
     * Bind principal data to the markup
     * 
     * @param currentPage currentPage
     */
    private void initPrincipalFields(final CallerPage currentPage) {

        add(new Label("Dosage.ref", new PropertyModel<String>(dosageModel, "ref"))
                .add(new ReplaceEmptyLabelBehavior()));

        add(new PropertyLabelLinkPanel<Personne>("Dosage.manipulateur", new PropertyModel<>(dosageModel,
                "manipulateur"), getStringModel("Read")) {
            @Override
            public void onClick() {
                setResponsePage(new ReadPersonnePage(getModelObject().getIdPersonne(), currentPage));
            }
        });

        add(new Label("Dosage.organisme", new PropertyModel<String>(dosageModel, "organisme"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new Label("Dosage.date", new DisplayShortDatePropertyModel(dosageModel, "date", getLocale()))
                .add(new ReplaceEmptyLabelBehavior()));

        // concatenation de concMasse par défaut et de uniteConcMasse par défaut
        add(new Label("Dosage.concMasseDefaut", new Model<Serializable>(dosageModel) {
            /** {@inheritDoc} */
            @Override
            public String getObject() {
                String masse = (String) new DisplayDecimalPropertyModel(super.getObject(), "concMasseDefaut",
                        DecimalDisplFormat.SMALL, getLocale()).getObject();

                UniteConcMasse uniteObject = new PropertyModel<UniteConcMasse>(super.getObject(), "uniteConcMasseDefaut")
                        .getObject();
                String unite = null;
                if (uniteObject != null) {
                    unite = uniteObject.getValeur();
                }

                if (masse == null && unite == null) {
                    return ReplaceEmptyLabelBehavior.NULL_PROPERTY;
                } else if (masse == null) {
                    return ReplaceEmptyLabelBehavior.NULL_PROPERTY + unite;
                } else if (unite == null) {
                    return masse + ReplaceEmptyLabelBehavior.NULL_PROPERTY;
                }
                return masse + " " + unite;
            }
        }));

        add(new Label("Dosage.stadeDefaut", new DisplayEnumPropertyModel(dosageModel, "stadeDefaut", this))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new MultiLineLabel("Dosage.complement", new PropertyModel<String>(dosageModel, "complement"))
                .add(new ReplaceEmptyLabelBehavior()));
        add(new PropertyLabelLinkPanel<Personne>("Dosage.createur", new PropertyModel<Personne>(dosageModel,
                "createur"), getStringModel("Read")) {
            @Override
            public void onClick() {
                setResponsePage(new ReadPersonnePage(getModelObject().getIdPersonne(), currentPage));
            }
        });
    }

    /**
     * Bind fields for dosage method to the markup
     */
    private void initTestMethodFields() {
        WebMarkupContainer methodeCont = new WebMarkupContainer("Dosage.methode") {
            @Override
            public boolean isVisible() {
                // description cachée si pas de méthode sélectionnée
                return dosageModel.getObject().getMethode() != null;
            }
        };
        methodeCont.setOutputMarkupId(true);
        add(methodeCont);

        Label methodeCible = new Label("Dosage.acronymeMethode", new PropertyModel<String>(dosageModel, "methode.acronyme"));
        methodeCont.add(methodeCible);

        Label methodeDomaine = new Label("Dosage.domaineMethode", new PropertyModel<String>(dosageModel,
                "methode.domaine"));
        methodeCont.add(methodeDomaine);

        MultiLineLabel methodeDesc = new MultiLineLabel("Dosage.descriptionMethode", new PropertyModel<String>(
                dosageModel, "methode.description"));
        methodeCont.add(methodeDesc);

        Label methodeValeurMesuree = new Label("Dosage.valeurMesureeMethode", new PropertyModel<String>(dosageModel,
                "methode.valeurMesuree"));
        methodeCont.add(methodeValeurMesuree);

        Label methodeCritereActivite = new Label("Dosage.seuilMethode", new PropertyModel<String>(
                dosageModel, "methode.seuil"));
        methodeCont.add(methodeCritereActivite);

        Label methodeUniteResultat = new Label("Dosage.uniteResultatMethode", new PropertyModel<String>(dosageModel,
                "methode.unite"));
        methodeCont.add(methodeUniteResultat);

        Label nomMethode = new Label("Dosage.nomMethode", new PropertyModel<String>(dosageModel, "methode.nom"));
        methodeCont.add(nomMethode);

    }

    /**
     * Bind fields concerning results
     * 
     * @param currentPage currentPage
     */
    private void initResultsFields(final CallerPage currentPage) {
        // Déclaration tableau des resultats
        // Pas de possibilité d'avoir le tableau vide car toujours au moins un résultat de type produit
        final MarkupContainer dosagesTable = new WebMarkupContainer("Dosage.resultats.Table");
        dosagesTable.setOutputMarkupId(true);
        add(dosagesTable);

        final WebMarkupContainer resultNotAccessibleCont = new WebMarkupContainer(
                "Dosage.resultats.resultsNotAccessibles");
        resultNotAccessibleCont.setOutputMarkupPlaceholderTag(true);
        dosagesTable.add(resultNotAccessibleCont);

        // Model de liste des résultats
        final LoadableDetachableModel<List<ResultatDosageForUI>> listResultsModel = new LoadableDetachableModel<>() {
            @Override
            protected List<ResultatDosageForUI> load() {
                boolean isOneResultNotAccessible = false;

                List<ResultatDosageForUI> listResults = new ArrayList<>();

                for (ResultatDosage res : dosageModel.getObject().getSortedResultats()) {
                    // les résultats de type blanc ou témoin sont tjr accessibles
                    if (dosageService.isResultatDosageAccessibleByUser(res, ((AuthSession) getPage()
                            .getSession()).getUtilisateur())) {
                        listResults.add(new ResultatDosageForUI(res));
                    } else {
                        isOneResultNotAccessible = true;
                    }
                }
                // si un des résultats non accessible, on rend visible le message d'avertissement
                resultNotAccessibleCont.setVisibilityAllowed(isOneResultNotAccessible);
                return listResults;
            }
        };

        // Contenu tableaux resultats
        dosagesTable.add(new ListView<>("Dosage.resultats.List", listResultsModel) {
            @Override
            protected void populateItem(ListItem<ResultatDosageForUI> item) {
                if (item.getIndex() % 2 == 1) {
                    item.add(new AttributeModifier("class", "odd"));
                }

                final IModel<ResultatDosageForUI> resultatModel = item.getModel();
                final ResultatDosageForUI resultat = new ResultatDosageForUI(item.getModelObject());

                // Colonnes
                item.add(new Label("Dosage.resultats.List.analyseNb", new PropertyModel<String>(resultat, "analyseNb")));
                item.add(new Label("Dosage.resultats.List.refEchantillon", new PropertyModel<String>(resultat, "reference.ref")));
                item.add(new Label("Dosage.resultats.List.concMasse", new PropertyModel<String>(resultat,"concMasse")));
                item.add(new Label("Dosage.resultats.List.uniteConcMasse", new PropertyModel<String>(resultat, "uniteConcMasse")));
                item.add(new Label("Dosage.resultats.List.compose", new PropertyModel<String>(resultat,"composeDose")));
                item.add(new Label("Dosage.resultats.List.molecule", new PropertyModel<String>(resultat,"moleculeDosee.nomCommun")));
                item.add(new Label("Dosage.resultats.List.valeur", new DisplayDecimalPropertyModel(resultat, "valeur",
                        DecimalDisplFormat.LARGE, getSession().getLocale())));
                item.add(new Label("Dosage.resultats.List.SD", new DisplayDecimalPropertyModel(resultat, "SD",
                        DecimalDisplFormat.LARGE, getSession().getLocale())));
                item.add(new Label("Dosage.resultats.List.supSeuil", new DisplayBooleanPropertyModel(resultatModel,
                        "supSeuil", (TemplatePage) this.getPage())).add(new ReplaceEmptyLabelBehavior()));

                item.add(new Label("Dosage.resultats.List.erreur", new PropertyModel<String>(resultat, "erreur.nom")));
                // info-bulle comprenant la description de l'erreur
                item.add(new SimpleTooltipPanel("Dosage.resultats.List.erreur.info", new PropertyModel<String>(
                        resultat, "erreur.description")) {
                    /** {@inheritDoc} */
                    @Override
                    public boolean isVisible() {
                        return resultat.getErreur() != null;
                    }
                });
            }
        });
    }

    /**
     * Redirection vers une autre page
     */
    private void redirect() {
        callerPage.responsePage(this);
    }
}
