/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.dosage;

import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.Extrait;
import nc.ird.cantharella.data.model.Fraction;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.DocumentSearchResult;
import nc.ird.cantharella.service.services.DocumentService;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.columns.ComposePropertyColumn;
import nc.ird.cantharella.web.pages.columns.LinkProduitOrLotPropertyColumn;
import nc.ird.cantharella.web.pages.columns.TaxonomyPropertyColumn;
import nc.ird.cantharella.web.pages.domain.ImportFormPanel;
import nc.ird.cantharella.web.pages.domain.document.DocumentTooltipColumn;
import nc.ird.cantharella.web.pages.domain.document.panel.DocumentLinkPanel;
import nc.ird.cantharella.web.pages.domain.extraction.ReadExtractionPage;
import nc.ird.cantharella.web.pages.domain.lot.ReadLotPage;
import nc.ird.cantharella.web.pages.domain.molecule.ReadMoleculePage;
import nc.ird.cantharella.web.pages.domain.purification.ReadPurificationPage;
import nc.ird.cantharella.web.pages.model.EntityModel;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.columns.BooleanPropertyColumn;
import nc.ird.cantharella.web.utils.columns.CheckBoxColumn;
import nc.ird.cantharella.web.utils.columns.DecimalPropertyColumn;
import nc.ird.cantharella.web.utils.columns.LinkPropertyColumn;
import nc.ird.cantharella.web.utils.columns.LinkableImagePropertyColumn;
import nc.ird.cantharella.web.utils.columns.MapValuePropertyColumn;
import nc.ird.cantharella.web.utils.data.TableExportToolbar;
import nc.ird.cantharella.web.utils.data.ZipTableExportToolbar;
import nc.ird.cantharella.web.utils.models.DisplayDecimalPropertyModel.DecimalDisplFormat;
import nc.ird.cantharella.web.utils.models.DocumentListDataProvider;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.TextFilteredPropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.ChainingModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Page de consultation des résultats des tests biologiques
 * 
 * @author Adrien Cheype
 */
@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public final class ListDosagePage extends TemplatePage {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ListDosagePage.class);

    /** Service : test biologique */
    @SpringBean
    private DosageService dosageService;

    /** Service : imports */
    @SpringBean(name="importResultatDosageServiceImpl")
    private ImportService importService;

    private FileUploadField fileUpload;

    static final Map<Integer, ResultatDosage> datas = new HashMap<>();
    static final Map<Integer, AnalyseDosage> dataAnalyses = new HashMap<>();

    /**
     * Constructeur
     */
    public ListDosagePage() {
        super(ListDosagePage.class);

        final CallerPage currentPage = new CallerPage(ListDosagePage.class);

        add(new Link<Void>("ListDosagePage.NewDosage") {
            @Override
            public void onClick() {
                setResponsePage(new ManageDosagePage(currentPage, true));
            }
        });

        Utilisateur utilisateur = getSession().getUtilisateur();

        // Liste des résultats de tests biologiques
        final List<ResultatDosage> resDosages = dosageService.listResultatsDosage(utilisateur);
        List<ResultatDosageForUI> resDosagesForUI = resDosages.stream().map(ResultatDosageForUI::new).collect(Collectors.toList());
        LoadableDetachableSortableListDataProvider<ResultatDosageForUI> resDosagesDataProvider = new LoadableDetachableSortableListDataProvider<>(
                resDosagesForUI, this.getSession().getLocale());

        // Upload
        ImportFormPanel form = new ImportFormPanel(getResource() + ".importForm", getResource(), utilisateur,
                () -> {
                    List<ResultatDosage> resDosagesTemp = dosageService.listResultatsDosage(utilisateur);
                    resDosagesDataProvider.setList(resDosagesTemp.stream().map(ResultatDosageForUI::new).collect(Collectors.toList()));
                });
        add(form);

        // On englobe le "DataView" dans un composant neutre que l'on pourra
        // rafraichir quand la liste évoluera
        final MarkupContainer resDosagesRefresh = new WebMarkupContainer("ListDosagePage.ResultatsDosage.Refresh");
        resDosagesRefresh.setOutputMarkupId(true);
        add(resDosagesRefresh);

        DataTable<ResultatDosageForUI, String> resDosagesDataTable = initDosagesDataTable(this,
                "ListDosagePage.ResultatsDosage", currentPage, resDosagesDataProvider, dosageService);
        resDosagesRefresh.add(resDosagesDataTable);
    }

    /**
     * Init data table with testsbio list.
     * 
     * This method is static to be reused in several places.
     * 
     * @param templatePage parent page
     * @param componentId data table id
     * @param callerPage caller page
     * @param dosageService test bio service
     * @return data table component
     */
    public static DataTable<ResultatDosageForUI, String> initDosagesDataTable(final TemplatePage templatePage,
                                                                         final String componentId,
                                                                         final CallerPage callerPage,
                                                                         LoadableDetachableSortableListDataProvider<ResultatDosageForUI> resDosagesDataProvider,
                                                                         final DosageService dosageService) {

        List<IColumn<ResultatDosageForUI, String>> columns = new ArrayList<>();

        columns.add(new LinkableImagePropertyColumn<>("images/read.png", templatePage
                .getStringModel("Read"), templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<ResultatDosageForUI>> item, String componentId,
                    IModel<ResultatDosageForUI> model) {
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                templatePage.setResponsePage(new ReadDosagePage(datas.get(rowIndex).getDosage().getIdDosage(),
                        callerPage));
            }
        });

        columns.add(new TextFilteredPropertyColumn<ResultatDosageForUI, String, String>(templatePage
                .getStringModel("Dosage.acronyme2"), "dosage.methode.acronyme", "dosage.methode.acronyme"));

        columns.add(new DecimalPropertyColumn<>(templatePage
                .getStringModel("ResultatDosage.concMasse2"), "concMasse", "concMasse", DecimalDisplFormat.SMALL,
                templatePage.getLocale()));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("ResultatDosage.uniteConcMasse2"),
                "uniteConcMasse", "uniteConcMasse"));

        columns.add(new LinkProduitOrLotPropertyColumn<>(templatePage
                .getStringModel("ResultatDosage.refEchantillon"), "reference", "reference", templatePage) {
            @Override
            public void onClickIfExtrait(Extrait extrait) {
                templatePage.setResponsePage(new ReadExtractionPage(extrait.getExtraction().getIdExtraction(),
                        callerPage));
            }

            @Override
            public void onClickIfFraction(Fraction fraction) {
                templatePage.setResponsePage(new ReadPurificationPage(fraction.getPurification().getIdPurification(),
                        callerPage));
            }

            @Override
            public void onClickIfLot(Lot lot) {
                templatePage.setResponsePage(new ReadLotPage(lot.getIdLot(),callerPage));
            }
        });

        columns.add(new PropertyColumn<>(templatePage
                .getStringModel("ResultatDosage.compose"), "composeDose", "composeDose"));

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("ResultatDosage.molecule"),
                "moleculeDosee.nomCommun", "moleculeDosee.nomCommun", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<ResultatDosageForUI>> item, String componentId,
                                IModel<ResultatDosageForUI> model) {
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                templatePage.setResponsePage(new ReadMoleculePage(datas.get(rowIndex).getMoleculeDosee().getIdMolecule(),
                        callerPage));
            }
        });

        columns.add(new DecimalPropertyColumn<>(templatePage
                .getStringModel("Dosage.valeur"), "valeur", "valeur", DecimalDisplFormat.LARGE, templatePage
                .getLocale()));

        columns.add(new DecimalPropertyColumn<>(templatePage
                .getStringModel("ResultatDosage.SD"), "SD", "SD", DecimalDisplFormat.LARGE, templatePage
                .getLocale()));

        columns.add(new PropertyColumn<>(templatePage
                .getStringModel("Dosage.uniteResultat2"), "dosage.methode.unite", "dosage.methode.unite"));

        columns.add(new BooleanPropertyColumn<>(templatePage.getStringModel("ResultatDosage.supSeuil"),
                "supSeuil", "supSeuil", templatePage));

        columns.add(new TaxonomyPropertyColumn<>(
                templatePage.getStringModel("Specimen.famille"), "lotSource.specimenRef.famille",
                "lotSource.specimenRef.famille"));

        columns.add(new TaxonomyPropertyColumn<>(templatePage.getStringModel("Specimen.genre"),
                "lotSource.specimenRef.genre", "lotSource.specimenRef.genre"));

        columns.add(new TaxonomyPropertyColumn<>(templatePage.getStringModel("Specimen.espece"),
                "lotSource.specimenRef.espece", "lotSource.specimenRef.espece"));

        columns.add(new MapValuePropertyColumn<>(templatePage
                .getStringModel("Campagne.codePays"), "lotSource.campagne.codePays", "lotSource.campagne.codePays",
                WebContext.COUNTRIES.get(templatePage.getSession().getLocale())));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("ResultatDosage.analyseNb"),
                "analyseNb", "analyseNb"));

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("Dosage.ref"),
                "dosage.ref", "dosage.ref", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<ResultatDosageForUI>> item, String componentId,
                    IModel<ResultatDosageForUI> model) {
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                templatePage.setResponsePage(new ReadDosagePage(datas.get(rowIndex).getDosage().getIdDosage(),
                        callerPage));
            }
        });

        columns.add(new DocumentTooltipColumn<>(templatePage
                .getStringModel("ListDocumentsPage.AttachedDocuments")) {
            @Override
            public void onClick(Item<ICellPopulator<ResultatDosageForUI>> item, IModel<ResultatDosageForUI> model) {
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                templatePage.setResponsePage(new ReadDosagePage(datas.get(rowIndex).getDosage().getIdDosage(),
                        callerPage));
            }
        });

        columns.add(new LinkableImagePropertyColumn<>("images/edit.png", templatePage
                .getStringModel("Update"), templatePage.getStringModel("Update")) {
            // pas de lien d'édition si l'utilisateur n'a pas les droits
            @Override
            public void populateItem(Item<ICellPopulator<ResultatDosageForUI>> item, String componentId,
                    IModel<ResultatDosageForUI> model) {
                //init dosageId for onClick
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                datas.put(rowIndex, model.getObject());
                if (dosageService.updateOrdeleteDosageEnabled(model.getObject().getDosage(), templatePage
                        .getSession().getUtilisateur())) {
                    item.add(new LinkableImagePanel(item, componentId, model));
                } else {
                    // label vide
                    item.add(new Label(componentId));
                }
            }

            @Override
            public void onClick(Item<ICellPopulator<ResultatDosageForUI>> item, String componentId,
                    IModel<ResultatDosageForUI> model) {
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                templatePage.setResponsePage(new ManageDosagePage(datas.get(rowIndex).getDosage().getIdDosage(), callerPage));
            }
        });

        final DataTable<ResultatDosageForUI, String> resDosagesDataTable = new AjaxFallbackDefaultDataTable<>(
                componentId, columns, resDosagesDataProvider, WebContext.ROWS_PER_PAGE);
        resDosagesDataTable.addBottomToolbar(new TableExportToolbar(resDosagesDataTable, "dosages", templatePage
                .getSession().getLocale()));

        return resDosagesDataTable;
    }


    /**
     * Init data table with testsbio list.
     *
     * This method is static to be reused in several places.
     *
     * @param templatePage parent page
     * @param componentId data table id
     * @param callerPage caller page
     * @param dosageService test bio service
     * @return data table component
     */
    public static DataTable<AnalyseDosage, String> initAnalyseDosagesDataTable(final TemplatePage templatePage,
                                                                              final String componentId,
                                                                              final CallerPage callerPage,
                                                                              LoadableDetachableSortableListDataProvider<AnalyseDosage> analyseDosagesDataProvider,
                                                                              final DosageService dosageService,
                                                                              final List<String> composes) {

        List<IColumn<AnalyseDosage, String>> columns = new ArrayList<>();

        columns.add(new LinkableImagePropertyColumn<>("images/read.png", templatePage
                .getStringModel("Read"), templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<AnalyseDosage>> item, String componentId,
                                IModel<AnalyseDosage> model) {
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                templatePage.setResponsePage(new ReadDosagePage(datas.get(rowIndex).getDosage().getIdDosage(),
                        callerPage));
            }
        });

        columns.add(new TextFilteredPropertyColumn<AnalyseDosage, String, String>(templatePage
                .getStringModel("Dosage.acronyme2"), "dosage.methode.acronyme", "dosage.methode.acronyme"));

        columns.add(new LinkProduitOrLotPropertyColumn<>(templatePage
                .getStringModel("ResultatDosage.refEchantillon"), "reference", "reference", templatePage) {
            @Override
            public void onClickIfExtrait(Extrait extrait) {
                templatePage.setResponsePage(new ReadExtractionPage(extrait.getExtraction().getIdExtraction(),
                        callerPage));
            }

            @Override
            public void onClickIfFraction(Fraction fraction) {
                templatePage.setResponsePage(new ReadPurificationPage(fraction.getPurification().getIdPurification(),
                        callerPage));
            }

            @Override
            public void onClickIfLot(Lot lot) {
                templatePage.setResponsePage(new ReadLotPage(lot.getIdLot(),callerPage));
            }
        });

        //compose n'est jamais null (initialisé à liste vide)
        for (int i=0; i< composes.size(); i++) {
            columns.add(
                new ComposePropertyColumn<>(new ChainingModel<>(composes.get(i)),
                        "valeurForCompose." + composes.get(i), "lotSource.specimenRef.espece", composes, i));
        }

        columns.add(new PropertyColumn<>(templatePage.getStringModel("ResultatDosage.uniteConcMasse2"),
                "dosage.methode.unite", "dosage.methode.unite"));
        columns.add(new TaxonomyPropertyColumn<>(
                templatePage.getStringModel("Specimen.famille"), "lotSource.specimenRef.famille",
                "lotSource.specimenRef.famille"));

        columns.add(new TaxonomyPropertyColumn<>(templatePage.getStringModel("Specimen.genre"),
                "lotSource.specimenRef.genre", "lotSource.specimenRef.genre"));

        columns.add(new TaxonomyPropertyColumn<>(templatePage.getStringModel("Specimen.espece"),
                "lotSource.specimenRef.espece", "lotSource.specimenRef.espece"));

        columns.add(new MapValuePropertyColumn<>(templatePage
                .getStringModel("Campagne.codePays"), "lotSource.campagne.codePays", "lotSource.campagne.codePays",
                WebContext.COUNTRIES.get(templatePage.getSession().getLocale())));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("ResultatDosage.analyseNb"),
                "analyseNb", "analyseNb"));

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("Dosage.ref"),
                "dosage.ref", "dosage.ref", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<AnalyseDosage>> item, String componentId,
                                IModel<AnalyseDosage> model) {
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                templatePage.setResponsePage(new ReadDosagePage(datas.get(rowIndex).getDosage().getIdDosage(),
                        callerPage));
            }
        });

        /*columns.add(new DocumentTooltipColumn<>(templatePage
                .getStringModel("ListDocumentsPage.AttachedDocuments")) {
            @Override
            public void onClick(IModel<AnalyseDosage> model) {
                templatePage.setResponsePage(new ReadDosagePage(model.getObject().getDosage().getIdDosage(), callerPage));
            }
        });*/

        columns.add(new LinkableImagePropertyColumn<>("images/edit.png", templatePage
                .getStringModel("Update"), templatePage.getStringModel("Update")) {
            // pas de lien d'édition si l'utilisateur n'a pas les droits
            @Override
            public void populateItem(Item<ICellPopulator<AnalyseDosage>> item, String componentId,
                                     IModel<AnalyseDosage> model) {
                //init dosageId for onClick
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                dataAnalyses.put(rowIndex, model.getObject());
                if (dosageService.updateOrdeleteDosageEnabled(model.getObject().getDosage(), templatePage
                        .getSession().getUtilisateur())) {
                    item.add(new LinkableImagePanel(item, componentId, model));
                } else {
                    // label vide
                    item.add(new Label(componentId));
                }
            }

            @Override
            public void onClick(Item<ICellPopulator<AnalyseDosage>> item, String componentId,
                                IModel<AnalyseDosage> model) {
                Item rowItem = item.findParent( Item.class );
                int rowIndex = rowItem.getIndex();
                templatePage.setResponsePage(new ManageDosagePage(datas.get(rowIndex).getDosage().getIdDosage(), callerPage));
            }
        });

        final DataTable<AnalyseDosage, String> resDosagesDataTable = new AjaxFallbackDefaultDataTable<>(
                componentId, columns, analyseDosagesDataProvider, WebContext.ROWS_PER_PAGE);
        resDosagesDataTable.addBottomToolbar(new TableExportToolbar(resDosagesDataTable, "dosages", templatePage
                .getSession().getLocale()));

        return resDosagesDataTable;
    }


    public static DataTable<Document, String> initDosagesDocumentDataTable(final TemplatePage templatePage,
                                                                            final String componentId,
                                                                            final CallerPage callerPage,
                                                                            DocumentSearchResult documentSearchResult,
                                                                            DocumentService documentService) {

        List<Document> dosagesDocuments = documentSearchResult.getDosagesDocs();
        Map<Integer, Dosage> dosagesMap = documentSearchResult.getDosages();

        LoadableDetachableSortableListDataProvider<Document> dosagesDataProvider = new DocumentListDataProvider(
                dosagesDocuments, templatePage.getSession().getLocale(), DocumentListDataProvider.TableType.TESTBIO,
                dosagesMap);

        List<IColumn<Document, String>> columns = new ArrayList<>();
        columns.add(new LinkableImagePropertyColumn<>("images/read.png", templatePage
                .getStringModel("Read"), templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                templatePage.setResponsePage(new ReadDosagePage(model.getObject().getDosageId(), callerPage));
            }
        });

        columns.add(new PropertyColumn<>(templatePage.getStringModel("MethodeDosage.acronyme"), "cible",
                "cible") {
            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                Dosage dosage = dosagesMap.get(model.getObject().getDosageId());
                item.add(new Label(componentId, dosage.getMethode().getAcronyme()));
            }
        });

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("Dosage.ref"), "ref", "ref") {

            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                Dosage dosage = dosagesMap.get(model.getObject().getDosageId());
                item.add(new LinkPanel(item, componentId, model, new Model<>() {
                    @Override
                    public String getObject() {
                        return dosage.getRef();
                    }
                }));
            }

            @Override
            public void onClick(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                templatePage.setResponsePage(new ReadDosagePage(model.getObject().getDosageId(), callerPage));
            }
        });

        columns.add(new PropertyColumn<>(
                templatePage.getStringModel("Dosage.date"), "date", "date") {
            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                Dosage dosage = dosagesMap.get(model.getObject().getDosageId());
                item.add(new Label(componentId, dosage.getDate()));
            }
        });

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Dosage.methode"), "methode",
                "methode") {
            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                Dosage dosage = dosagesMap.get(model.getObject().getDosageId());
                item.add(new Label(componentId, dosage.getMethode().getNom()));
            }
        });

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.titre"), "titre",
                "titre"));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.type"), "type",
                "typeDocument"));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.apercu"), "apercu",
                "apercu") {
            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                item.add(new DocumentLinkPanel(componentId, new EntityModel<>(model.getObject())));
            }
        });

        columns.add(new CheckBoxColumn<>((IModel<String>) () -> null));

        final DataTable<Document, String> dosagesDataTable = new AjaxFallbackDefaultDataTable<>(
                componentId, columns, dosagesDataProvider, WebContext.ROWS_PER_PAGE);
        dosagesDataTable.addBottomToolbar(new ZipTableExportToolbar(dosagesDataTable, "dosages", documentService));
        return dosagesDataTable;
    }
}
