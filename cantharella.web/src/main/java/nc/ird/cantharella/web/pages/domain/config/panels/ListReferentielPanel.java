package nc.ird.cantharella.web.pages.domain.config.panels;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.EntreeReferentiel;
import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.web.pages.domain.ImportFormPanel;
import nc.ird.cantharella.web.pages.domain.config.ManageUniteConcMassePage;
import nc.ird.cantharella.web.pages.domain.dosage.ManageDosagePage;
import nc.ird.cantharella.web.pages.domain.dosage.ResultatDosageForUI;
import nc.ird.cantharella.web.utils.behaviors.JSConfirmationBehavior;
import nc.ird.cantharella.web.utils.columns.LinkableImagePropertyColumn;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import nc.ird.cantharella.web.utils.security.AuthSession;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListReferentielPanel extends Panel {

    @SpringBean
    private RemedeService remedeService;

    private final IModel<EntreeReferentiel.Referentiel> referentielModel;

    List<EntreeReferentiel> entreeReferentiels;

    /**
     * Constructor
     *
     * @param id The panel ID
     */
    public ListReferentielPanel(String id) {
        super(id);

        referentielModel = new Model<>();

        // On englobe le "DataView" dans un composant neutre que l'on pourra
        // rafraichir quand la liste évoluera
        final MarkupContainer referentielRefresh = new WebMarkupContainer("ListReferentielPanel.Referentiel.Refresh");
        referentielRefresh.setOutputMarkupId(true);
        add(referentielRefresh);

        final Form<Void> formView = new Form<>("Form");

        DropDownChoice<EntreeReferentiel.Referentiel> referentielInput =
                new DropDownChoice<>("Referentiel.type", referentielModel, List.of(EntreeReferentiel.Referentiel.values()));
        referentielInput.setNullValid(false);
        formView.add(referentielInput);

        add(formView);

        // Liste des entreeReferentiels
        entreeReferentiels = new ArrayList<>();
        LoadableDetachableSortableListDataProvider<EntreeReferentiel> entreesReferentielDataProvider =
                new LoadableDetachableSortableListDataProvider<>(entreeReferentiels, getSession().getLocale());

        referentielInput.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                if (referentielInput.getModelObject() != null) {
                    entreesReferentielDataProvider.setList(remedeService.listEntreeReferentiels(referentielInput.getModelObject()).stream().distinct().collect(Collectors.toList()));
                }
                // refresh the station choices component
                target.add(referentielRefresh);
            }
        });

        Utilisateur utilisateur = ((AuthSession) super.getSession()).getUtilisateur();

        // Upload
        ImportFormPanel form = new ImportFormPanel("ListReferentielPanel.importForm", "ListReferentielPanel", utilisateur,
                () -> entreesReferentielDataProvider.setList(remedeService.listEntreeReferentiels(referentielInput.getModelObject())));
        add(form);

        referentielRefresh.add(new DataView<>("ListReferentielPanel.Referentiel", entreesReferentielDataProvider) {
            @Override
            protected void populateItem(Item<EntreeReferentiel> item) {
                item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));

                EntreeReferentiel entreeReferentiel = item.getModelObject();

                // Colonnes
                item.add(new Label("ListReferentielPanel.Referentiel.code", new PropertyModel<String>(entreeReferentiel, "code")));
                item.add(new Label("ListReferentielPanel.Referentiel.valeur", new PropertyModel<String>(entreeReferentiel, "valeur")));

                // Action : Delete entree referentiel
                Link<EntreeReferentiel> updateLink = new Link<>(
                        "ListReferentielPanel.Referentiel.Delete", new Model<>(item.getModelObject())) {
                    @Override
                    public void onClick() {
                        try {
                            remedeService.deleteEntreeReferentiel(item.getModelObject());
                        } catch (DataConstraintException e) {
                            throw new RuntimeException(e);
                        }
                        entreesReferentielDataProvider.setList(remedeService.listEntreeReferentiels(referentielInput.getModelObject()));
                    }
                };

                updateLink.add(new JSConfirmationBehavior(new StringResourceModel("Confirm", this, null)));
                updateLink.setVisibilityAllowed(remedeService.canDeleteEntreeReferentiel(item.getModelObject()));
                item.add(updateLink);
            }
        });
    }

}
