package nc.ird.cantharella.web.utils.models;

import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Molecule;
import nc.ird.cantharella.data.model.Purification;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.TestBio;
import nc.ird.cantharella.data.model.utils.AbstractModel;
import nc.ird.cantharella.web.config.WebContext;
import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DocumentListDataProvider extends LoadableDetachableSortableListDataProvider<Document> {

    public static final String NOM = "nom";

    /**
     * Table type
     */
    public enum TableType {
        CAMPAGNE,
        STATION,
        LOT,
        SPECIMEN,
        EXTRACTION,
        PURIFICATION,
        TESTBIO,
        MOLECULE,
        DOSAGE,
        PERSONNE_INTERROGEE,
        REMEDE
    }

    protected TableType type;

    protected Map<Integer, ? extends AbstractModel> objects;

    final static String TITRE_PROPERTY = "titre";
    final static String TYPE_PROPERTY = "type";
    final static String APERCU_PROPERTY = "apercu";

    public DocumentListDataProvider(List<Document> list, Locale locale, TableType type, Map<Integer, ? extends AbstractModel> objects) {
        super (list, locale);
        this.type = type;
        this.objects = objects;
    }

    @Override
    public Iterator<Document> iterator(long first, long count) {

        if (getSort() != null && !StringUtils.isEmpty(getSort().getProperty())) {
            list.sort((o1, o2) -> {
                Comparable<?> c1 = null;
                Object c2 = null;
                try {
                    if (getSort().getProperty().endsWith(TITRE_PROPERTY)) {
                        c1 = o1.getTitre();
                        c2 = o2.getTitre();
                    } else if (getSort().getProperty().endsWith(TYPE_PROPERTY)) {
                        c1 = o1.getTypeDocument().getNom();
                        c2 = o2.getTypeDocument().getNom();
                    } else if (getSort().getProperty().endsWith(APERCU_PROPERTY)) {
                        c1 = o1.getFileName();
                        c2 = o2.getFileName();
                    } else if (type.equals(TableType.CAMPAGNE)) {
                        if (getSort().getProperty().endsWith(NOM)) {
                            c1 = ((Campagne)objects.get(o1.getCampagneId())).getNom();
                            c2 = ((Campagne)objects.get(o2.getCampagneId())).getNom();
                        }
                        if (getSort().getProperty().endsWith(CODE_PAYS_PROPERTY)) {
                            c1 = ((Campagne)objects.get(o1.getCampagneId())).getCodePays();
                            c2 = ((Campagne)objects.get(o2.getCampagneId())).getCodePays();
                        }
                    } else if (type.equals(TableType.STATION)) {
                        if (getSort().getProperty().endsWith(NOM)) {
                            c1 = ((Station)objects.get(o1.getStationId())).getNom();
                            c2 = ((Station)objects.get(o2.getStationId())).getNom();
                        }
                        if (getSort().getProperty().endsWith("localite")) {
                            c1 = ((Station)objects.get(o1.getStationId())).getLocalite();
                            c2 = ((Station)objects.get(o2.getStationId())).getLocalite();
                        }
                        if (getSort().getProperty().endsWith(CODE_PAYS_PROPERTY)) {
                            c1 = ((Station)objects.get(o1.getStationId())).getCodePays();
                            c2 = ((Station)objects.get(o2.getStationId())).getCodePays();
                        }
                    } else if (type.equals(TableType.SPECIMEN)) {
                        if (getSort().getProperty().endsWith("ref")) {
                            c1 = ((Specimen)objects.get(o1.getSpecimenId())).getRef();
                            c2 = ((Specimen)objects.get(o2.getSpecimenId())).getRef();
                        }
                        if (getSort().getProperty().endsWith("embranchement")) {
                            c1 = ((Specimen)objects.get(o1.getSpecimenId())).getEmbranchement();
                            c2 = ((Specimen)objects.get(o2.getSpecimenId())).getEmbranchement();
                        }
                        if (getSort().getProperty().endsWith("genre")) {
                            c1 = ((Specimen)objects.get(o1.getSpecimenId())).getGenre();
                            c2 = ((Specimen)objects.get(o2.getSpecimenId())).getGenre();
                        }
                        if (getSort().getProperty().endsWith("espece")) {
                            c1 = ((Specimen)objects.get(o1.getSpecimenId())).getEspece();
                            c2 = ((Specimen)objects.get(o2.getSpecimenId())).getEspece();
                        }
                        if (getSort().getProperty().endsWith("station2")) {
                            c1 = ((Specimen)objects.get(o1.getSpecimenId())).getStation().getNom();
                            c2 = ((Specimen)objects.get(o2.getSpecimenId())).getStation().getNom();
                        }
                    } else if (type.equals(TableType.LOT)) {
                        if (getSort().getProperty().endsWith("ref")) {
                            c1 = ((Lot)objects.get(o1.getLotId())).getRef();
                            c2 = ((Lot)objects.get(o2.getLotId())).getRef();
                        }
                        if (getSort().getProperty().endsWith("embranchement")) {
                            c1 = ((Lot)objects.get(o1.getLotId())).getSpecimenRef().getEmbranchement();
                            c2 = ((Lot)objects.get(o2.getLotId())).getSpecimenRef().getEmbranchement();
                        }
                        if (getSort().getProperty().endsWith("genre")) {
                            c1 = ((Lot)objects.get(o1.getLotId())).getSpecimenRef().getGenre();
                            c2 = ((Lot)objects.get(o2.getLotId())).getSpecimenRef().getGenre();
                        }
                        if (getSort().getProperty().endsWith("espece")) {
                            c1 = ((Lot)objects.get(o1.getLotId())).getSpecimenRef().getEspece();
                            c2 = ((Lot)objects.get(o2.getLotId())).getSpecimenRef().getEspece();
                        }
                        if (getSort().getProperty().endsWith("station2")) {
                            c1 = ((Lot)objects.get(o1.getLotId())).getSpecimenRef().getStation().getNom();
                            c2 = ((Lot)objects.get(o2.getLotId())).getSpecimenRef().getStation().getNom();
                        }
                        if (getSort().getProperty().endsWith("campagne")) {
                            c1 = ((Lot)objects.get(o1.getLotId())).getCampagne().getNom();
                            c2 = ((Lot)objects.get(o2.getLotId())).getCampagne().getNom();
                        }
                    } else if (type.equals(TableType.EXTRACTION)) {
                        if (getSort().getProperty().endsWith("lot.ref")) {
                            c1 = ((Extraction)objects.get(o1.getExtractionId())).getLot().getRef();
                            c2 = ((Extraction)objects.get(o2.getExtractionId())).getLot().getRef();
                        }
                        if (getSort().getProperty().endsWith("extraction.ref")) {
                            c1 = ((Extraction)objects.get(o1.getExtractionId())).getRef();
                            c2 = ((Extraction)objects.get(o2.getExtractionId())).getRef();
                        }
                        if (getSort().getProperty().endsWith("date")) {
                            c1 = ((Extraction)objects.get(o1.getExtractionId())).getDate();
                            c2 = ((Extraction)objects.get(o2.getExtractionId())).getDate();
                        }
                        if (getSort().getProperty().endsWith("methode")) {
                            c1 = ((Extraction)objects.get(o1.getExtractionId())).getMethode().getNom();
                            c2 = ((Extraction)objects.get(o2.getExtractionId())).getMethode().getNom();
                        }
                        if (getSort().getProperty().endsWith("campagne")) {
                            c1 = ((Extraction)objects.get(o1.getExtractionId())).getLot().getCampagne().getNom();
                            c2 = ((Extraction)objects.get(o2.getExtractionId())).getLot().getCampagne().getNom();
                        }
                    } else if (type.equals(TableType.PURIFICATION)) {
                        if (getSort().getProperty().endsWith("lot.ref")) {
                            c1 = ((Purification)objects.get(o1.getPurificationId())).getLotSource().getRef();
                            c2 = ((Purification)objects.get(o2.getPurificationId())).getLotSource().getRef();
                        }
                        if (getSort().getProperty().endsWith("testbio.ref")) {
                            c1 = ((Purification)objects.get(o1.getPurificationId())).getProduit().getRef();
                            c2 = ((Purification)objects.get(o2.getPurificationId())).getProduit().getRef();
                        }
                        if (getSort().getProperty().endsWith("purification.ref")) {
                            c1 = ((Purification)objects.get(o1.getPurificationId())).getRef();
                            c2 = ((Purification)objects.get(o2.getPurificationId())).getRef();
                        }
                        if (getSort().getProperty().endsWith("date")) {
                            c1 = ((Purification)objects.get(o1.getPurificationId())).getDate();
                            c2 = ((Purification)objects.get(o2.getPurificationId())).getDate();
                        }
                        if (getSort().getProperty().endsWith("methode")) {
                            c1 = ((Purification)objects.get(o1.getPurificationId())).getMethode().getNom();
                            c2 = ((Purification)objects.get(o2.getPurificationId())).getMethode().getNom();
                        }
                    } else if (type.equals(TableType.TESTBIO)) {
                        if (getSort().getProperty().endsWith("cible")) {
                            c1 = ((TestBio)objects.get(o1.getResultatTestBioId())).getMethode().getCible();
                            c2 = ((TestBio)objects.get(o2.getResultatTestBioId())).getMethode().getCible();
                        }
                        if (getSort().getProperty().endsWith("ref")) {
                            c1 = ((TestBio)objects.get(o1.getResultatTestBioId())).getRef();
                            c2 = ((TestBio)objects.get(o2.getResultatTestBioId())).getRef();
                        }
                        if (getSort().getProperty().endsWith("date")) {
                            c1 = ((TestBio)objects.get(o1.getResultatTestBioId())).getDate();
                            c2 = ((TestBio)objects.get(o2.getResultatTestBioId())).getDate();
                        }
                        if (getSort().getProperty().endsWith("methode")) {
                            c1 = ((TestBio)objects.get(o1.getResultatTestBioId())).getMethode().getNom();
                            c2 = ((TestBio)objects.get(o2.getResultatTestBioId())).getMethode().getNom();
                        }
                    } else if (type.equals(TableType.MOLECULE)) {
                        if (getSort().getProperty().endsWith("idMolecule")) {
                            c1 = ((Molecule)objects.get(o1.getMoleculeId())).getIdMolecule();
                            c2 = ((Molecule)objects.get(o2.getMoleculeId())).getIdMolecule();
                        }
                        if (getSort().getProperty().endsWith("nomCommun")) {
                            c1 = ((Molecule)objects.get(o1.getMoleculeId())).getNomCommun();
                            c2 = ((Molecule)objects.get(o2.getMoleculeId())).getNomCommun();
                        }
                        if (getSort().getProperty().endsWith("familleChimique")) {
                            c1 = ((Molecule)objects.get(o1.getMoleculeId())).getFamilleChimique();
                            c2 = ((Molecule)objects.get(o2.getMoleculeId())).getFamilleChimique();
                        }
                        if (getSort().getProperty().endsWith("formuleBrute")) {
                            c1 = ((Molecule)objects.get(o1.getMoleculeId())).getFormuleBrute();
                            c2 = ((Molecule)objects.get(o2.getMoleculeId())).getFormuleBrute();
                        }
                        if (getSort().getProperty().endsWith("masseMolaire")) {
                            c1 = ((Molecule)objects.get(o1.getMoleculeId())).getMasseMolaire();
                            c2 = ((Molecule)objects.get(o2.getMoleculeId())).getMasseMolaire();
                        }
                    }

                    // Exceptions
                    // Countries are sorted by country name, not by country code
                    if (getSort().getProperty().endsWith(CODE_PAYS_PROPERTY)) {
                        if (c1 != null) {
                            c1 = WebContext.COUNTRIES.get(locale).get(c1);
                        }
                        if (c2 != null) {
                            c2 = WebContext.COUNTRIES.get(locale).get(c2);
                        }
                    }

                    return (getSort().isAscending() ? 1 : -1) * comparator.compare(c1, c2);
                } catch (Exception e) {
                    //LOG.error(e.getMessage(), e);
                    throw new UnexpectedException(e);
                }
            });
        }

        return list.subList((int) first, (int) Math.min(first + count, size())).iterator();
    }
}
