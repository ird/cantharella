/*
 * #%L
 * Cantharella :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.web.pages.domain.remede;

import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.data.model.Remede;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.DocumentSearchResult;
import nc.ird.cantharella.service.services.DocumentService;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.columns.TaxonomyPropertyColumn;
import nc.ird.cantharella.web.pages.domain.ImportFormPanel;
import nc.ird.cantharella.web.pages.domain.campagne.ReadCampagnePage;
import nc.ird.cantharella.web.pages.domain.document.DocumentTooltipColumn;
import nc.ird.cantharella.web.pages.domain.document.panel.DocumentLinkPanel;
import nc.ird.cantharella.web.pages.domain.lot.ReadLotPage;
import nc.ird.cantharella.web.pages.domain.personneInterrogee.ReadPersonneInterrogeePage;
import nc.ird.cantharella.web.pages.domain.specimen.ReadSpecimenPage;
import nc.ird.cantharella.web.pages.model.EntityModel;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.RemedeListBehavior;
import nc.ird.cantharella.web.utils.columns.CheckBoxColumn;
import nc.ird.cantharella.web.utils.columns.LinkPropertyColumn;
import nc.ird.cantharella.web.utils.columns.LinkableImagePropertyColumn;
import nc.ird.cantharella.web.utils.columns.MapValuePropertyColumn;
import nc.ird.cantharella.web.utils.columns.ShortDatePropertyColumn;
import nc.ird.cantharella.web.utils.data.TableExportToolbar;
import nc.ird.cantharella.web.utils.data.ZipTableExportToolbar;
import nc.ird.cantharella.web.utils.models.DocumentListDataProvider;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.*;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Page de consultation des résultats des tests biologiques
 *
 * @author Adrien Cheype
 */
@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public final class ListRemedePage extends TemplatePage {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ListRemedePage.class);

    /** Service : test biologique */
    @SpringBean
    private RemedeService remedeService;

    private FileUploadField fileUpload;

    /**
     * Constructeur
     */
    public ListRemedePage() {
        super(ListRemedePage.class);

        final CallerPage currentPage = new CallerPage(ListRemedePage.class);

        add(new Link<Void>("ListRemedePage.NewRemede") {
            @Override
            public void onClick() {
                setResponsePage(new ManageRemedePage(currentPage, true));
            }
        });

        Utilisateur utilisateur = getSession().getUtilisateur();

        // Liste des résultats de tests biologiques
        final List<IngredientRemede> ingredients = remedeService.listIngredients(utilisateur);
        LoadableDetachableSortableListDataProvider<IngredientRemede> ingredientsDataProvider = new LoadableDetachableSortableListDataProvider<>(
                ingredients, this.getSession().getLocale());

        SortableRemedeDataProvider ingredientsByCitationDataProvider = new SortableRemedeDataProvider(ingredients, this.getSession().getLocale(), remedeService);

        // Upload
        ImportFormPanel form = new ImportFormPanel(getResource() + ".importForm", getResource(), utilisateur,
                () -> {
                    List<IngredientRemede> ingredientsRefreshed = remedeService.listIngredients(utilisateur);
                    ingredientsDataProvider.setList(ingredientsRefreshed);
                    ingredientsByCitationDataProvider.setList(ingredientsRefreshed);
                });
        add(form);

        // On englobe le "DataView" dans un composant neutre que l'on pourra
        // rafraichir quand la liste évoluera
        final MarkupContainer ingredientsRefresh = new WebMarkupContainer("ListRemedePage.IngredientsRemede.Refresh");
        ingredientsRefresh.setOutputMarkupId(true);
        add(ingredientsRefresh);

        DataTable<IngredientRemede, String> resRemedesDataTable = initRemedeDataTable(this,
                "ListRemedePage.IngredientsRemede", currentPage, ingredientsDataProvider, remedeService);

        DataTable<IngredientRemede, String> citationRemedesDataTable = initRemedeDataTable(this,
                "ListRemedePage.CitationIngredientsRemede", currentPage, ingredientsByCitationDataProvider, remedeService);

        resRemedesDataTable.setOutputMarkupId(true); // pour l'update Ajax
        resRemedesDataTable.setOutputMarkupPlaceholderTag(true);

        citationRemedesDataTable.setOutputMarkupId(true); // pour l'update Ajax
        citationRemedesDataTable.setOutputMarkupPlaceholderTag(true);


        ingredientsRefresh.add(resRemedesDataTable);
        ingredientsRefresh.add(citationRemedesDataTable);

        Button citationButton = new Button("ListRemedePage.Citation") {};
        ingredientsRefresh.add(citationButton);

        Button alphanumericalButton = new Button("ListRemedePage.Alphanumerique") {};
        ingredientsRefresh.add(alphanumericalButton);

        add(new RemedeListBehavior());
    }

    /**
     * Init data table with Remede list.
     * This method is static to be reused in several places.
     *
     * @param templatePage parent page
     * @param componentId data table id
     * @param callerPage caller page
     * @param remedeService test bio service
     * @return data table component
     */
    public static DataTable<IngredientRemede, String> initRemedeDataTable(final TemplatePage templatePage,
                                                                           final String componentId, final CallerPage callerPage,
                                                                          LoadableDetachableSortableListDataProvider<IngredientRemede> remedesDataProvider,
                                                                           final RemedeService remedeService) {

        List<IColumn<IngredientRemede, String>> columns = new ArrayList<>();

        columns.add(new LinkableImagePropertyColumn<>("images/read.png", templatePage
                .getStringModel("Read"), templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadRemedePage(model.getObject().getRemede().getIdRemede(),
                        callerPage));
            }
        });

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("Remede.reference"),
                "remede.ref", "remede.ref", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                    IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadRemedePage(model.getObject().getRemede().getIdRemede(),
                        callerPage));
            }
        });

        columns.add(new ShortDatePropertyColumn<>(templatePage
                .getStringModel("Remede.date"), "remede.date", "remede.date",
                templatePage.getLocale()));

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("Remede.prescripteur"),
                "remede.prescripteur", "remede.prescripteur", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                    IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadPersonneInterrogeePage(model.getObject().getRemede().getPrescripteur().getIdPersonne(),
                        callerPage));
            }

            @Override
            public void populateItem(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                     IModel<IngredientRemede> model) {


                item.add(new LinkPanel(item, componentId, model, new Model<>() {
                    @Override
                    public String getObject() {
                        String name = "";

                        if (model.getObject().getRemede().getPrescripteur() != null) {
                            name = model.getObject().getRemede().getPrescripteur().getPrenom() + " "
                                    + model.getObject().getRemede().getPrescripteur().getNom();
                        }
                        return name;
                    }
                }));
            }
        });

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Remede.nom"),
                "remede.nom", "remede.nom"));

        columns.add(new MapValuePropertyColumn<>(
                templatePage.getStringModel("Remede.langue"), "remede.codeLangue", "remede.codeLangue", WebContext.LANGUAGES
                .get(templatePage.getSession().getLocale())));

       columns.add(new PropertyColumn<>(templatePage.getStringModel("Remede.indicationBiomedicale"),
                "remede.indicationBiomedicale", "remede.indicationBiomedicale"));

        //nb ingredients
        columns.add(new PropertyColumn<>(templatePage.getStringModel("Remede.nbIngredients"),
                "remede.ingredients.size", "remede.ingredients.size"));

        columns.add(new LinkPropertyColumn<>(templatePage
                .getStringModel("IngredientRemede.specimen2"),
                "specimen.ref", "specimen.ref",
                templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadSpecimenPage(model.getObject().getSpecimen().getIdSpecimen(),
                        callerPage));
            }
        });

        columns.add(new TaxonomyPropertyColumn<>( templatePage.getStringModel("Specimen.famille"),
                "specimen.famille", "specimen.famille"));

        columns.add(new TaxonomyPropertyColumn<>(templatePage.getStringModel("Specimen.genre"),
                "specimen.genre", "specimen.genre"));

        columns.add(new TaxonomyPropertyColumn<>(templatePage.getStringModel("Specimen.espece"),
                "specimen.espece", "specimen.espece"));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("IngredientRemede.nomVernaculaire"),
                "nomVernaculaire", "nomVernaculaire"));

        columns.add(new LinkPropertyColumn<>(templatePage
                .getStringModel("Remede.campagne"), "remede.campagne", "remede.campagne", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                    IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadCampagnePage(model.getObject().getRemede().getCampagne().getIdCampagne(),
                        callerPage));
            }
        });

        columns.add(new MapValuePropertyColumn<>(templatePage
                .getStringModel("Campagne.codePays"), "remede.campagne.codePays", "remede.campagne.codePays",
                WebContext.COUNTRIES.get(templatePage.getSession().getLocale())));

        columns.add(new DocumentTooltipColumn<>(templatePage.getStringModel("ListDocumentsPage.AttachedDocuments")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, IModel<IngredientRemede> model) {
                int idRemede = model.getObject().getRemede().getIdRemede();
                templatePage.setResponsePage(new ReadRemedePage(idRemede, callerPage));
            }
        });

        columns.add(new LinkableImagePropertyColumn<>("images/edit.png", templatePage
                .getStringModel("Update"), templatePage.getStringModel("Update")) {
            // pas de lien d'édition si l'utilisateur n'a pas les droits
            @Override
            public void populateItem(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                     IModel<IngredientRemede> model) {
                if (remedeService.updateOrdeleteRemedeEnabled(model.getObject().getRemede(), templatePage
                        .getSession().getUtilisateur())) {
                    item.add(new LinkableImagePanel(item, componentId, model));
                } else {
                    // label vide
                    item.add(new Label(componentId));
                }
            }

            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ManageRemedePage(model.getObject().getRemede().getIdRemede(),
                        callerPage));
            }
        });

        final DataTable<IngredientRemede, String> remedesDataTable =
                new AjaxFallbackDefaultDataTable<>(componentId, columns, remedesDataProvider, WebContext.ROWS_PER_PAGE);
        remedesDataTable.addBottomToolbar(new TableExportToolbar(remedesDataTable, "Remede", templatePage
                .getSession().getLocale()));
        return remedesDataTable;
    }


    /**
     * Init data table with Remede list.
     *
     * This method is static to be reused in several places.
     *
     * @param templatePage parent page
     * @param componentId data table id
     * @param callerPage caller page
     * @param remedeService test bio service
     * @return data table component
     */
    public static DataTable<IngredientRemede, String> initRemedeSearchDataTable(final TemplatePage templatePage,
                                                                          final String componentId, final CallerPage callerPage, LoadableDetachableSortableListDataProvider<IngredientRemede> remedesDataProvider,
                                                                          final RemedeService remedeService) {

        List<IColumn<IngredientRemede, String>> columns = new ArrayList<>();

        columns.add(new LinkableImagePropertyColumn<>("images/read.png", templatePage
                .getStringModel("Read"), templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadRemedePage(model.getObject().getRemede().getIdRemede(),
                        callerPage));
            }
        });

        columns.add(new LinkPropertyColumn<>(templatePage
                .getStringModel("Remede.reference"), "remede.ref", "remede.ref", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadRemedePage(model.getObject().getRemede().getIdRemede(),
                        callerPage));
            }
        });

        columns.add(new ShortDatePropertyColumn<>(templatePage
                .getStringModel("Remede.date"), "remede.date", "remede.date",
                templatePage.getLocale()));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Remede.nom"),
                "remede.nom", "remede.nom"));

        columns.add(new MapValuePropertyColumn<>(
                templatePage.getStringModel("Remede.langue"), "remede.codeLangue", "remede.codeLangue", WebContext.LANGUAGES
                .get(templatePage.getSession().getLocale())));

       columns.add(new PropertyColumn<>(templatePage.getStringModel("Remede.indicationBiomedicale"),
                "remede.indicationBiomedicale", "remede.indicationBiomedicale"));

        //nb ingredients
        columns.add(new PropertyColumn<>(templatePage.getStringModel("Remede.nbIngredients"),
                "remede.ingredients.size", "remede.ingredients.size"));

        columns.add(new LinkPropertyColumn<>(templatePage
                .getStringModel("IngredientRemede.specimen2"),
                "specimen.ref", "specimen.ref",
                templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadSpecimenPage(model.getObject().getSpecimen().getIdSpecimen(),
                        callerPage));
            }
        });

        columns.add(new TaxonomyPropertyColumn<>( templatePage.getStringModel("Specimen.famille"),
                "specimen.famille", "specimen.famille"));

        columns.add(new TaxonomyPropertyColumn<>(templatePage.getStringModel("Specimen.genre"),
                "specimen.genre", "specimen.genre"));

        columns.add(new TaxonomyPropertyColumn<>(templatePage.getStringModel("Specimen.espece"),
                "specimen.espece", "specimen.espece"));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("IngredientRemede.nomVernaculaire"),
                "nomVernaculaire", "nomVernaculaire"));

        columns.add(new LinkPropertyColumn<>(templatePage
                .getStringModel("Remede.campagne"), "remede.campagne", "remede.campagne", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ReadCampagnePage(model.getObject().getRemede().getCampagne().getIdCampagne(),
                        callerPage));
            }
        });

        columns.add(new MapValuePropertyColumn<>(templatePage
                .getStringModel("Campagne.codePays"), "remede.campagne.codePays", "remede.campagne.codePays",
                WebContext.COUNTRIES.get(templatePage.getSession().getLocale())));

        columns.add(new DocumentTooltipColumn<>(templatePage.getStringModel("ListDocumentsPage.AttachedDocuments")) {
            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, IModel<IngredientRemede> model) {
                int idRemede = model.getObject().getRemede().getIdRemede();
                templatePage.setResponsePage(new ReadRemedePage(idRemede, callerPage));
            }
        });

        columns.add(new LinkableImagePropertyColumn<>("images/edit.png", templatePage
                .getStringModel("Update"), templatePage.getStringModel("Update")) {
            // pas de lien d'édition si l'utilisateur n'a pas les droits
            @Override
            public void populateItem(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                     IModel<IngredientRemede> model) {
                if (remedeService.updateOrdeleteRemedeEnabled(model.getObject().getRemede(), templatePage
                        .getSession().getUtilisateur())) {
                    item.add(new LinkableImagePanel(item, componentId, model));
                } else {
                    // label vide
                    item.add(new Label(componentId));
                }
            }

            @Override
            public void onClick(Item<ICellPopulator<IngredientRemede>> item, String componentId,
                                IModel<IngredientRemede> model) {
                templatePage.setResponsePage(new ManageRemedePage(model.getObject().getRemede().getIdRemede(),
                        callerPage));
            }
        });

        final DataTable<IngredientRemede, String> remedesDataTable =
                new AjaxFallbackDefaultDataTable<>(componentId, columns, remedesDataProvider, WebContext.ROWS_PER_PAGE);
        remedesDataTable.addBottomToolbar(new TableExportToolbar(remedesDataTable, "Remede", templatePage
                .getSession().getLocale()));
        // DRAFT FOR FILTER TABLE
        // remedesDataTable.addTopToolbar(new NavigationToolbar(remedesDataTable));
        // remedesDataTable.addTopToolbar(new HeadersToolbar(remedesDataTable, remedesDataProvider));

        // create the form used to contain all filter components
        /*
         * final FilterForm filterForm = new FilterForm("filter-form", remedesDataProvider) { private static final
         * long serialVersionUID = 1L;
         * @Override protected void onSubmit() { remedesDataTable.setCurrentPage(0); } }; remedesDataTable
         * .addTopToolbar(new FilterToolbar(remedesDataTable, filterForm, remedesDataProvider));
         */
        return remedesDataTable;
    }

    public static DataTable<Document, String> initRemedeDocumentDataTable(final TemplatePage templatePage,
                                                                            final String componentId,
                                                                            final CallerPage callerPage,
                                                                            DocumentSearchResult documentSearchResult,
                                                                            DocumentService documentService) {

        List<Document> RemedeDocuments = documentSearchResult.getRemedesDocs();
        Map<Integer, Remede> remedeMap = documentSearchResult.getRemedes();

        LoadableDetachableSortableListDataProvider<Document> remedeDataProvider = new DocumentListDataProvider(
                RemedeDocuments, templatePage.getSession().getLocale(), DocumentListDataProvider.TableType.REMEDE,
                remedeMap);

        List<IColumn<Document, String>> columns = new ArrayList<>();
        columns.add(new LinkableImagePropertyColumn<>("images/read.png", templatePage
                .getStringModel("Read"), templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                templatePage.setResponsePage(new ReadRemedePage(model.getObject().getRemedeId(), callerPage));
            }
        });

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("Remede.reference"), "ref", "ref") {

            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                Remede remede = remedeMap.get(model.getObject().getRemedeId());
                item.add(new LinkPanel(item, componentId, model, new Model<>() {
                    @Override
                    public String getObject() {
                        return remede.getRef();
                    }
                }));
            }

            @Override
            public void onClick(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                templatePage.setResponsePage(new ReadRemedePage(model.getObject().getRemedeId(), callerPage));
            }
        });

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Remede.nom"), "nom", "nom") {

            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                Remede remede = remedeMap.get(model.getObject().getRemedeId());
                if (remede != null) { //remede should never be null, but we may have some caused by an old bug
                    item.add(new Label(componentId, remede.getNom()));
                }
            }
        });

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Remede.langue"), "codeLangue", "codeLangue") {

            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                Remede remede = remedeMap.get(model.getObject().getRemedeId());
                String langue = WebContext.LANGUAGES.get(templatePage.getSession().getLocale()).get(remede.getCodeLangue());
                item.add(new Label(componentId, langue));
            }
        });

        /*columns.add(new MapValuePropertyColumn<>(
                templatePage.getStringModel("Remede.langue"), "codeLangue", "codeLangue", WebContext.LANGUAGES
                .get(templatePage.getSession().getLocale())));*/

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.titre"), "titre",
                "titre"));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.type"), "type",
                "typeDocument"));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.apercu"), "apercu",
                "apercu") {
            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                item.add(new DocumentLinkPanel(componentId, new EntityModel<>(model.getObject())));
            }
        });

        columns.add(new CheckBoxColumn<>((IModel<String>) () -> null));

        final DataTable<Document, String> remedeDataTable = new AjaxFallbackDefaultDataTable<>(
                componentId, columns, remedeDataProvider, WebContext.ROWS_PER_PAGE);
        remedeDataTable.addBottomToolbar(new ZipTableExportToolbar(remedeDataTable, "remedes", documentService));
        return remedeDataTable;
    }
}
