package nc.ird.cantharella.web.pages.domain.personneInterrogee;

import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.DocumentSearchResult;
import nc.ird.cantharella.service.services.DocumentService;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.service.services.PersonneInterrogeeService;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.ImportFormPanel;
import nc.ird.cantharella.web.pages.domain.campagne.ReadCampagnePage;
import nc.ird.cantharella.web.pages.domain.document.DocumentTooltipColumn;
import nc.ird.cantharella.web.pages.domain.document.panel.DocumentLinkPanel;
import nc.ird.cantharella.web.pages.domain.dosage.ReadDosagePage;
import nc.ird.cantharella.web.pages.domain.station.ReadStationPage;
import nc.ird.cantharella.web.pages.model.EntityModel;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.columns.CheckBoxColumn;
import nc.ird.cantharella.web.utils.columns.EnumPropertyColumn;
import nc.ird.cantharella.web.utils.columns.LinkPropertyColumn;
import nc.ird.cantharella.web.utils.columns.LinkableImagePropertyColumn;
import nc.ird.cantharella.web.utils.columns.MapValuePropertyColumn;
import nc.ird.cantharella.web.utils.data.TableExportToolbar;
import nc.ird.cantharella.web.utils.data.ZipTableExportToolbar;
import nc.ird.cantharella.web.utils.models.DocumentListDataProvider;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.TextFilteredPropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public class ListPersonnesInterrogeesPage extends TemplatePage {

    /** Service : personnes interrogées */
    @SpringBean
    private PersonneInterrogeeService personneInterrogeeService;

    /**
     * Constructeur
     */
    public ListPersonnesInterrogeesPage() {
        super(ListPersonnesInterrogeesPage.class);

        final CallerPage currentPage = new CallerPage(ListPersonnesInterrogeesPage.class);

        add(new Link<Void>("ListPersonnesInterrogeesPage.NewPersonneInterrogee") {
            @Override
            public void onClick() {
                setResponsePage(new ManagePersonneInterrogeePage(currentPage, true));
            }
        });

        Utilisateur utilisateur = getSession().getUtilisateur();

        // Liste des personnes interrogées
        final List<PersonneInterrogee> resPersonnesInterrogees = personneInterrogeeService.listPersonneInterrogee(utilisateur);
        LoadableDetachableSortableListDataProvider<PersonneInterrogee> personneInterrogeeDataProvider = new LoadableDetachableSortableListDataProvider<>(
                resPersonnesInterrogees, this.getSession().getLocale());

        // Upload
        ImportFormPanel form = new ImportFormPanel(getResource() + ".importForm", getResource(), utilisateur,
                () -> personneInterrogeeDataProvider.setList(personneInterrogeeService.listPersonneInterrogee(utilisateur)));
        add(form);

        // On englobe le "DataView" dans un composant neutre que l'on pourra
        // rafraichir quand la liste évoluera
        final MarkupContainer personnesInterrogeesRefresh = new WebMarkupContainer("ListPersonnesInterrogeesPage.PersonnesInterrogees.Refresh");
        personnesInterrogeesRefresh.setOutputMarkupId(true);
        add(personnesInterrogeesRefresh);

        DataTable<PersonneInterrogee, String> personnesInterrogeesDataTable = initPersonneInterrogeeDataTable(this,
                "ListPersonnesInterrogeesPage.PersonnesInterrogees", currentPage, personneInterrogeeDataProvider, personneInterrogeeService);
        personnesInterrogeesRefresh.add(personnesInterrogeesDataTable);
    }

    /**
     * Init data table with testsbio list.
     *
     * This method is static to be reused in several places.
     *
     * @param templatePage parent page
     * @param componentId data table id
     * @param callerPage caller page
     * @param personneInterrogeeService test bio service
     * @return data table component
     */
    static public DataTable<PersonneInterrogee, String> initPersonneInterrogeeDataTable(final TemplatePage templatePage,
    final String componentId, final CallerPage callerPage, LoadableDetachableSortableListDataProvider<PersonneInterrogee> personneInterrogeeDataProvider,
    final PersonneInterrogeeService personneInterrogeeService) {

        List<IColumn<PersonneInterrogee, String>> columns = new ArrayList<>();

        columns.add(new LinkableImagePropertyColumn<>("images/read.png", templatePage
                .getStringModel("Read"), templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<PersonneInterrogee>> item, String componentId,
                                IModel<PersonneInterrogee> model) {
                templatePage.setResponsePage(new ReadPersonneInterrogeePage(model.getObject().getIdPersonne(),
                        callerPage));
            }
        });

        columns.add(new TextFilteredPropertyColumn<PersonneInterrogee, String, String>(templatePage
                .getStringModel("PersonneInterrogee.nom"), "nom", "nom"));

        columns.add(new TextFilteredPropertyColumn<PersonneInterrogee, String, String>(templatePage
                .getStringModel("PersonneInterrogee.prenom"), "prenom", "prenom"));

        columns.add(new EnumPropertyColumn<>(templatePage.getStringModel("PersonneInterrogee.genre"),
                "genre", "genre", templatePage));

        columns.add(new TextFilteredPropertyColumn<PersonneInterrogee, String, String>(templatePage
                .getStringModel("PersonneInterrogee.anneeNaissance"), "anneeNaissance", "anneeNaissance"));

        columns.add(new TextFilteredPropertyColumn<PersonneInterrogee, String, String>(templatePage
                .getStringModel("PersonneInterrogee.experience"), "experience", "experience"));

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("PersonneInterrogee.station"),
                "station", "station", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<PersonneInterrogee>> item, String componentId,
                                IModel<PersonneInterrogee> model) {
                templatePage.setResponsePage(new ReadStationPage(model.getObject().getStation().getIdStation(),
                        callerPage));
            }
        });

        columns.add(new TextFilteredPropertyColumn<PersonneInterrogee, String, String>(templatePage
                .getStringModel("PersonneInterrogee.ville"), "ville", "ville"));

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("PersonneInterrogee.campagne"),
                "campagne", "campagne", templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<PersonneInterrogee>> item, String componentId,
                                IModel<PersonneInterrogee> model) {
                templatePage.setResponsePage(new ReadCampagnePage(model.getObject().getCampagne().getIdCampagne(),
                        callerPage));
            }
        });

        columns.add(new MapValuePropertyColumn<>(templatePage.getStringModel("PersonneInterrogee.codePays"),
                "codePays", "codePays", WebContext.COUNTRIES.get(templatePage.getSession().getLocale())));

        columns.add(new DocumentTooltipColumn<>(templatePage
                .getStringModel("ListDocumentsPage.AttachedDocuments")) {
            @Override
            public void onClick(Item<ICellPopulator<PersonneInterrogee>> item, IModel<PersonneInterrogee> model) {
                templatePage.setResponsePage(new ReadPersonneInterrogeePage(model.getObject().getIdPersonne(), callerPage));
            }
        });

        columns.add(new LinkableImagePropertyColumn<>("images/edit.png", templatePage
                .getStringModel("Update"), templatePage.getStringModel("Update")) {
            // pas de lien d'édition si l'utilisateur n'a pas les droits
            @Override
            public void populateItem(Item<ICellPopulator<PersonneInterrogee>> item, String componentId,
                                     IModel<PersonneInterrogee> model) {
                if (personneInterrogeeService.updateOrdeletePersonneInterrogeeEnabled(model.getObject(), templatePage
                        .getSession().getUtilisateur())) {
                    item.add(new LinkableImagePanel(item, componentId, model));
                } else {
                    // label vide
                    item.add(new Label(componentId));
                }
            }

            @Override
            public void onClick(Item<ICellPopulator<PersonneInterrogee>> item, String componentId,
                                IModel<PersonneInterrogee> model) {
                templatePage.setResponsePage(new ManagePersonneInterrogeePage(model.getObject().getIdPersonne(),
                        callerPage));
            }
        });

        final DataTable<PersonneInterrogee, String> personnesInterrogeesDataTable = new AjaxFallbackDefaultDataTable<>(
                componentId, columns, personneInterrogeeDataProvider, WebContext.ROWS_PER_PAGE);
        personnesInterrogeesDataTable.addBottomToolbar(new TableExportToolbar(personnesInterrogeesDataTable, "personnesInterrogees", templatePage
                .getSession().getLocale()));
        return personnesInterrogeesDataTable;
    }

    public static DataTable<Document, String> initPersonnesInterrogeesDocumentDataTable(final TemplatePage templatePage,
                                                                           final String componentId,
                                                                           final CallerPage callerPage,
                                                                           DocumentSearchResult documentSearchResult,
                                                                           DocumentService documentService) {

        List<Document> personnesInterrogeesDocuments = documentSearchResult.getPersonnesInterrogeesDocs();
        Map<Integer, PersonneInterrogee> personnesInterrogeesMap = documentSearchResult.getPersonnesInterrogees();

        LoadableDetachableSortableListDataProvider<Document> personnesInterrogeesDataProvider = new DocumentListDataProvider(
                personnesInterrogeesDocuments, templatePage.getSession().getLocale(), DocumentListDataProvider.TableType.PERSONNE_INTERROGEE,
                personnesInterrogeesMap);

        List<IColumn<Document, String>> columns = new ArrayList<>();
        columns.add(new LinkableImagePropertyColumn<>("images/read.png", templatePage
                .getStringModel("Read"), templatePage.getStringModel("Read")) {
            @Override
            public void onClick(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                templatePage.setResponsePage(new ReadDosagePage(model.getObject().getDosageId(), callerPage));
            }
        });

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("PersonneInterrogee.nom"), "nom", "nom") {

            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                PersonneInterrogee personneInterrogee = personnesInterrogeesMap.get(model.getObject().getPersonneInterrogeeId());
                item.add(new LinkPanel(item, componentId, model, new Model<>() {
                    @Override
                    public String getObject() {
                        return personneInterrogee.getNom();
                    }
                }));
            }

            @Override
            public void onClick(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                templatePage.setResponsePage(new ReadPersonneInterrogeePage(model.getObject().getPersonneInterrogeeId(), callerPage));
            }
        });

        columns.add(new LinkPropertyColumn<>(templatePage.getStringModel("PersonneInterrogee.prenom"), "prenom", "prenom") {

            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                PersonneInterrogee personneInterrogee = personnesInterrogeesMap.get(model.getObject().getPersonneInterrogeeId());
                item.add(new LinkPanel(item, componentId, model, new Model<>() {
                    @Override
                    public String getObject() {
                        return personneInterrogee.getPrenom();
                    }
                }));
            }

            @Override
            public void onClick(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                templatePage.setResponsePage(new ReadPersonneInterrogeePage(model.getObject().getPersonneInterrogeeId(), callerPage));
            }
        });

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.titre"), "titre",
                "titre"));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.type"), "type",
                "typeDocument"));

        columns.add(new PropertyColumn<>(templatePage.getStringModel("Document.apercu"), "apercu",
                "apercu") {
            @Override
            public void populateItem(Item<ICellPopulator<Document>> item, String componentId, IModel<Document> model) {
                item.add(new DocumentLinkPanel(componentId, new EntityModel<>(model.getObject())));
            }
        });

        columns.add(new CheckBoxColumn<>((IModel<String>) () -> null));

        final DataTable<Document, String> personnesInterrogeesDataTable = new AjaxFallbackDefaultDataTable<>(
                componentId, columns, personnesInterrogeesDataProvider, WebContext.ROWS_PER_PAGE);
        personnesInterrogeesDataTable.addBottomToolbar(new ZipTableExportToolbar(personnesInterrogeesDataTable, "personnesInterrogees", documentService));
        return personnesInterrogeesDataTable;
    }
}
