package nc.ird.cantharella.web.pages.model;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Identifiable;
import nc.ird.cantharella.web.config.WebApplicationImpl;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.io.Serializable;

/**
 * Coming from http://wicketinaction.com/2008/09/building-a-smart-entitymodel/ to avoid LazyInitializationException
 * @param <T>
 */
public class EntityModel<T extends Identifiable<?>> extends AbstractEntityModel<T> {

    @SpringBean
    private GenericDao dao;

    public EntityModel(T entity) {
        super(entity);
        WebApplicationImpl.injectSpringBeans(this);
    }

    @Override
    protected T load(Class clazz, Serializable id) throws DataNotFoundException {
        return (T) dao.read(clazz, id);
    }
}