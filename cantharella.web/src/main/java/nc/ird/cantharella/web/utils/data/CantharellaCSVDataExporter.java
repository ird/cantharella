package nc.ird.cantharella.web.utils.data;

import org.apache.wicket.Session;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.CSVDataExporter;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.IExportableColumn;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.convert.IConverter;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

/**
 * CSV Data Exporter from Wicket, just displaying an empty string if a null value is present to prevent "no cell bug in export"
 */
public class CantharellaCSVDataExporter extends CSVDataExporter {

    public CantharellaCSVDataExporter() {
        super();
    }

    @Override
    public <T> void exportData(IDataProvider<T> dataProvider, List<IExportableColumn<T, ?>> columns, OutputStream outputStream)
            throws IOException
    {

        try (Grid grid = new Grid(new OutputStreamWriter(outputStream, Charset.forName(getCharacterSet()))))
        {
            writeHeaders(columns, grid);
            writeData(dataProvider, columns, grid);
        }
    }

    private <T> void writeHeaders(List<IExportableColumn<T, ?>> columns, Grid grid) throws IOException
    {
        if (isExportHeadersEnabled())
        {
            for (IExportableColumn<T, ?> col : columns)
            {
                IModel<String> displayModel = col.getDisplayModel();
                String display = wrapModel(displayModel).getObject();
                grid.cell(quoteValue(display));
            }
            grid.row();
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private <T> void writeData(IDataProvider<T> dataProvider, List<IExportableColumn<T, ?>> columns, Grid grid) throws IOException
    {
        long numberOfRows = dataProvider.size();
        Iterator<? extends T> rowIterator = dataProvider.iterator(0, numberOfRows);
        while (rowIterator.hasNext())
        {
            T row = rowIterator.next();

            for (IExportableColumn<T, ?> col : columns)
            {
                IModel<?> dataModel = col.getDataModel(dataProvider.model(row));

                Object value = wrapModel(dataModel).getObject();
                if (value != null)
                {
                    Class<?> c = value.getClass();

                    String s;

                    IConverter converter = getConverterLocator().getConverter(c);

                    if (converter == null)
                    {
                        s = value.toString();
                    }
                    else
                    {
                        s = converter.convertToString(value, Session.get().getLocale());
                    }

                    grid.cell(quoteValue(s));
                } else {
                    grid.cell("");
                }
            }
            grid.row();
        }
    }

    private class Grid implements Closeable {

        private Writer writer;

        boolean first = true;

        public Grid(Writer writer) {
            this.writer = writer;
        }

        public void cell(String value) throws IOException {
            if (first) {
                first = false;
            }
            else {
                writer.write(getDelimiter());
            }

            writer.write(value);
        }

        public void row() throws IOException {
            writer.write("\r\n");
            first = true;
        }

        @Override
        public void close() throws IOException {
            writer.close();
        }
    }
}
