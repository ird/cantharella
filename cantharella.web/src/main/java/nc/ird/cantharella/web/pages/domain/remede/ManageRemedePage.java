package nc.ird.cantharella.web.pages.domain.remede;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Disponibilite;
import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.EntreeReferentiel;
import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.data.model.Partie;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Remede;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.validation.utils.ModelValidator;
import nc.ird.cantharella.service.services.CampagneService;
import nc.ird.cantharella.service.services.ConfigurationService;
import nc.ird.cantharella.service.services.DocumentService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.service.services.PersonneInterrogeeService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.ProduitService;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.service.services.SpecimenService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.utils.CollectionTools;
import nc.ird.cantharella.web.config.WebContext;
import nc.ird.cantharella.web.pages.TemplatePage;
import nc.ird.cantharella.web.pages.domain.campagne.ManageCampagnePage;
import nc.ird.cantharella.web.pages.domain.document.ManageConsentementDocumentPage;
import nc.ird.cantharella.web.pages.domain.document.panel.DocumentLinkPanel;
import nc.ird.cantharella.web.pages.domain.document.panel.ManageListDocumentsPanel;
import nc.ird.cantharella.web.pages.domain.personne.ManagePersonnePage;
import nc.ird.cantharella.web.pages.domain.personneInterrogee.ManagePersonneInterrogeePage;
import nc.ird.cantharella.web.pages.renderers.PersonneRenderer;
import nc.ird.cantharella.web.utils.CallerPage;
import nc.ird.cantharella.web.utils.behaviors.JSConfirmationBehavior;
import nc.ird.cantharella.web.utils.forms.AutoCompleteTextFieldString;
import nc.ird.cantharella.web.utils.forms.AutoCompleteTextFieldString.ComparisonMode;
import nc.ird.cantharella.web.utils.forms.SubmittableButton;
import nc.ird.cantharella.web.utils.forms.SubmittableButtonEvents;
import nc.ird.cantharella.web.utils.models.DisplayEnumPropertyModel;
import nc.ird.cantharella.web.utils.panels.SimpleTooltipPanel;
import nc.ird.cantharella.web.utils.renderers.MapChoiceRenderer;
import nc.ird.cantharella.web.utils.security.AuthRole;
import nc.ird.cantharella.web.utils.security.AuthRoles;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteTextField;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Iterator;
import java.util.Collections;
import java.util.Locale;
import java.util.stream.Collectors;

@AuthRoles({ AuthRole.ADMIN, AuthRole.USER })
public class ManageRemedePage extends TemplatePage {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ManageRemedePage.class);

    /** Action : create */
    private static final String ACTION_CREATE = "Create";

    /** Action : update */
    private static final String ACTION_UPDATE = "Update";

    /** Action : delete */
    private static final String ACTION_DELETE = "Delete";

    /** remede Model */
    private final IModel<Remede> remedeModel;

    /** model for adding resultat */
    private Model<IngredientRemede> newIngredientModel;

    private IModel<EntreeReferentiel.Referentiel> referentielModel;

    private IModel<EntreeReferentiel> classificationModel;

    /** Service : testBios */
    @SpringBean
    private RemedeService remedeService;

    /** Service : testBios */
    @SpringBean
    private SpecimenService specimenService;

    /** Service : testBios */
    @SpringBean
    private CampagneService campagneService;

    /** Service : testBios */
    @SpringBean
    private StationService stationService;

    /** Service : testBios */
    @SpringBean
    private DocumentService documentService;

    /** Service : testBios */
    @SpringBean
    private LotService lotService;

    /** Service : configuration */
    @SpringBean
    private ConfigurationService configurationService;

    /** Service : personnes */
    @SpringBean
    private PersonneService personneService;

    /** Service : personnes */
    @SpringBean
    private PersonneInterrogeeService personneInterrogeeService;

    /** Service : produits */
    @SpringBean
    private ProduitService produitService;

    /** Liste des personnes existantes */
    private final List<Personne> personnes;

    /** Liste des campagnes existantes */
    private final List<Campagne> campagnes;

    private final List<String> nomsVernaculaires;

    private final List<String> nomsRemedes;

    /** Liste des organismes testeurs déjà renseignés pour les personnes */
    private final List<Partie> parties;

    private final List<Disponibilite> disponibilites;

    /** Model validateur */
    @SpringBean(name = "webModelValidator")
    private ModelValidator validator;

    /** Page appelante */
    private final CallerPage callerPage;

    /** Saisie multiple */
    private boolean multipleEntry;

    /** Bouton d'ajout d'un ingrédient **/
    Button addIngredientButton;

    /** Bouton d'ajout d'une classification **/
    Button addClassificationButton;

    TextField<String> recoTextInput;
    Label recoLabel;
    TextField<String> dispoAnalyseTextInput;
    Label dispoAnalyseLabel;

    DropDownChoice<Station> provenanceInput;

    DropDownChoice<PersonneInterrogee> prescripteurInput;

    DropDownChoice<Specimen> specimenChoice;

    MarkupContainer consentementDiv;

    Component consentementLink;

    /**
     * Constructeur (mode création)
     *
     * @param callerPage Page appelante
     * @param multipleEntry Saisie multiple
     */
    public ManageRemedePage(CallerPage callerPage, boolean multipleEntry) {
        this(null, null, callerPage, multipleEntry);
    }

    /**
     * Constructeur (mode édition)
     *
     * @param idManip Id du Remede
     * @param callerPage Page appelante
     */
    public ManageRemedePage(Integer idManip, CallerPage callerPage) {
        this(idManip, null, callerPage, false);
    }

    /**
     * Constructeur (mode saisie de la remede suivante)
     *
     * @param remede Remede
     * @param callerPage Page appelante
     */
    public ManageRemedePage(Remede remede, CallerPage callerPage) {
        this(null, remede, callerPage, true);
    }

    /**
     * Constructeur. Si refManip et remede sont nuls, on créée une nouvelle remede d'testBio. Si refManip est renseignée,
     * on édite la remede correspondante. Si remede est renseigné, on créée une nouvelle manipulation à partir des
     * informations qu'elle contient.
     *
     * @param idRemede Id de la remede d'testBio
     * @param remede Manip d'testBio
     * @param callerPage Page appelante
     * @param multipleEntry Saisie multiple
     */
    private ManageRemedePage(Integer idRemede, Remede remede, final CallerPage callerPage, boolean multipleEntry) {
        super(ManageRemedePage.class);
        assert idRemede == null || remede == null;
        this.callerPage = callerPage;
        final CallerPage currentPage = new CallerPage(this);
        this.multipleEntry = multipleEntry;

        newIngredientModel = new Model<>(new IngredientRemede());

        // Initialisation du modèle
        try {
            remedeModel = new Model<>(idRemede == null && remede == null ? new Remede() : remede != null ? remede
                    : remedeService.loadRemede(idRemede));
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
        boolean createMode = idRemede == null;
        if (createMode) {
            remedeModel.getObject().setCreateur(getSession().getUtilisateur());
        }

        // Initialisation des listes (pour le dropDownChoice)
        personnes = personneService.listPersonnes();
        campagnes = campagneService.listCampagnes(getSession().getUtilisateur());
        parties = lotService.listParties();
        disponibilites = remedeService.listDisponibilites();
        nomsRemedes = remedeService.listNomRemede();
        nomsVernaculaires = remedeService.listNomVernaculaireIngredients();

        // bind with markup
        final Form<Void> formView = new Form<>("Form");

        // initialisation du formulaire
        initPrincipalFields(formView, currentPage);
        initRemedeFields(formView);
        initClassificationFields(formView);
        initIngredientsFields(formView);
        initPreparationFields(formView);

        // add list document panel
        ManageListDocumentsPanel manageListDocumentsPanel = new ManageListDocumentsPanel("ManageListDocumentsPanel",
                remedeModel, currentPage);
        formView.add(manageListDocumentsPanel);

        // Action : create the testBio
        Button createButton = new SubmittableButton(ACTION_CREATE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                //pas de création du document en cascade donc on le crée à la main avant
                if (remedeModel.getObject().getConsentement() != null) {
                    remedeModel.getObject().getConsentement().setRemedeId(remedeModel.getObject().getIdRemede());
                    documentService.createOrUpdateDocument(remedeModel.getObject().getConsentement());
                }
                remedeService.createRemede(remedeModel.getObject());
                createConsentementLink();
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_CREATE);
                redirect();
            }

            @Override
            public void onValidate() {
                validateModel();
            }
        });
        createButton.setVisibilityAllowed(createMode);
        formView.add(createButton);

        // Action : update the testBio
        Button updateButton = new SubmittableButton(ACTION_UPDATE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                //pas de création du document en cascade donc on le crée à la main avant
                if (remedeModel.getObject().getConsentement() != null) {
                    remedeModel.getObject().getConsentement().setRemedeId(remedeModel.getObject().getIdRemede());
                    documentService.createOrUpdateDocument(remedeModel.getObject().getConsentement());
                }
                if (remedeModel.getObject().getConsentementToDelete() != null) {
                    try {
                        documentService.deleteDocument(remedeModel.getObject().getConsentementToDelete());
                    } catch (DataNotFoundException e) {
                        //do nothing
                    }
                }
                remedeService.updateRemede(remedeModel.getObject());

                createConsentementLink();
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_UPDATE);
                callerPage.responsePage((TemplatePage) getPage());
            }

            @Override
            public void onValidate() {
                validateModel();
            }
        });
        updateButton.setVisibilityAllowed(!createMode);
        formView.add(updateButton);

        // Action : suppression
        Button deleteButton = new SubmittableButton(ACTION_DELETE, new SubmittableButtonEvents() {
            @Override
            public void onProcess() throws DataConstraintException {
                remedeService.deleteRemede(remedeModel.getObject());
            }

            @Override
            public void onSuccess() {
                successNextPage(ACTION_DELETE);
                callerPage.responsePage((TemplatePage) getPage());
            }
        });
        deleteButton.setVisibilityAllowed(!createMode);
        deleteButton.setDefaultFormProcessing(false);
        deleteButton.add(new JSConfirmationBehavior(getStringModel("Confirm")));
        formView.add(deleteButton);

        formView.add(new Link<Void>("Cancel") {
            // Cas où le formulaire est annulé
            @Override
            public void onClick() {
                callerPage.responsePage((TemplatePage) getPage());
            }
        });

        formView.setDefaultButton(addIngredientButton);
        add(formView);

    }

    /**
     * Initialise les champs principaux
     *
     * @param formView le formulaire
     */
    private void initPrincipalFields(Form<Void> formView, final CallerPage currentPage) {

        final DropDownChoice<Campagne> campagneChoice = new DropDownChoice<>("Remede.campagne", new PropertyModel<>(remedeModel,
                "campagne"), campagnes);
        formView.add(campagneChoice);

        final DropDownChoice<Station> stationChoice = new DropDownChoice<>("Remede.station",
                new PropertyModel<>(remedeModel, "station"),
                remedeModel.getObject().getCampagne() == null ? new ArrayList<>() : remedeModel.getObject()
                        .getCampagne().getStations());
        stationChoice.setOutputMarkupId(true);
        stationChoice.setNullValid(false);
        stationChoice.setEnabled(remedeModel.getObject().getCampagne() != null);
        formView.add(stationChoice);

        formView.add(new SimpleTooltipPanel("Remede.station.info", getStringModel("Remede.station.info")));

        campagneChoice.setNullValid(false);
        campagneChoice.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                List<Station> stations = new ArrayList<>();
                List<PersonneInterrogee> prescripteurs = new ArrayList<>();
                List<Specimen> specimens = new ArrayList<>();
                if (remedeModel.getObject().getCampagne() != null) {
                    campagneService.refreshCampagne(remedeModel.getObject().getCampagne());
                    stations = stationService.listStationsForCampagne(remedeModel.getObject().getCampagne(), getSession().getUtilisateur());
                    prescripteurs = personneInterrogeeService.listPersonneInterrogeeForCampagne(getSession().getUtilisateur(), remedeModel.getObject().getCampagne());
                    specimens = specimenService.listSpecimensForCampagne(getSession().getUtilisateur(), remedeModel.getObject().getCampagne());
                }
                stationChoice.setChoices(stations);
                stationChoice.setEnabled(remedeModel.getObject().getCampagne() != null);
                remedeModel.getObject().setStation(null);
                // refresh the station choices component
                target.add(stationChoice);

                provenanceInput.setChoices(stations);
                provenanceInput.setEnabled(remedeModel.getObject().getCampagne() != null);
                target.add(provenanceInput);

                prescripteurInput.setChoices(prescripteurs);
                prescripteurInput.setEnabled(remedeModel.getObject().getCampagne() != null);
                target.add(prescripteurInput);

                specimenChoice.setChoices(specimens);
                specimenChoice.setEnabled(remedeModel.getObject().getCampagne() != null);
                target.add(specimenChoice);
            }
        });

        // Action : création d'une nouvelle personne
        // ajaxSubmitLink permet de sauvegarder l'état du formulaire
        formView.add(new AjaxSubmitLink("NewCampagne") {
            @Override
            protected void onSubmit(AjaxRequestTarget arg0) {
                setResponsePage(new ManageCampagnePage(new CallerPage((TemplatePage) getPage()), false));
            }

            // si erreur, le formulaire est également enregistré puis la redirection effectuée
            @Override
            protected void onError(AjaxRequestTarget target) {
                setResponsePage(new ManagePersonnePage(new CallerPage((TemplatePage) getPage()), false));
            }
        });

        formView.add(new DateTextField("Remede.date", new PropertyModel<>(remedeModel, "date"),"yyyy-MM-dd"));

        formView.add(new DropDownChoice<>("Remede.enqueteur", new PropertyModel<>(remedeModel, "enqueteur"),
                personnes, new PersonneRenderer()).setNullValid(false));

        final IModel<Document> documentModel = new Model<>(remedeModel.getObject().getConsentement());

        //Refreshable div
        consentementDiv = new WebMarkupContainer("Remede.consentement");
        consentementDiv.setOutputMarkupId(true);
        formView.add(consentementDiv);

        //addButton
        consentementDiv.add(new AjaxSubmitLink("Remede.consentement.add") {
            @Override
            public void onSubmit(AjaxRequestTarget arg0) {
                setResponsePage(new ManageConsentementDocumentPage(remedeModel.getObject().getConsentement(),
                        remedeModel.getObject(), new CallerPage((TemplatePage) getPage()), false));
            }

            @Override
            protected void onError(AjaxRequestTarget target) {
                setResponsePage(new ManageConsentementDocumentPage(remedeModel.getObject().getConsentement(),
                        remedeModel.getObject(), new CallerPage((TemplatePage) getPage()), false));
            }

            @Override
            public boolean isEnabled() {
                return remedeModel.getObject().getConsentement() == null;
            }

            @Override
            public boolean isVisible() {
                return remedeModel.getObject().getConsentement() == null;
            }
        });

        //modifyButton
        consentementDiv.add(new AjaxSubmitLink("Remede.consentement.modify") {
            @Override
            public void onSubmit(AjaxRequestTarget arg0) {
                setResponsePage(new ManageConsentementDocumentPage(remedeModel.getObject().getConsentement(),
                        remedeModel.getObject(), new CallerPage((TemplatePage) getPage()), false));
            }

            @Override
            protected void onError(AjaxRequestTarget target) {
                setResponsePage(new ManageConsentementDocumentPage(remedeModel.getObject().getConsentement(),
                        remedeModel.getObject(), new CallerPage((TemplatePage) getPage()), false));
            }

            @Override
            public boolean isEnabled() {
                return remedeModel.getObject().getConsentement() != null;
            }

            @Override
            public boolean isVisible() {
                return remedeModel.getObject().getConsentement() != null;
            }
        });

        //DeleteButton
        Button deleteButton = new AjaxFallbackButton("Remede.consentement.delete", formView) {
            @Override
            protected void onSubmit(Optional<AjaxRequestTarget> target) {
                // Suppression
                remedeModel.getObject().setConsentementToDelete(remedeModel.getObject().getConsentement());
                remedeModel.getObject().setConsentement(null);

                if (target.isPresent()) {
                    target.get().add(consentementDiv);
                    refreshFeedbackPage(target.get());
                }
            }

            @Override
            public boolean isEnabled() {
                return remedeModel.getObject().getConsentement() != null;
            }

            @Override
            public boolean isVisible() {
                return remedeModel.getObject().getConsentement() != null;
            }
        };
        deleteButton.setDefaultFormProcessing(false);
        consentementDiv.add(deleteButton);

        createConsentementLink();

        // Action : création d'une nouvelle personne
        // ajaxSubmitLink permet de sauvegarder l'état du formulaire
        formView.add(new AjaxSubmitLink("NewPersonne") {
            @Override
            protected void onSubmit(AjaxRequestTarget arg0) {
                setResponsePage(new ManagePersonnePage(new CallerPage((TemplatePage) getPage()), false));
            }

            // si erreur, le formulaire est également enregistré puis la redirection effectuée
            @Override
            protected void onError(AjaxRequestTarget target) {
                setResponsePage(new ManagePersonnePage(new CallerPage((TemplatePage) getPage()), false));
            }
        });

        formView.add(new TextField<>("Remede.reference", new PropertyModel<String>(remedeModel, "ref")));

        prescripteurInput = new DropDownChoice<>("Remede.prescripteur",
                new PropertyModel<>(remedeModel, "prescripteur"),
                remedeModel.getObject().getStation() == null ? new ArrayList<>() : personneInterrogeeService.listPersonneInterrogeeForCampagne(getSession().getUtilisateur(), remedeModel.getObject().getCampagne()));
        prescripteurInput.setOutputMarkupId(true);
        prescripteurInput.setOutputMarkupPlaceholderTag(true);
        prescripteurInput.setEnabled(remedeModel.getObject().getStation() != null);
        prescripteurInput.setNullValid(true);

        formView.add(prescripteurInput);

        // Action : création d'une nouvelle personne
        // ajaxSubmitLink permet de sauvegarder l'état du formulaire
        formView.add(new AjaxSubmitLink("NewPersonneInterrogee") {
            @Override
            protected void onSubmit(AjaxRequestTarget arg0) {
                setResponsePage(new ManagePersonneInterrogeePage(new CallerPage((TemplatePage) getPage()), false));
            }

            // si erreur, le formulaire est également enregistré puis la redirection effectuée
            @Override
            protected void onError(AjaxRequestTarget target) {
                setResponsePage(new ManagePersonneInterrogeePage(new CallerPage((TemplatePage) getPage()), false));
            }
        });

        //consentement

        formView.add(new TextArea<>("Remede.complement", new PropertyModel<String>(remedeModel, "complement")));
        // Créateur en lecture seule
        formView.add(new TextField<>("Remede.createur", new PropertyModel<String>(remedeModel, "createur"))
                .setEnabled(false));
    }

    /**
     * Initialise les champs relatifs à la méthode
     *
     * @param formView le formulaire
     */
    private void initRemedeFields(final Form<Void> formView) {

        formView.add(new AutoCompleteTextFieldString("Remede.nom",
                new PropertyModel<>(remedeModel, "nom"), nomsRemedes, ComparisonMode.CONTAINS));

        formView.add(new DropDownChoice<>(
                        "Remede.langue",
                        new PropertyModel<>(remedeModel,"codeLangue"),
                        WebContext.LANGUAGE_CODES.get(getSession().getLocale()),
                        new MapChoiceRenderer<>(WebContext.LANGUAGES.get(getSession().getLocale()))));

        //referentiel

        formView.add(new TextArea<>("Remede.indicationBiomedicale", new PropertyModel<String>(remedeModel, "indicationBiomedicale")));

        formView.add(new TextArea<>("Remede.indicationVernaculaire", new PropertyModel<String>(remedeModel, "indicationVernaculaire")));

        //nb ingredients ?

    }

    /**
     * Initialise les champs relatifs à la méthode
     *
     * @param formView le formulaire
     */
    private void initPreparationFields(final Form<Void> formView) {

        formView.add(new TextArea<>("Remede.preparation", new PropertyModel<>(remedeModel, "preparation")));
        formView.add(new TextField<>("Remede.conservation", new PropertyModel<>(remedeModel, "conservation")));
        formView.add(new TextField<>("Remede.administration", new PropertyModel<>(remedeModel, "administration")));
        formView.add(new TextField<>("Remede.posologie", new PropertyModel<>(remedeModel, "posologie")));

        //reco
        CheckBox reco = new CheckBox("Remede.recommandations", new PropertyModel<>(remedeModel, "recommandations"));
        reco.setOutputMarkupId(true);
        reco.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                updateReco(Optional.ofNullable(target));
            }
        });
        formView.add(reco);
        recoLabel = new Label("Remede.recoLabel", new StringResourceModel("Remede.recommandationsText", this)) {
            @Override
            public boolean isVisible() {
                return remedeModel.getObject().getRecommandations();
            }
        };
        recoLabel.setOutputMarkupId(true);
        recoLabel.setOutputMarkupPlaceholderTag(true);
        formView.add(recoLabel);

        recoTextInput = new TextField<>("Remede.recommandationsText", new PropertyModel<>(remedeModel, "recommandationsText")) {
            @Override
            public boolean isEnabled() {
                return remedeModel.getObject().getRecommandations();
            }

            @Override
            public boolean isVisible() {
                return remedeModel.getObject().getRecommandations();
            }
        };
        recoTextInput.setOutputMarkupId(true);
        recoTextInput.setOutputMarkupPlaceholderTag(true);
        formView.add(recoTextInput);

        formView.add(new TextField<>("Remede.effetsIndesirables", new PropertyModel<>(remedeModel, "effetsIndesirables")));
        formView.add(new TextField<>("Remede.patientsParAn", new PropertyModel<>(remedeModel, "patientsParAn")));

        //analyse
        CheckBox dispoAnalyse = new CheckBox("Remede.dispoAnalyse", new PropertyModel<>(remedeModel, "dispoAnalyse"));
        dispoAnalyse.setOutputMarkupId(true);
        dispoAnalyse.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                updateDispoAnalyse(Optional.ofNullable(target));
            }
        });
        formView.add(dispoAnalyse);
        dispoAnalyseLabel = new Label("Remede.analyseLabel", new StringResourceModel("Remede.dispoAnalyseLabo", this)) {
            @Override
            public boolean isVisible() {
                return remedeModel.getObject().getDispoAnalyse();
            }
        };
        dispoAnalyseLabel.setOutputMarkupId(true);
        dispoAnalyseLabel.setOutputMarkupPlaceholderTag(true);
        formView.add(dispoAnalyseLabel);

        dispoAnalyseTextInput = new TextField<>("Remede.dispoAnalyseText", new PropertyModel<>(remedeModel, "dispoAnalyseLabo")) {
            @Override
            public boolean isEnabled() {
                return remedeModel.getObject().getDispoAnalyse();
            }

            @Override
            public boolean isVisible() {
                return remedeModel.getObject().getDispoAnalyse();
            }
        };
        dispoAnalyseTextInput.setOutputMarkupId(true);
        dispoAnalyseTextInput.setOutputMarkupPlaceholderTag(true);
        formView.add(dispoAnalyseTextInput);
    }

    /**
     * Initialise les champs relatifs aux ingrédients
     *
     * @param formView Le formulaire
     */
    private void initIngredientsFields(final Form<Void> formView) {

        // Déclaration tableau des resultats
        final MarkupContainer ingredientsTable = new WebMarkupContainer("Remede.ingredients.Table");
        ingredientsTable.setOutputMarkupId(true);

        // Contenu tableaux resultats
        ingredientsTable.add(new ListView<IngredientRemede>("Remede.ingredients.List",
                new PropertyModel<>(remedeModel, "sortedIngredients")) {
            @Override
            protected void populateItem(ListItem<IngredientRemede> item) {
                if (item.getIndex() % 2 == 1) {
                    item.add(new AttributeModifier("class", "odd"));
                }

                final IngredientRemede ingredient = item.getModelObject();

                // Colonnes
                item.add(new Label("Remede.ingredients.List.specimen",
                        new PropertyModel<String>(ingredient, "specimen.ref")));
                item.add(new Label("Remede.ingredients.List.typeOrganisme",
                        new DisplayEnumPropertyModel(ingredient,"specimen.typeOrganisme", (TemplatePage) this.getPage())));
                item.add(new Label("Remede.ingredients.List.famille",
                        new PropertyModel<String>(ingredient, "specimen.famille")));
                item.add(new Label("Remede.ingredients.List.genre",
                        new PropertyModel<String>(ingredient, "specimen.genre")));
                item.add(new Label("Remede.ingredients.List.espece",
                        new PropertyModel<String>(ingredient, "specimen.espece")));
                item.add(new Label("Remede.ingredients.List.nomVernaculaire",
                        new PropertyModel<String>(ingredient, "nomVernaculaire")));
                item.add(new Label("Remede.ingredients.List.partie",
                        new PropertyModel<String>(ingredient, "partie.nom")));
                item.add(new Label("Remede.ingredients.List.provenance",
                        new PropertyModel<String>(ingredient, "provenance")));
                item.add(new Label("Remede.ingredients.List.periode",
                        new PropertyModel<String>(ingredient, "periode")));
                item.add(new Label("Remede.ingredients.List.disponibilite",
                        new PropertyModel<String>(ingredient, "disponibilite")));

                // Action : suppression d'un résultat de test
                Button deleteButton = new AjaxFallbackButton("Remede.ingredients.List.Delete", formView) {
                    @Override
                    protected void onSubmit(Optional<AjaxRequestTarget> target) {
                        // Suppression
                        remedeModel.getObject().getIngredients().remove(ingredient);

                        if (target.isPresent()) {
                            target.get().add(ingredientsTable);
                            refreshFeedbackPage(target.get());
                        }
                    }
                };
                deleteButton.setDefaultFormProcessing(false);
                item.add(deleteButton);
            }
        });

        // champs d'input

        specimenChoice = new DropDownChoice<>("Remede.ingredients.specimen",
                new PropertyModel<>(newIngredientModel, "specimen"),
                remedeModel.getObject().getStation() == null ? new ArrayList<>() : specimenService.listSpecimensForCampagne(getSession().getUtilisateur(), remedeModel.getObject().getCampagne())) {
            @Override
            @SuppressWarnings("unchecked")
            public boolean isRequired() {
                Form<Void> form = (Form<Void>) findParent(Form.class);
                return form.getRootForm().findSubmitter() == addIngredientButton;
            }
        };
        specimenChoice.setOutputMarkupId(true);
        specimenChoice.setOutputMarkupPlaceholderTag(true);
        specimenChoice.setEnabled(remedeModel.getObject().getStation() != null);
        specimenChoice.setNullValid(false);
        ingredientsTable.add(specimenChoice);

        //labels
        Label specimenTypeLabel = new Label("Remede.ingredients.List.specimen.type", Model.of(""));
        specimenTypeLabel.setOutputMarkupId(true);
        specimenTypeLabel.setOutputMarkupPlaceholderTag(true);

        Label specimenFamilleLabel = new Label("Remede.ingredients.List.specimen.famille", Model.of(""));
        specimenFamilleLabel.setOutputMarkupId(true);
        specimenFamilleLabel.setOutputMarkupPlaceholderTag(true);
        specimenFamilleLabel.setEscapeModelStrings(false);

        Label specimenGenreLabel = new Label("Remede.ingredients.List.specimen.genre", Model.of(""));
        specimenGenreLabel.setOutputMarkupId(true);
        specimenGenreLabel.setOutputMarkupPlaceholderTag(true);
        specimenGenreLabel.setEscapeModelStrings(false);

        Label specimenEspeceLabel = new Label("Remede.ingredients.List.specimen.espece", Model.of(""));
        specimenEspeceLabel.setOutputMarkupId(true);
        specimenEspeceLabel.setOutputMarkupPlaceholderTag(true);
        specimenEspeceLabel.setEscapeModelStrings(false);

        ingredientsTable.add(specimenTypeLabel);
        ingredientsTable.add(specimenFamilleLabel);
        ingredientsTable.add(specimenGenreLabel);
        ingredientsTable.add(specimenEspeceLabel);

        //onchange specimen
        specimenChoice.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                if (specimenChoice.getModelObject() != null) {
                    specimenTypeLabel.setDefaultModelObject(specimenChoice.getModelObject().getTypeOrganisme().name().toLowerCase(Locale.ROOT));
                    specimenFamilleLabel.setDefaultModelObject("<i>" + specimenChoice.getModelObject().getFamille() + "</i>");
                    specimenGenreLabel.setDefaultModelObject("<i>" + specimenChoice.getModelObject().getGenre() + "</i>");
                    specimenEspeceLabel.setDefaultModelObject("<i>" + specimenChoice.getModelObject().getEspece() + "</i>");
                } else {
                    specimenTypeLabel.setDefaultModelObject("");
                    specimenFamilleLabel.setDefaultModelObject("");
                    specimenGenreLabel.setDefaultModelObject("");
                    specimenEspeceLabel.setDefaultModelObject("");
                }
                // refresh the classification choices component
                target.add(specimenTypeLabel);
                target.add(specimenFamilleLabel);
                target.add(specimenGenreLabel);
                target.add(specimenEspeceLabel);
            }
        });

        ingredientsTable.add(new AutoCompleteTextFieldString("Remede.ingredients.nomVernaculaire",
                new PropertyModel<>(newIngredientModel, "nomVernaculaire"), nomsVernaculaires, ComparisonMode.CONTAINS));

        final DropDownChoice<Partie> partiesChoice = new DropDownChoice<>("Remede.ingredients.partie",
                new PropertyModel<>(newIngredientModel, "partie"), parties);
        partiesChoice.setOutputMarkupId(true);
        partiesChoice.setOutputMarkupPlaceholderTag(true);
        partiesChoice.setNullValid(true);
        ingredientsTable.add(partiesChoice);

        provenanceInput = new DropDownChoice<>("Remede.ingredients.provenance",
                new PropertyModel<>(newIngredientModel, "provenance"),
                remedeModel.getObject().getCampagne() == null ? new ArrayList<>() : remedeModel.getObject()
                        .getCampagne().getStations());
        provenanceInput.setOutputMarkupId(true);
        provenanceInput.setOutputMarkupPlaceholderTag(true);
        provenanceInput.setEnabled(remedeModel.getObject().getCampagne() != null);
        provenanceInput.setNullValid(true);
        ingredientsTable.add(provenanceInput);

        ingredientsTable.add(new TextField<>("Remede.ingredients.periode",
                new PropertyModel<>(newIngredientModel, "periode")));

        final DropDownChoice<Disponibilite> disponibiliteChoice = new DropDownChoice<>("Remede.ingredients.disponibilite",
                new PropertyModel<>(newIngredientModel, "disponibilite"), disponibilites);
        disponibiliteChoice.setOutputMarkupId(true);
        disponibiliteChoice.setOutputMarkupPlaceholderTag(true);
        disponibiliteChoice.setNullValid(true);
        ingredientsTable.add(disponibiliteChoice);

        // Bouton AJAX pour ajouter un résultat de test
        addIngredientButton = new AjaxFallbackButton("Remede.ingredients.Add", formView) {
            @Override
            protected void onSubmit(Optional<AjaxRequestTarget> target) {
                try {
                    // ajout du remede
                    newIngredientModel.getObject().setRemede(remedeModel.getObject());

                    // ajout à la liste
                    IngredientRemede ingredientAdded = newIngredientModel.getObject().clone();
                    remedeModel.getObject().getIngredients().add(ingredientAdded);

                    List<String> errors = validator.validate(newIngredientModel.getObject(), getSession().getLocale());

                    if (errors.isEmpty()) {
                        // réinit des champs de la ligne "ajout"
                        newIngredientModel.getObject().setSpecimen(null);
                        newIngredientModel.getObject().setNomVernaculaire(null);
                        newIngredientModel.getObject().setPartie(null);
                        newIngredientModel.getObject().setProvenance(null);
                        newIngredientModel.getObject().setPeriode(null);
                        newIngredientModel.getObject().setDisponibilite(null);
                    } else {
                        remedeModel.getObject().getIngredients().remove(ingredientAdded);
                        addValidationErrors(errors);
                    }
                } catch (CloneNotSupportedException e) {
                    LOG.error(e.getMessage(), e);
                    throw new UnexpectedException(e);
                }

                if (target.isPresent()) {
                    target.get().add(ingredientsTable);
                    refreshFeedbackPage(target.get());
                }
            }

            @Override
            protected void onError(Optional<AjaxRequestTarget> target) {
                target.ifPresent(ajaxRequestTarget -> refreshFeedbackPage(ajaxRequestTarget));
            }

        };
        ingredientsTable.add(addIngredientButton);
        formView.add(ingredientsTable);
    }


    /**
     * Initialise les champs relatifs aux ingrédients
     *
     * @param formView Le formulaire
     */
    private void initClassificationFields(final Form<Void> formView) {

        referentielModel = new Model<>();

        classificationModel = new Model<>();

        // Déclaration tableau des resultats
        final MarkupContainer classificationTable = new WebMarkupContainer("Remede.classification.Table");
        classificationTable.setOutputMarkupId(true);


        classificationTable.add(new SimpleTooltipPanel("Remede.classification.info", getStringModel("Remede.classification.info")));

        // Contenu tableaux resultats
        classificationTable.add(new ListView<EntreeReferentiel>("Remede.classification.List",
                new PropertyModel<>(remedeModel, "classification")) {
            @Override
            protected void populateItem(ListItem<EntreeReferentiel> item) {
                if (item.getIndex() % 2 == 1) {
                    item.add(new AttributeModifier("class", "odd"));
                }

                final EntreeReferentiel entreeReferentiel = item.getModelObject();

                // Colonnes
                item.add(new Label("Remede.classification.List.referentiel",
                        new PropertyModel<String>(entreeReferentiel, "referentiel")));
                item.add(new Label("Remede.classification.List.valeur",
                        new PropertyModel<String>(entreeReferentiel, "valeur")));

                // Action : suppression d'un résultat de test
                Button deleteButton = new AjaxFallbackButton("Remede.classification.List.Delete", formView) {
                    @Override
                    protected void onSubmit(Optional<AjaxRequestTarget> target) {
                        // Suppression
                        remedeModel.getObject().getClassification().remove(entreeReferentiel);

                        if (target.isPresent()) {
                            target.get().add(classificationTable);
                            refreshFeedbackPage(target.get());
                        }
                    }
                };
                deleteButton.setDefaultFormProcessing(false);
                item.add(deleteButton);
            }
        });

        // champs d'input
        DropDownChoice<EntreeReferentiel.Referentiel> referentielInput =
                new DropDownChoice<>("Remede.classification.referentiel", referentielModel, List.of(EntreeReferentiel.Referentiel.values()));
        referentielInput.setNullValid(false);
        classificationTable.add(referentielInput);

        final AutoCompleteTextField<EntreeReferentiel> field = new AutoCompleteTextField<>("Remede.classification.classification", classificationModel) {
            @Override
            protected Iterator<EntreeReferentiel> getChoices(String input) {
                if (Strings.isEmpty(input)) {
                    List<EntreeReferentiel> emptyList = Collections.emptyList();
                    return emptyList.iterator();
                }

                if (input.equals("*")) {
                    List<EntreeReferentiel> referentiels = remedeService.listEntreeReferentiels(referentielModel.getObject());
                    referentiels = referentiels.stream().distinct().collect(Collectors.toList());
                    return referentiels.iterator();
                }

                List<EntreeReferentiel> choices = new ArrayList<>(10);
                List<EntreeReferentiel> referentiels = remedeService.listEntreeReferentiels(referentielModel.getObject());
                referentiels = referentiels.stream().distinct().collect(Collectors.toList());

                for (final EntreeReferentiel ref : referentiels) {
                    if (ref.getValeur().toUpperCase(Locale.ROOT).contains(input.toUpperCase(Locale.ROOT))) {
                        choices.add(ref);
                        if (choices.size() == 10) {
                            break;
                        }
                    }
                }

                return choices.iterator();
            }
        };
        classificationTable.add(field);

        referentielInput.add(new AjaxFormComponentUpdatingBehavior("change") {
            protected void onUpdate(AjaxRequestTarget target) {
                if (referentielInput.getModelObject() != null) {
                    field.setEnabled(true);
                    field.setModelObject(null);
                } else {
                    field.setEnabled(false);
                }
                // refresh the classification choices component
                target.add(field);
            }
        });
        classificationTable.add(new AttributeModifier("class", Model.of("property")));

        // Bouton AJAX pour ajouter un résultat de test
        addClassificationButton = new AjaxFallbackButton("Remede.classification.Add", formView) {
            @Override
            protected void onSubmit(Optional<AjaxRequestTarget> target) {
                // Ajout
                if (field.getModelObject() != null) {
                    String entreeString = String.valueOf(field.getModelObject());

                    List<EntreeReferentiel> referentiels = remedeService.listEntreeReferentiels(referentielInput.getModelObject());
                    referentiels = referentiels.stream().distinct().collect(Collectors.toList());

                    for (EntreeReferentiel ref: referentiels){
                        if (ref.getValeur().equals(entreeString)) {
                            remedeModel.getObject().getClassification().add(ref);
                        }
                    }
                }

                if (target.isPresent()) {
                    target.get().add(classificationTable);
                    refreshFeedbackPage(target.get());
                }
            }

            @Override
            protected void onError(Optional<AjaxRequestTarget> target) {
                target.ifPresent(ajaxRequestTarget -> refreshFeedbackPage(ajaxRequestTarget));
            }

        };
        classificationTable.add(addClassificationButton);
        formView.add(classificationTable);
    }

    private void updateReco(Optional<AjaxRequestTarget> target) {
        target.ifPresent(ajaxRequestTarget -> ajaxRequestTarget.add(recoTextInput));
        target.ifPresent(ajaxRequestTarget -> ajaxRequestTarget.add(recoLabel));
    }

    private void updateDispoAnalyse(Optional<AjaxRequestTarget> target) {
        target.ifPresent(ajaxRequestTarget -> ajaxRequestTarget.add(dispoAnalyseTextInput));
        target.ifPresent(ajaxRequestTarget -> ajaxRequestTarget.add(dispoAnalyseLabel));
    }

    /** {@inheritDoc} */
    @Override
    protected void onBeforeRender() {
        // On rafraichit le modèle lorsque la page est rechargée (par exemple après l'ajout d'une nouvelle entité
        // Personne ou Lot)
        refreshModel();

        super.onBeforeRender();
    }

    /**
     * Redirection vers une autre page. Cas où le formulaire est validé
     */
    private void redirect() {
        if (multipleEntry) {
            // Redirection de nouveau vers l'écran de saisie d'une nouvelle testBio
            Remede nextManip = new Remede();
            setResponsePage(new ManageRemedePage(nextManip, callerPage));
        } else if (callerPage != null) {
            // On passe l'id du testBio associée à cette page, en paramètre de la prochaine page, pour lui permettre de
            // l'exploiter si besoin
            callerPage.addPageParameter(Remede.class.getSimpleName(), remedeModel.getObject());
            callerPage.responsePage(this);
        }
    }

    /**
     * Refresh model, appelé au rechargement de la page
     */
    private void refreshModel() {

        // Récupère (et supprime) les éventuels nouveaux objets créés dans les paramètres de la page.
        String key = Personne.class.getSimpleName();
        if (getPageParameters().getNamedKeys().contains(key)) {
            CollectionTools.setter(personnes, personneService.listPersonnes());
            try {
                Personne createdPersonne = personneService.loadPersonne(getPageParameters().get(key).toInt());
                remedeModel.getObject().setEnqueteur(createdPersonne);
            } catch (DataNotFoundException e) {
                LOG.error(e.getMessage(), e);
                throw new UnexpectedException(e);
            }
            getPageParameters().remove(key);
        }

        key = PersonneInterrogee.class.getSimpleName();
        if (getPageParameters().getNamedKeys().contains(key)) {
            if (remedeModel.getObject().getCampagne() != null) {
                List<PersonneInterrogee> prescripteurs = personneInterrogeeService.listPersonneInterrogeeForCampagne(getSession().getUtilisateur(), remedeModel.getObject().getCampagne());
                prescripteurInput.setChoices(prescripteurs);
                prescripteurInput.setEnabled(remedeModel.getObject().getCampagne() != null);

                try {
                    PersonneInterrogee createdPersonne = personneInterrogeeService.loadPersonneInterrogee(getPageParameters().get(key).toInt());
                    remedeModel.getObject().setPrescripteur(createdPersonne);
                } catch (DataNotFoundException e) {
                    LOG.error(e.getMessage(), e);
                    throw new UnexpectedException(e);
                }
            }

            getPageParameters().remove(key);
        }

        key = Campagne.class.getSimpleName();
        if (getPageParameters().getNamedKeys().contains(key)) {
            CollectionTools.setter(campagnes, campagneService.listCampagnes(getSession().getUtilisateur()));
            try {
                Campagne createdCampagne = campagneService.loadCampagne(getPageParameters().get(key).toInt());
                remedeModel.getObject().setCampagne(createdCampagne);
            } catch (DataNotFoundException e) {
                LOG.error(e.getMessage(), e);
                throw new UnexpectedException(e);
            }
            getPageParameters().remove(key);
        }

        createConsentementLink();
    }

    /**
     * Validate model
     */
    private void validateModel() {
        addValidationErrors(validator.validate(remedeModel.getObject(), getSession().getLocale()));

        if (remedeModel.getObject().getIngredients().isEmpty()) {
            error(getString("Remede.ingredients.empty"));
        }
    }

    protected void createConsentementLink() {
        if (remedeModel.getObject().getConsentement() != null) {
            consentementLink = new DocumentLinkPanel("Remede.consentement.link", new Model<>(remedeModel.getObject().getConsentement())){

                @Override
                public boolean isVisible() {
                    return remedeModel.getObject().getConsentement() != null;
                }

            };
        } else {
            consentementLink = new Label("Remede.consentement.link");
        }

        consentementLink.setOutputMarkupId(true);
        consentementDiv.addOrReplace(consentementLink);
    }
}
