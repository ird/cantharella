package nc.ird.cantharella.web.pages.domain.config.panels;

import nc.ird.cantharella.data.model.ErreurDosage;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.web.pages.domain.config.ManageErreurDosagePage;
import nc.ird.cantharella.web.utils.models.LoadableDetachableSortableListDataProvider;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

public class ListErreurDosagePanel extends Panel {

    /** Service : test */
    @SpringBean
    private DosageService dosageService;

    /**
     * Constructor
     *
     * @param id The panel ID
     */
    public ListErreurDosagePanel(String id) {
        super(id);

        add(new BookmarkablePageLink<Void>("ListErreurDosagePanel.NewErreurDosage", ManageErreurDosagePage.class));

        // On englobe le "DataView" dans un composant neutre que l'on pourra
        // rafraichir quand la liste évoluera
        final MarkupContainer erreursDosageRefresh = new WebMarkupContainer(
                "ListErreurDosagePanel.ErreursDosage.Refresh");
        erreursDosageRefresh.setOutputMarkupId(true);
        add(erreursDosageRefresh);

        // Liste des erreursTest
        final List<ErreurDosage> erreursTest = dosageService.listErreursDosage();
        LoadableDetachableSortableListDataProvider<ErreurDosage> erreursDataProvider = new LoadableDetachableSortableListDataProvider<>(
                erreursTest, getSession().getLocale());

        erreursDosageRefresh
                .add(new DataView<>("ListErreurDosagePanel.ErreursDosage", erreursDataProvider) {
                    @Override
                    protected void populateItem(Item<ErreurDosage> item) {
                        item.add(new AttributeModifier("class", item.getIndex() % 2 == 0 ? "even" : "odd"));

                        ErreurDosage erreurTest = item.getModelObject();
                        // Colonnes
                        item.add(new Label("ListErreurDosagePanel.ErreursDosage.nom", new PropertyModel<String>(
                                erreurTest, "nom")));
                        item.add(new Label("ListErreurDosagePanel.ErreursDosage.description",
                                new PropertyModel<String>(erreurTest, "description")));

                        // Action : mise à jour (redirection vers le formulaire)
                        Link<ErreurDosage> updateLink = new Link<>(
                                "ListErreurDosagePanel.ErreursDosage.Update", new Model<>(erreurTest)) {
                            @Override
                            public void onClick() {
                                setResponsePage(new ManageErreurDosagePage(getModelObject().getIdErreurDosage()));
                            }
                        };
                        item.add(updateLink);
                    }
                });
    }
}
