###
# #%L
# Cantharella :: Web
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# #L%
###
#Header & footer
TemplatePage.Subtitle=Base de données pharmacochimique des substances naturelles
TemplatePage.Copyright=Tous droits réservés
TemplatePage.MainNav=Navigation principale
TemplatePage.ContentNav=Aller au contenu
TemplatePage.Welcome=Bienvenue
TemplatePage.Logout=Déconnexion
TemplatePage.Logout.OK=Vous avez été déconnecté
TemplatePage.Administration=Administration
TemplatePage.Sampling=Échantillonage
TemplatePage.Processing=Traitement
TemplatePage.Chemistry=Chimie
TemplatePage.Biology=Biologie
TemplatePage.Knowledge=Connaissances
TemplatePage.Search=Chercher
TemplatePage.Search.Label=Recherche dans la base : 
TemplatePage.Search.Launch=Lancer la recherche dans la base
TemplatePage.Lang=fr
TemplatePage.Lang.Choice=Choix de la langue 
TemplatePage.Css.Menu=css/menu_fr.css

#PAGES & ACTIONS#

HomePage=Accueil
HomePage.InvalidUtilisateurs=Demandes de créations de comptes
HomePage.Login=Connexion
HomePage.Login.OK=Connexion réussie
HomePage.Login.KO=Identifiants incorrects
HomePage.Login.NotYetValid=Votre demande de création de compte n'a pas encore été validée
HomePage.Statistics=Statistiques
HomePage.Rights=Vos droits
HomePage.Rights.Admin=Vous êtes connectés en tant qu'administrateur et avez accès à toutes les informations.
HomePage.Rights.User=Vous avez des droits sur l''ensemble des lots de {0} campagne(s) et spécifiquement sur {1} lot(s).
HomePage.Rights.User.Details=+ de détails
HomePage.Rights.User2=Vous êtes également créateur de {0} campagnes(s) et avez des droits sur l''ensemble de ses lots. 

#for statistics
Personnes.stat=Personnes
Campagnes.stat=Campagnes
Stations.stat=Stations
Specimens.stat=Spécimens
Lots.stat=Lots
Extractions.stat=Extractions
Purifications.stat=Purifications
Molecules.stat=Molécules
ResultatsTestsBio.stat=Résultats de tests biologiques
Dosages.stat=Dosages
PersonnesInterrogees.stat=Personnes Interrogées
Remedes.stat=Remèdes

ErrorPage=Erreur
ErrorPage.AccessDeniedPage=Vous n'êtes pas autorisé à accéder à cette page
ErrorPage.InternalErrorPage=Erreur interne
ErrorPage.PageExpiredErrorPage=La page a expiré

ContactPage=Contact
ContactPage.Info=Envoyez un message aux gestionnaires de la base en remplissant le formulaire ci-dessous.
ContactPage.Send.OK=Message envoyé
ContactPage.Send.EmailException=Problème lors de l'envoi du message

ImprintPage=Mentions légales
ImprintPage.AccessRights=Droit d'accès, de rectification, de suppression des données nominatives
ImprintPage.Editor=Editeur
ImprintPage.Hosting=Hébergement
ImprintPage.Reproduction=Droits de reproduction, copyright, licence
ImprintPage.Reproduction.data=Concernant les données
ImprintPage.Reproduction.source=Concernant la partie logicielle du système d’information
ImprintPage.Reproduction.5=Cantharella est mis à votre disposition sous licence <a href="http://www.gnu.org/licenses/why-affero-gpl.html">Affero GPL</a>.
ImprintPage.Reproduction.6=Tout le code source de ce produit est disponible sur la forge de <a href="http://forge.codelutin.com/projects/cantharella">Code Lutin</a> et sur HAL (<a href="https://hal.archives-ouvertes.fr/hal-01887126">HAL-01887126</a>) en vertu des licences open source des composants utilisés :
ImprintPage.Responsability=Limitation de responsabilité
ImprintPage.Responsability.1=Les utilisateurs sont responsables des interrogations qu'ils formulent ainsi que l'interprétation et l'utilisation qu'ils font des résultats. Ils doivent se conformer aux réglementations en vigueur et aux recommandations de la CNIL lorsque les données ont un caractère nominatif.
ImprintPage.Responsability.2=L'IRD n'assure aucune garantie, de quelque nature et à quelque titre que ce soit, en rapport avec cette application informatique et son contenu. En aucun cas l'IRD ne pourra être tenu pour responsable de tout dommage, de quelque nature que ce soit, notamment perte d'exploitation, perte de données ou toute autre perte financière résultant de l'exploitation ou de l'impossibilité d'exploiter.
ImprintPage.Credits=Crédits
ImprintPage.Credits.1.Title=Porteur du projet Cantharella
ImprintPage.Credits.1.Value=Sylvain PETEK
ImprintPage.Credits.2.Title=Chef de projet informatique
ImprintPage.Credits.2.Value=Adrien CHEYPE
ImprintPage.Credits.3.Title=Conception
ImprintPage.Credits.3.Value=Sylvain PETEK<br />Adrien CHEYPE<br />François CHASSAGNE (module Connaissance)<br />Maelle CARRAZ (module Connaissance)
ImprintPage.Credits.4.Title=Développements
ImprintPage.Credits.4.Value=Adrien CHEYPE<br />Mickael TRICOT<br />Alban DIGUER<br />Benjamin POUSSIN<br />Éric CHATELLIER<br />Yannick MARTEL<br />Cécilia BOSSARD<br />Jean COUTEAU
ImprintPage.Credits.5.Title=Conception graphique
ImprintPage.Credits.5.Value=Elise COSTE, Pôle web de l'IRD<br />Adrien CHEYPE
ImprintPage.Credits.6.Title=Logo
ImprintPage.Credits.6.Value=Fabrice CHARLEUX
ImprintPage.Credits.7.Title=Photos du bandeau
ImprintPage.Credits.7.Value=Banque d'images <a href="https://www.multimedia.ird.fr/">IRD Multimedia</a>, <a href="http://www.pedagogeeks.fr/archives/1018/molecule">Pedagogeeks</a> (sous licence Creative Commons) pour l'onglet Chimie, François Chassagne (sous licence CC-BY-NC-SA) pour l’onglet Connaissance
ImprintPage.Credits.8.Title=Financements
ImprintPage.Credits.8.Value=<a href="https://www.ird.fr/dsi/">DDUNI</a> de l'IRD : AAP SPIRALES 2008, 2009, 2010, 2012<br /><a href="http://www.ird.fr/l-ird/organigramme/direction-generale-deleguee-a-l-aird-dgd-aird/direction-de-la-valorisation-au-sud-dvs">DVS</a> de l'IRD : AAP « Maturation de projet innovant » : 2011<br /><a href="https://www-iuem.univ-brest.fr/lemar/">LEMAR</a> (IRD) : 2020 <br/> Département <a href="https://www.ird.fr/ressources-oceaniques">OCEANS</a> de l'IRD : 2021<br/>Département <a href="https://www.ird.fr/sante-et-societes-sas">SAS</a> de l'IRD : 2024<br/>Projets ANR HABIS, ANR Save-C, CGO MEREOS (Région Bretagne) : 2024<br/>Plateforme <a href="https://www-iuem.univ-brest.fr/lemar/les-poles/lipidocean">Lipidocéan</a> : 2024
ImprintPage.Credits.9.Title=Soutiens
ImprintPage.Credits.9.Value=<a href="http://www.upmc.fr/fr/recherche/pole_4/pole_vie_et_sante/systematique_adaptation_evolution_sae_umr_7138.html">UMR7138</a> – Systématique, Adaptation, Evolution (SAE)<br /><a href="http://www.pharmadev.ird.fr/">UMR152</a> – Pharma-Dev<br /><a href="https://wwz.ifremer.fr/umr_eio/">UMR241</a> - Ecosystèmes Insulaires Océaniens (EIO)<br /><a href="https://www-iuem.univ-brest.fr/lemar/">UMR6539</a> - Laboratoire des sciences de l’Evironnement MARin (LEMAR) 

ResetPasswordPage=Mot de passe oublié
ResetPasswordPage.Reset.OK=Nouveau mot de passe envoyé par mail
ResetPasswordPage.Reset.DataNotFoundException=Le courriel ne correspond à aucun utilisateur
ResetPasswordPage.Reset.EmailException=Problème lors de l'envoi du nouveau mot de passe

RegisterPage=Demande de création de compte
RegisterPage.Register.OK=Demande de création de compte effectuée
RegisterPage.Register.DataConstraintException=Le courriel ou le prénom + nom existe déjà

ManageUtilisateurPage=Gestion du compte utilisateur
ManageUtilisateurPage.Profile=Profil
ManageUtilisateurPage.Authorizations=Droits
ManageUtilisateurPage.Update.OK=Compte mis à jour
ManageUtilisateurPage.Update.DataConstraintException=Le courriel ou le prénom + nom existe déjà
ManageUtilisateurPage.Valid.OK=Compte validé et mis à jour
ManageUtilisateurPage.Valid.DataConstraintException=Le courriel ou le prénom + nom existe déjà
ManageUtilisateurPage.Delete.OK=Utilisateur supprimé
ManageUtilisateurPage.Delete.DataConstraintException=Il existe des données crées par cet utilisateur
ManageUtilisateurPage.Delete.EmailException=Problème lors de la confirmation de suppression de compte utilisateur
ManageUtilisateurPage.Delete.Info=La suppression efface le compte utilisateur, mais conserve la personne s'il y a des données liées.
ManageUtilisateurPage.Reject.OK=Demande de création de compte rejetée
ManageUtilisateurPage.Authorizations.Lot=Échantillons / Connaissances
ManageUtilisateurPage.Authorizations.Lot.info=Références des Lots / Remèdes
Authorizations.Campagnes.Lot.null=Tous

Import.EmptyFile=fichier CSV vierge à compléter
Import.CodePays=Liste des codes pays
Import.CodeLangue=Liste des codes langues
Import.csvInfo=Encoding utf-8 et ";" comme séparateur. Latitude et longitude en WGS84
Import.ListPersonnesPage=Importer des personnes
Import.ListStationsPage=Importer des stations
Import.ListSpecimensPage=Importer des spécimens
Import.ListPurificationsPage=Importer des purifications
Import.ListTestsBioPage=Importer des tests biologiques
Import.ListDosagePage=Importer des dosages
Import.ListLotsPage=Importer des lots
Import.ListExtractionsPage=Importer des extractions
Import.ListMoleculesPage=Importer des molécules
Import.ListPersonnesInterrogeesPage=Importer des personnes interrogées
Import.ListReferentielPanel=Importer un référentiel
Import.ListRemedePage=Importer des remèdes
Import.Import=Importer
Import.ImportSucess=Import effectué
Import.ImportFailed=Erreurs à l'import

ListPersonnesPage=Personnes
ListPersonnesPage.NewPersonne=Nouvelle personne
ListPersonnesPage.IsNotValid=(non validé) 

ListCampagnesPage=Campagnes
ListCampagnesPage.NewCampagne=Nouvelle campagne

ListLotsPage=Lots
ListLotsPage.NewLot=Nouveau lot

ListStationsPage=Stations
ListStationsPage.NewStation=Nouvelle station

ListSpecimensPage=Spécimens
ListSpecimensPage.NewSpecimen=Nouveau spécimen

ListExtractionsPage=Extractions
ListExtractionsPage.NewExtraction=Nouvelle extraction

ListPurificationsPage=Purifications
ListPurificationsPage.NewPurification=Nouvelle purification

ListMoleculesPage=Molécules
ListMoleculesPage.NewMolecule=Nouvelle molécule

ListTestsBioPage=Résultats des tests biologiques
ListTestsBioPage2=Tests biologiques
ListTestsBioPage.NewTestBio=Nouveau test biologique

ListDosagesPage=Dosages
ListDosagesPage2=Dosages

ListDosagePage=Dosages
ListDosagePage.NewDosage=Nouveau dosage

ListPersonnesInterrogeesPage=Personnes interrogées
ListPersonnesInterrogeesPage.NewPersonneInterrogee=Nouvelle personne interrogée

ListRemedePage=Remèdes
ListRemedePage.NewRemede=Nouveau remède

ListDocumentsPage.NewDocument=Joindre un nouveau document
ListDocumentsPage.AttachedDocuments=Document(s) attaché(s)

ManageCampagnePage=Gestion d'une campagne
ManageCampagnePage.Create.OK=Campagne ajoutée
ManageCampagnePage.Create.DataConstraintException=Le nom existe déjà
ManageCampagnePage.Update.OK=Campagne mise à jour
ManageCampagnePage.Update.DataConstraintException=Le nom existe déjà
ManageCampagnePage.Delete.OK=Campagne supprimée
ManageCampagnePage.Delete.DataConstraintException=Il existe des données liées à cette campagne
ManageCampagnePage.Authorizations=Droits
ManageCampagnePage.Campagnes.Lot.null=Tous
ManageCampagnePage.Authorizations.Lot=Échantillons / Connaissances
ManageCampagnePage.Authorizations.Lot.info=Références des Lots / Remèdes

ManageLotPage=Gestion d'un lot
ManageLotPage.Create.OK=Lot créé
ManageLotPage.Create.DataConstraintException=La référence existe déjà
ManageLotPage.Update.OK=Lot mis à jour
ManageLotPage.Update.DataConstraintException=La référence existe déjà
ManageLotPage.Delete.OK=Lot supprimé
ManageLotPage.Delete.DataConstraintException=Il existe des données liées à ce lot

ManagePersonnePage=Gestion d'une personne
ManagePersonnePage.Create.OK=Personne créée
ManagePersonnePage.Create.DataConstraintException=Le courriel ou le prénom + nom existe déjà
ManagePersonnePage.Update.OK=Personne mise à jour
ManagePersonnePage.Update.DataConstraintException=Le courriel ou le prénom + nom existe déjà
ManagePersonnePage.Delete.OK=Personne supprimée
ManagePersonnePage.Delete.DataConstraintException=Il existe des données liées à cette personne
ManagePersonnePage.CreateUtilisateur=Créer un compte utilisateur
ManagePersonnePage.CreateUtilisateur.OK=Compte utilisateur créé
ManagePersonnePage.CreateUtilisateur.DataConstraintException=Le courriel ou le prénom + nom existe déjà
ManagePersonnePage.CreateUtilisateur.EmailException=Problème lors de l'envoi du mot de passe

ManageStationPage=Gestion d'une station
ManageStationPage.Create.OK=Station ajoutée
ManageStationPage.Create.DataConstraintException=Le nom existe déjà
ManageStationPage.Update.OK=Station mise à jour
ManageStationPage.Update.DataConstraintException=Le nom existe déjà
ManageStationPage.Delete.OK=Station supprimée
ManageStationPage.Delete.DataConstraintException=Il existe des données liées à cette station

ManageSpecimenPage=Gestion d'un spécimen
ManageSpecimenPage.Create.OK=Spécimen ajouté
ManageSpecimenPage.Create.DataConstraintException=La référence existe déjà
ManageSpecimenPage.Update.OK=Spécimen mis à jour
ManageSpecimenPage.Update.DataConstraintException=La référence existe déjà
ManageSpecimenPage.Delete.OK=Spécimen supprimée
ManageSpecimenPage.Delete.DataConstraintException=Il existe des données liées à ce spécimen

ManageExtractionPage=Gestion d'une extraction
ManageExtractionPage.Create.OK=Extraction ajoutée
ManageExtractionPage.Create.DataConstraintException=Erreur non identifiée lors de la création
ManageExtractionPage.Update.OK=Extraction mise à jour
ManageExtractionPage.Update.DataConstraintException=Erreur non identifiée lors de la mise à jour
ManageExtractionPage.Delete.OK=Extraction supprimée
ManageExtractionPage.Delete.DataConstraintException=Il existe des données liées à cette extraction

ManagePurificationPage=Gestion d'une purification
ManagePurificationPage.Create.OK=Purification ajoutée
ManagePurificationPage.Create.DataConstraintException=Erreur non identifiée lors de la création
ManagePurificationPage.Update.OK=Purification mise à jour
ManagePurificationPage.Update.DataConstraintException=Erreur non identifiée lors de la mise à jour
ManagePurificationPage.Delete.OK=Purification supprimée
ManagePurificationPage.Delete.DataConstraintException=Il existe des données liées à cette purification

ManageMoleculePage=Gestion d'une molécule
ManageMoleculePage.Create.DataConstraintException=Impossible de sauver la molécule
ManageMoleculePage.Create.OK=Molecule ajoutée
ManageMoleculePage.Update.DataConstraintException=Impossible de modifier la molécule
ManageMoleculePage.Update.OK=Molecule mise à jour
ManageMoleculePage.Delete.DataConstraintException=Impossible de supprimer la molécule
ManageMoleculePage.Delete.OK=Molecule supprimée

ManageTestBioPage=Gestion d'un test biologique
ManageTestBioPage.Create.OK=Test biologique ajouté
ManageTestBioPage.Create.DataConstraintException=Erreur non identifiée lors de la création
ManageTestBioPage.Update.OK=Test biologique mis à jour
ManageTestBioPage.Update.DataConstraintException=Erreur non identifiée lors de la mise à jour
ManageTestBioPage.Delete.OK=Test biologique supprimé
ManageTestBioPage.Delete.DataConstraintException=Il existe des données liées à ce test biologique

ManageDosagePage=Gestion d'un dosage
ManageDosagePage.Create.OK=Dosage ajouté
ManageDosagePage.Create.DataConstraintException=Erreur non identifiée lors de la création
ManageDosagePage.Update.OK=Dosage mis à jour
ManageDosagePage.Update.DataConstraintException=Erreur non identifiée lors de la mise à jour
ManageDosagePage.Delete.OK=Dosage supprimé
ManageDosagePage.Delete.DataConstraintException=Il existe des données liées à ce dosage

ManageDocumentPage=Gestion d'un document
ManageDocumentPage.Create.OK=Document ajouté
ManageDocumentPage.CreateLater.OK=L'enregistrement du document s'effectuera à la validation du formulaire
ManageDocumentPage.Update.OK=Document mis à jour
ManageDocumentPage.UpdateDocument.OK=Document mis à jour
ManageDocumentPage.UpdateLater.OK=La modification du document s'effectuera à la validation du formulaire
ManageDocumentPage.Delete.OK=Document supprimé
ManageDocumentPage.DeleteLater.OK=La suppression du document s'effectuera à la validation du formulaire
ManageDocumentPage.Error.notAllowedExtension=Extension de fichier non autorisée
ManageDocumentPage.Error.emptyFile=Le fichier est requis
ManageDocumentPage.Form.uploadTooLarge=La taille du document ne peut pas dépasser ${maxSize.megabytes()} Mo

UpdateUtilisateurPage=Gestion du compte
UpdateUtilisateurPage.Password=Mot de passe
UpdateUtilisateurPage.Profile=Profil
UpdateUtilisateurPage.UpdateProfile.OK=Profil mis à jour
UpdateUtilisateurPage.UpdateProfile.DataConstraintException=Le courriel ou le prénom + nom existe déjà
UpdateUtilisateurPage.UpdatePassword.OK=Mot de passe mis à jour
UpdateUtilisateurPage.Delete.OK=Compte supprimé
UpdateUtilisateurPage.Delete.DataConstraintException=Suppression impossible, il doit rester au moins un administrateur
UpdateUtilisateurPage.Delete.EmailException=Problème lors de la confirmation de suppression de compte utilisateur

ListConfigurationPage=Configuration
ListConfigurationPage.Parties=Liste des parties d'un lot
ListPartiePanel.NewPartie=Nouvelle partie
ListConfigurationPage.MethodesExtraction=Liste des méthodes d'extraction
ListMethodeExtractionPanel.NewMethodeExtraction=Nouvelle méthode
ListConfigurationPage.MethodesPurification=Liste des méthodes de purification
ListMethodePurificationPanel.NewMethodePurification=Nouvelle méthode
ListConfigurationPage.MethodesTestBio=Liste des méthodes de test biologique
ListMethodeTestBioPanel.NewMethodeTestBio=Nouvelle méthode
ListConfigurationPage.ErreursTestBio=Liste des erreurs pour un test biologique
ListErreurTestBioPanel.NewErreurTestBio=Nouvelle erreur
ListConfigurationPage.MethodesDosage=Liste des méthodes de dosage
ListMethodeDosagePanel.NewMethodeDosage=Nouvelle méthode de dosage
ListConfigurationPage.ErreursDosage=Liste des erreurs pour un dosage
ListErreurDosagePanel.NewErreurDosage=Nouvelle erreur
ListConfigurationPage.TypesDocument=Liste des types de documents
ListTypeDocumentPanel.NewTypeDocument=Nouveau type de document
ListConfigurationPage.UnitesConcMasse=Liste des unités pour un dosage ou un test biologique
ListUniteConcMassePanel.NewUniteConcMasse=Nouvelle unité
ListConfigurationPage.Disponibilites=Liste des disponibilités
ListDisponibilitePanel.NewDisponibilite=Nouvelle disponibilité
ListConfigurationPage.Referentiels=Liste des référentiels pour un remède
ListConfigurationPage.ManageConfiguration=Configuration de l'instance
ListConfigurationPage.RebuildLuceneIndex=Reconstruire l'index lucene
RebuildLuceneIndexPanel.rebuildInfo=Appuyer une seule fois sur le bouton et attendre environ 1 minute l'affichage du message de fin d'indexation.

ManagePartiePage=Gestion d'une partie
ManagePartiePage.Create.OK=Partie créée
ManagePartiePage.Create.DataConstraintException=La partie existe déjà
ManagePartiePage.Update.OK=Partie mise à jour
ManagePartiePage.Update.DataConstraintException=La partie existe déjà
ManagePartiePage.Delete.OK=Partie supprimée
ManagePartiePage.Delete.DataConstraintException=Il existe des données liées à cette partie

ManageErreurTestBioPage=Gestion d'une erreur
ManageErreurTestBioPage.Create.OK=Erreur créée
ManageErreurTestBioPage.Create.DataConstraintException=Le nom existe déjà
ManageErreurTestBioPage.Update.OK=Erreur mise à jour
ManageErreurTestBioPage.Update.DataConstraintException=Le nom existe déjà
ManageErreurTestBioPage.Delete.OK=Erreur supprimée
ManageErreurTestBioPage.Delete.DataConstraintException=Il existe des données liées à cette erreur

ManageMethodeTestBioPage=Gestion d'une méthode de test
ManageMethodeTestBioPage.Create.OK=Méthode créée
ManageMethodeTestBioPage.Create.DataConstraintException=Le nom ou la cible existe déjà
ManageMethodeTestBioPage.Update.OK=Méthode mise à jour
ManageMethodeTestBioPage.Update.DataConstraintException=Le nom ou la cible existe déjà
ManageMethodeTestBioPage.Delete.OK=Méthode supprimée
ManageMethodeTestBioPage.Delete.DataConstraintException=Il existe des données liées à cette méthode

ManageMethodeDosagePage=Gestion d'une méthode de dosage
ManageMethodeDosagePage.Create.OK=Méthode créée
ManageMethodeDosagePage.Create.DataConstraintException=Le nom ou l'acronyme existe déjà
ManageMethodeDosagePage.Update.OK=Méthode mise à jour
ManageMethodeDosagePage.Update.DataConstraintException=Le nom ou l'acronyme existe déjà
ManageMethodeDosagePage.Delete.OK=Méthode supprimée
ManageMethodeDosagePage.Delete.DataConstraintException=Il existe des données liées à cette méthode

ManageErreurDosagePage=Gestion d'une erreur
ManageErreurDosagePage.Create.OK=Erreur créée
ManageErreurDosagePage.Create.DataConstraintException=Le nom existe déjà
ManageErreurDosagePage.Update.OK=Erreur mise à jour
ManageErreurDosagePage.Update.DataConstraintException=Le nom existe déjà
ManageErreurDosagePage.Delete.OK=Erreur supprimée
ManageErreurDosagePage.Delete.DataConstraintException=Il existe des données liées à cette erreur

ManageMethodeExtractionPage=Gestion d'une méthode d'extraction
ManageMethodeExtractionPage.Create.OK=Méthode créée
ManageMethodeExtractionPage.Create.DataConstraintException=Le nom existe déjà
ManageMethodeExtractionPage.Update.OK=Méthode mise à jour
ManageMethodeExtractionPage.Update.DataConstraintException=Le nom existe déjà
ManageMethodeExtractionPage.Delete.OK=Méthode supprimée
ManageMethodeExtractionPage.Delete.DataConstraintException=Il existe des données liées à cette méthode

ManageMethodePurificationPage=Gestion d'une méthode de purification
ManageMethodePurificationPage.Create.OK=Méthode créée
ManageMethodePurificationPage.Create.DataConstraintException=Le nom existe déjà
ManageMethodePurificationPage.Update.OK=Méthode mise à jour
ManageMethodePurificationPage.Update.DataConstraintException=Le nom existe déjà
ManageMethodePurificationPage.Delete.OK=Méthode supprimée
ManageMethodePurificationPage.Delete.DataConstraintException=Il existe des données liées à cette méthode

ManageTypeDocumentPage=Gestion d'un type de document
ManageTypeDocumentPage.Create.OK=Type de document créé
ManageTypeDocumentPage.Create.DataConstraintException=Le nom existe déjà
ManageTypeDocumentPage.Update.OK=Type de document mise à jour
ManageTypeDocumentPage.Update.DataConstraintException=Le nom existe déjà
ManageTypeDocumentPage.Delete.OK=Type de document supprimé
ManageTypeDocumentPage.Delete.DataConstraintException=Il existe des données liées à ce type de document

ManageUniteConcMassePage=Gestion d'une unité
ManageUniteConcMassePage.Create.OK=Unité créée
ManageUniteConcMassePage.Create.DataConstraintException=Le code ou l'unité existe déjà
ManageUniteConcMassePage.Update.OK=Unité mise à jour
ManageUniteConcMassePage.Update.DataConstraintException=Le code ou l'unité existe déjà
ManageUniteConcMassePage.Delete.OK=Unité supprimée
ManageUniteConcMassePage.Delete.DataConstraintException=Il existe des données liées à cette unité

ManageDisponibilitePage=Gestion d'une disponibilité
ManageDisponibilitePage.Create.OK=Disponibilité créée
ManageDisponibilitePage.Create.DataConstraintException=La disponibilité existe déjà
ManageDisponibilitePage.Update.OK=Disponibilité mise à jour
ManageDisponibilitePage.Update.DataConstraintException=La disponibilité existe déjà
ManageDisponibilitePage.Delete.OK=Disponibilité supprimée
ManageDisponibilitePage.Delete.DataConstraintException=Il existe des données liées à cette disponibilité

ManagePersonneInterrogeePage=Gestion d'une personne interrogée
ManagePersonneInterrogeePage.Create.OK=Personne interrogée créée
ManagePersonneInterrogeePage.Create.DataConstraintException=La personne existe déjà
ManagePersonneInterrogeePage.Update.OK=Personne interrogée mise à jour
ManagePersonneInterrogeePage.Update.DataConstraintException=La personne existe déjà
ManagePersonneInterrogeePage.Delete.OK=Personne interrogée supprimée
ManagePersonneInterrogeePage.Delete.DataConstraintException=Il existe des données liées à cette personne interrogée

ListRemedePage.Citation=Tri par citation
ListRemedePage.Alphanumerique=Tri alphanumérique

ManageRemedePage=Gestion d'un remède
ManageRemedePage.Create.OK=Remède créé
ManageRemedePage.Create.DataConstraintException=Le remède existe déjà
ManageRemedePage.Update.OK=Remède mis à jour
ManageRemedePage.Update.DataConstraintException=Le remède existe déjà
ManageRemedePage.Delete.OK=Remède supprimé
ManageRemedePage.Delete.DataConstraintException=Il existe des données liées à ce remède

ReadCampagnePage=Consultation d'une campagne
ReadLotPage=Consultation d'un lot
ReadPersonnePage=Consultation d'une personne
ReadSpecimenPage=Consultation d'un spécimen
ReadUtilisateurPage=Consultation d'un utilisateur
ReadStationPage=Consultation d'une station
ReadExtractionPage=Consultation d'une extraction
ReadPurificationPage=Consultation d'une purification
ReadMoleculePage=Consultation d'une molécule
ReadTestBioPage=Consultation d'un test biologique
ReadDosagePage=Consultation d'un dosage
ReadPersonneInterrogeePage=Consultation d'une personne interrogée
ReadRemedePage=Consultation d'un remède
ReadDocumentPage=Consultation d'un document

SearchPage=Recherche
SearchPage.Search=Rechercher
SearchPage.Transpose=Transposer
SearchPage.SearchOnMap=Rechercher dans la zone affichée
SearchPage.Query=Requête
SearchPage.Country=Pays
SearchPage.Latitude=Bornes de latitude
SearchPage.Longitude=Bornes de longitude
SearchPage.Clear=Réinitialiser
SearchPage.DocumentType=Type de document
SearchPage.Campagnes=Campagnes
SearchPage.Specimens=Specimens
SearchPage.Lots=Lots
SearchPage.Extractions=Extractions
SearchPage.Purifications=Purifications
SearchPage.ResultatTestBios=Tests biologiques
SearchPage.Stations=Stations
SearchPage.Molecules=Molécules
SearchPage.Dosages=Dosages
SearchPage.PersonnesInterrogees=Personnes Interrogées
SearchPage.Remedes=Remèdes
SearchPage.QuerySyntax=Rechercher des termes (ou des parties de termes avec *). Pour une utilisation plus avancée de la recherche, consulter la ${advancedLink}.
SearchPage.QuerySyntaxAdvanced=synthaxe complète

#MODELS#

CaptchaModel.captcha=Captcha
CaptchaModel.captchaText=Recopier le nombre
CaptchaModel.captchaText.KO=Recopier le nombre correspondant à l'image

RegisterModel.password=Mot de passe
RegisterModel.passwordConfirmation=Confirmation
RegisterModel.passwordConfirmation.KO=Les mots de passe doivent être identiques

LoginModel.password=Mot de passe
LoginModel.rememberMe=Se souvenir de moi

ManageStationModel.latitudeDegrees=Latitude degrés
ManageStationModel.latitudeDegrees.IConverter.Integer=Latitude degrés - doit être un nombre entier
ManageStationModel.latitudeMinutes=Latitude minutes
ManageStationModel.latitudeMinutes.IConverter.Double=Latitude minutes - doit être un nombre décimal
ManageStationModel.latitudeOrientation=Latitude orientation
ManageStationModel.longitudeDegrees=Longitude degrés
ManageStationModel.longitudeDegrees.IConverter.Integer=Longitude degrés - doit être un nombre entier
ManageStationModel.longitudeMinutes=Longitude minutes
ManageStationModel.longitudeMinutes.IConverter.Double=Longitude minutes - doit être un nombre décimal
ManageStationModel.longitudeOrientation=Longitude orientation

UpdateUtilisateurModel.password=Mot de passe 2
UpdateUtilisateurModel.password.KO=Mot de passe invalide
UpdateUtilisateurModel.currentPassword=Mot de passe
UpdateUtilisateurModel.currentPassword.KO=Mot de passe invalide
UpdateUtilisateurModel.newPassword=Nouveau mot de passe
UpdateUtilisateurModel.newPasswordConfirmation=Confirmation
UpdateUtilisateurModel.newPasswordConfirmation.KO=Les nouveaux mots de passe doivent être identiques

ContactModel.mail=Votre courriel
ContactModel.subject=Votre sujet
ContactModel.message=Votre message


Campagne.dates.KO=Les dates ne sont pas correctement saisies
Campagne.dateDeb.IConverter.Date=La date de début est mal formatée (JJ/MM/AA)
Campagne.dateFin.IConverter.Date=La date de fin est mal formatée (JJ/MM/AA)
Campagne.mentionLegale.info=Indiquez dans l'ordre : les organismes producteurs des données – les bailleurs de fond – la/les année(s) concernée(s) par le programme
Campagne.mentionLegale.info2=organismes producteurs des données – bailleurs de fond – année(s) concernée(s) par le programme 
Campagne.complement.info=N'oubliez pas d'indiquer les politiques de confidentialité et de publication des données
Campagne.stationsNotAccessibles=Certaines stations ne sont pas affichées car vous n'avez pas les droits sur l'ensemble de la campagne.

Station.coordonnees=Coordonnées
Station.coordonnees.KO=Pour enregistrer des coordonnées, tous les champs doivent être renseignés

Specimen.dateDepot.IConverter.Date=La date de dépot est mal formatée (JJ/MM/AA)

Lot.station.info=Si la station voulue n'apparait pas, contactez le créateur de la campagne pour qu'il l'ajoute à la liste des stations prospectées
Lot.dateRecolte.IConverter.Date=La date de récolte est mal formatée (JJ/MM/AA)
Lot.masseFraiche.IConverter.BigDecimal=La masse fraîche doit être un nombre décimal
Lot.masseSeche.IConverter.BigDecimal=La masse sèche doit être un nombre décimal

Extraction.masseDepart.IConverter.BigDecimal=Masse à extraire - doit être un nombre décimal
Extraction.date.IConverter.Date=La date est mal formatée (JJ/MM/AA)
Extraction.extraits.masseObtenue.IConverter.BigDecimal=Masse obtenue - doit être un nombre décimal 
Extraction.extraits.masseObtenue=Masse obtenue
Extraction.notUnique=Réf. extraction - existe déjà
Extrait.notUnique=Réf. extrait - existe déjà
Extrait.isReferenced=Il existe des données liées à cet extrait
TypeExtrait.isReferenced=Le type d'initial '${0}' ne peut être supprimé car il existe des données qui le réfèrent

Purification.masseDepart.IConverter.BigDecimal=Masse à purifier - doit être un nombre décimal
Purification.fractions.masseObtenue.IConverter.BigDecimal=Masse obtenue - doit être un nombre décimal 
Purification.fractions.masseObtenue=Masse obtenue
Purification.date.IConverter.Date=La date est mal formatée (JJ/MM/AA)
Purification.notUnique=Réf. purification - existe déjà
Fraction.notUnique=Réf. fraction - existe déjà
Fraction.isReferenced=Il existe des données liées à cette fraction
ParamMethoPuri.isReferenced=Le paramètre '${0}' ne peut être supprimé car il existe des données qui le réfèrent
MethodePurification.parametres.index.IConverter.Integer=L'index doit être un entier
MethodePurification.parametres.index=Index

Molecule.provenanceNotAccessibles=Certains produits associés ne sont pas affichés car vous n'avez pas les droits nécéssaires
Molecule.masseMolaire.Required=Masse molaire - ne peut être nul
Molecule.masseMolaire.IConverter.BigDecimal=Masse molaire - doit être un nombre décimal
Molecule.downloadMolFile=Télécharger au format mol
Molecule.formuleBrute.info=Indiquez dans l'ordre : Carbone, Hydrogène puis ordre alphabetique
Molecule.nouvMolecul.info=Les molécules sont visibles par tous les utilsateurs. Attention à ce que la molécule soit bien publiée.

TestBio.date.IConverter.Date=La date est mal formatée (JJ/MM/AA)
TestBio.concMasseDefaut.IConverter.BigDecimal=Conc./Masse par défaut - doit être un nombre décimal
TestBio.resultats.valeur.IConverter.BigDecimal=Valeur - doit être un nombre décimal
TestBio.resultats.repere.Required=Repère - ne peut être nul
TestBio.resultats.repere=Repère
TestBio.resultats.typeResultat.Required=Type - ne peut être nul
TestBio.resultats.typeResultat=Type
TestBio.resultats.produit.Required=Réf produit - ne peut être nul
TestBio.resultats.produit=Réf. produit
TestBio.resultats.produitTemoin.Required=Prod. Témoin - ne peut être nul
TestBio.resultats.produitTemoin=Prod. Témoin
TestBio.resultats.concMasse.IConverter.BigDecimal=Conc./Masse - doit être un nombre décimal
TestBio.resultats.concMasse=Conc./Masse
TestBio.resultats.uniteConcMasse=Conc./Masse
TestBio.resultats.concMasse.KO=Pour enregistrer Conc./Masse, les deux champs doivent être renseignés
TestBio.resultats.stade.Required=Stade - ne peut être nul
TestBio.resultats.stade=Stade
TestBio.resultats.valeur.Required=Valeur - ne peut être nulle
TestBio.resultats.valeur=Valeur
TestBio.notUnique=Réf. test - existe déjà
TestBio.resultats.noProduit=Le test biologique doit comporter au moins un résultat de type "produit"
TestBio.resultsNotAccessibles=Certains résultats ne sont pas affichés car vous n'avez pas les droits nécessaires sur leur produit.

Dosage.resultsNotAccessibles=Certains résultats ne sont pas affichés car vous n'avez pas les droits nécessaires sur leur produit ou lot.
Dosage.resultats.analyseRefMismatch=Il ne peut y avoir qu'un échantillon par analyse.
Dosage.resultats.composeDose=Pour cette analyse, la valeur de ce composé ou de cette molécule a déjà été saisie.
Dosage.resultats.compose.Required=Pour cette analyse, vous devez saisir un composé.
Dosage.resultats.moleculeDosee=Pour cette analyse, la valeur de ce composé ou de cette molécule a déjà été saisie.
Dosage.resultats.molecule.Required=ou choisir une molécule dans la liste.
Dosage.resultats.refEchantillon.Required=Pour cette analyse, veuillez indiquer la référence de l’échantillon
Dosage.resultats.valeur.Required=Pour cette analyse, la valeur est obligatoire
Dosage.resultats.analyseNb.Required=Pour cette analyse, le numéro d'analyse est obligatoire

PersonneInterrogee.station.info=Si la station voulue n'apparait pas, contactez le créateur de la campagne pour qu'il l'ajoute à la liste des stations prospectées

Remede.ingredientsNotAccessibles=Certains résultats ne sont pas affichés car vous n'avez pas les droits nécessaires sur leur lot.
Remede.ingredients.empty=Le remède doit comporter au moins un ingrédient
Remede.station.info=Si la station voulue n'apparait pas, contactez le créateur de la campagne pour qu'il l'ajoute à la liste des stations prospectées
Remede.classification.info=Tapez un mot clé ou * pour afficher toutes les valeurs


Document.dateCreation.IConverter.Date=La date est mal formatée (JJ/MM/AA)
Document.link=Lien
Document.type=Type
Document.file=Fichier
Document.file.info=La taille du fichier doit faire moins de ${0}Mo
Document.apercu=Aperçu

#BASIC#
Actions=Actions
Submit=Valider
Cancel=Annuler
Valid=Valider
Rebuild=Reconstruire
Reject=Refuser
Create=Ajouter
Add=Ajouter
Update=Modifier
Delete=Supprimer
Back=Retour
Read=Consulter
ReadExtraction=Consulter l'extraction
ReadPurification=Consulter la purification
ReadLot=Consulter le lot
TODO=ToDo
Confirm=Êtes-vous sûr ?
ExportTo=Exporter en
List.none=La liste est vide.
Boolean.true=oui
Boolean.false=non

NavigatorLabel=Lignes ${from} à ${to} (${of} résultats)
datatable.no-records-found=Aucun résultat.