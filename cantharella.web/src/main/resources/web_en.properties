###
# #%L
# Cantharella :: Web
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# #L%
###
#Header & footer
TemplatePage.Subtitle=Pharmacochemical database of natural substances
TemplatePage.Copyright=All rights reserved
TemplatePage.MainNav=Main Navigation
TemplatePage.ContentNav=Go to content
TemplatePage.Welcome=Welcome
TemplatePage.Logout=Logout
TemplatePage.Logout.OK=You were disconnected
TemplatePage.Administration=Administration
TemplatePage.Sampling=Sampling
TemplatePage.Processing=Processing
TemplatePage.Chemistry=Chemistry
TemplatePage.Biology=Biology
TemplatePage.Knowledge=Knowledge
TemplatePage.Search=Search
TemplatePage.Search.Label=Search in the database: 
TemplatePage.Search.Launch=Launch to search in the database
TemplatePage.Lang=en
TemplatePage.Lang.Choice=Language Choice
TemplatePage.Css.Menu=css/menu_en.css


#PAGES & ACTIONS#

HomePage=Home
HomePage.InvalidUtilisateurs=Requests for accounts creations
HomePage.Login=Log in
HomePage.Login.OK=Successful login
HomePage.Login.KO=Invalid email or password
HomePage.Login.NotYetValid=Your account creation request has not yet been validated
HomePage.Statistics=Statistics
HomePage.Rights=Your rights
HomePage.Rights.Admin=You are logged in as administrator, you have access to all information.
HomePage.Rights.User=You have rights over all samples of {0} campaign(s) and especially over {1} sample(s).
HomePage.Rights.User.Details=+ details
HomePage.Rights.User2=You are also creator of {0} campaign(s) and have rights over all its samples. 

#for statistics
Personnes.stat=Persons
Campagnes.stat=Campaigns
Stations.stat=Locations
Specimens.stat=Specimens
Lots.stat=Samples
Extractions.stat=Extractions
Purifications.stat=Purifications
Molecules.stat=Molecules
ResultatsTestsBio.stat=Bioassay results
Dosages.stat=Dosages
PersonnesInterrogees.stat=Respondents
Remedes.stat=Remedies

ErrorPage=Error
ErrorPage.AccessDeniedPage=You are not allowed to access this page
ErrorPage.InternalErrorPage=Internal error
ErrorPage.PageExpiredErrorPage=The page has expired

ContactPage=Contact
ContactPage.Info=Send a message to the database managers by completing the form below.
ContactPage.Send.OK=Message sent
ContactPage.Send.EmailException=Problems when sending the message

ImprintPage=Imprint
ImprintPage.AccessRights=Right of access, rectification, deletion of the name specific data
ImprintPage.Editor=Publisher
ImprintPage.Hosting=Hosting
ImprintPage.Reproduction=Copyright and license
ImprintPage.Reproduction.data=Concerning the data
ImprintPage.Reproduction.source=Concerning the software part of the Information System
ImprintPage.Reproduction.5=Cantharella is at your disposal under license <a href="http://www.gnu.org/licenses/why-affero-gpl.html">Affero GPL</a>.
ImprintPage.Reproduction.6=All the source code of this product is available on the <a href="http://forge.codelutin.com/projects/cantharella">Code Lutin's</a> forge and on HAL (<a href="https://hal.archives-ouvertes.fr/hal-01887126">HAL-01887126</a>) in accordance to the licenses of the used open source components:
ImprintPage.Responsability=Limitation of responsability
ImprintPage.Responsability.1=Users are responsible of the queries formulated , of the interpretation and use of the results. They must comply with the regulations and recommendations of the CNIL concerning the data of a personal nature.
ImprintPage.Responsability.2=IRD provides no guarantee of any kind and for any purpose whatsoever, in relation to the software application and its contents. In any case, the IRD can not be held responsible for any damage of any nature whatsoever, including loss of use, loss of data or any other financial loss resulting from the use of or inability to use.
ImprintPage.Credits=Credits
ImprintPage.Credits.1.Title=Project Leader
ImprintPage.Credits.1.Value=Sylvain PETEK
ImprintPage.Credits.2.Title=IT Project Manager
ImprintPage.Credits.2.Value=Adrien CHEYPE
ImprintPage.Credits.3.Title=Conception
ImprintPage.Credits.3.Value=Sylvain PETEK<br />Adrien CHEYPE<br />François CHASSAGNE (Knowledge module)<br />Maelle CARRAZ (Knowledge module)
ImprintPage.Credits.4.Title=Development
ImprintPage.Credits.4.Value=Adrien CHEYPE<br />Mickael TRICOT<br />Alban DIGUER<br />Benjamin POUSSIN<br />Éric CHATELLIER<br />Yannick MARTEL<br />Cécilia BOSSARD<br />Jean COUTEAU
ImprintPage.Credits.5.Title=Graphic design
ImprintPage.Credits.5.Value=Elise COSTE, Pôle web of the IRD<br />Adrien CHEYPE
ImprintPage.Credits.6.Title=Logo
ImprintPage.Credits.6.Value=Fabrice CHARLEUX
ImprintPage.Credits.7.Title=Pictures of the banner
ImprintPage.Credits.7.Value=Image Bank <a href="https://www.multimedia.ird.fr/">IRD Multimedia</a>, <a href="http://www.pedagogeeks.fr/archives/1018/molecule">Pedagogeeks</a> (license Creative Commons) for the Chemistry tab and François Chassagne (licence CC-BY-NC-SA) for the Knowledge tab.
ImprintPage.Credits.8.Title=Funding
ImprintPage.Credits.8.Value=<a href="https://www.ird.fr/dsi/">DDUNI</a> de l'IRD : AAP SPIRALES 2008, 2009, 2010, 2012<br /><a href="http://www.ird.fr/l-ird/organigramme/direction-generale-deleguee-a-l-aird-dgd-aird/direction-de-la-valorisation-au-sud-dvs">DVS</a> de l'IRD : AAP « Maturation de projet innovant » : 2011<br /><a href="https://www-iuem.univ-brest.fr/lemar/">LEMAR</a> (IRD) : 2020 <br/> Département <a href="https://www.ird.fr/ressources-oceaniques">OCEANS</a> de l'IRD : 2021<br/>Département <a href="https://www.ird.fr/sante-et-societes-sas">SAS</a> de l'IRD : 2024<br/>Projets ANR HABIS, ANR Save-C, CGO MEREOS (Région Bretagne) : 2024<br/>Plateforme <a href="https://www-iuem.univ-brest.fr/lemar/les-poles/lipidocean">Lipidocéan</a> : 2024
ImprintPage.Credits.9.Title=Support
ImprintPage.Credits.9.Value=<a href="http://www.upmc.fr/fr/recherche/pole_4/pole_vie_et_sante/systematique_adaptation_evolution_sae_umr_7138.html">UMR7138</a> – Systématique, Adaptation, Evolution (SAE)<br /><a href="http://www.pharmadev.ird.fr/">UMR152</a> – Pharma-Dev<br /><a href="https://wwz.ifremer.fr/umr_eio/">UMR241</a> - Ecosystèmes Insulaires Océaniens (EIO)<br /><a href="https://www-iuem.univ-brest.fr/lemar/">UMR6539</a> - Laboratoire des sciences de l’Evironnement MARin (LEMAR)

ResetPasswordPage=Password forgotten
ResetPasswordPage.Reset.OK=New password sent by email
ResetPasswordPage.Reset.DataNotFoundException=Email does not correspond to any registred user
ResetPasswordPage.Reset.EmailException=Problems when sending the new password

RegisterPage=Account creation request
RegisterPage.Register.OK=Account creation request sent
RegisterPage.Register.DataConstraintException=Email or First name + Last name already exist

ManageUtilisateurPage=User account management
ManageUtilisateurPage.Profile=Profile
ManageUtilisateurPage.Authorizations=Rights
ManageUtilisateurPage.Update.OK=Account updated
ManageUtilisateurPage.Update.DataConstraintException=Email or first name + last name already exist
ManageUtilisateurPage.Valid.OK=Account validated and updated
ManageUtilisateurPage.Valid.DataConstraintException=Email or first name + last name already exist
ManageUtilisateurPage.Delete.OK=User deleted
ManageUtilisateurPage.Delete.DataConstraintException=There are data created by this user
ManageUtilisateurPage.Delete.EmailException=Problem during the delete confirmation of the user account
ManageUtilisateurPage.Delete.Info=The removal erases the user account, but keeps the person if there are linked data.
ManageUtilisateurPage.Reject.OK=Account creation request rejected
ManageUtilisateurPage.Authorizations.Lot=Samples / Knowledge
ManageUtilisateurPage.Authorizations.Lot.info=Reference of Samples / Remedies
Authorizations.Campagnes.Lot.null=All

Import.EmptyFile=empty CSV file to complete
Import.CodePays=List of codePays
Import.CodeLangue=List of codeLangue
Import.csvInfo=Encoding in utf-8 and ";" as separator. Latitude and longitude in WGS84
Import.ListPersonnesPage=Import persons
Import.ListStationsPage=Import stations
Import.ListSpecimensPage=Import specimens
Import.ListPurificationsPage=Import purifications
Import.ListDosagePage=Import dosages
Import.ListTestsBioPage=Import bioassays
Import.ListLotsPage=Import samples
Import.ListExtractionsPage=Import extractions
Import.ListMoleculesPage=Import molecules
Import.ListPersonnesInterrogeesPage=Import respondents
Import.ListRemedePage=Import remedies
Import.ListReferentielPanel=Importer a referential
Import.Import=Import
Import.ImportSucess=Well imported
Import.ImportFailed=Errors on import

ListPersonnesPage=Persons
ListPersonnesPage.NewPersonne=New person
ListPersonnesPage.IsNotValid=(not validated)

ListCampagnesPage=Campaigns
ListCampagnesPage.NewCampagne=New campaign

ListLotsPage=Samples
ListLotsPage.NewLot=New sample

ListStationsPage=Locations
ListStationsPage.NewStation=New location

ListSpecimensPage=Specimens
ListSpecimensPage.NewSpecimen=New specimen

ListExtractionsPage=Extractions
ListExtractionsPage.NewExtraction=New extraction

ListPurificationsPage=Purifications
ListPurificationsPage.NewPurification=New purification

ListMoleculesPage=Molecules
ListMoleculesPage.NewMolecule=New molecule

ListTestsBioPage=Bioassay results 
ListTestsBioPage2=Bioassays 
ListTestsBioPage.NewTestBio=New bioassay

ListDosagesPage=Dosages
ListDosagesPage2=Dosages

ListDosagePage=Dosages
ListDosagePage.NewDosage=New dosage

ListPersonnesInterrogeesPage=Respondents
ListPersonnesInterrogeesPage.NewPersonneInterrogee=New respondent

ListRemedePage=Remedies
ListRemedePage.NewRemede=New remedy

ListDocumentsPage.NewDocument=Attach new document
ListDocumentsPage.AttachedDocuments=Attached documents

ManageCampagnePage=Campaign management 
ManageCampagnePage.Create.OK=Campaign added
ManageCampagnePage.Create.DataConstraintException=The name already exists
ManageCampagnePage.Update.OK=Campaign updated
ManageCampagnePage.Update.DataConstraintException=The name already exists 
ManageCampagnePage.Delete.OK=Campaign deleted
ManageCampagnePage.Delete.DataConstraintException=There are data linked to this campaign 
ManageCampagnePage.Authorizations=Rights
ManageCampagnePage.Campagnes.Lot.null=All
ManageCampagnePage.Authorizations.Lot=Samples / Knowledge
ManageCampagnePage.Authorizations.Lot.info=Reference of Samples / Remedies

ManageLotPage=Sample management
ManageLotPage.Create.OK=Sample created
ManageLotPage.Create.DataConstraintException=The reference already exists
ManageLotPage.Update.OK=Sample updated
ManageLotPage.Update.DataConstraintException=The reference already exists
ManageLotPage.Delete.OK=Sample deleted
ManageLotPage.Delete.DataConstraintException=There are data linked to this sample 

ManagePersonnePage=Person management
ManagePersonnePage.Create.OK=Person created
ManagePersonnePage.Create.DataConstraintException=Email or first name + last name already exist
ManagePersonnePage.Update.OK=Person updated
ManagePersonnePage.Update.DataConstraintException=Email or first name + last name already exist
ManagePersonnePage.Delete.OK=Person deleted
ManagePersonnePage.Delete.DataConstraintException= There are data linked to this person
ManagePersonnePage.CreateUtilisateur=Create a user account
ManagePersonnePage.CreateUtilisateur.OK=User account created
ManagePersonnePage.CreateUtilisateur.DataConstraintException=Email or first name + last name already exist
ManagePersonnePage.CreateUtilisateur.EmailException=Problems when sending the password

ManageStationPage=Location management
ManageStationPage.Create.OK=Location added
ManageStationPage.Create.DataConstraintException=The name already exists 
ManageStationPage.Update.OK=Location updated
ManageStationPage.Update.DataConstraintException=The name already exists 
ManageStationPage.Delete.OK=Location deleted 
ManageStationPage.Delete.DataConstraintException=There are data linked to this location

ManageSpecimenPage=Specimen management
ManageSpecimenPage.Create.OK=Specimen added
ManageSpecimenPage.Create.DataConstraintException=The reference already exists 
ManageSpecimenPage.Update.OK=Specimen updated
ManageSpecimenPage.Update.DataConstraintException=The reference already exists 
ManageSpecimenPage.Delete.OK=Specimen deleted
ManageSpecimenPage.Delete.DataConstraintException=There are data linked to this specimen

ManageExtractionPage=Extraction management
ManageExtractionPage.Create.OK=Extraction added
ManageExtractionPage.Create.DataConstraintException=Unidentified error during the creation
ManageExtractionPage.Update.OK=Extraction updated
ManageExtractionPage.Update.DataConstraintException=Unidentified error during the update
ManageExtractionPage.Delete.OK=Extraction deleted
ManageExtractionPage.Delete.DataConstraintException=There are data linked to this extraction

ManagePurificationPage=Purification management
ManagePurificationPage.Create.OK=Purification added
ManagePurificationPage.Create.DataConstraintException=Unidentified error during the creation
ManagePurificationPage.Update.OK=Purification updated
ManagePurificationPage.Update.DataConstraintException=Unidentified error during the update 
ManagePurificationPage.Delete.OK=Purification deleted
ManagePurificationPage.Delete.DataConstraintException=There are data linked to this purification

ManageMoleculePage=Molecule management
ManageMoleculePage.Create.DataConstraintException=Can't save molecule
ManageMoleculePage.Create.OK=Molecule added
ManageMoleculePage.Update.DataConstraintException=Cant' update molecule
ManageMoleculePage.Update.OK=Molecule updated
ManageMoleculePage.Delete.DataConstraintException=Can't delete molecule
ManageMoleculePage.Delete.OK=Molecule deleted

ManageTestBioPage=Bioassay management
ManageTestBioPage.Create.OK=Bioassay added
ManageTestBioPage.Create.DataConstraintException=Unidentified error during the creation
ManageTestBioPage.Update.OK=Bioassay updated
ManageTestBioPage.Update.DataConstraintException=Unidentified error during the update
ManageTestBioPage.Delete.OK=Bioassay deleted
ManageTestBioPage.Delete.DataConstraintException=There are data linked to this biassay

ManageDosagePage=Dosage management
ManageDosagePage.Create.OK=Dosage added
ManageDosagePage.Create.DataConstraintException=Unidentified error during the creation
ManageDosagePage.Update.OK=Dosage updated
ManageDosagePage.Update.DataConstraintException=Unidentified error during the update
ManageDosagePage.Delete.OK=Dosage deleted
ManageDosagePage.Delete.DataConstraintException=There is data linked to this biassay

ManageDocumentPage=Document management
ManageDocumentPage.Create.OK=Document added
ManageDocumentPage.CreateLater.OK=Document will be saved on form submit
ManageDocumentPage.Update.OK=Document updated
ManageDocumentPage.UpdateDocument.OK=Document updated
ManageDocumentPage.UpdateLater.OK=Document will be updated on form submit
ManageDocumentPage.Delete.OK=Document deleted
ManageDocumentPage.DeleteLater.OK=Document will be deleted on form submit
ManageDocumentPage.Error.notAllowedExtension=File extension not allowed
ManageDocumentPage.Error.emptyFile=File is mandatory
ManageDocumentPage.Form.uploadTooLarge=Document size can't be bigger than ${maxSize.megabytes()} Mb

UpdateUtilisateurPage=Account management
UpdateUtilisateurPage.Password=Password
UpdateUtilisateurPage.Profile=Profile
UpdateUtilisateurPage.UpdateProfile.OK=Profile updated 
UpdateUtilisateurPage.UpdateProfile.DataConstraintException=Email or First name + Last name already exist 
UpdateUtilisateurPage.UpdatePassword.OK=Password updated
UpdateUtilisateurPage.Delete.OK=Account deleted
UpdateUtilisateurPage.Delete.DataConstraintException=Deletion impossible, there must be at least one administrator
UpdateUtilisateurPage.Delete.EmailException=Problem during the deletion confirmation of the user account 

ListConfigurationPage=Configuration
ListConfigurationPage.Parties=Sample parts list
ListPartiePanel.NewPartie=New part
ListConfigurationPage.MethodesExtraction=Extraction methods list
ListMethodeExtractionPanel.NewMethodeExtraction=New method
ListConfigurationPage.MethodesPurification= Purification methods list 
ListMethodePurificationPanel.NewMethodePurification=New method
ListConfigurationPage.MethodesTestBio=Bioassay methods list
ListMethodeTestBioPanel.NewMethodeTestBio=New method
ListConfigurationPage.ErreursTestBio= Bioassay errors list
ListErreurTestBioPanel.NewErreurTestBio=New error
ListConfigurationPage.MethodesDosage=Dosage methods list
ListMethodeDosagePanel.NewMethodeDosage=New dosage method
ListConfigurationPage.ErreursDosage=Dosage errors list
ListErreurDosagePanel.NewErreurDosage=New error
ListConfigurationPage.TypesDocument=Document types list
ListTypeDocumentPanel.NewTypeDocument=New document type
ListConfigurationPage.UnitesConcMasse=Units for dosages and bioassays list
ListUniteConcMassePanel.NewUniteConcMasse=New unit
ListConfigurationPage.Disponibilites=Availabilities list
ListDisponibilitePanel.NewDisponibilite=New availability
ListConfigurationPage.Referentiels=Referentials for remedies list
ListConfigurationPage.ManageConfiguration=Instance configuration
ListConfigurationPage.RebuildLuceneIndex=Rebuild lucene index
RebuildLuceneIndexPanel.rebuildInfo=Push rebuild button only once and wait about 1 minute for end process message to display.

ManagePartiePage=Managing a part
ManagePartiePage.Create.OK=Part created
ManagePartiePage.Create.DataConstraintException=The part already exists
ManagePartiePage.Update.OK=Part updated
ManagePartiePage.Update.DataConstraintException=The part already exists 
ManagePartiePage.Delete.OK=Part deleted
ManagePartiePage.Delete.DataConstraintException=There are data linked to this part

ManageErreurTestBioPage=Managing an error
ManageErreurTestBioPage.Create.OK=Error created
ManageErreurTestBioPage.Create.DataConstraintException=The name already exists
ManageErreurTestBioPage.Update.OK=Error updated
ManageErreurTestBioPage.Update.DataConstraintException=The name already exists
ManageErreurTestBioPage.Delete.OK=Error deleted
ManageErreurTestBioPage.Delete.DataConstraintException=There are data linked to this error

ManageErreurDosagePage=Managing an error
ManageErreurDosagePage.Create.OK=Error created
ManageErreurDosagePage.Create.DataConstraintException=The name already exists
ManageErreurDosagePage.Update.OK=Error updated
ManageErreurDosagePage.Update.DataConstraintException=The name already exists
ManageErreurDosagePage.Delete.OK=Error deleted
ManageErreurDosagePage.Delete.DataConstraintException=There are data linked to this error

ManageMethodeTestBioPage=Managing a bioassay method
ManageMethodeTestBioPage.Create.OK=Method created
ManageMethodeTestBioPage.Create.DataConstraintException=The name or the target already exists
ManageMethodeTestBioPage.Update.OK=Method updated
ManageMethodeTestBioPage.Update.DataConstraintException=The name or the target already exists
ManageMethodeTestBioPage.Delete.OK=Method deleted
ManageMethodeTestBioPage.Delete.DataConstraintException=There are data linked to this method

ManageMethodeExtractionPage=Managing an extraction method
ManageMethodeExtractionPage.Create.OK=Method created
ManageMethodeExtractionPage.Create.DataConstraintException=The name already exists
ManageMethodeExtractionPage.Update.OK=Method updated
ManageMethodeExtractionPage.Update.DataConstraintException=The name already exists
ManageMethodeExtractionPage.Delete.OK=Method deleted
ManageMethodeExtractionPage.Delete.DataConstraintException=There are data linked to this method

ManageMethodePurificationPage=Managing a purification method
ManageMethodePurificationPage.Create.OK=Method created
ManageMethodePurificationPage.Create.DataConstraintException=The name already exists
ManageMethodePurificationPage.Update.OK=Method updated
ManageMethodePurificationPage.Update.DataConstraintException=The name already exists
ManageMethodePurificationPage.Delete.OK=Method deleted
ManageMethodePurificationPage.Delete.DataConstraintException=There are data linked to this method

ManageTypeDocumentPage=Managing a document type
ManageTypeDocumentPage.Create.OK=Document type created
ManageTypeDocumentPage.Create.DataConstraintException=The name already exists
ManageTypeDocumentPage.Update.OK=Document type updated
ManageTypeDocumentPage.Update.DataConstraintException=The name already exists
ManageTypeDocumentPage.Delete.OK=Document type deleted
ManageTypeDocumentPage.Delete.DataConstraintException=There are data linked to this document type

ManageUniteConcMassePage=Managing a unit
ManageUniteConcMassePage.Create.OK=Unit created
ManageUniteConcMassePage.Create.DataConstraintException=The code or unit already exists
ManageUniteConcMassePage.Update.OK=Unit updated
ManageUniteConcMassePage.Update.DataConstraintException=The code or unit already exists
ManageUniteConcMassePage.Delete.OK=Unit deleted
ManageUniteConcMassePage.Delete.DataConstraintException=There is data linked to this unit

ManagePersonneInterrogeePage=Managing a respondent
ManagePersonneInterrogeePage.Create.OK=Respondent created
ManagePersonneInterrogeePage.Create.DataConstraintException=The respondent already exists
ManagePersonneInterrogeePage.Update.OK=Respondent updated
ManagePersonneInterrogeePage.Update.DataConstraintException=The respondent already exists
ManagePersonneInterrogeePage.Delete.OK=Respondent deleted
ManagePersonneInterrogeePage.Delete.DataConstraintException=There is data linked to this respondent

ManageDisponibilitePage=Managing an availability
ManageDisponibilitePage.Create.OK=Availability created
ManageDisponibilitePage.Create.DataConstraintException=The availability already exists
ManageDisponibilitePage.Update.OK=Availability updated
ManageDisponibilitePage.Update.DataConstraintException=The availability already exists
ManageDisponibilitePage.Delete.OK=Availability deleted
ManageDisponibilitePage.Delete.DataConstraintException=There is data linked to this availability

ManageRemedePage=Managing a remedy
ManageRemedePage.Create.OK=Remedy created
ManageRemedePage.Create.DataConstraintException=
ManageRemedePage.Update.OK=Remedy updated
ManageRemedePage.Update.DataConstraintException=The remedy already exists
ManageRemedePage.Delete.OK=Remedy deleted
ManageRemedePage.Delete.DataConstraintException=There is data linked to this remedy

ListRemedePage.Citation=Occurrence sorting
ListRemedePage.Alphanumerique=Alphanumerical sorting

ReadCampagnePage=Viewing a campaign
ReadLotPage=Viewing a sample
ReadPersonnePage=Viewing a person
ReadSpecimenPage=Viewing a specimen
ReadUtilisateurPage=Viewing a user
ReadStationPage=Viewing a location
ReadExtractionPage=Viewing an extraction
ReadPurificationPage=Viewing a purification
ReadMoleculePage=Viewing a molecule
ReadTestBioPage=Viewing a bioassay
ReadPersonneInterrogeePage=Viewing a respondent
ReadRemedePage=Viewing a remedy
ReadDocumentPage=Viewing a document

SearchPage=Search
SearchPage.Search=Search
SearchPage.SearchOnMap=Search on the displayed zone
SearchPage.Transpose=Transpose
SearchPage.Query=Query
SearchPage.Country=Country
SearchPage.Latitude=Latitude boundaries
SearchPage.Longitude=Longitude boundaries
SearchPage.Clear=Clear
SearchPage.DocumentType=Document type
SearchPage.Campagnes=Campaigns
SearchPage.Specimens=Specimens
SearchPage.Lots=Samples
SearchPage.Extractions=Extractions
SearchPage.Purifications=Purifications
SearchPage.ResultatTestBios=Bioassay
SearchPage.Stations=Stations
SearchPage.Molecules=Molecules
SearchPage.Dosages=Dosages
SearchPage.PersonnesInterrogees=Respondents
SearchPage.Remedes=Remedies
SearchPage.QuerySyntax=Search terms (or parts of words using *). For a more advanced search, consult the ${advancedLink}.
SearchPage.QuerySyntaxAdvanced=complete syntax

#MODELS#

CaptchaModel.captcha=Captcha
CaptchaModel.captchaText=Type the number 
CaptchaModel.captchaText.KO=Type the number corresponding to the image 

RegisterModel.password=Password
RegisterModel.passwordConfirmation=Confirmation
RegisterModel.passwordConfirmation.KO=Passwords must be the same 

LoginModel.password=Password
LoginModel.rememberMe=Remember me 

ManageStationModel.latitudeDegrees=Latitude degrees
ManageStationModel.latitudeDegrees.IConverter.Integer=Latitude degrees - must be an integer
ManageStationModel.latitudeMinutes=Latitude minutes
ManageStationModel.latitudeMinutes.IConverter.Double=Latitude minutes - must a decimal number
ManageStationModel.latitudeOrientation=Latitude orientation
ManageStationModel.longitudeDegrees=Longitude degrees
ManageStationModel.longitudeDegrees.IConverter.Integer=Longitude degrees - must be an integer
ManageStationModel.longitudeMinutes=Longitude minutes
ManageStationModel.longitudeMinutes.IConverter.Double=Longitude minutes - must a decimal number
ManageStationModel.longitudeOrientation=Longitude orientation

UpdateUtilisateurModel.password=Password 2
UpdateUtilisateurModel.password.KO=Invalid password
UpdateUtilisateurModel.currentPassword=Password
UpdateUtilisateurModel.currentPassword.KO=Invalid password 
UpdateUtilisateurModel.newPassword=New password 
UpdateUtilisateurModel.newPasswordConfirmation=Confirmation
UpdateUtilisateurModel.newPasswordConfirmation.KO=New passwords must be the same 

ContactModel.mail=Your email
ContactModel.subject=Your Subject
ContactModel.message=Your message


Campagne.dates.KO=The dates are not correctly entered 
Campagne.dateDeb.IConverter.Date=The start date is incorrectly formatted (DD/MM/YY)
Campagne.dateFin.IConverter.Date=The end date is incorrectly formatted (DD/MM/YY)
Campagne.mentionLegale.info=Specify in order: the data-producing organizations - the fund providers - the period concerned by the program
Campagne.mentionLegale.info2= the data-producing organizations - the fund providers - the period concerned by the program 
Campagne.complement.info= Don’t forget to mention the policies about confidentiality and data publication
Campagne.stationsNotAccessibles= Some locations are not displayed because you don’t have the rights over the entire campaign.

Station.coordonnees=Coordinates 
Station.coordonnees.KO=To save coordinates, all fields must be completed

Specimen.dateDepot.IConverter.Date=The deposit date is incorrectly formatted (DD/MM/YY)

Lot.station.info=If the required location does not appear, contact the campaign creator so he adds it to the explored locations list 
Lot.dateRecolte.IConverter.Date=The harvest date is incorrectly formatted (DD/MM/YY)
Lot.masseFraiche.IConverter.BigDecimal=The fresh mass must be a decimal number 
Lot.masseSeche.IConverter.BigDecimal= The dry mass must be a decimal number 

Extraction.masseDepart.IConverter.BigDecimal=Mass to be extract - must be a decimal number
Extraction.date.IConverter.Date=The date is incorrectly formatted (DD/MM/YY)
Extraction.extraits.masseObtenue.IConverter.BigDecimal=Resultant mass - must be a decimal number 
Extraction.extraits.masseObtenue=Resultant mass 
Extraction.notUnique=No. extraction – already exists
Extrait.notUnique=Ref. extract - already exists
Extrait.isReferenced=There are data linked to this extract
TypeExtrait.isReferenced=The type of initials '${0}' can’t be deleted because there are data which refer it 
MethodePurification.parametres.index.IConverter.Integer=The index must be an integer

Purification.masseDepart.IConverter.BigDecimal=Mass to be purify - must be a decimal number 
Purification.fractions.masseObtenue.IConverter.BigDecimal=Resultant mass - must be a decimal number 
Purification.fractions.masseObtenue=Resultant mass 
Purification.date.IConverter.Date=The date is incorrectly formatted (DD/MM/YY)
Purification.notUnique=No. purification - already exists
Fraction.notUnique=Ref. fraction - already exists 
Fraction.isReferenced=There are data linked to this fraction
ParamMethoPuri.isReferenced=The parameter '${0}' can’t be deleted because there are data which refer it 
MethodePurification.parametres.index=Index

Molecule.provenanceNotAccessibles=Some related products are not displayed because you don't have the required rights
Molecule.masseMolaire.Required=Molar mass - can’t be null
Molecule.masseMolaire.IConverter.BigDecimal=Molar mass - must be a decimal number
Molecule.downloadMolFile=Download .mol file
Molecule.formuleBrute.info=Specify in order : Carbon, Hydrogen and then alphabetically
Molecule.nouvMolecul.info=All users can see molecules. Make sure that molecule is properly published.

TestBio.date.IConverter.Date=The date is incorrectly formatted (DD/MM/YY)
TestBio.concMasseDefaut.IConverter.BigDecimal=Default Conc./Mass - must be a decimal number
TestBio.resultats.valeur.IConverter.BigDecimal=Value - must be a decimal number
TestBio.resultats.repere.Required=Mark – can’t be null
TestBio.resultats.repere=Mark
TestBio.resultats.typeResultat.Required=Type – can’t be null
TestBio.resultats.typeResultat=Type
TestBio.resultats.produit.Required=Ref product – can’t be null 
TestBio.resultats.produit=Ref. product
TestBio.resultats.produitTemoin.Required=Control prod. – can’t be null 
TestBio.resultats.produitTemoin=Control prod.
TestBio.resultats.concMasse.IConverter.BigDecimal=Conc./Mass - must be a decimal number
TestBio.resultats.concMasse=Conc./Mass
TestBio.resultats.uniteConcMasse=Conc./Mass
TestBio.resultats.concMasse.KO=To save Conc./Mass, the two fields must be completed 
TestBio.resultats.stade.Required=Stage - can’t be null
TestBio.resultats.stade=Stage
TestBio.resultats.valeur.Required=Value - can’t be null
TestBio.resultats.valeur=Value
TestBio.notUnique=No. test – already exists
TestBio.resultats.noProduit=The bioassay must contain at least a result of type "product"
TestBio.resultsNotAccessibles=Some results are not displayed because you don't have the required rights over their products.

Dosage.resultsNotAccessibles=Some results are not displayed because you don't have the required rights over their products or lot.
Dosage.resultats.analyseRefMismatch=You can have only one sample per analysis.
Dosage.resultats.composeDose=The same compound already exist for this analysis.
Dosage.resultats.moleculeDosee=The same molecule already exist for this analysis.
Dosage.resultats.compose.Required=For this analysis, you must enter a compound
Dosage.resultats.molecule.Required=or select a molecule in the list.
Dosage.resultats.refEchantillon.Required=For this analysis, the sample ref is mandatory
Dosage.resultats.valeur.Required=For this analysis, the value is mandatory
Dosage.resultats.analyseNb.Required=For this analysis, the number is mandatory

PersonneInterrogee.station.info=If the required location does not appear, contact the campaign creator so he adds it to the explored locations list 

Remede.ingredientsNotAccessibles=Some results are not displayed because you don't have the required rights over their lot.
Remede.ingredients.empty=Remedy must have at least one ingredient
Remede.station.info=If the required location does not appear, contact the campaign creator so he adds it to the explored locations list
Remede.classification.info=Type a keyword or * to display all the values

Document.dateCreation.IConverter.Date=The date is incorrectly formatted (DD/MM/YY)
Document.link=Link
Document.type=Type
Document.file=File
Document.file.info=File size must be less than ${0}Mb
Document.apercu=Preview

#BASIC#
Actions=Actions
Submit=Submit
Cancel=Cancel 
Valid=Valid
Reject=Reject
Rebuild=Rebuild
Create=Create
Add=Add
Update=Modify
Delete=Delete
Back=Back
Read=View
ReadExtraction=View the extraction
ReadPurification=View the purification
ReadLot=View the sample
TODO=ToDo
Confirm=Are you sure?
ExportTo=Export to
List.none=The list is empty.
Boolean.true=yes
Boolean.false=no

NavigatorLabel=Lines ${from} to ${to} (${of} results)
datatable.no-records-found=No result.
