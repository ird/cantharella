var map;
var layerGroup = L.layerGroup();

function addMap(cssClass) {

    if ($('#latitude').text() && $('#latitude').text() !== "-" && $('#longitude').text() && $('#longitude').text() !== "-") {

        var latitude = parseLatitude($('#latitude').text());
        var longitude = parseLongitude($('#longitude').text());
        var nom = $('#nom').text();

        map = L.map('map').setView([latitude, longitude], 12);
        $('#map').toggleClass(cssClass);
        $("#map").removeAttr("style");

        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        }).addTo(map);

        var marker = L.marker([latitude, longitude]);
        marker.feature = {};
        marker.feature.type = 'Feature';
        marker.feature.properties = {};
        marker.feature.properties.name = nom;
        marker.bindPopup(nom);
        marker.addTo(layerGroup);
        layerGroup.addTo(map);
        map.invalidateSize();

        addDownloadLinks();
    } else {
        removeDownloadLinks();
    }
}

function addCampagneMap() {

  //get lat/lon array (one pair per station)
  var stations = [];
  $('tr.station').each(function (index, element) {
      if (element.children[3].innerText && element.children[3].innerText !== "-" && element.children[4].innerText && element.children[4].innerText !== "-") {
          var latitude = parseLatitude(element.children[3].innerText.trim());
          var longitude = parseLongitude(element.children[4].innerText.trim());
          var nom = element.children[0].innerText;
          stations.push({latitude, longitude, nom});
      }
  });

  if (stations.length) {
    map = L.map('map').setView([stations[0].latitude, stations[0].longitude], 12);
    map.setMaxBounds([
          [-90, -360],
          [90, 360]
      ]);
    $('#map').toggleClass("bigMap");
    $("#map").removeAttr("style");

    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community',
      maxZoom:17,
      minZoom:1
    }).addTo(map);

    var bounds = L.latLngBounds();

    //add marker per station
    var markers = [];
    stations.forEach(element => {
        let lat_lng = [element.latitude, element.longitude];
        var markerOptions = {
           title: element.nom
        }
        var marker = L.marker(lat_lng, markerOptions);
        marker.feature = {};
        marker.feature.type = 'Feature';
        marker.feature.properties = {};
        marker.feature.properties.name = element.nom;
        marker.bindPopup(element.nom);
        marker.addTo(layerGroup);
        markers.push(marker);
        bounds.extend(lat_lng);
    });

    layerGroup.addTo(map);

    map.invalidateSize();

    //map set bounding box
    map.fitBounds(bounds);

    addDownloadLinks();
  }
}

function addSearchMap() {

    //get lat/lon array (one pair per station)
    var stations = [];
    $('.mapValues span').each(function (index, element) {
        stations = JSON.parse(element.innerText);
    });

    if (stations[0] !== undefined) {
        map = L.map('map').setView([parseLatitude(stations[0].latitude), parseLongitude(stations[0].longitude)], 12);
        $('#map').toggleClass("bigMap");
        $("#map").removeAttr("style");

	    map.setMaxBounds([
            [-90, -360],
            [90, 360]
        ]);

        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community',
            maxZoom:13,
            minZoom:1
        }).addTo(map);

        var bounds = L.latLngBounds();

        //add marker per station
        var markers = [];
        stations.forEach(element => {
            let lat_lng = [parseLatitude(element.latitude), parseLongitude(element.longitude)];
            var markerOptions = {
               title: element.nom
            }
            var marker = L.marker(lat_lng, markerOptions);
            marker.feature = {};
            marker.feature.type = 'Feature';
            marker.feature.properties = {};
            marker.feature.properties.name = element.nom;
            marker.bindPopup(element.nom);
            marker.addTo(layerGroup);
            markers.push(marker);
            bounds.extend(lat_lng);
        });

        layerGroup.addTo(map);

        map.invalidateSize();

        //map set bounding box
        map.fitBounds(bounds);

        if (map.getZoom() > 13) {
          map.setZoom(13);
        }

        addDownloadLinks();

        document.getElementById('searchLink').onclick = function(e) {
            e.preventDefault();
            var bounds = map.getBounds();
            $('#SearchPageNElatitude')[0].value = bounds.getNorthEast().lat;
            $('#SearchPageNElongitude')[0].value = bounds.getNorthEast().lng;
            $('#SearchPageSOlatitude')[0].value = bounds.getSouthWest().lat;
            $('#SearchPageSOlongitude')[0].value = bounds.getSouthWest().lng;
            $('#searchForm')[0].submit();
        }
    } else {
        $('#map').toggleClass("bigMap");
        $("#map").removeAttr("style");
        map = L.map('map').setView([0, 0], 1);

        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        }).addTo(map);
    }
}

function addDownloadLinks () {

    document.getElementById('downloadMapImg').onclick = function(e) {
        e.preventDefault();
        downloadImage(map);
    };
    document.getElementById('downloadMapKml').onclick = function(e) {
        e.preventDefault();
        downloadKML();
    };
    document.getElementById('downloadMapGeoJson').onclick = function(e) {
        e.preventDefault();
        downloadGeojson();
    };
}

function removeDownloadLinks () {
    $('.map_links').hide();
}

function downloadImage(map) {
    leafletImage(map, function(err, canvas) {
        var a = document.createElement('a');
        a.href = canvas.toDataURL();
        a.setAttribute('download', 'export.png');
        a.style.display = 'none';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    });
}

function downloadGeojson() {
    var geojson = layerGroup.toGeoJSON();
    var a = document.createElement('a');
    a.setAttribute('href', 'data:json/geojson;charset=utf-8,' + encodeURIComponent(JSON.stringify(geojson)));
    a.setAttribute('download', 'export.json');
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function downloadKML() {
    var geojson = layerGroup.toGeoJSON();
    var a = document.createElement('a');
    a.setAttribute('href', 'data:json/geojson;charset=utf-8,' + encodeURIComponent(tokml(geojson)));
    a.setAttribute('download', 'export.kml');
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

/**
* DMS to DD conversion
**/

const SPACE_EXPRESSION = /\s+/;
const LATITUDE_EXPRESSION = /^(\d{1,2})°(?:\s*(\d{1,2}).(\d{1,3})[′'])?\s*(N|S|N\/S)$/;  // 0- 90° 0-59′ N/S
const LONGITUDE_EXPRESSION = /^(\d{1,3})°(?:\s*(\d{1,2}).(\d{1,3})[′'])?\s*(E|W|E\/W)$/; // 0-180° 0-59′ E/W

const parseCoordinate = (expression, limit, surfaces, text) => {
  	expression.lastIndex = 0;
	const match = expression.exec(text.trim());
  	if (match) {
    	const degrees = parseInt(match[1]); // 0-90° or 0-180°
      	if (degrees > limit) {
        	throw new Error('Incorrect degrees value (should be in range from 0 to ' + limit + ').');
        }
        const minutes = parseInt(match[2] || '0'); // 0-59′
        if (minutes > 59) {
          	throw new Error('Incorrect minutes value (should be in range from 0 to 59).');
        }

        const seconds = parseInt(match[3] || '0' )

    	if (degrees === 0 && minutes === 0) {
        	return 0;
        }
        const surface = match[4] // N/S or E/W
        if (surface === surfaces[0]) {
        	return +(degrees + minutes / 60 + (seconds / 60000));
        }
      	if (surface === surfaces[1]) {
        	return -(degrees + minutes / 60 + (seconds / 60000));
        }
      	throw new Error('Incorrect surface value (should be ' + surfaces[0] + ' or ' + surfaces[1] + ').');
    }
  	throw new Error('Incorrect coordinate format.');
};

const parseLatitude = (latitude) => {
    return parseCoordinate(LATITUDE_EXPRESSION, 90, 'NS', latitude);   // N/S
};

const parseLongitude = (longitude) => {
    return parseCoordinate(LONGITUDE_EXPRESSION, 180, 'EW', longitude); // E/W
};

const parsePosition = (position) => {
  	if (position) {
        const parts = position.split(SPACE_EXPRESSION);
      	if (parts.length === 2) {
        	const latitude = parseLatitude(parts[0]);
          	const longitude = parseLongitude(parts[1]);
          	return {latitude, longitude};
        }
    }
  	return new Error('Incorrect position format.');
};