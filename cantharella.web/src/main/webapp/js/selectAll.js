function selectAll(element) {
    $(element).parents("table").children("tbody").find("input:checkbox").toArray().forEach(checkbox => {
        if (checkbox.checked !== $(element).find("input:checkbox")[0].checked) {
            checkbox.click();
        }
    })
}