function addCitationButton() {
    document.getElementById('citationButton').onclick = function(e) {
      e.preventDefault();
      document.getElementById("ingredientsRemede").style.visibility = "collapse";
      document.getElementById("citationButton").children[0].disabled = true;
      document.getElementById("citationButton").disabled = true;
      document.getElementById("citationIngredientsRemede").style.visibility = "visible";
      document.getElementById("alphanumeriqueButton").children[0].disabled = false;
      document.getElementById("alphanumeriqueButton").disabled = false;
    }

    document.getElementById('alphanumeriqueButton').onclick = function(e) {
      e.preventDefault();
      document.getElementById("ingredientsRemede").style.visibility = "visible";
      document.getElementById("citationButton").children[0].disabled = false;
      document.getElementById("citationButton").disabled = false;
      document.getElementById("citationIngredientsRemede").style.visibility = "collapse";
      document.getElementById("alphanumeriqueButton").disabled = true;
      document.getElementById("alphanumeriqueButton").children[0].disabled = true;
    }

  document.getElementById("ingredientsRemede").style.visibility = "visible";
  document.getElementById("citationButton").children[0].disabled = false;
  document.getElementById("citationButton").disabled = false;
  document.getElementById("citationIngredientsRemede").style.visibility = "collapse";
  document.getElementById("alphanumeriqueButton").disabled = true;
  document.getElementById("alphanumeriqueButton").children[0].disabled = true;
}