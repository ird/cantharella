function addTransposeButton() {
    document.getElementById("resultatsDosage").style.visibility = "visible";
    document.getElementById("transposeDosage").style.visibility = "collapse";

    document.getElementById('transposeButton').onclick = function(e) {
      e.preventDefault();
      if (document.getElementById("resultatsDosage").style.visibility === "visible") {
        document.getElementById("resultatsDosage").style.visibility = "collapse";
        document.getElementById("transposeDosage").style.visibility = "visible";
      } else {
        document.getElementById("resultatsDosage").style.visibility = "visible";
        document.getElementById("transposeDosage").style.visibility = "collapse";
      }
    }
}