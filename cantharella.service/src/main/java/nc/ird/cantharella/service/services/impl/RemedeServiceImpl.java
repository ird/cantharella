package nc.ird.cantharella.service.services.impl;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.RemedeDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.*;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.utils.AssertTools;
import nc.ird.cantharella.utils.BeanTools;
import nc.ird.cantharella.utils.CollectionTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Service
public class RemedeServiceImpl implements RemedeService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(RemedeService.class);

    /** DAO */
    @Autowired
    private GenericDao dao;

    @Autowired
    StationService stationService;

    @Override
    public void createRemede(Remede remede) throws DataConstraintException {
        LOG.info("createRemede: " + remede.getRef());
        dao.create(remede);
    }

    @Override
    public void deleteRemede(Remede remede) throws DataConstraintException {
        AssertTools.assertNotNull(remede);
        LOG.info("deleteRemede: " + remede.getRef());
        try {
            dao.delete(remede);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }

    }

    @Override
    public Remede loadRemede(Integer idRemede) throws DataNotFoundException {
        return dao.read(Remede.class, idRemede);
    }

    @Override
    public Remede loadRemede(String idRemede) throws DataNotFoundException {
        return dao.read(Remede.class, "ref", idRemede);
    }

    @Override
    public void updateRemede(Remede remede) throws DataConstraintException {
        LOG.info("updateRemede: " + remede.getRef());
        try {
            dao.update(remede);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    @Override
    public boolean updateOrdeleteRemedeEnabled(Remede remede, Utilisateur utilisateur) {
        return utilisateur.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR
                || Objects.equals(utilisateur.getIdPersonne(), remede.getCreateur().getIdPersonne())
                || Objects.equals(remede.getCampagne().getAdministrateur().getIdPersonne(), utilisateur.getIdPersonne())
                || Objects.equals(remede.getCampagne().getCreateur().getIdPersonne(), utilisateur.getIdPersonne());
    }

    @Override
    public void refreshRemede(Remede remede) {
        AssertTools.assertNotNull(remede);
        dao.refresh(remede);
    }

    @Override
    public List<Remede> listRemede(Utilisateur utilisateur) {
        AssertTools.assertNotNull(utilisateur);
        if (utilisateur.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR) {
            // si admin, on ajoute toutes les testBios de la base
            return (List<Remede>) dao.list(RemedeDao.CRITERIA_LIST);
        }
        // gestion des droits en plus pour les utilisateurs
        return new ArrayList<>(listRemedeForUser(utilisateur));
    }

    @Override
    public SortedSet<Remede> listRemedeForUser(Utilisateur utilisateur) {

        AssertTools.assertNotNull(utilisateur);
        SortedSet<Remede> remedes = new TreeSet<>();
        for (Campagne c : utilisateur.getCampagnesCreees()) {
            remedes.addAll(c.getRemedes());
        }
        for (Campagne c : utilisateur.getCampagnesDroits().keySet()) {
            remedes.addAll(c.getRemedes());
        }
        remedes.addAll(utilisateur.getRemedesCrees());
        remedes.addAll(utilisateur.getRemedesDroits().keySet());

        return remedes;
    }

    @Override
    public List<IngredientRemede> listIngredients(Utilisateur utilisateur) {
        AssertTools.assertNotNull(utilisateur);
        if (utilisateur.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR) {
            // si admin, on ajoute toutes les testBios de la base
            return (List<IngredientRemede>) dao.list(RemedeDao.CRITERIA_LIST_INGREDIENTS);
        }
        // gestion des droits en plus pour les utilisateurs
        return listIngredientsForUser(utilisateur);
    }

    @Override
    public List<IngredientRemede> listIngredientsForUser(Utilisateur utilisateur) {

        AssertTools.assertNotNull(utilisateur);
        SortedSet<Remede> remedes = listRemedeForUser(utilisateur);

        List<IngredientRemede> ingredients = remedes.stream().map(Remede::getIngredients).flatMap(List::stream).collect(Collectors.toList());

        return ingredients;
    }

    @Override
    public List<String> listNomRemede() {
        return (List<String>) dao.list(RemedeDao.CRITERIA_DISTINCT_NOM_REMEDE);
    }

    @Override
    public List<String> listNomVernaculaireIngredients() {
        return (List<String>) dao.list(RemedeDao.CRITERIA_DISTINCT_NOM_VERNACULAIRE_INGREDIENT);
    }

    @Override
    public void createDisponibilite(Disponibilite disponibilite) throws DataConstraintException {
        LOG.info("createDisponibilite: " + disponibilite.getNom());
        dao.create(disponibilite);

    }

    @Override
    public void deleteDisponibilite(Disponibilite disponibilite) throws DataConstraintException {
        AssertTools.assertNotNull(disponibilite);
        LOG.info("deleteDisponibilite: " + disponibilite.getNom());
        try {
            dao.delete(disponibilite);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    @Override
    public List<Disponibilite> listDisponibilites() {
        return dao.readList(Disponibilite.class, "nom");
    }

    @Override
    public Disponibilite loadDisponibilite(Integer idDisponibilite) throws DataNotFoundException {
        return dao.read(Disponibilite.class, idDisponibilite);
    }

    @Override
    public void updateDisponibilite(Disponibilite disponibilite) throws DataConstraintException {
        LOG.info("updateDisponibilite: " + disponibilite.getNom());
        try {
            dao.update(disponibilite);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    @Override
    public List<EntreeReferentiel> listEntreeReferentiels(EntreeReferentiel.Referentiel referentiel) {
        return (List<EntreeReferentiel>) dao.list(RemedeDao.getCriteriaListEntreeReferentiel(referentiel));
    }

    @Override
    public boolean canDeleteEntreeReferentiel(EntreeReferentiel entree) {
        return dao.count(RemedeDao.getCriteriaEntreeReferentielUsed(entree)) == 0;
    }

    @Override
    public void deleteEntreeReferentiel(EntreeReferentiel entree) throws DataConstraintException {
        AssertTools.assertNotNull(entree);
        LOG.info("deleteEntreeReferentiel " + entree);
        try {
            dao.delete(entree);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    @Override
    public long countRemedes() {
        return dao.count(Remede.class);
    }

    @Override
    public long countPersonnesInterrogees() {
        return dao.count(PersonneInterrogee.class);
    }

    @Override
    public long countPrescripteurOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountPrescripteur(ingredient.getRemede().getPrescripteur()));
    }

    @Override
    public long countCodeLangueOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountCodeLangue(ingredient.getRemede().getCodeLangue()));
    }

    @Override
    public long countDateOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountDate(ingredient.getRemede().getDate()));
    }

    @Override
    public long countNomRemedeOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountNomRemede(ingredient.getRemede().getNom()));
    }

    @Override
    public long countIndicationsVernaculairesOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountIndicationsVernaculaires(ingredient.getRemede().getIndicationVernaculaire()));
    }

    @Override
    public long countSpecimenRefOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountSpecimenRef(ingredient.getSpecimen().getRef()));
    }

    @Override
    public long countSpecimenFamilleOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountSpecimenFamille(ingredient.getSpecimen().getFamille()));
    }

    @Override
    public long countSpecimenGenreOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountSpecimenGenre(ingredient.getSpecimen().getGenre()));
    }

    @Override
    public long countSpecimenEspeceOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountSpecimenEspece(ingredient.getSpecimen().getEspece()));
    }

    @Override
    public long countNomVernaculaireOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountNomVernaculaire(ingredient.getNomVernaculaire()));
    }

    @Override
    public long countCodePaysOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountCodePays(ingredient.getRemede().getCampagne().getCodePays()));
    }

    @Override
    public long countCampagneOccurence(IngredientRemede ingredient) {
        return dao.count(RemedeDao.getCriteriaCountCampagne(ingredient.getRemede().getCampagne()));
    }

    @Override
    public boolean isRemedeAccessibleByUser(Remede remede, Utilisateur utilisateur) {

        Utilisateur loadedFromDbUser;
        try {
            loadedFromDbUser = dao.read(Utilisateur.class, utilisateur.getIdValue());
        } catch (DataNotFoundException dnfe) {
            LOG.error("User not present in db, should never happen");
            return false;
        }

        // si administrateur ou créateur, accès ok
        if (loadedFromDbUser.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR || loadedFromDbUser.equals(remede.getCreateur())) {
            return true;
        }
        // accessible si l'utilisateur a créé la campagne
        if (loadedFromDbUser.getCampagnesCreees().contains(remede.getCampagne())) {
            return true;
        }

        // accessible si l'utilisateur est administrateur de la campagne
        if (loadedFromDbUser.getCampagnesAdministrees().contains(remede.getCampagne())) {
            return true;
        }

        // accessible si l'utilisateur a le droit à la campagne
        // FIXME echatellier 20130502 ne fonctionne pas car les clés composites
        // ne sont pas les mêmes instances que les clés
        /*if (utilisateur.getCampagnesDroits().containsKey(lot.getCampagne())) {
            return true;
        }
        // accessible si l'utilisateur a le droit au lot
        if (utilisateur.getLotsDroits().containsKey(lot)) {
            return true;
        }*/

        // FIXME echatellier 20130502 code temporaire pour pallier au problème
        // des clés composites
        if (CollectionTools.containsWithValue(loadedFromDbUser.getCampagnesDroits().keySet(), "idCampagne",
                BeanTools.AccessType.GETTER, remede.getCampagne().getIdCampagne())) {
            return true;
        }
        if (CollectionTools.containsWithValue(loadedFromDbUser.getRemedesDroits().keySet(), "idRemede",
                BeanTools.AccessType.GETTER, remede.getIdRemede())) {
            return true;
        }

        // pas d'accès sinon
        return false;
    }

    @Override
    public boolean isRemedePersonneInterrogeeAccessibleByUser(Remede remede, PersonneInterrogee personneInterrogee, Utilisateur utilisateur) {

        if (isRemedeAccessibleByUser(remede, utilisateur) && remede.getPrescripteur() != null && remede.getPrescripteur().equals(personneInterrogee)) {
            return true;
        }

        // pas d'accès sinon
        return false;
    }
}
