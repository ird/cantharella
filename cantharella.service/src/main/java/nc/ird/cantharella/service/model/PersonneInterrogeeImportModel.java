package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class PersonneInterrogeeImportModel extends AbstractImportExportModel<PersonneInterrogeeImport> {

    public static final char CSV_SEPARATOR = ';';

    public PersonneInterrogeeImportModel() {

        super(CSV_SEPARATOR);

        // "Cible";
        newMandatoryColumn("Campagne", "campagne");
        // "Conc";
        newMandatoryColumn("Station", "station");
        // "Unité";
        newMandatoryColumn("Nom", "nom");
        // "Réf. produit";
        newMandatoryColumn("Prénom", "prenom");
        // "Prod. témoin";
        newMandatoryColumn("Genre", "genre");
        // "Prod. témoin";
        newMandatoryColumn("Année", "annee");
        // "Valeur";
        newMandatoryColumn("Expérience", "experience");
        // "SD";
        newMandatoryColumn("Profession", "profession");
        // "Actif";
        newMandatoryColumn("Labellisation", "labellisation");
        // "Erreur"
        newMandatoryColumn("Téléphone", "telephone");
        // "Repère";
        newMandatoryColumn("Courriel", "courriel");
        // "Réf. test"
        newMandatoryColumn("Adresse", "adresse");
        // "Organisme testeur
        newMandatoryColumn("Code postal", "codePostal");
        // "Nom manipulateur"
        newMandatoryColumn("Ville", "ville");
        // "Prénom manipulateur"
        newMandatoryColumn("Pays", "codePays");
        // "Date"
        newMandatoryColumn("Complément", "complement");

        // Column for export

        // "Cible";
        newColumnForExport("Campagne", "campagne");
        // "Conc";
        newColumnForExport("Station", "station");
        // "Unité";
        newColumnForExport("Nom", "nom");
        // "Réf. produit";
        newColumnForExport("Prénom", "prenom");
        // "Réf. produit";
        newColumnForExport("Genre", "genre");
        // "Réf. produit";
        newColumnForExport("Année", "annee");
        // "Valeur";
        newColumnForExport("Expérience", "experience");
        // "SD";
        newColumnForExport("Profession", "profession");
        // "Actif";
        newColumnForExport("Labellisation", "labellisation");
        // "Erreur"
        newColumnForExport("Téléphone", "telephone");
        // "Repère";
        newColumnForExport("Courriel", "courriel");
        // "Réf. test"
        newColumnForExport("Adresse", "adresse");
        // "Organisme testeur
        newColumnForExport("Code postal", "codePostal");
        // "Nom manipulateur"
        newColumnForExport("Ville", "ville");
        // "Prénom manipulateur"
        newColumnForExport("Pays", "codePays");
        // "Date"
        newColumnForExport("Complément", "complement");
    }

    @Override
    public PersonneInterrogeeImport newEmptyInstance() {
        return new PersonneInterrogeeImport();
    }

}
