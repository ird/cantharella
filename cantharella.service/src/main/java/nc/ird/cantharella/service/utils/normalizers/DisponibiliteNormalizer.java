package nc.ird.cantharella.service.utils.normalizers;

import nc.ird.cantharella.data.model.Disponibilite;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.AssertTools;

public class DisponibiliteNormalizer  extends Normalizer<Disponibilite> {

    /** {@inheritDoc} */
    @Override
    protected Disponibilite normalize(Disponibilite disponibilite) {
        AssertTools.assertNotNull(disponibilite);
        // Unique fields
        disponibilite.setNom(Normalizer.normalize(ConfigNameNormalizer.class, disponibilite.getNom()));
        return disponibilite;
    }
}
