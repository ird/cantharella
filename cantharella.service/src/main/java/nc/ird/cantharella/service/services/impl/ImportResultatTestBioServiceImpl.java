package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.PersonneDao;
import nc.ird.cantharella.data.dao.impl.UniteConcMasseDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.ErreurTestBio;
import nc.ird.cantharella.data.model.MethodeTestBio;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.TestBio;
import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.ResultatTestBioImport;
import nc.ird.cantharella.service.model.ResultatTestBioImportModel;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.ProduitService;
import nc.ird.cantharella.service.services.TestBioService;
import nc.ird.cantharella.service.utils.normalizers.ErreurTestNormalizer;
import nc.ird.cantharella.service.utils.normalizers.TestBioNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Service
@Qualifier("importResultatTestBioService")
public class ImportResultatTestBioServiceImpl extends AbstractImportService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ImportResultatTestBioServiceImpl.class);
    protected final String DATE_FORMAT = "d/MM/uuuu";

    /** Messages d'internationalisation */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /** DAO */
    @Autowired
    private GenericDao dao;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private ProduitService produitService;

    @Autowired
    private TestBioService testBioService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

        // Chargement de certaines données en amont
        List<Produit> produits = produitService.listProduits(loadedUtilisateur);
        Map<String, Produit> produitByRef = Maps.uniqueIndex(produits, Produit::getRef);

        List<MethodeTestBio> methodeTestBios = testBioService.listMethodesTestBio();
        Map<String, MethodeTestBio> methodeTestBioByCible = Maps.uniqueIndex(methodeTestBios, MethodeTestBio::getCible);

        List<ErreurTestBio> erreurTestBios = testBioService.listErreursTestBio();
        Map<String, ErreurTestBio> erreurTestBioByName = Maps.uniqueIndex(erreurTestBios, ErreurTestBio::getNom);


        Map<String, TestBio> testBiosParRef = new HashMap<>();
        List<String> existingTestBiosRefs = new ArrayList<>();

        int currentLine = 0;
        for (ImportRow<T> resultatTestBioImportRow : importData) {
            currentLine++;

            boolean valid = resultatTestBioImportRow.isValid();
            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = resultatTestBioImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("type")) {
                        String message = messages.getMessage("csv.import.type.unknown", new Object[] { currentLine, Stream.of(ResultatTestBio.TypeResultat.values()).map(Enum::name).collect(Collectors.toList())}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("stade")) {
                        String message = messages.getMessage("csv.import.stade.unknown", new Object[] { currentLine, Stream.of(ResultatTestBio.Stade.values()).map(Enum::name).collect(Collectors.toList())}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("actif")) {
                        String message = messages.getMessage("csv.import.boolean.invalid", new Object[] {currentLine, Lists.newArrayList("oui", "non", "o", "n")}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("conc") || headerName.equalsIgnoreCase("valeur")) {
                        String message = messages.getMessage("csv.import.numeric.invalid", new Object[] {currentLine, headerName}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("unité")) {
                        String message = messages.getMessage("csv.import.conc.unity.invalid", new Object[] {currentLine, Lists.newArrayList("μg", "ng", "mg/ml", "μg/ml", "ng/ml")}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }
            ResultatTestBioImport resultatTestBioImport = (ResultatTestBioImport) resultatTestBioImportRow.getBean();
            String refTestBio = resultatTestBioImport.getRefTestBio();

            // Mandatory fields/relation
            if (refTestBio == null || refTestBio.isBlank()) {
                String message = messages.getMessage("csv.import.reftest.empty", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);

            } else if (!testBiosParRef.containsKey(refTestBio) && existingTestBiosRefs.contains(refTestBio)) {
                // Already loaded
                String message = messages.getMessage("csv.import.reftest.existing", new Object[] {currentLine, refTestBio}, locale);
                importResult.addErrorLine(message);

            } else {
                // Load
                boolean exists = false;
                try {
                    exists = testBioService.loadTestBio(refTestBio) != null;
                } catch (DataNotFoundException dce) {
                    //do nothing exists already false
                }
                if (exists) {
                    String message = messages.getMessage("csv.import.reftest.existing", new Object[] {currentLine, refTestBio}, locale);
                    importResult.addErrorLine(message);
                    existingTestBiosRefs.add(refTestBio);
                }
            }

            // Check date of each line
            Date parsedDate = null;
            try {
                LocalDate date = LocalDate.parse(resultatTestBioImport.getDate(), DateTimeFormatter.ofPattern(DATE_FORMAT).withResolverStyle(ResolverStyle.STRICT));
                parsedDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                if (date.isAfter(LocalDate.now())) {
                    String message = messages.getMessage("csv.import.date.future", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } catch (DateTimeParseException e) {
                String message = messages.getMessage("csv.import.date.invalid", new Object[] {currentLine, resultatTestBioImport.getDate()}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve methode
            String methodeCible = resultatTestBioImport.getMethodeCible();
            MethodeTestBio methode = null;
            if (StringUtils.isBlank(methodeCible)) {
                String message = messages.getMessage("csv.import.method.empty", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                methode = methodeTestBioByCible.get(methodeCible);
                if (methode == null) {
                    String message = messages.getMessage("csv.import.method.unknown", new Object[] {currentLine, methodeCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Need to find the personne
            String nomManipulateur = resultatTestBioImport.getNomManipulateur();
            String prenomManipulateur = resultatTestBioImport.getPrenomManipulateur();
            Map<String, Object> manipulateurParameters = ImmutableMap.of("nom", nomManipulateur, "prenom", prenomManipulateur);
            List<Personne> personnes = (List<Personne>) dao.list(PersonneDao.HQL_FIND_PERSONNE_PAR_NOM_PRENOM, manipulateurParameters);
            Personne technician = null;
            if (personnes.isEmpty()) {
                String message = messages.getMessage("csv.import.technician.unknown", new Object[] {currentLine, nomManipulateur, prenomManipulateur}, locale);
                importResult.addErrorLine(message);
            } else {
                technician = personnes.get(0);
            }

            TestBio testBio = testBiosParRef.get(refTestBio);
            if (testBio == null) {
                testBio = new TestBio();
                testBio.setRef(refTestBio);
                testBio.setCreateur(loadedUtilisateur);
                testBio.setOrganismeTesteur(resultatTestBioImport.getOrganismeTesteur());

                testBio.setMethode(methode);

                testBio.setManipulateur(technician);
                testBio.setDate(parsedDate);

            } else {
                // Be sure test bio data are same
                if (testBio.getDate() == null) {
                    testBio.setDate(parsedDate);
                } else if (!testBio.getDate().equals(parsedDate)) {
                        String message = messages.getMessage("csv.import.date.different", new Object[] {currentLine, refTestBio}, locale);
                        importResult.addErrorLine(message);
                }

                if (testBio.getManipulateur() == null) {
                    testBio.setManipulateur(technician);
                } else if (!testBio.getManipulateur().equals(technician)) {
                    String message = messages.getMessage("csv.import.technician.different", new Object[] {currentLine, refTestBio}, locale);
                    importResult.addErrorLine(message);
                }

                if (testBio.getMethode() == null) {
                    testBio.setMethode(methode);
                } else if (!testBio.getMethode().equals(methode)) {
                    String message = messages.getMessage("csv.import.method.different", new Object[] {currentLine, refTestBio, testBio.getMethode().getCible()}, locale);
                    importResult.addErrorLine(message);
                }

            }

            ResultatTestBio resultatTestBio = new ResultatTestBio();
            String repere = resultatTestBioImport.getRepere();
            if (StringUtils.isBlank(repere)) {
                String message = messages.getMessage("csv.import.mark.empty", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                // Refs #11002 "pour le repère, on ne peut pas avoir 2 fois le même repère pour une manip de test donnée."
                boolean hasRepere = testBio.getResultats().stream().anyMatch(resultat -> resultat.getRepere().equalsIgnoreCase(repere));
                if (hasRepere) {
                    String message = messages.getMessage("csv.import.mark.existing", new Object[] {currentLine, repere}, locale);
                    importResult.addErrorLine(message);
                }
            }
            resultatTestBio.setRepere(repere);


            ResultatTestBio.TypeResultat typeResultat = resultatTestBioImport.getType();
            resultatTestBio.setTypeResultat(typeResultat);

            if (typeResultat == ResultatTestBio.TypeResultat.PRODUIT) {
                String nomProduit = resultatTestBioImport.getNomProduit();
                Produit produit = produitByRef.get(nomProduit.toUpperCase());
                if (produit == null) {
                    String message = messages.getMessage("csv.import.product.unknown", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                } else {
                    resultatTestBio.setProduit(produit);
                }
                resultatTestBio.setStade(resultatTestBioImport.getStade());
                resultatTestBio.setConcMasse(resultatTestBioImport.getConcMasse());

                // Need to find the unit
                String unite = resultatTestBioImport.getUniteConcMasse();
                Map<String, Object> uniteParameters = ImmutableMap.of("valeur", unite );
                List<UniteConcMasse> unites = (List<UniteConcMasse>) dao.list(UniteConcMasseDao.HQL_FIND_UNITE_PAR_VALEUR, uniteParameters);
                UniteConcMasse uniteConcMasse = null;
                if (unites.isEmpty()) {
                    String message = messages.getMessage("csv.import.technician.unknown", new Object[] {currentLine, nomManipulateur, prenomManipulateur}, locale);
                    importResult.addErrorLine(message);
                } else {
                    uniteConcMasse = unites.get(0);
                }
                resultatTestBio.setUniteConcMasse(uniteConcMasse);

                if (StringUtils.isNotBlank(resultatTestBioImport.getProduitTemoin())) {
                    String message = messages.getMessage("csv.import.product.control.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } else if (typeResultat == ResultatTestBio.TypeResultat.TEMOIN) {
                String produitTemoin = resultatTestBioImport.getProduitTemoin();
                if (StringUtils.isBlank(produitTemoin)) {
                    String message = messages.getMessage("csv.import.control.empty", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                } else {
                    resultatTestBio.setProduitTemoin(produitTemoin);
                    resultatTestBio.setConcMasse(resultatTestBioImport.getConcMasse());

                    // Need to find the unit
                    String unite = resultatTestBioImport.getUniteConcMasse();
                    Map<String, Object> uniteParameters = ImmutableMap.of("valeur", unite );
                    List<UniteConcMasse> unites = (List<UniteConcMasse>) dao.list(UniteConcMasseDao.HQL_FIND_UNITE_PAR_VALEUR, uniteParameters);
                    UniteConcMasse uniteConcMasse = null;
                    if (unites.isEmpty()) {
                        String message = messages.getMessage("csv.import.technician.unknown", new Object[] {currentLine, nomManipulateur, prenomManipulateur}, locale);
                        importResult.addErrorLine(message);
                    } else {
                        uniteConcMasse = unites.get(0);
                    }
                    resultatTestBio.setUniteConcMasse(uniteConcMasse);
                }
                if (StringUtils.isNotBlank(resultatTestBioImport.getNomProduit())) {
                    String message = messages.getMessage("csv.import.control.refproduct.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
                if (resultatTestBioImport.getStade() != null) {
                    String message = messages.getMessage("csv.import.control.stade.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } else if (typeResultat == ResultatTestBio.TypeResultat.BLANC) {
                if (StringUtils.isNotBlank(resultatTestBioImport.getNomProduit())) {
                    String message = messages.getMessage("csv.import.blank.refproduct.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
                if (StringUtils.isNotBlank(resultatTestBioImport.getProduitTemoin())) {
                    String message = messages.getMessage("csv.import.blank.control.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
                if (resultatTestBioImport.getConcMasse() != null) {
                    String message = messages.getMessage("csv.import.blank.conc.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
                if (resultatTestBioImport.getStade() != null) {
                    String message = messages.getMessage("csv.import.blank.stade.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            }

            String nomErreur = resultatTestBioImport.getNomErreur();
            if (StringUtils.isNotBlank(nomErreur)) {
                ErreurTestBio erreur = erreurTestBioByName.get(nomErreur);
                if (erreur == null) {
                    String message = messages.getMessage("csv.import.error.unknown", new Object[] {currentLine, nomErreur}, locale);
                    importResult.addErrorLine(message);
                } else {
                    Normalizer.normalize(ErreurTestNormalizer.class, erreur);
                    resultatTestBio.setErreur(erreur);
                }
                if (resultatTestBioImport.getValeur() != null) {
                    String message = messages.getMessage("csv.import.error.value.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } else {
                resultatTestBio.setValeur(resultatTestBioImport.getValeur());
            }
            resultatTestBio.setActif(resultatTestBioImport.getEstActif());

            if (testBio.getComplement() != null && !testBio.getComplement().isEmpty() && !testBio.getComplement().equals(resultatTestBioImport.getComplement())) {
                String message = messages.getMessage("csv.import.error.complement.conflict", new Object[] {currentLine, refTestBio}, locale);
                importResult.addErrorLine(message);
            }
            testBio.setComplement(resultatTestBioImport.getComplement());

            testBio.getResultats().add(resultatTestBio);
            resultatTestBio.setTestBio(testBio);

            Normalizer.normalize(TestBioNormalizer.class, testBio);
            testBiosParRef.put(refTestBio, testBio);
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (TestBio testBio : testBiosParRef.values()) {
                try {
                    dao.create(testBio);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        ResultatTestBioImportModel model = new ResultatTestBioImportModel();

        File headers = File.createTempFile("resultatTestBios", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
