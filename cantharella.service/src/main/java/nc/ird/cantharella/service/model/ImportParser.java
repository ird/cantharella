package nc.ird.cantharella.service.model;

import nc.ird.cantharella.data.config.DataContext;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.UniteConcMasse;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParser;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImportParser {

    protected static final ValueParser<BigDecimal> BIG_DECIMAL_PARSER = value -> {
        BigDecimal result = null;
        if (StringUtils.isNotBlank(value)) {
            value = value.replaceAll(",", ".");
            value = value.replaceAll(" ", "");
            value = value.replaceAll(" ", ""); // espace insécable
            try {
                result = new BigDecimal(value);
            } catch (NumberFormatException e) {
                throw new ParseException(value + " is not a valid numeric value.", 0);
            }
        }
        return result;
    };

    protected static final ValueParser<Integer> INTEGER_PARSER = value -> {
        Integer result = null;
        if (StringUtils.isNotBlank(value)) {
            try {
                result = Integer.valueOf(value);
            } catch (NumberFormatException e) {
                throw new ParseException(value + " is not a valid integer value.", 0);
            }
        }
        return result;
    };

    protected static final ValueParser<ResultatTestBio.TypeResultat> TYPE_RESULTAT_PARSER = value -> {
        try {
            return ResultatTestBio.TypeResultat.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            String message = String.format("'%s'  is not a valid Type value. Expected one of those: %s.", value, StringUtils.joinWith(", ", Stream.of(ResultatTestBio.TypeResultat.values()).map(Enum::name).collect(Collectors.toList())));
            throw new ParseException(message, 0);
        }
    };
    protected static final ValueParser<ResultatTestBio.Stade> STADE_PARSER = value -> {
        try {
            return value.isEmpty() ? null : ResultatTestBio.Stade.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            String message = String.format("'%s'  is not a valid stade value. Expected one of those: %s.", value, StringUtils.joinWith(", ", Stream.of(ResultatTestBio.Stade.values()).map(Enum::name).collect(Collectors.toList())));
            throw new ParseException(message, 0);
        }
    };

    protected static final ValueParser<Boolean> BOOLEAN_PARSER = value -> {
        switch (value.toLowerCase()) {
            case "t" :
            case "true" :
            case "yes" :
            case "oui" :
            case "y" :
            case "o" :
            case "1" :
                return true;
            case "f" :
            case "false" :
            case "no" :
            case "non" :
            case "n" :
            case "0" :
                return false;
            default:
                throw new ParseException(value + " is not a valid boolean value.", 0);
        }
    };

    protected static final ValueParser<Integer> REFERENTIEL_PARSER = value -> {
        if(DataContext.REFERENTIELS.containsValue(value.toUpperCase())) {
            return DataContext.REFERENTIELS.entrySet().stream().filter(entry -> entry.getValue().equals(value)).map(Map.Entry::getKey).findFirst().get();
        } else {
            String message = String.format("'%s'  is not a valid referential value. Expected one of those: %s.", value, StringUtils.joinWith(", ", Stream.of(DataContext.REFERENTIELS.values()).collect(Collectors.toList())));
            throw new ParseException(message, 0);
        }
    };
    
    protected static final ValueParser<Character> ORIENTATION_PARSER = value -> {
        if (value.equals("")) {
            return ' ';
        }
        if(value.length() != 1) {
            String message = String.format("'%s'  is not a valid orientation value. Expected one of those: N, S, E, W.", value);
            throw new ParseException(message, 0);
        }
        if(!value.toUpperCase().matches("[NSEW]")) {
            String message = String.format("'%s'  is not a valid orientation value. Expected one of those: N, S, E, W.", value);
            throw new ParseException(message, 0);
        }
        return value.toUpperCase().charAt(0);
    };

    protected static final ValueParser<Specimen.TypeOrganisme> TYPE_ORGANISME_PARSER = value -> {
        try {
            return Specimen.TypeOrganisme.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            String message = String.format("'%s'  is not a valid Organism type value. Expected one of those: %s.", value, StringUtils.joinWith(", ", Stream.of(Specimen.TypeOrganisme.values()).map(Enum::name).collect(Collectors.toList())));
            throw new ParseException(message, 0);
        }
    };

}
