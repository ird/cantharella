package nc.ird.cantharella.service.model;

import nc.ird.cantharella.data.model.ResultatTestBio;

import java.math.BigDecimal;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class ResultatTestBioImport {

    protected String methodeCible;

    protected BigDecimal concMasse;

    protected String uniteConcMasse;

    protected String nomProduit;

    protected String produitTemoin;

    protected BigDecimal valeur;

    protected String initialesTypeExtraitSource;

    protected Boolean estActif;

    protected String repere;

    protected ResultatTestBio.Stade stade;

    protected ResultatTestBio.TypeResultat type;

    protected String nomErreur;

    protected String refTestBio;

    protected String organismeTesteur;

    protected String nomManipulateur;

    protected String prenomManipulateur;

    protected String date;

    protected String complement;

    public String getMethodeCible() {
        return methodeCible;
    }

    public void setMethodeCible(String methodeCible) {
        this.methodeCible = methodeCible;
    }

    public BigDecimal getConcMasse() {
        return concMasse;
    }

    public void setConcMasse(BigDecimal concMasse) {
        this.concMasse = concMasse;
    }

    public String getUniteConcMasse() {
        return uniteConcMasse;
    }

    public void setUniteConcMasse(String uniteConcMasse) {
        this.uniteConcMasse = uniteConcMasse;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public String getProduitTemoin() {
        return produitTemoin;
    }

    public void setProduitTemoin(String produitTemoin) {
        this.produitTemoin = produitTemoin;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }

    public String getInitialesTypeExtraitSource() {
        return initialesTypeExtraitSource;
    }

    public void setInitialesTypeExtraitSource(String initialesTypeExtraitSource) {
        this.initialesTypeExtraitSource = initialesTypeExtraitSource;
    }

    public Boolean getEstActif() {
        return estActif;
    }

    public void setEstActif(Boolean estActif) {
        this.estActif = estActif;
    }

    public String getRepere() {
        return repere;
    }

    public void setRepere(String repere) {
        this.repere = repere;
    }

    public String getRefTestBio() {
        return refTestBio;
    }

    public void setRefTestBio(String refTestBio) {
        this.refTestBio = refTestBio;
    }

    public String getOrganismeTesteur() {
        return organismeTesteur;
    }

    public void setOrganismeTesteur(String organismeTesteur) {
        this.organismeTesteur = organismeTesteur;
    }

    public ResultatTestBio.Stade getStade() {
        return stade;
    }

    public void setStade(ResultatTestBio.Stade stade) {
        this.stade = stade;
    }

    public ResultatTestBio.TypeResultat getType() {
        return type;
    }

    public void setType(ResultatTestBio.TypeResultat type) {
        this.type = type;
    }

    public String getNomErreur() {
        return nomErreur;
    }

    public void setNomErreur(String nomErreur) {
        this.nomErreur = nomErreur;
    }

    public String getNomManipulateur() {
        return nomManipulateur;
    }

    public void setNomManipulateur(String nomManipulateur) {
        this.nomManipulateur = nomManipulateur;
    }

    public String getPrenomManipulateur() {
        return prenomManipulateur;
    }

    public void setPrenomManipulateur(String prenomManipulateur) {
        this.prenomManipulateur = prenomManipulateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }
}
