package nc.ird.cantharella.service.services;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.ErreurDosage;
import nc.ird.cantharella.data.model.MethodeDosage;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.utils.normalizers.DosageNormalizer;
import nc.ird.cantharella.service.utils.normalizers.ErreurDosageNormalizer;
import nc.ird.cantharella.service.utils.normalizers.MethodeDosageNormalizer;
import nc.ird.cantharella.service.utils.normalizers.UniqueFieldNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalize;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.SortedSet;

public interface DosageService {

    /**
     * Créée un dosage
     *
     * @param dosage La manipulation
     * @throws DataConstraintException Si le dosage (réf) existe déjà
     */
    void createDosage(@Normalize(DosageNormalizer.class) Dosage dosage) throws DataConstraintException;

    /**
     * Supprime un dosage
     *
     * @param dosage Le dosage
     * @throws DataConstraintException En cas de données liées
     */
    void deleteDosage(Dosage dosage) throws DataConstraintException;

    /**
     * Liste l'ensemble des résultats de dosage selon les droits d'un utilisateur (triés par réf produit)
     *
     * @param utilisateur L'utilisateur
     * @return la liste des résultats
     */
    @Transactional(readOnly = true)
    List<ResultatDosage> listResultatsDosage(Utilisateur utilisateur);

    /**
     * Liste l'ensemble des résultats de dosages selon les droits d'un utilisateur (triés par réf produit)
     *
     * @param utilisateur L'utilisateur non admin
     * @return la liste des résultats
     */
    SortedSet<ResultatDosage> listResultatsDosageForUser(Utilisateur utilisateur);

    /**
     * Détermine si un utilisateur peut accéder à un résultat de dosage
     *
     * @param resultatDosage Le résultat
     * @param utilisateur L'utilisateur
     * @return TRUE s'il a le droit
     */
    @Transactional(readOnly = true)
    boolean isResultatDosageAccessibleByUser(ResultatDosage resultatDosage, Utilisateur utilisateur);

    /**
     * Vérifie si le dosage de référence donnée est unique dans la base
     *
     * @param dosage Le dosage
     * @return TRUE si le dosage est unique
     */
    @Transactional(readOnly = true)
    boolean isDosageUnique(Dosage dosage);

    /**
     * Charge un dosage
     *
     * @param idDosage ID du dosage
     * @return Le dosage
     * @throws DataNotFoundException Si non trouvé
     */
    Dosage loadDosage(Integer idDosage) throws DataNotFoundException;

    /**
     * Charge un dosage
     *
     * @param ref Référence du dosage
     * @return Le dosage
     * @throws DataNotFoundException Si non trouvé
     */
    Dosage loadDosage(@Normalize(UniqueFieldNormalizer.class) String ref) throws DataNotFoundException;

    /**
     * Met à jour un dosage
     *
     * @param dosage Le dosage
     * @throws DataConstraintException En cas de doublons (champs uniques)
     */
    void updateDosage(@Normalize(DosageNormalizer.class) Dosage dosage) throws DataConstraintException;

    /**
     * Rafraichit un dosage (pour éviter des LazyLoadingException)
     *
     * @param dosage le dosage
     */
    void refreshDosage(Dosage dosage);

    /**
     * Détermine si un utilisateur peut modifier ou supprimer un dosage
     *
     * @param dosage La manipulation
     * @param utilisateur L'utilisateur
     * @return TRUE s'il a le droit
     */
    boolean updateOrdeleteDosageEnabled(Dosage dosage, Utilisateur utilisateur);
    /**
     * Détermine si un résultat est unique par rapport à une une liste
     *
     * @param resultatDosage Le résultat de dosage
     * @param liste La liste des résultats
     * @return TRUE
     */
    @Transactional(readOnly = true)
    boolean isResultatDosageUniqueInList(final ResultatDosage resultatDosage, final List<ResultatDosage> liste);

    /**
     * Créée une méthode pour un dosage
     *
     * @param methode La méthode
     * @throws DataConstraintException Si la méthode (nom) existe déjà
     */
    void createMethodeDosage(@Normalize(MethodeDosageNormalizer.class) MethodeDosage methode)
            throws DataConstraintException;

    /**
     * Supprime une méthode pour un dosage
     *
     * @param methode La méthode
     * @throws DataConstraintException En cas de données liées
     */
    void deleteMethodeDosage(MethodeDosage methode) throws DataConstraintException;

    /**
     * Liste les méthodes existantes pour un dosage (triés par nom)
     *
     * @return la liste des méthodes
     */
    @Transactional(readOnly = true)
    List<MethodeDosage> listMethodesDosage();

    /**
     * Liste les domaines existants pour les méthodes de dosage
     *
     * @return Les cibles
     */
    @Transactional(readOnly = true)
    List<String> listDomainesMethodes();

    /**
     * Liste les unités de résultat existants pour les méthodes de dosage
     *
     * @return Les unités
     */
    @Transactional(readOnly = true)
    List<String> listUnitesResultatMethodes();

    /**
     * Charge une méthode pour un dosage
     *
     * @param idMethode ID de la méthode
     * @return La méthode correspondante
     * @throws DataNotFoundException Si non trouvée
     */
    MethodeDosage loadMethodeDosage(Integer idMethode) throws DataNotFoundException;

    /**
     * Charge une méthode pour un dosage
     *
     * @param nom Nom de la méthode
     * @return La méthode correspondante
     * @throws DataNotFoundException Si non trouvée
     */
    MethodeDosage loadMethodeDosage(@Normalize(UniqueFieldNormalizer.class) String nom) throws DataNotFoundException;

    /**
     * Met à jour une méthode pour un dosage
     *
     * @param methode La méthode
     * @throws DataConstraintException En cas de doublons (champs uniques)
     */
    void updateMethodeDosage(@Normalize(MethodeDosageNormalizer.class) MethodeDosage methode)
            throws DataConstraintException;

    /**
     * Rafraichit une méthode de dosage (pour éviter des LazyLoadingException)
     *
     * @param methode La méthode
     */
    void refreshMethodeDosage(MethodeDosage methode);

    /**
     * Créée une erreur pour un dosage
     *
     * @param erreurTest L'erreur
     * @throws DataConstraintException Si l'erreur (nom) existe déjà
     */
    void createErreurDosage(@Normalize(ErreurDosageNormalizer.class) ErreurDosage erreurTest)
            throws DataConstraintException;

    /**
     * Supprime une erreur pour un dosage
     *
     * @param erreurTest L'erreur
     * @throws DataConstraintException En cas de données liées
     */
    void deleteErreurDosage(ErreurDosage erreurTest) throws DataConstraintException;

    /**
     * Liste les erreurs existantes pour un dosage
     *
     * @return la liste des erreurs
     */
    List<ErreurDosage> listErreursDosage();

    /**
     * Charge une erreur pour un dosage
     *
     * @param idErreurTest ID de l'erreur
     * @return L'erreur
     * @throws DataNotFoundException Si non trouvée
     */
    ErreurDosage loadErreurDosage(Integer idErreurTest) throws DataNotFoundException;

    /**
     * Charge une erreur pour un dosage
     *
     * @param nom Nom de l'erreur
     * @return L'erreur
     * @throws DataNotFoundException Si non trouvée
     */
    ErreurDosage loadErreurDosage(@Normalize(UniqueFieldNormalizer.class) String nom) throws DataNotFoundException;

    /**
     * Met à jour une erreur pour un dosage
     *
     * @param erreurTest L'erreur
     * @throws DataConstraintException En cas de doublons (champs uniques)
     */
    void updateErreurDosage(@Normalize(ErreurDosageNormalizer.class) ErreurDosage erreurTest)
            throws DataConstraintException;

    long countResultatsDosage();
}
