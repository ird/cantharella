package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.config.DataContext;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.PartieDao;
import nc.ird.cantharella.data.dao.impl.PersonneDao;
import nc.ird.cantharella.data.dao.impl.PersonneInterrogeeDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.*;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.IngredientRemedeImport;
import nc.ird.cantharella.service.model.IngredientRemedeImportModel;
import nc.ird.cantharella.service.services.CampagneService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.service.services.SpecimenService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.service.utils.normalizers.RemedeNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Service
@Qualifier("importIngredientRemedeService")
public class ImportIngredientRemedeServiceImpl extends AbstractImportService {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImportIngredientRemedeServiceImpl.class);
    protected final String DATE_FORMAT = "dd/MM/uuuu";

    /**
     * Messages d'internationalisation
     */
    @Resource(name = "serviceMessageSource")
    protected MessageSourceAccessor messages;

    /**
     * DAO
     */
    @Autowired
    protected GenericDao dao;

    @Autowired
    protected PersonneService personneService;

    @Autowired
    protected CampagneService campagneService;

    @Autowired
    protected StationService stationService;

    @Autowired
    protected SpecimenService specimenService;

    @Autowired
    protected RemedeService remedeService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

        Map<String, Remede> remedesParRef = new HashMap<>();
        List<String> existingRemedesRef = new ArrayList<>();

        List<Campagne> campagnes = campagneService.listCampagnes(loadedUtilisateur);
        Map<String, Campagne> campagnesByName = Maps.uniqueIndex(campagnes, Campagne::getNom);

        List<Station> stations = stationService.listStations(loadedUtilisateur);
        //force to uppercase because we have sometimes stations with lowercase names
        Map<String, Station> stationsByName = Maps.uniqueIndex(stations, s -> s.getNom().toUpperCase());

        List<Specimen> specimens = specimenService.listSpecimens(loadedUtilisateur);
        Map<String, Specimen> specimensByRef = Maps.uniqueIndex(specimens, Specimen::getRef);

        List<Partie> parties = (List<Partie>) dao.list(PartieDao.getCriteriaFindAllParties());
        Map<String, Partie> partiesByName = Maps.uniqueIndex(parties, Partie::getNom);

        int currentLine = 0;
        for (ImportRow<T> ingredientRemedeImportRow : importData) {
            currentLine++;

            boolean valid = ingredientRemedeImportRow.isValid();
            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = ingredientRemedeImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("Nombre d'ingrédients")
                            || headerName.equalsIgnoreCase("Nombre patients")) {
                        String message = messages.getMessage("csv.import.numeric.invalid", new Object[]{currentLine, headerName}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("Recommandations")) {
                        String message = messages.getMessage("csv.import.boolean.invalid", new Object[]{currentLine, headerName, Lists.newArrayList("oui", "non", "o", "n")}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }

            IngredientRemedeImport ingredientRemedeImport = (IngredientRemedeImport) ingredientRemedeImportRow.getBean();
            String refRemede = ingredientRemedeImport.getReference();

            // Mandatory fields/relation
            if (refRemede == null || refRemede.isBlank()) {
                String message = messages.getMessage("csv.import.refremede.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else if (!remedesParRef.containsKey(refRemede) && existingRemedesRef.contains(refRemede)) {
                // Already loaded
                String message = messages.getMessage("csv.import.refremede.existing", new Object[] {currentLine, refRemede}, locale);
                importResult.addErrorLine(message);

            }  else {
                // Load
                boolean exists = false;
                try {
                    exists = remedeService.loadRemede(refRemede) != null;
                } catch (DataNotFoundException dce) {
                    //do nothing exists already false
                }
                if (exists) {
                    String message = messages.getMessage("csv.import.refremede.existing", new Object[] {currentLine, refRemede}, locale);
                    importResult.addErrorLine(message);
                    existingRemedesRef.add(refRemede);
                }
            }

            // Parse date
            Date parsedDate = null;
            try {
                LocalDate date = LocalDate.parse(ingredientRemedeImport.getDate(), DateTimeFormatter.ofPattern(DATE_FORMAT).withResolverStyle(ResolverStyle.STRICT));
                parsedDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                if (date.isAfter(LocalDate.now())) {
                    String message = messages.getMessage("csv.import.date.future", new Object[]{currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } catch (DateTimeParseException e) {
                String message = messages.getMessage("csv.import.date.invalid", new Object[]{currentLine, ingredientRemedeImport.getDate()}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve campagne
            String campagneCible = ingredientRemedeImport.getCampagne();
            Campagne campagne = null;
            if (StringUtils.isBlank(campagneCible)) {
                String message = messages.getMessage("csv.import.campagne.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                try {
                    campagne = campagneService.loadCampagne(campagneCible.toUpperCase());
                } catch (DataNotFoundException eee) {
                    String message = messages.getMessage("csv.import.campagne.unknown", new Object[]{currentLine, campagneCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Retrieve station
            String stationCible = ingredientRemedeImport.getStation();
            Station station = null;
            if (StringUtils.isBlank(stationCible)) {
                String message = messages.getMessage("csv.import.station.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                station = stationsByName.get(stationCible.toUpperCase());
                if (station == null) {
                    String message = messages.getMessage("csv.import.station.unknown", new Object[]{currentLine, stationCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Retrieve provenance station
            String provenanceCible = ingredientRemedeImport.getStation();
            Station provenance = null;
            if (!StringUtils.isBlank(provenanceCible)) {
                provenance = stationsByName.get(provenanceCible.toUpperCase());
                if (provenance == null) {
                    String message = messages.getMessage("csv.import.provenance.unknown", new Object[]{currentLine, provenanceCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            //check if station is in campagne or else no right to import
            if (campagne != null && station != null && !campagne.getStations().contains(station)) {
                String message = messages.getMessage("csv.import.station.not.in.campagne", new Object[]{currentLine, stationCible, campagneCible}, locale);
                importResult.addErrorLine(message);
            }

            //check if provenance is in campagne or else no right to import
            if (campagne != null && provenance != null && !campagne.getStations().contains(provenance)) {
                String message = messages.getMessage("csv.import.provenance.not.in.campagne", new Object[]{currentLine, provenanceCible, campagneCible}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve specimen
            String specimenCible = ingredientRemedeImport.getIngredientRefSpecimen();
            Specimen specimen = null;
            if (StringUtils.isBlank(specimenCible)) {
                String message = messages.getMessage("csv.import.specimen.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                specimen = specimensByRef.get(specimenCible.toUpperCase());
                if (specimen == null) {
                    String message = messages.getMessage("csv.import.specimen.unknown", new Object[]{currentLine, specimenCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Retrieve partie
            String partieCible = ingredientRemedeImport.getPartie();
            Partie partie = null;
            if (!StringUtils.isBlank(partieCible)) {
                partie = partiesByName.get(partieCible);
                if (partie == null) {
                    String message = messages.getMessage("csv.import.partie.unknown", new Object[]{currentLine, partieCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Retrieve classification
            String referentielCible = ingredientRemedeImport.getReferentiel();
            String entreeCible = ingredientRemedeImport.getReferentielCode();
            EntreeReferentiel classification = null;
            try {
                EntreeReferentiel.Referentiel referentiel = EntreeReferentiel.Referentiel.valueOf(referentielCible);
                classification = remedeService.listEntreeReferentiels(referentiel).stream().filter(entree -> entreeCible.equals(entree.getCode())).findFirst().orElse(null);
                if (classification == null) {
                    String message = messages.getMessage("csv.import.classification.unknown", new Object[]{currentLine, entreeCible, referentielCible}, locale);
                    importResult.addErrorLine(message);
                }
            }  catch (IllegalArgumentException eee) {
                String message = messages.getMessage("csv.import.referentiel2.unknown", new Object[]{currentLine, referentielCible}, locale);
                importResult.addErrorLine(message);
            }

            // Need to find the enqueteur
            String nomEnqueteur = ingredientRemedeImport.getNomEnqueteur();
            String prenomEnqueteur = ingredientRemedeImport.getPrenomEnqueteur();
            Map<String, Object> enqueteurParameters = ImmutableMap.of("nom", nomEnqueteur, "prenom", prenomEnqueteur);
            List<Personne> personnes = (List<Personne>) dao.list(PersonneDao.HQL_FIND_PERSONNE_PAR_NOM_PRENOM, enqueteurParameters);
            Personne enqueteur = null;
            if (personnes.isEmpty()) {
                String message = messages.getMessage("csv.import.enqueteur.unknown", new Object[] {currentLine, nomEnqueteur, prenomEnqueteur}, locale);
                importResult.addErrorLine(message);
            } else {
                enqueteur = personnes.get(0);
            }

            // Need to find the prescripteur
            String nomPrescripteur = ingredientRemedeImport.getNomPrescripteur();
            String prenomPrescripteur = ingredientRemedeImport.getPrenomPrescripteur();
            Map<String, Object> prescripteurParameters = ImmutableMap.of("nom", nomPrescripteur, "prenom", prenomPrescripteur);
            List<PersonneInterrogee> personneInterrogees = (List<PersonneInterrogee>) dao.list(PersonneInterrogeeDao.HQL_FIND_PERSONNE_INTERROGEE_PAR_NOM_PRENOM, prescripteurParameters);
            PersonneInterrogee prescripteur = null;
            if (personneInterrogees.isEmpty()) {
                String message = messages.getMessage("csv.import.prescripteur.unknown", new Object[] {currentLine, nomPrescripteur, prenomPrescripteur}, locale);
                importResult.addErrorLine(message);
            } else {
                prescripteur = personneInterrogees.get(0);
            }

            if (!DataContext.LANGUAGE_CODES.contains(ingredientRemedeImport.getCodeLangue())) {
                String message = messages.getMessage("csv.import.codeLangue.unknown", new Object[] {currentLine, ingredientRemedeImport.getCodeLangue()}, locale);
                importResult.addErrorLine(message);
            }

            //test disponibilite
            List<Disponibilite> disponibilites = remedeService.listDisponibilites();
            Disponibilite dispo = new Disponibilite();
            dispo.setNom(ingredientRemedeImport.getDisponibilite());
            if (!disponibilites.contains(dispo)) {
                String message = messages.getMessage("csv.import.disponibilite.unknown", new Object[] {currentLine, ingredientRemedeImport.getDisponibilite()}, locale);
                importResult.addErrorLine(message);
            }

            Remede remede = remedesParRef.get(refRemede);
            if (remede == null) {
                remede = new Remede();
                remede.setRef(refRemede);
                remede.setCampagne(campagne);
                remede.setStation(provenance);
                remede.setDate(parsedDate);
                remede.setEnqueteur(enqueteur);
                remede.setPrescripteur(prescripteur);
                remede.setNom(ingredientRemedeImport.getNomRemede());
                remede.setCodeLangue(ingredientRemedeImport.getCodeLangue());
                if (classification != null) {
                    List<EntreeReferentiel> classificationImported = new ArrayList<>();
                    classificationImported.add(classification);
                    remede.setClassification(classificationImported);
                }
                remede.setIndicationBiomedicale(ingredientRemedeImport.getIndicationBiomedicale());
                remede.setIndicationVernaculaire(ingredientRemedeImport.getIndicationVernaculaire());
                remede.setNbIngredients(ingredientRemedeImport.getNbIngredients());
                remede.setPreparation(ingredientRemedeImport.getPreparation());
                remede.setConservation(ingredientRemedeImport.getConservation());
                remede.setAdministration(ingredientRemedeImport.getAdministration());
                remede.setPosologie(ingredientRemedeImport.getPosologie());
                remede.setRecommandations(ingredientRemedeImport.getRecommandations() != null ? ingredientRemedeImport.getRecommandations() : false);
                remede.setRecommandationsText(ingredientRemedeImport.getRecommandationsText());
                remede.setEffetsIndesirables(ingredientRemedeImport.getEffetsIndesirables());
                remede.setPatientsParAn(ingredientRemedeImport.getNbPatients());
                remede.setComplement(ingredientRemedeImport.getComplement());
                remede.setCreateur(loadedUtilisateur);
            } else {
                // Be sure fields are same
                if (remede.getCampagne() == null) {
                    remede.setCampagne(campagne);
                } else if (!remede.getCampagne().equals(campagne)) {
                        String message = messages.getMessage("csv.import.campagne.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getStation() == null) {
                    remede.setStation(station);
                } else if (!remede.getStation().equals(station)) {
                        String message = messages.getMessage("csv.import.station.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getDate() == null) {
                    remede.setDate(parsedDate);
                } else if (!remede.getDate().equals(parsedDate)) {
                        String message = messages.getMessage("csv.import.remede.date.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getEnqueteur() == null) {
                    remede.setEnqueteur(enqueteur);
                } else if (!remede.getEnqueteur().equals(enqueteur)) {
                        String message = messages.getMessage("csv.import.enqueteur.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getPrescripteur() == null) {
                    remede.setPrescripteur(prescripteur);
                } else if (!remede.getDate().equals(parsedDate)) {
                        String message = messages.getMessage("csv.import.prescripteur.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getNom() == null) {
                    remede.setNom(ingredientRemedeImport.getNomRemede());
                } else if (!remede.getNom().equals(ingredientRemedeImport.getNomRemede())) {
                        String message = messages.getMessage("csv.import.nomRemede.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getCodeLangue() == null) {
                    remede.setCodeLangue(ingredientRemedeImport.getCodeLangue());
                } else if (!remede.getCodeLangue().equals(ingredientRemedeImport.getCodeLangue())) {
                        String message = messages.getMessage("csv.import.codeLangue.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getClassification() == null && classification != null) {
                    remede.setClassification(List.of(classification));
                } else if (!remede.getClassification().contains(classification)) {
                        String message = messages.getMessage("csv.import.classification.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getIndicationBiomedicale() == null) {
                    remede.setIndicationBiomedicale(ingredientRemedeImport.getIndicationBiomedicale());
                } else if (!remede.getIndicationBiomedicale().equals(ingredientRemedeImport.getIndicationBiomedicale())) {
                        String message = messages.getMessage("csv.import.indicationBiomedicale.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getIndicationVernaculaire() == null) {
                    remede.setIndicationVernaculaire(ingredientRemedeImport.getIndicationVernaculaire());
                } else if (!remede.getIndicationVernaculaire().equals(ingredientRemedeImport.getIndicationVernaculaire())) {
                        String message = messages.getMessage("csv.import.indicationVernaculaire.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getNbIngredients() == null) {
                    remede.setNbIngredients(ingredientRemedeImport.getNbIngredients());
                } else if (!remede.getNbIngredients().equals(ingredientRemedeImport.getNbIngredients())) {
                        String message = messages.getMessage("csv.import.nbIngredients.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getPreparation() == null) {
                    remede.setPreparation(ingredientRemedeImport.getPreparation());
                } else if (!remede.getPreparation().equals(ingredientRemedeImport.getPreparation())) {
                        String message = messages.getMessage("csv.import.preparation.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getConservation() == null) {
                    remede.setConservation(ingredientRemedeImport.getConservation());
                } else if (!remede.getConservation().equals(ingredientRemedeImport.getConservation())) {
                        String message = messages.getMessage("csv.import.conservation.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getAdministration() == null) {
                    remede.setAdministration(ingredientRemedeImport.getAdministration());
                } else if (!remede.getAdministration().equals(ingredientRemedeImport.getAdministration())) {
                        String message = messages.getMessage("csv.import.administration.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getPosologie() == null) {
                    remede.setPosologie(ingredientRemedeImport.getPosologie());
                } else if (!remede.getPosologie().equals(ingredientRemedeImport.getPosologie())) {
                        String message = messages.getMessage("csv.import.posologie.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (!remede.getRecommandations() == ingredientRemedeImport.getRecommandations()) {
                        String message = messages.getMessage("csv.import.recommandations.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getRecommandationsText() == null) {
                    remede.setRecommandationsText(ingredientRemedeImport.getRecommandationsText());
                } else if (!remede.getRecommandationsText().equals(ingredientRemedeImport.getRecommandationsText())) {
                        String message = messages.getMessage("csv.import.recommandationsText.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getEffetsIndesirables() == null) {
                    remede.setEffetsIndesirables(ingredientRemedeImport.getEffetsIndesirables());
                } else if (!remede.getEffetsIndesirables().equals(ingredientRemedeImport.getEffetsIndesirables())) {
                        String message = messages.getMessage("csv.import.effetsIndesirables.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getPatientsParAn() == null) {
                    remede.setPatientsParAn(ingredientRemedeImport.getNbPatients());
                } else if (!remede.getPatientsParAn().equals(ingredientRemedeImport.getNbPatients())) {
                        String message = messages.getMessage("csv.import.nbPatients.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

                if (remede.getComplement() == null) {
                    remede.setComplement(ingredientRemedeImport.getComplement());
                } else if (!remede.getComplement().equals(ingredientRemedeImport.getComplement())) {
                        String message = messages.getMessage("csv.import.complement.different", new Object[] {currentLine, refRemede}, locale);
                        importResult.addErrorLine(message);
                }

            }

            IngredientRemede ingredient = new IngredientRemede();
            ingredient.setPartie(partie);
            ingredient.setPeriode(ingredientRemedeImport.getPeriode());
            ingredient.setProvenance(provenance);
            ingredient.setDisponibilite(dispo.getNom());
            ingredient.setNomVernaculaire(ingredientRemedeImport.getIngredientNomVernaculaire());
            ingredient.setSpecimen(specimen);

            remede.getIngredients().add(ingredient);
            ingredient.setRemede(remede);

            Normalizer.normalize(RemedeNormalizer.class, remede);
            remedesParRef.put(ingredientRemedeImport.getReference(), remede);
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (Remede remede : remedesParRef.values()) {
                if (remede.getIngredients().size() != remede.getNbIngredients()) {
                    String message = messages.getMessage("csv.import.nbIngredients.import.different", new Object[] {remede.getRef()}, locale);
                    importResult.addErrorLine(message);
                }
                try {
                    dao.create(remede);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        IngredientRemedeImportModel model = new IngredientRemedeImportModel();

        File headers = File.createTempFile("remedes", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
