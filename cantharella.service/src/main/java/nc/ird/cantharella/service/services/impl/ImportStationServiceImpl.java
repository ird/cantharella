package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.Lists;
import nc.ird.cantharella.data.config.DataContext;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.StationImport;
import nc.ird.cantharella.service.model.StationImportModel;
import nc.ird.cantharella.service.utils.normalizers.StationNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.CoordTools;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("importStationService")
public class ImportStationServiceImpl extends AbstractImportService {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImportStationServiceImpl.class);
    protected final String DATE_FORMAT = "d/MM/uuuu";

    /**
     * Messages d'internationalisation
     */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /**
     * DAO
     */
    @Autowired
    private GenericDao dao;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        ImportResult importResult = new ImportResult();

        Map<String, Station> stationsParRef = new HashMap<>();
        List<String> existingStationsRef = new ArrayList<>();

        int currentLine = 0;
        for (ImportRow<T> stationImportRow : importData) {
            currentLine++;

            boolean valid = stationImportRow.isValid();
            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = stationImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("Latitude (degrés)")
                            || headerName.equalsIgnoreCase("Longitude (degrés)")
                            || headerName.equalsIgnoreCase("Latitude (minutes)")
                            || headerName.equalsIgnoreCase("Longitude (minutes)")) {
                        String message = messages.getMessage("csv.import.numeric.invalid", new Object[]{currentLine, headerName}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("Latitude (N/S)")
                            || headerName.equalsIgnoreCase("Longitude (E/W)")) {
                        String message = messages.getMessage("csv.import.orientation.unknown", new Object[]{currentLine}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("Référentiel")) {
                        String message = messages.getMessage("csv.import.referentiel.unknown", new Object[]{currentLine, Stream.of(DataContext.REFERENTIELS.values()).collect(Collectors.toList())}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }

            StationImport stationImport = (StationImport) stationImportRow.getBean();
            String refStation = stationImport.getNom();

            // Mandatory fields/relation
            if (refStation == null || refStation.isBlank()) {
                String message = messages.getMessage("csv.import.station.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else if (stationsParRef.containsKey(refStation) || existingStationsRef.contains(refStation)) {
                // Already loaded
                String message = messages.getMessage("csv.import.station.existing", new Object[]{currentLine, refStation}, locale);
                importResult.addErrorLine(message);
            } else {
                // Load
                boolean exists = dao.exists(Station.class, "nom", refStation);
                if (exists) {
                    String message = messages.getMessage("csv.import.station.existing", new Object[]{currentLine, refStation}, locale);
                    importResult.addErrorLine(message);
                    existingStationsRef.add(refStation);
                }
            }

            if (stationImport.getCodePays() == null || stationImport.getCodePays().isBlank()) {
                String message = messages.getMessage("csv.import.pays.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            if (stationImport.getCodePays() != null && stationImport.getCodePays().length() != 2) {
                String message = messages.getMessage("csv.import.pays.invalid", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            // Check latitude values
            boolean latitudeKO = false;
            if (stationImport.getLatitudeDegres() != null && stationImport.getLatitudeDegres() < CoordTools.LATITUDE_MIN_DEGREES) {
                String message = messages.getMessage("csv.import.latitude.degrees.tosmall", new Object[]{currentLine, CoordTools.LATITUDE_MIN_DEGREES}, locale);
                importResult.addErrorLine(message);
                latitudeKO = true;
            }
            if (stationImport.getLatitudeDegres() != null && stationImport.getLatitudeDegres() > CoordTools.LATITUDE_MAX_DEGREES) {
                String message = messages.getMessage("csv.import.latitude.degrees.tobig", new Object[]{currentLine, CoordTools.LATITUDE_MAX_DEGREES}, locale);
                importResult.addErrorLine(message);
                latitudeKO = true;
            }
            if (stationImport.getLatitudeMinutes() != null && stationImport.getLatitudeMinutes().compareTo(CoordTools.LATITUDE_MIN_MINUTES) < 0) {
                String message = messages.getMessage("csv.import.latitude.minutes.tosmall", new Object[]{currentLine, CoordTools.LATITUDE_MIN_MINUTES}, locale);
                importResult.addErrorLine(message);
                latitudeKO = true;
            }
            if (stationImport.getLatitudeMinutes() != null && stationImport.getLatitudeMinutes().compareTo(CoordTools.LATITUDE_MAX_MINUTES) > 0) {
                String message = messages.getMessage("csv.import.latitude.minutes.tobig", new Object[]{currentLine, CoordTools.LATITUDE_MAX_MINUTES}, locale);
                importResult.addErrorLine(message);
                latitudeKO = true;
            }
            // Check longitude values
            boolean longitudeKO = false;
            if (stationImport.getLongitudeDegres() != null && stationImport.getLongitudeDegres() < CoordTools.LONGITUDE_MIN_DEGREES) {
                String message = messages.getMessage("csv.import.longitude.degrees.tosmall", new Object[]{currentLine, CoordTools.LONGITUDE_MIN_DEGREES}, locale);
                importResult.addErrorLine(message);
                longitudeKO = true;
            }
            if (stationImport.getLongitudeDegres() != null && stationImport.getLongitudeDegres() > CoordTools.LONGITUDE_MAX_DEGREES) {
                String message = messages.getMessage("csv.import.longitude.degrees.tobig", new Object[]{currentLine, CoordTools.LONGITUDE_MAX_DEGREES}, locale);
                importResult.addErrorLine(message);
                longitudeKO = true;
            }
            if (stationImport.getLongitudeMinutes() != null && stationImport.getLongitudeMinutes().compareTo(CoordTools.LONGITUDE_MIN_MINUTES) < 0) {
                String message = messages.getMessage("csv.import.longitude.minutes.tosmall", new Object[]{currentLine, CoordTools.LONGITUDE_MIN_MINUTES}, locale);
                importResult.addErrorLine(message);
                longitudeKO = true;
            }
            if (stationImport.getLongitudeMinutes() != null && stationImport.getLongitudeMinutes().compareTo(CoordTools.LONGITUDE_MAX_MINUTES) > 0) {
                String message = messages.getMessage("csv.import.longitude.minutes.tobig", new Object[]{currentLine, CoordTools.LONGITUDE_MAX_MINUTES}, locale);
                importResult.addErrorLine(message);
                longitudeKO = true;
            }

            Station station = stationsParRef.get(refStation);
            if (station == null) {
                station = new Station();
                station.setNom(stationImport.getNom());
                station.setCodePays(stationImport.getCodePays());
                station.setLocalite(stationImport.getLocalite());
                station.setComplement(stationImport.getComplement());
                if (stationImport.getLatitudeDegres() != null
                        && stationImport.getLatitudeMinutes() != null
                        && stationImport.getLatitudeOrientation() != null && stationImport.getLatitudeOrientation() != ' '
                        && !latitudeKO) {

                    station.setLatitude(
                            CoordTools.latitude(stationImport.getLatitudeDegres(),
                                    stationImport.getLatitudeMinutes(),
                                    stationImport.getLatitudeOrientation()));
                }
                if (stationImport.getLongitudeDegres() != null
                        && stationImport.getLongitudeMinutes() != null
                        && stationImport.getLongitudeOrientation() != null && stationImport.getLongitudeOrientation() != ' '
                        && !longitudeKO) {

                    station.setLongitude(
                            CoordTools.longitude(stationImport.getLongitudeDegres(),
                                    stationImport.getLongitudeMinutes(),
                                    stationImport.getLongitudeOrientation()));
                }
                station.setReferentiel(DataContext.REFERENTIELS.entrySet().stream().filter(entry -> "WGS84".equals(entry.getValue())).map(Map.Entry::getKey).findFirst().orElse(null));
                station.setCreateur(utilisateur);
            } else {
                // Be sure name are same
                if (station.getNom() == null) {
                    station.setNom(stationImport.getNom());
                } else if (!station.getNom().equals(stationImport.getNom())) {
                    String message = messages.getMessage("csv.import.station.nom.different", new Object[]{currentLine, stationImport.getNom()}, locale);
                    importResult.addErrorLine(message);
                }
            }

            Normalizer.normalize(StationNormalizer.class, station);
            stationsParRef.put(stationImport.getNom(), station);
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (Station station : stationsParRef.values()) {
                try {
                    dao.create(station);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        StationImportModel model = new StationImportModel();

        File headers = File.createTempFile("stations", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
