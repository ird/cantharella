package nc.ird.cantharella.service.services;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;

import java.io.File;
import java.io.InputStream;
import java.util.Locale;

public interface ImportServiceFactory {

    File exportHeader(String type) throws Exception;

    <T> ImportResult importData(String entityType, InputStream input, Utilisateur utilisateur, Locale locale) throws DataConstraintException;
}
