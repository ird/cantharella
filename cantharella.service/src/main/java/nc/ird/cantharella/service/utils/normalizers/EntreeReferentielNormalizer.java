package nc.ird.cantharella.service.utils.normalizers;

import nc.ird.cantharella.data.model.EntreeReferentiel;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.AssertTools;

public class EntreeReferentielNormalizer extends Normalizer<EntreeReferentiel> {

    /** {@inheritDoc} */
    @Override
    protected EntreeReferentiel normalize(EntreeReferentiel entreeReferentiel) {
        AssertTools.assertNotNull(entreeReferentiel);
        entreeReferentiel.setCode(entreeReferentiel.getCode().toUpperCase());

        return entreeReferentiel;
    }

}
