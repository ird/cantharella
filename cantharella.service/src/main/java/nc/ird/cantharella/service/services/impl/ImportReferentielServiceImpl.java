package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.Lists;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.EntreeReferentiel;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.ReferentielImport;
import nc.ird.cantharella.service.model.ReferentielImportModel;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.service.utils.normalizers.EntreeReferentielNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author jcouteau (couteau@codelutin.com)
 */
@Service
@Qualifier("importReferentielService")
public class ImportReferentielServiceImpl extends AbstractImportService {

    /** Messages d'internationalisation */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /** DAO */
    @Autowired
    private GenericDao dao;

    @Autowired
    private RemedeService remedeService;

    @Autowired
    private PersonneService personneService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

        Map<String, EntreeReferentiel> referentielIcpc3 = new HashMap<>();
        Map<String, EntreeReferentiel> referentielIcd11 = new HashMap<>();
        Map<String, EntreeReferentiel> referentielEbdcs = new HashMap<>();

        Map<String, EntreeReferentiel> oldReferentielIcpc3 =
                remedeService.listEntreeReferentiels(EntreeReferentiel.Referentiel.ICPC3)
                        .stream()
                        .distinct()
                        .collect(Collectors.toMap(EntreeReferentiel::getCode, a -> a));
        Map<String, EntreeReferentiel> oldReferentielIcd11 =
                remedeService.listEntreeReferentiels(EntreeReferentiel.Referentiel.ICD11)
                        .stream()
                        .distinct()
                        .collect(Collectors.toMap(EntreeReferentiel::getCode, a -> a));
        Map<String, EntreeReferentiel> oldReferentielEbdcs =
                remedeService.listEntreeReferentiels(EntreeReferentiel.Referentiel.EBDCS)
                        .stream()
                        .distinct()
                        .collect(Collectors.toMap(EntreeReferentiel::getCode, a -> a));

        int currentLine = 0;
        for (ImportRow<T> referentielImportRow : importData) {
            currentLine++;

            ReferentielImport referentielImport = (ReferentielImport) referentielImportRow.getBean();
            String code = referentielImport.getCode();

            // Mandatory fields/relation
            // Retrieve campagne
            String referentielName = referentielImport.getReferentiel();
            EntreeReferentiel.Referentiel referentiel = null;
            if (StringUtils.isBlank(referentielName)) {
                String message = messages.getMessage("csv.import.referentiel.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                try {
                    referentiel = EntreeReferentiel.Referentiel.valueOf(referentielName);
                } catch (IllegalArgumentException iae) {
                    String message = messages.getMessage("csv.import.referentiel.unknown", new Object[]{currentLine, StringUtils.joinWith(", ", Stream.of(EntreeReferentiel.Referentiel.values()).map(Enum::name).collect(Collectors.toList()))}, locale);
                    importResult.addErrorLine(message);
                }
            }

            String valeur = referentielImport.getValeur();
            if (valeur == null || valeur.isBlank()) {
                String message = messages.getMessage("csv.import.valeur.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            if (referentiel != null) {
                EntreeReferentiel entreeReferentiel = null;

                switch (referentiel) {
                    case EBDCS:
                        entreeReferentiel = referentielEbdcs.get(code);
                        break;
                    case ICD11:
                        entreeReferentiel = referentielIcd11.get(code);
                        break;
                    case ICPC3:
                        entreeReferentiel = referentielIcpc3.get(code);
                        break;
                }

                if (entreeReferentiel == null) {

                    switch (referentiel) {
                        case EBDCS:
                            entreeReferentiel = oldReferentielEbdcs.get(code);
                            break;
                        case ICD11:
                            entreeReferentiel = oldReferentielIcd11.get(code);
                            break;
                        case ICPC3:
                            entreeReferentiel = oldReferentielIcpc3.get(code);
                            break;
                    }
                }

                if (entreeReferentiel == null) {
                    entreeReferentiel = new EntreeReferentiel();
                    entreeReferentiel.setCode(code);
                    entreeReferentiel.setValeur(valeur);
                    entreeReferentiel.setReferentiel(referentiel);
                } else {
                    entreeReferentiel.setValeur(valeur);
                }

                Normalizer.normalize(EntreeReferentielNormalizer.class, entreeReferentiel);

                switch (referentiel) {
                    case EBDCS:
                        referentielEbdcs.put(code, entreeReferentiel);
                        break;
                    case ICD11:
                        referentielIcd11.put(code, entreeReferentiel);
                        break;
                    case ICPC3:
                        referentielIcpc3.put(code, entreeReferentiel);
                        break;
                }
            }
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (EntreeReferentiel entreeReferentiel : referentielEbdcs.values()) {
                try {
                    dao.createOrUpdate(entreeReferentiel);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            for (EntreeReferentiel personne : referentielIcd11.values()) {
                try {
                    dao.createOrUpdate(personne);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            for (EntreeReferentiel personne : referentielIcpc3.values()) {
                try {
                    dao.createOrUpdate(personne);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        ReferentielImportModel model = new ReferentielImportModel();

        File headers = File.createTempFile("referentiel", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }


}
