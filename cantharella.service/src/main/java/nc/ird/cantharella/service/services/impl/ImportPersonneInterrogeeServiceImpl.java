package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.exceptions.ImportException;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.PersonneInterrogeeImport;
import nc.ird.cantharella.service.model.PersonneInterrogeeImportModel;
import nc.ird.cantharella.service.services.CampagneService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.service.utils.normalizers.PersonneInterrogeeNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author jcouteau (couteau@codelutin.com)
 */
@Service
@Qualifier("importPersonneInterrogeeService")
public class ImportPersonneInterrogeeServiceImpl extends AbstractImportService {

    /** Messages d'internationalisation */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /** DAO */
    @Autowired
    private GenericDao dao;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private CampagneService campagneService;

    @Autowired
    private StationService stationService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

        // Chargement de certaines données en amont
        List<Campagne> campagnes = campagneService.listCampagnes(loadedUtilisateur);
        Map<String, Campagne> campagneByNom = Maps.uniqueIndex(campagnes, Campagne::getNom);

        List<Station> stations = stationService.listStations(loadedUtilisateur);
        Map<String, Station> stationByNom = Maps.uniqueIndex(stations, Station::getNom);

        Map<String, PersonneInterrogee> personnesParNoms = new HashMap<>();

        Map<String, PersonneInterrogee> personnesInterrogeesParRef = new HashMap<>();

        Pattern emailPattern = Pattern.compile("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");

        Pattern allCountryTelephonePattern = Pattern.compile("^(\\+\\d{1,3}( )?)?((\\(\\d{1,3}\\))|\\d{1,3})[- .]?\\d{3,4}[- .]?\\d{4}$");

        int currentLine = 0;
        for (ImportRow<T> personneInterrogeeImportRow : importData) {
            currentLine++;

            PersonneInterrogeeImport personneInterrogeeImport = (PersonneInterrogeeImport) personneInterrogeeImportRow.getBean();

            // Mandatory fields/relation
            // Retrieve campagne
            String campagneName = personneInterrogeeImport.getCampagne();
            Campagne campagne = null;
            if (StringUtils.isBlank(campagneName)) {
                String message = messages.getMessage("csv.import.campagne.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                campagne = campagneByNom.get(campagneName);
                if (campagne == null) {
                    String message = messages.getMessage("csv.import.campagne.unknown", new Object[]{currentLine, campagneName}, locale);
                    importResult.addErrorLine(message);
                }
            }
            // Retrieve station
            String stationName = personneInterrogeeImport.getStation();
            Station station = null;
            if (StringUtils.isBlank(stationName)) {
                String message = messages.getMessage("csv.import.station.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                station = stationByNom.get(stationName);
                if (station == null) {
                    String message = messages.getMessage("csv.import.station.unknown", new Object[]{currentLine, stationName}, locale);
                    importResult.addErrorLine(message);
                }
            }
            //check if station is in campagne or else no right to import
            if (campagne != null && station != null && !campagne.getStations().contains(station)) {
                String message = messages.getMessage("csv.import.station.not.in.campagne", new Object[]{currentLine, stationName, campagneName}, locale);
                importResult.addErrorLine(message);
            }

            String nomPersonne = personneInterrogeeImport.getNom();
            if (nomPersonne == null || nomPersonne.isBlank()) {
                String message = messages.getMessage("csv.import.personne.nom.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            String prenomPersonne = personneInterrogeeImport.getPrenom();
            if (prenomPersonne == null || prenomPersonne.isBlank()) {
                String message = messages.getMessage("csv.import.personne.prenom.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            if (personnesParNoms.containsKey(nomPersonne + prenomPersonne)) {
                String message = messages.getMessage("csv.import.personne.nom.existing", new Object[]{currentLine, nomPersonne, prenomPersonne}, locale);
                importResult.addErrorLine(message);
            } else {
                DetachedCriteria CRITERIA_UNIQUE_NOM_PRENOM = DetachedCriteria
                        .forClass(PersonneInterrogee.class)
                        .add(Restrictions.eq("nom", nomPersonne))
                        .add(Restrictions.eq("prenom", prenomPersonne))
                        .add(Restrictions.eq("campagne", campagne));
                List<?> homonymes = dao.list(CRITERIA_UNIQUE_NOM_PRENOM);
                if (homonymes != null && !homonymes.isEmpty()) {
                    String message = messages.getMessage("csv.import.personne.nom.existing", new Object[]{currentLine, nomPersonne, prenomPersonne}, locale);
                    importResult.addErrorLine(message);
                }
            }
            String genre = personneInterrogeeImport.getGenre();
            PersonneInterrogee.Genre genrePersonne = null;
            if (genre == null || genre.isBlank()) {
                String message = messages.getMessage("csv.import.genre.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                switch (genre) {
                    case "M":
                        genrePersonne = PersonneInterrogee.Genre.M;
                        break;
                    case "F":
                        genrePersonne = PersonneInterrogee.Genre.F;
                        break;
                    case "NSP":
                        genrePersonne = PersonneInterrogee.Genre.NSP;
                        break;
                    default:
                        String message = messages.getMessage("csv.import.genre.invalid", new Object[]{currentLine, Lists.newArrayList("M", "F", "NSP")}, locale);
                        importResult.addErrorLine(message);
                        break;
                }
            }
            String courriel = personneInterrogeeImport.getCourriel();
            if (courriel != null && !courriel.isBlank() &&  !emailPattern.matcher(courriel).matches()) {
                String message = messages.getMessage("csv.import.courriel.invalid", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            String telephone = personneInterrogeeImport.getTelephone();
            if (telephone != null && !telephone.isBlank() && !allCountryTelephonePattern.matcher(telephone).matches()) {
                String message = messages.getMessage("csv.import.telephone.invalid", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            String ville = personneInterrogeeImport.getVille();
            if (ville == null || ville.isBlank()) {
                String message = messages.getMessage("csv.import.ville.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            String pays = personneInterrogeeImport.getCodePays();
            if (pays == null || pays.isBlank()) {
                String message = messages.getMessage("csv.import.pays.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            if (pays != null && pays.length() != 2) {
                String message = messages.getMessage("csv.import.pays.invalid", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            String annee = personneInterrogeeImport.getAnnee();
            Integer anneeNaissance = null;
            if (annee != null && !annee.isBlank()) {
                try {
                    anneeNaissance = Integer.valueOf(annee);
                } catch (NumberFormatException e) {
                    String message = messages.getMessage("csv.import.anneeNaissance.invalid", new Object[]{currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            }
            String experience = personneInterrogeeImport.getExperience();
            Integer experiencePersonne = null;
            if (experience != null && !experience.isBlank()) {
                try {
                    experiencePersonne = Integer.valueOf(experience);
                } catch (NumberFormatException e) {
                    String message = messages.getMessage("csv.import.experience.invalid", new Object[]{currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            }


            String refPersonne = prenomPersonne + nomPersonne + ville;

            try {
                PersonneInterrogee personneInterrogee = personnesInterrogeesParRef.get(refPersonne);

                if (personneInterrogee == null) {
                    personneInterrogee = new PersonneInterrogee();
                    personneInterrogee.setStation(station);
                    personneInterrogee.setCampagne(campagne);
                    personneInterrogee.setPrenom(personneInterrogeeImport.getPrenom());
                    personneInterrogee.setNom(personneInterrogeeImport.getNom());
                    personneInterrogee.setGenre(genrePersonne);
                    personneInterrogee.setAnneeNaissance(anneeNaissance);
                    personneInterrogee.setExperience(experiencePersonne);
                    personneInterrogee.setProfession(personneInterrogeeImport.getProfession());
                    personneInterrogee.setLabellisation(personneInterrogeeImport.getLabellisation());
                    personneInterrogee.setTelephone(telephone);
                    personneInterrogee.setCourriel(personneInterrogeeImport.getCourriel());
                    personneInterrogee.setAdressePostale(personneInterrogeeImport.getAdresse());
                    personneInterrogee.setCodePostal(personneInterrogeeImport.getCodePostal());
                    personneInterrogee.setVille(personneInterrogeeImport.getVille());
                    personneInterrogee.setCodePays(personneInterrogeeImport.getCodePays());
                    personneInterrogee.setCreateur(utilisateur);
                    personneInterrogee.setComplement(personneInterrogeeImport.getComplement());
                } else {

                    // Be sure name are same
                    if (personneInterrogee.getNom() == null) {
                        personneInterrogee.setNom(personneInterrogeeImport.getNom());
                    } else if (!personneInterrogee.getNom().equals(personneInterrogeeImport.getNom())) {
                        String message = messages.getMessage("csv.import.personneInterrogee.nom.different", new Object[]{currentLine, personneInterrogeeImport.getNom(), personneInterrogee.getNom()}, locale);
                        throw new ImportException(message);
                    }
                    if (personneInterrogee.getPrenom() == null) {
                        personneInterrogee.setPrenom(personneInterrogeeImport.getPrenom());
                    } else if (!personneInterrogee.getPrenom().equals(personneInterrogeeImport.getPrenom())) {
                        String message = messages.getMessage("csv.import.personneInterrogee.prenom.different", new Object[]{currentLine, personneInterrogeeImport.getPrenom(), personneInterrogee.getPrenom()}, locale);
                        throw new ImportException(message);
                    }
                }

                Normalizer.normalize(PersonneInterrogeeNormalizer.class, personneInterrogee);
                personnesInterrogeesParRef.put(refPersonne, personneInterrogee);
                personnesParNoms.put(nomPersonne + prenomPersonne, personneInterrogee);

            } catch (ImportException e) {
                importResult.addErrorLine(e.getMessage());
            }
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (PersonneInterrogee personne : personnesInterrogeesParRef.values()) {
                try {
                    dao.create(personne);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        PersonneInterrogeeImportModel model = new PersonneInterrogeeImportModel();

        File headers = File.createTempFile("personnesInterrogees", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }


}
