package nc.ird.cantharella.service.model;

import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Molecule;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Purification;
import nc.ird.cantharella.data.model.Remede;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.TestBio;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DocumentSearchResult  implements Serializable {

    /** Search result specimens. */
    protected List<Document> specimenDocs;

    protected Map<Integer, Specimen> specimens;

    /** Search result lots. */
    protected List<Document> lotDocs;

    protected Map<Integer, Lot> lots;

    /** Search result campagnes. */
    protected List<Document> campagneDocs;

    protected Map<Integer, Campagne> campagnes;

    /** Search result extractions. */
    protected List<Document> extractionDocs;

    protected Map<Integer, Extraction> extractions;

    /** Search result purification. */
    protected List<Document> purificationDocs;

    protected Map<Integer, Purification> purifications;

    /** Search result resultatTestBios. */
    protected List<Document> resultatTestBioDocs;

    protected Map<Integer, TestBio> testBios;

    /** Search result stations. */
    protected List<Document> stationDocs;

    protected Map<Integer, Station> stations;

    /** Search result molecules. */
    protected List<Document> moleculeDocs;

    protected Map<Integer, Molecule> molecules;

    /** Search result molecules. */
    protected List<Document> dosagesDocs;

    protected Map<Integer, Dosage> dosages;

    /** Search result molecules. */
    protected List<Document> personnesInterrogeesDocs;

    protected Map<Integer, PersonneInterrogee> personnesInterrogees;

    protected List<Document> remedesDocs;

    protected Map<Integer, Remede> remedes;

    public List<Document> getSpecimenDocs() {
        return specimenDocs;
    }

    public void setSpecimenDocs(List<Document> specimenDocs) {
        this.specimenDocs = specimenDocs;
    }

    public List<Document> getLotDocs() {
        return lotDocs;
    }

    public void setLotDocs(List<Document> lotDocs) {
        this.lotDocs = lotDocs;
    }

    public List<Document> getExtractionDocs() {
        return extractionDocs;
    }

    public void setExtractionDocs(List<Document> extractionDocs) {
        this.extractionDocs = extractionDocs;
    }

    public List<Document> getPurificationDocs() {
        return purificationDocs;
    }

    public void setPurificationDocs(List<Document> purificationDocs) {
        this.purificationDocs = purificationDocs;
    }

    public List<Document> getResultatTestBioDocs() {
        return resultatTestBioDocs;
    }

    public void setResultatTestBioDocs(List<Document> resultatTestBioDocs) {
        this.resultatTestBioDocs = resultatTestBioDocs;
    }

    public List<Document> getStationDocs() {
        return stationDocs;
    }

    public void setStationDocs(List<Document> stationDocs) {
        this.stationDocs = stationDocs;
    }

    public List<Document> getMoleculeDocs() {
        return moleculeDocs;
    }

    public void setMoleculeDocs(List<Document> moleculeDocs) {
        this.moleculeDocs = moleculeDocs;
    }

    public List<Document> getCampagneDocs() {
        return campagneDocs;
    }

    public void setCampagneDocs(List<Document> campagneDocs) {
        this.campagneDocs = campagneDocs;
    }

    public Map<Integer, Specimen> getSpecimens() {
        return specimens;
    }

    public void setSpecimens(Map<Integer, Specimen> specimens) {
        this.specimens = specimens;
    }

    public Map<Integer, Lot> getLots() {
        return lots;
    }

    public void setLots(Map<Integer, Lot> lots) {
        this.lots = lots;
    }

    public Map<Integer, Campagne> getCampagnes() {
        return campagnes;
    }

    public void setCampagnes(Map<Integer, Campagne> campagnes) {
        this.campagnes = campagnes;
    }

    public Map<Integer, Extraction> getExtractions() {
        return extractions;
    }

    public void setExtractions(Map<Integer, Extraction> extractions) {
        this.extractions = extractions;
    }

    public Map<Integer, Purification> getPurifications() {
        return purifications;
    }

    public void setPurifications(Map<Integer, Purification> purifications) {
        this.purifications = purifications;
    }

    public Map<Integer, TestBio> getTestBios() {
        return testBios;
    }

    public void setTestBios(Map<Integer, TestBio> testBios) {
        this.testBios = testBios;
    }

    public Map<Integer, Station> getStations() {
        return stations;
    }

    public void setStations(Map<Integer, Station> stations) {
        this.stations = stations;
    }

    public Map<Integer, Molecule> getMolecules() {
        return molecules;
    }

    public void setMolecules(Map<Integer, Molecule> molecules) {
        this.molecules = molecules;
    }

    public List<Document> getDosagesDocs() {
        return dosagesDocs;
    }

    public void setDosagesDocs(List<Document> dosagesDocs) {
        this.dosagesDocs = dosagesDocs;
    }

    public Map<Integer, Dosage> getDosages() {
        return dosages;
    }

    public void setDosages(Map<Integer, Dosage> dosages) {
        this.dosages = dosages;
    }

    public List<Document> getPersonnesInterrogeesDocs() {
        return personnesInterrogeesDocs;
    }

    public void setPersonnesInterrogeesDocs(List<Document> personnesInterrogeesDocs) {
        this.personnesInterrogeesDocs = personnesInterrogeesDocs;
    }

    public Map<Integer, PersonneInterrogee> getPersonnesInterrogees() {
        return personnesInterrogees;
    }

    public void setPersonnesInterrogees(Map<Integer, PersonneInterrogee> personnesInterrogees) {
        this.personnesInterrogees = personnesInterrogees;
    }

    public List<Document> getRemedesDocs() {
        return remedesDocs;
    }

    public void setRemedesDocs(List<Document> remedesDocs) {
        this.remedesDocs = remedesDocs;
    }

    public Map<Integer, Remede> getRemedes() {
        return remedes;
    }

    public void setRemedes(Map<Integer, Remede> remedes) {
        this.remedes = remedes;
    }
}
