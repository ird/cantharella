package nc.ird.cantharella.service.services;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Configuration;
import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.service.utils.normalizers.UniteConcMasseNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalize;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ConfigurationService {

    /**
     * Liste des unites
     *
     * @return la liste des unites
     */
    @Transactional(readOnly = true)
    List<UniteConcMasse> listUnites();

    /**
     * Créée une méthode pour un dosage
     *
     * @param methode La méthode
     * @throws DataConstraintException Si la méthode (nom) existe déjà
     */
    void createUniteConcMasse(@Normalize(UniteConcMasseNormalizer.class) UniteConcMasse methode)
            throws DataConstraintException;

    /**
     * Supprime une méthode pour un dosage
     *
     * @param unite La méthode
     * @throws DataConstraintException En cas de données liées
     */
    void deleteUniteConcMasse(UniteConcMasse unite) throws DataConstraintException;

    /**
     * Charge une méthode pour un dosage
     *
     * @param idMethode ID de la méthode
     * @return La méthode correspondante
     * @throws DataNotFoundException Si non trouvée
     */
    UniteConcMasse loadUniteConcMasse(Integer idMethode) throws DataNotFoundException;

    /**
     * Met à jour une méthode pour un dosage
     *
     * @param methode La méthode
     * @throws DataConstraintException En cas de doublons (champs uniques)
     */
    void updateUniteConcMasse(@Normalize(UniteConcMasseNormalizer.class) UniteConcMasse methode)
            throws DataConstraintException;

    Configuration loadConfiguration() throws DataNotFoundException;

    void updateConfiguration(Configuration configuration) throws DataConstraintException;

}
