package nc.ird.cantharella.service.model;

import java.math.BigDecimal;

public class ExtractionImport {

    String refLot;

    BigDecimal masseAextraire;

    String complement;

    String methodeExtraction;

    String typeExtrait;

    String refExtrait;

    BigDecimal masseExtrait;

    String refExtraction;

    String nomManipulateur;

    String prenomManipulateur;

    String date;

    public String getRefLot() {
        return refLot;
    }

    public void setRefLot(String refLot) {
        this.refLot = refLot;
    }

    public BigDecimal getMasseAextraire() {
        return masseAextraire;
    }

    public void setMasseAextraire(BigDecimal masseAextraire) {
        this.masseAextraire = masseAextraire;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getMethodeExtraction() {
        return methodeExtraction;
    }

    public void setMethodeExtraction(String methodeExtraction) {
        this.methodeExtraction = methodeExtraction;
    }

    public String getTypeExtrait() {
        return typeExtrait;
    }

    public void setTypeExtrait(String typeExtrait) {
        this.typeExtrait = typeExtrait;
    }

    public String getRefExtrait() {
        return refExtrait;
    }

    public void setRefExtrait(String refExtrait) {
        this.refExtrait = refExtrait;
    }

    public BigDecimal getMasseExtrait() {
        return masseExtrait;
    }

    public void setMasseExtrait(BigDecimal masseExtrait) {
        this.masseExtrait = masseExtrait;
    }

    public String getRefExtraction() {
        return refExtraction;
    }

    public void setRefExtraction(String refExtraction) {
        this.refExtraction = refExtraction;
    }

    public String getNomManipulateur() {
        return nomManipulateur;
    }

    public void setNomManipulateur(String nomManipulateur) {
        this.nomManipulateur = nomManipulateur;
    }

    public String getPrenomManipulateur() {
        return prenomManipulateur;
    }

    public void setPrenomManipulateur(String prenomManipulateur) {
        this.prenomManipulateur = prenomManipulateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
