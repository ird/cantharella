package nc.ird.cantharella.service.services;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.utils.normalizers.PersonneInterrogeeNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalize;

import java.util.List;
import java.util.SortedSet;

public interface PersonneInterrogeeService {

    /**
     * Créée une personne interrogée
     *
     * @param dosage La manipulation
     * @throws DataConstraintException Si la personneInterrogee existe déjà
     */
    void createPersonneInterrogee(@Normalize(PersonneInterrogeeNormalizer.class) PersonneInterrogee dosage) throws DataConstraintException;

    /**
     * Supprime une personne interrogée
     *
     * @param personneInterrogee La personne interrogée
     * @throws DataConstraintException En cas de données liées
     */
    void deletePersonneInterrogee(PersonneInterrogee personneInterrogee) throws DataConstraintException;

    /**
     * Charge une personne interrogée
     *
     * @param idPersonneInterrogee ID de la personne interrogée
     * @return La personne interrogée
     * @throws DataNotFoundException Si non trouvé
     */
    PersonneInterrogee loadPersonneInterrogee(Integer idPersonneInterrogee) throws DataNotFoundException;

    /**
     * Met à jour une personne interrogée
     *
     * @param personneInterrogee La personne interrogée
     * @throws DataConstraintException En cas de doublons (champs uniques)
     */
    void updatePersonneInterrogee(@Normalize(PersonneInterrogeeNormalizer.class) PersonneInterrogee personneInterrogee) throws DataConstraintException;

    /**
     * Rafraichit une personne interrogée (pour éviter des LazyLoadingException)
     *
     * @param personneInterrogee le dosage
     */
    void refreshPersonneInterrogee(PersonneInterrogee personneInterrogee);

    List<PersonneInterrogee> listPersonneInterrogee(Utilisateur utilisateur);

    List<PersonneInterrogee> listPersonneInterrogeeForCampagne(Utilisateur utilisateur, Campagne campagne);

    SortedSet<PersonneInterrogee> listPersonneInterrogeeForUser(Utilisateur utilisateur);

    /**
     * Détermine si un utilisateur peut modifier ou supprimer une personne interrogée
     *
     * @param personneInterrogee La manipulation
     * @param utilisateur L'utilisateur
     * @return TRUE s'il a le droit
     */
    boolean updateOrdeletePersonneInterrogeeEnabled(PersonneInterrogee personneInterrogee, Utilisateur utilisateur);

    boolean isPersonneInterrogeeAccessibleByUser(PersonneInterrogee personneInterrogee, Utilisateur utilisateur);

}
