/*
 * #%L
 * Cantharella :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.service.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.IngredientRemede;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Molecule;
import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.data.model.Purification;
import nc.ird.cantharella.data.model.Remede;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.ResultatTestBio.TypeResultat;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.TestBio;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.DocumentSearchResult;
import nc.ird.cantharella.service.model.SearchBean;
import nc.ird.cantharella.service.model.SearchResult;
import nc.ird.cantharella.service.services.*;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.search.engine.search.common.BooleanOperator;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.massindexing.MassIndexer;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implémentation du service de recherche.
 * 
 * @author echatellier
 */
@Service
public class SearchServiceImpl implements SearchService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(SearchServiceImpl.class);

    /** DAO */
    @Autowired
    private GenericDao dao;

    /** Hibernate session factory. */
    @Resource
    private SessionFactory sessionFactory;

    /** Lot service for permissions **/
    @Autowired
    private LotService lotService;

    /** Remede service for permissions **/
    @Autowired
    private RemedeService remedeService;

    @Autowired
    private CampagneService campagneService;

    /** Station service for permissions **/
    @Autowired
    private StationService stationService;

    /** Specimen service for permission **/
    @Autowired
    private SpecimenService specimenService;

    /** Specimen service for permission **/
    @Autowired
    private PersonneInterrogeeService personneInterrogeeService;

    /** {@inheritDoc} */
    @Override
    public void reIndex() {
        long before = System.currentTimeMillis();
        if (LOG.isInfoEnabled()) {
            LOG.info("Starting full rebuild on lucene index");
        }
        // get hibernate search session
        Session session = sessionFactory.getCurrentSession();
        SearchSession searchSession = Search.session(session);
        try {
            MassIndexer indexer = searchSession.massIndexer();
            indexer.batchSizeToLoadObjects(200);
            indexer.threadsToLoadObjects(2);
            indexer.idFetchSize(200);
            indexer.startAndWait();

            if (LOG.isInfoEnabled()) {
                long after = System.currentTimeMillis();
                LOG.info("Lucene index rebuilded in " + (after - before) + " ms");
            }
        } catch (InterruptedException ex) {
            throw new UnexpectedException("Can't rebuild index", ex);
        }
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public SearchResult search(SearchBean search, Utilisateur utilisateur) {
        SearchResult result = new SearchResult();

        // get hibernate search session
        Session session = sessionFactory.getCurrentSession();
        SearchSession searchSession = Search.session(session);

        String query = normalizeSearchQuery(search);

        // default init to empty list if query is null or empty
        List<Specimen> specimens = Collections.EMPTY_LIST;
        List<Lot> lots = Collections.EMPTY_LIST;
        List<Extraction> extractions = Collections.EMPTY_LIST;
        List<Purification> purifications = Collections.EMPTY_LIST;
        List<ResultatTestBio> resultatTestBios = Collections.EMPTY_LIST;
        List<Station> resultatStations = Collections.EMPTY_LIST;
        List<Molecule> resultatMolecules = Collections.EMPTY_LIST;
        List<ResultatDosage> resultatDosages = Collections.EMPTY_LIST;
        List<PersonneInterrogee> personnesInterrogees = Collections.EMPTY_LIST;
        List<IngredientRemede> remedes = Collections.EMPTY_LIST;

        if (!(search.getQuery() == null) && !search.getQuery().isEmpty()) {
            specimens = getSpecimen(searchSession,  query, search.getCountry());
            lots = getLots(searchSession, query, search.getCountry());
            extractions = getExtractions(searchSession,  query, search.getCountry());
            purifications = getPurifications(searchSession,  query, search.getCountry());
            resultatTestBios = getResultatsTestBio(searchSession,  query, search.getCountry());
            resultatStations = getStations(searchSession,  query, search.getCountry());
            resultatMolecules = getMolecules(searchSession,  query, search.getCountry());
            resultatDosages = getResultatsDosage(searchSession,  query, search.getCountry());
            personnesInterrogees = getPersonnesInterrogees(searchSession,  query, search.getCountry());
            remedes = getRemedes(searchSession,  query, search.getCountry());
        }

        result.setSpecimens(specimens);
        result.setLots(lots);
        result.setExtractions(extractions);
        result.setPurifications(purifications);
        result.setResultatTestBios(resultatTestBios);
        result.setStations(resultatStations);
        result.setMolecules(resultatMolecules);
        result.setResultatDosages(resultatDosages);
        result.setPersonnesInterrogees(personnesInterrogees);
        result.setRemedes(remedes);

        // security manually managed
        filterResults(result, utilisateur);

        return filterResultsGeography(result, search);
    }

    private String normalizeSearchQuery(SearchBean search) {

        if (null ==  search.getQuery()) {
            return "";
        }

        String[] words = search.getQuery().split(" ");

        List<String> queryParts = new ArrayList<>();

        for (String part:words) {
            if (part.contains("_")) {
                part = "\"" + part + "\"";
            }
            queryParts.add(part);
        }

        return String.join(" ", queryParts)
                .toUpperCase()
                //.replace("_","* ")
                /*.replace("(","")
                .replace(")","")*/
                .replace(" AND ", " +")
                .replace(" OR ", " |")
                .replace(" NOT ", " -");
    }

    public DocumentSearchResult searchDocs(SearchBean search, Utilisateur utilisateur) {

        String query = normalizeSearchQuery(search);

        SearchResult searchResult = search(search, utilisateur);
        DocumentSearchResult documents = new DocumentSearchResult();

        // get hibernate search session (only for campagnes)
        Session session = sessionFactory.getCurrentSession();
        SearchSession searchSession = Search.session(session);

        //Campagne are not searched in usual search
        List<Campagne> campagnes = filterCampagneResult(getCampagnes(searchSession, query, search.getCountry()), utilisateur);

        List<Document> docs = campagnes.stream()
                                       .map(Campagne::getDocuments)
                                       .flatMap(Collection::stream)
                                       .filter(doc ->
                                               search.getDocumentType() == null ||
                                                       (search.getDocumentType() != null &&
                                                               doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                                       .distinct()
                                       .collect(Collectors.toList());
        documents.setCampagneDocs(docs);

        documents.setCampagnes(campagnes.stream().collect(Collectors.toMap(Campagne::getIdCampagne, m -> m)));

        docs = searchResult.getLots().stream()
                                      .map(Lot::getDocuments)
                                      .flatMap(Collection::stream)
                                      .filter(doc ->
                                              search.getDocumentType() == null ||
                                                      (search.getDocumentType() != null &&
                                                      doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                                      .distinct()
                                      .collect(Collectors.toList());
        documents.setLotDocs(docs);

        documents.setLots(searchResult.getLots().stream().collect(Collectors.toMap(Lot::getIdLot, m -> m)));

        docs = searchResult.getStations().stream()
                                         .map(Station::getDocuments)
                                         .flatMap(Collection::stream)
                                         .filter(doc ->
                                                 search.getDocumentType() == null ||
                                                         (search.getDocumentType() != null &&
                                                         doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                                         .distinct()
                                         .collect(Collectors.toList());
        documents.setStationDocs(docs);

        documents.setStations(searchResult.getStations().stream().collect(Collectors.toMap(Station::getIdStation, m -> m)));

        docs = searchResult.getSpecimens().stream()
                                          .map(Specimen::getDocuments)
                                          .flatMap(Collection::stream)
                                          .filter(doc ->
                                                  search.getDocumentType() == null ||
                                                          (search.getDocumentType() != null &&
                                                          doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                                          .distinct()
                                          .collect(Collectors.toList());
        documents.setSpecimenDocs(docs);

        documents.setSpecimens(searchResult.getSpecimens().stream().collect(Collectors.toMap(Specimen::getIdSpecimen, m -> m)));

        docs = searchResult.getExtractions().stream()
                                            .map(Extraction::getDocuments)
                                            .flatMap(Collection::stream)
                                            .filter(doc ->
                                                    search.getDocumentType() == null ||
                                                            (search.getDocumentType() != null &&
                                                            doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                                            .distinct()
                                            .collect(Collectors.toList());
        documents.setExtractionDocs(docs);

        documents.setExtractions(searchResult.getExtractions().stream().collect(Collectors.toMap(Extraction::getIdExtraction, m -> m)));

        docs = searchResult.getPurifications().stream()
                                              .map(Purification::getDocuments)
                                              .flatMap(Collection::stream)
                                              .filter(doc ->
                                                      search.getDocumentType() == null ||
                                                              (search.getDocumentType() != null &&
                                                              doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                                              .distinct()
                                              .collect(Collectors.toList());
        documents.setPurificationDocs(docs);

        documents.setPurifications(searchResult.getPurifications().stream().collect(Collectors.toMap(Purification::getIdPurification, m -> m)));

        docs = searchResult.getMolecules().stream()
                                          .map(Molecule::getDocuments)
                                          .flatMap(Collection::stream)
                                          .filter(doc ->
                                                  search.getDocumentType() == null ||
                                                          (search.getDocumentType() != null &&
                                                          doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                                          .distinct()
                                          .collect(Collectors.toList());
        documents.setMoleculeDocs(docs);

        documents.setMolecules(searchResult.getMolecules().stream().collect(Collectors.toMap(Molecule::getIdMolecule, m -> m)));

        docs = searchResult.getResultatTestBios().stream()
                                                 .map(ResultatTestBio::getDocuments)
                                                 .flatMap(Collection::stream)
                                                 .filter(doc ->
                                                         search.getDocumentType() == null ||
                                                                 (search.getDocumentType() != null &&
                                                                 doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                                                 .distinct()
                                                 .collect(Collectors.toList());
        documents.setResultatTestBioDocs(docs);

        documents.setTestBios(searchResult.getResultatTestBios().stream().map(ResultatTestBio::getTestBio).distinct().collect(Collectors.toMap(TestBio::getIdTestBio, m -> m)));

        docs = searchResult.getResultatDosages().stream()
                .map(ResultatDosage::getDocuments)
                .flatMap(Collection::stream)
                .filter(doc ->
                        search.getDocumentType() == null ||
                                (search.getDocumentType() != null &&
                                        doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                .distinct()
                .collect(Collectors.toList());
        documents.setDosagesDocs(docs);

        documents.setDosages(searchResult.getResultatDosages().stream().map(ResultatDosage::getDosage).distinct().collect(Collectors.toMap(Dosage::getIdDosage, m -> m)));

        docs = searchResult.getPersonnesInterrogees().stream()
                .map(PersonneInterrogee::getDocuments)
                .flatMap(Collection::stream)
                .filter(doc ->
                        search.getDocumentType() == null ||
                                (search.getDocumentType() != null &&
                                        doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                .distinct()
                .collect(Collectors.toList());
        documents.setPersonnesInterrogeesDocs(docs);

        documents.setPersonnesInterrogees(searchResult.getPersonnesInterrogees().stream().distinct().collect(Collectors.toMap(PersonneInterrogee::getIdPersonne, m -> m)));

        docs = searchResult.getRemedes().stream()
                .map(ir -> ir.getRemede().getDocuments())
                .flatMap(Collection::stream)
                .filter(doc ->
                        search.getDocumentType() == null ||
                                (search.getDocumentType() != null &&
                                        doc.getTypeDocument().getIdTypeDocument().equals(search.getDocumentType().getIdTypeDocument())))
                .distinct()
                .collect(Collectors.toList());
        documents.setRemedesDocs(docs);

        documents.setRemedes(searchResult.getRemedes().stream().map(IngredientRemede::getRemede).distinct().collect(Collectors.toMap(Remede::getIdRemede, m -> m)));

        return documents;
    }

    protected List<Specimen> getSpecimen(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "ref",
                "embranchement",
                "espece",
                "famille",
                "genre",
                "station.campagnes.nom",
                "station.localite",
                "station.nom"
        };

        List<Specimen> results = searchSession.search( Specimen.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("station.codePays")
                            .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<Lot> getLots(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "ref",
                "specimenRef.ref",
                "specimenRef.espece",
                "specimenRef.genre",
                "specimenRef.famille",
                "specimenRef.embranchement",
                "station.localite",
                "station.nom",
                "station.campagnes.nom",
                "campagne.nom",
                "partie.nom"};

        List<Lot> results = searchSession.search( Lot.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator(BooleanOperator.AND));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("station.codePays")
                            .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }


    protected List<Campagne> getCampagnes(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "nom"};

        List<Campagne> results = searchSession.search( Campagne.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("codePays")
                            .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<Extraction> getExtractions(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "ref",
                "lot.ref",
                "lot.specimenRef.ref",
                "lot.specimenRef.espece",
                "lot.specimenRef.genre",
                "lot.specimenRef.famille",
                "lot.specimenRef.embranchement",
                "lot.station.localite",
                "lot.station.nom",
                "lot.station.campagnes.nom",
                "lot.campagne.nom",
                "lot.partie.nom",
                "methode.nom"};

        List<Extraction> results = searchSession.search( Extraction.class )
               .where( f -> f.bool( b -> {
                   b.must(f.simpleQueryString().fields(fields)
                           .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                   if(StringUtils.isNotEmpty(country)) {
                       b.must(f.simpleQueryString().fields("lot.station.codePays")
                           .matching(country));
                   }
               } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<Purification> getPurifications(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "ref",
                "produit.produitLotRef",
                "produit.produitLotSpecimenEmbranchement",
                "produit.produitLotSpecimenFamille",
                "produit.produitLotSpecimenEspece",
                "produit.produitLotSpecimenGenre",
                "produit.produitLotCampagneNom",
                "produit.ref",
                "lotSource.ref",
                "lotSource.specimenRef.ref",
                "lotSource.specimenRef.espece",
                "lotSource.specimenRef.genre",
                "lotSource.specimenRef.famille",
                "lotSource.specimenRef.embranchement",
                "lotSource.station.localite",
                "lotSource.station.nom",
                "lotSource.station.campagnes.nom",
                "lotSource.campagne.nom",
                "lotSource.partie.nom",
                "methode.nom"
        };

        List<Purification> results = searchSession.search( Purification.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("lotSource.station.codePays", "produit.produitLotCampagneCodePays")
                            .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<ResultatTestBio> getResultatsTestBio(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "produit.produitLotRef",
                "produit.produitLotSpecimenEmbranchement",
                "produit.produitLotSpecimenFamille",
                "produit.produitLotSpecimenEspece",
                "produit.produitLotSpecimenGenre",
                "produit.produitLotCampagneNom",
                "produit.ref",
                "testBio.ref",
                "testBio.methode.nom",
                "testBio.methode.cible"};

        List<ResultatTestBio> results = searchSession.search( ResultatTestBio.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("produit.produitLotCampagneCodePays")
                            .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<ResultatDosage> getResultatsDosage(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "produit.produitLotRef",
                "produit.produitLotSpecimenEmbranchement",
                "produit.produitLotSpecimenFamille",
                "produit.produitLotSpecimenEspece",
                "produit.produitLotSpecimenGenre",
                "produit.produitLotCampagneNom",
                "produit.ref",
                "lot.ref",
                "lot.specimenRef.ref",
                "lot.specimenRef.espece",
                "lot.specimenRef.genre",
                "lot.specimenRef.famille",
                "lot.specimenRef.embranchement",
                "lot.station.localite",
                "lot.station.nom",
                "lot.station.campagnes.nom",
                "lot.campagne.nom",
                "lot.partie.nom",
                "moleculeDosee.nomIupca",
                "moleculeDosee.nomCommun",
                "moleculeDosee.identifieePar",
                "moleculeDosee.formuleBrute",
                "moleculeDosee.familleChimique",
                "composeDose",
                "dosage.ref",
                "dosage.methode.nom"};

        List<ResultatDosage> results = searchSession.search( ResultatDosage.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("lot.station.codePays", "produit.produitLotCampagneCodePays")
                                .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<PersonneInterrogee> getPersonnesInterrogees(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "campagne.nom",
                "station.nom",
                "station.localite",
                "nom",
                "prenom",
                "ville"};

        List<PersonneInterrogee> results = searchSession.search( PersonneInterrogee.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("station.codePays", "campagne.codePays", "codePays")
                                .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<IngredientRemede> getRemedes(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "remede.campagne.nom",
                "remede.station.nom",
                "remede.station.localite",
                "remede.enqueteur.nom",
                "remede.enqueteur.prenom",
                "remede.prescripteur.nom",
                "remede.prescripteur.prenom",
                "remede.prescripteur.ville",
                "remede.nom",
                "remede.ref",
                "remede.classification.code",
                "remede.classification.valeur",
                "specimen.ref",
                "specimen.espece",
                "specimen.genre",
                "specimen.famille",
                "specimen.embranchement",
                "nomVernaculaire",
                "provenance.nom",
                "provenance.localite",
        };

        List<IngredientRemede> results = searchSession.search( IngredientRemede.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("remede.station.codePays", "provenance.codePays", "remede.campagne.codePays")
                                .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<Station> getStations(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "campagnes.nom",
                "localite",
                "nom"
        };

        List<Station> results = searchSession.search( Station.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("codePays")
                            .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    protected List<Molecule> getMolecules(SearchSession searchSession, String strQuery, String country) {
        String[] fields = {
                "nomIupca",
                "nomCommun",
                "identifieePar",
                "formuleBrute",
                "familleChimique",
                "campagne.nom",
                "provenances.produit.produitLotRef",
                "provenances.produit.produitLotSpecimenEmbranchement",
                "provenances.produit.produitLotSpecimenFamille",
                "provenances.produit.produitLotSpecimenEspece",
                "provenances.produit.produitLotSpecimenGenre",
                "provenances.produit.produitLotCampagneNom",
                "provenances.produit.ref"
        };

        //Must replace "-" by "?" for wildcard search
        String query = strQuery.replace("-","?");

        List<Molecule> results = searchSession.search( Molecule.class )
                .where( f -> f.bool( b -> {
                    b.must(f.simpleQueryString().fields(fields)
                            .matching(strQuery).defaultOperator( BooleanOperator.AND ));
                    if(StringUtils.isNotEmpty(country)) {
                        b.must(f.simpleQueryString().fields("provenances.produit.produitLotCampagneCodePays")
                            .matching(country));
                    }
                } ) ).fetchAllHits();

        return new ArrayList<>(results);
    }

    /**
     * Iterate over all results and remove those that user is not allowed to see.
     * 
     * @see Utilisateur#getLotsDroits()
     * @see Utilisateur#getCampagnesDroits()
     */
    private void filterResults(SearchResult result, Utilisateur utilisateur) {
        // SearchResult#lots
        result.getLots().removeIf(lot -> !lotService.isLotAccessibleByUser(lot, utilisateur));

        // SearchResult#extractions
        result.getExtractions().removeIf(extraction -> !lotService.isLotAccessibleByUser(extraction.getLot(), utilisateur));

        // SearchResult#purifications
        result.getPurifications().removeIf(purification -> !lotService.isLotAccessibleByUser(purification.getLotSource(), utilisateur));

        // SearchResult#resultatTestBios
        // le lot peut être null pour les tests temoin
        result.getResultatTestBios().removeIf(resultatTestBio -> resultatTestBio.getTypeResultat() != TypeResultat.PRODUIT
                || !lotService.isLotAccessibleByUser(resultatTestBio.getLotSource(), utilisateur));


        // SearchResult#resultatDosages
        result.getResultatDosages().removeIf(resultatDosage -> !lotService.isLotAccessibleByUser(resultatDosage.getLotSource(), utilisateur));

        // SearchResult#stations
        List<Station> userStations = stationService.listStations(utilisateur);
        result.getStations().retainAll(userStations);

        // SearchResult#specimens
        List<Specimen> userSpecimens = specimenService.listSpecimens(utilisateur);
        result.getSpecimens().retainAll(userSpecimens);

        // SearchResult#molecules : all visible, no filtering

        // SearchResult#remedes
        result.getRemedes().removeIf(ingredient -> !remedeService.isRemedeAccessibleByUser(ingredient.getRemede(), utilisateur));

        // SearchResult#personnesInterrogees
        result.getPersonnesInterrogees().removeIf(personne -> !personneInterrogeeService.isPersonneInterrogeeAccessibleByUser(personne, utilisateur));
    }

    private List<Campagne> filterCampagneResult(List<Campagne> result, Utilisateur utilisateur) {
        List<Campagne> campagnesForUser = campagneService.listCampagnes(utilisateur);
        result.retainAll(campagnesForUser);
        return result;
    }

    /**
     * Iterate over all results and remove those that user is not allowed to see.
     *
     * @see Utilisateur#getLotsDroits()
     * @see Utilisateur#getCampagnesDroits()
     */
    private SearchResult filterResultsGeography(SearchResult result, SearchBean search) {

        if (!StringUtils.isBlank(search.getNElatitude())
                && !StringUtils.isBlank(search.getNElongitude())
                && !StringUtils.isBlank(search.getSOlatitude())
                && !StringUtils.isBlank(search.getSOlongitude())
        ) {

            String neLat = search.getNElatitude();
            String neLng = search.getNElongitude();
            String soLat = search.getSOlatitude();
            String soLng = search.getSOlongitude();


            // SearchResult#lots
            result.getLots().removeIf(lot -> !stationService.isStationInBoundingBox(lot.getStation(), neLat, neLng, soLat, soLng));

            // SearchResult#extractions
            result.getExtractions().removeIf(extraction -> !stationService.isStationInBoundingBox(extraction.getLot().getStation(), neLat, neLng, soLat, soLng));

            // SearchResult#purifications
            result.getPurifications().removeIf(purification -> !stationService.isStationInBoundingBox(purification.getLotSource().getStation(), neLat, neLng, soLat, soLng));

            // SearchResult#resultatTestBios
            // le lot peut être null pour les tests temoin
            result.getResultatTestBios().removeIf(resultatTestBio -> resultatTestBio.getTypeResultat() != TypeResultat.PRODUIT
                    || !stationService.isStationInBoundingBox(resultatTestBio.getLotSource().getStation(), neLat, neLng, soLat, soLng));

            // SearchResult#resultatDosages
            // le lot peut être null pour les tests temoin
            result.getResultatDosages().removeIf(resultatDosage -> !stationService.isStationInBoundingBox(resultatDosage.getLotSource().getStation(), neLat, neLng, soLat, soLng));

            // SearchResult#stations
            result.getStations().removeIf(station -> !stationService.isStationInBoundingBox(station, neLat, neLng, soLat, soLng));

            // SearchResult#specimens
            result.getSpecimens().removeIf(specimen -> !stationService.isStationInBoundingBox(specimen.getStation(), neLat, neLng, soLat, soLng));

            // SearchResult#molecules : all visible, no filtering

            // SearchResult#remedes
            result.getRemedes().removeIf(ingredient -> !stationService.isStationInBoundingBox(ingredient.getRemede().getStation(), neLat, neLng, soLat, soLng));

            // SearchResult#personnesInterrogees
            result.getPersonnesInterrogees().removeIf(personne -> !stationService.isStationInBoundingBox(personne.getStation(), neLat, neLng, soLat, soLng));
        }

        return result;
    }
}
