package nc.ird.cantharella.service.services.impl;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.Configuration;
import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.service.services.ConfigurationService;
import nc.ird.cantharella.utils.AssertTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ConfigurationServiceImpl implements ConfigurationService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationServiceImpl.class);

    /** DAO */
    @Autowired
    private GenericDao dao;

    /** {@inheritDoc} */
    @Override
    public List<UniteConcMasse> listUnites() {
        return dao.readList(UniteConcMasse.class, "valeur");
    }

    @Override
    public void createUniteConcMasse(UniteConcMasse methode) throws DataConstraintException {
        LOG.info("createUniteConcMasse: " + methode.getValeur());
        dao.create(methode);
    }

    @Override
    public void deleteUniteConcMasse(UniteConcMasse unite) throws DataConstraintException {
        AssertTools.assertNotNull(unite);
        LOG.info("deleteUniteConcMasse: " + unite.getValeur());
        try {
            dao.delete(unite);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }

    }

    @Override
    public UniteConcMasse loadUniteConcMasse(Integer idUnite) throws DataNotFoundException {
        return dao.read(UniteConcMasse.class, idUnite);
    }

    @Override
    public void updateUniteConcMasse(UniteConcMasse uniteConcMasse) throws DataConstraintException {
        LOG.info("updateUniteConcMasse: " + uniteConcMasse.getValeur());
        try {
            dao.update(uniteConcMasse);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    @Override
    public Configuration loadConfiguration() throws DataNotFoundException {
        return dao.read(Configuration.class, 1);
    }

    @Override
    public void updateConfiguration(Configuration configuration) throws DataConstraintException {
        LOG.info("updateConfiguration");
        try {
            dao.update(configuration);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }


}
