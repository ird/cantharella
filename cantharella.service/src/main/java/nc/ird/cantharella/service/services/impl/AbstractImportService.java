package nc.ird.cantharella.service.services.impl;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.services.ImportService;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportConf;
import org.nuiton.csv.ImportModel;
import org.nuiton.csv.ImportRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.Locale;

public abstract class AbstractImportService implements ImportService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractImportService.class);

    /** Messages d'internationalisation */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    private <T> Import2<T> importFile(InputStream input, ImportModel<T> model) {

        ImportConf importConf = new ImportConf();
        importConf.setStrictMode(false);

        Import2<T> resultatsImports = Import2.newImport(importConf, model, input);

        resultatsImports.prepareAndValidate();

        return resultatsImports;
    }

    public abstract <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException;

    public <T> ImportResult importData(InputStream input, Utilisateur utilisateur, Locale locale, ImportModel<T> importModel) throws DataConstraintException {

        ImportResult importResult = new ImportResult();

        Import2<T> importData;
        try {
            importData = importFile(input, importModel);
        } catch (ImportRuntimeException e) {
            String message = messages.getMessage("csv.import.fileerror", locale);
            importResult.addErrorLine(message);
            return  importResult;
        }

        return importLines(importData, utilisateur, locale);

    }
}
