package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.MethodeExtractionDao;
import nc.ird.cantharella.data.dao.impl.PersonneDao;
import nc.ird.cantharella.data.dao.impl.ProduitDao;
import nc.ird.cantharella.data.dao.impl.TypeExtraitDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.Extrait;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.MethodeExtraction;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.TypeExtrait;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ExtractionImport;
import nc.ird.cantharella.service.model.ExtractionImportModel;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.services.ExtractionService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.service.utils.normalizers.ExtractionNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Service
@Qualifier("importExtractionService")
public class ImportExtractionServiceImpl extends AbstractImportService  {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ImportExtractionServiceImpl.class);
    protected final String DATE_FORMAT = "d/MM/uuuu";

    /** Messages d'internationalisation */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /** DAO */
    @Autowired
    private GenericDao dao;

    /** Service : lots **/
    @Autowired
    private LotService lotService;

    /** Service : extraction **/
    @Autowired
    private ExtractionService extractionService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        ImportResult importResult = new ImportResult();

        Map<String, Extraction> extractionParRef = new HashMap<>();
        List<String> existingExtractionRef = new ArrayList<>();

        // Chargement de certaines données en amont
        List<MethodeExtraction> methodeExtractions = (List<MethodeExtraction>) dao.list(MethodeExtractionDao.getCriteriaFindAllMethodeExtraction());
        Map<String, MethodeExtraction> methodeExtractionByNom = Maps.uniqueIndex(methodeExtractions, MethodeExtraction::getNom);

        // Liste des extraits sans filtre sur les droits car il ne doit pas y avoir 2 extraits avec la même ref
        List<Produit> produits = (List<Produit>) dao.list(ProduitDao.getCriteriaFindAllProduits());
        Map<String, Produit> produitByRef = Maps.uniqueIndex(produits, Produit::getRef);

        List<TypeExtrait> typesExtrait = (List<TypeExtrait>) dao.list(TypeExtraitDao.getCriteriaFindAllTypeExtrait());
        //Map<String, TypeExtrait> typeExtraitByNom = Maps.uniqueIndex(typesExtrait, TypeExtrait::getInitiales);

        int currentLine = 0;
        for (ImportRow<T> extractionImportRow : importData) {
            currentLine++;

            boolean valid = extractionImportRow.isValid();

            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = extractionImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("Masse à extraire") || headerName.equalsIgnoreCase("Masse extrait")) {
                        String message = messages.getMessage("csv.import.numeric.invalid", new Object[] {currentLine, headerName}, locale);
                        importResult.addErrorLine(message);
                    }
                    if (headerName.equalsIgnoreCase("Date")) {
                        String message = messages.getMessage("csv.import.date.invalid", new Object[] {currentLine}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }


            ExtractionImport extractionImport = (ExtractionImport) extractionImportRow.getBean();
            String refLot = extractionImport.getRefLot();
            String refExtraction = extractionImport.getRefExtraction();

            // Mandatory fields/relation
            if (refLot == null || refLot.isBlank()) {
                String message = messages.getMessage("csv.import.lot.empty", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            }

            if (refExtraction == null || refExtraction.isBlank()) {
                String message = messages.getMessage("csv.import.extraction.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else if (!extractionParRef.containsKey(refExtraction) && existingExtractionRef.contains(refExtraction)) {
                // Already loaded
                String message = messages.getMessage("csv.import.extraction.existing", new Object[]{currentLine, refExtraction}, locale);
                importResult.addErrorLine(message);
            } else {
                // Load
                boolean exists = dao.exists(Extraction.class, "ref", refExtraction);
                if (exists) {
                    String message = messages.getMessage("csv.import.extraction.existing", new Object[]{currentLine, refExtraction}, locale);
                    importResult.addErrorLine(message);
                    existingExtractionRef.add(refExtraction);
                }
            }

            // Parse date extraction
            Date parsedDate = null;
            try {
                LocalDate date = LocalDate.parse(extractionImport.getDate(), DateTimeFormatter.ofPattern(DATE_FORMAT).withResolverStyle(ResolverStyle.STRICT));
                parsedDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                if (date.isAfter(LocalDate.now())) {
                    String message = messages.getMessage("csv.import.date.future", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } catch (DateTimeParseException e) {
                String message = messages.getMessage("csv.import.date.invalid", new Object[] {currentLine, extractionImport.getDate()}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve methode extraction
            String methodeExtraction = extractionImport.getMethodeExtraction();
            MethodeExtraction methode = null;
            if (StringUtils.isNotBlank(methodeExtraction)) {
                methode = methodeExtractionByNom.get(methodeExtraction);
                if (methode == null) {
                    String message = messages.getMessage("csv.import.methode.unknown", new Object[] {currentLine, methodeExtraction}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Retrieve lot
            Lot lot = null;
            if (StringUtils.isNotBlank(refLot)) {
                try {
                    lot = lotService.loadLot(refLot);
                } catch (DataNotFoundException e) {
                    String message = messages.getMessage("csv.import.lot.unknown", new Object[] {currentLine, refLot}, locale);
                    importResult.addErrorLine(message);
                }

                if (!lotService.isLotAccessibleByUser(lot, utilisateur)) {
                    String message = messages.getMessage("csv.import.lot.unknown", new Object[] {currentLine, refLot}, locale);
                    importResult.addErrorLine(message);
                }

            }

            // Need to find the personne
            String nomManipulateur = extractionImport.getNomManipulateur();
            String prenomManipulateur = extractionImport.getPrenomManipulateur();
            Map<String, Object> manipulateurParameters = ImmutableMap.of("nom", nomManipulateur, "prenom", prenomManipulateur);
            List<Personne> personnes = (List<Personne>) dao.list(PersonneDao.HQL_FIND_PERSONNE_PAR_NOM_PRENOM, manipulateurParameters);
            Personne technician = null;
            if (personnes.isEmpty()) {
                String message = messages.getMessage("csv.import.technician.unknown", new Object[] {currentLine, nomManipulateur, prenomManipulateur}, locale);
                importResult.addErrorLine(message);
            } else {
                technician = personnes.get(0);
            }

            Extraction extraction = extractionParRef.get(refExtraction);
            if (extraction == null) {
                extraction = new Extraction();
                extraction.setLot(lot);
                extraction.setMasseDepart(extractionImport.getMasseAextraire());
                extraction.setComplement(extractionImport.getComplement());
                extraction.setMethode(methode);
                extraction.setRef(refExtraction);
                extraction.setManipulateur(technician);
                extraction.setCreateur(utilisateur);
                extraction.setDate(parsedDate);
                extraction.setExtraits(new ArrayList<>());
            } else {
                // Be sure name are same
                if (extraction.getRef() == null) {
                    extraction.setRef(extractionImport.getRefExtraction());
                } else if (!extraction.getRef().equals(extractionImport.getRefExtraction())) {
                    String message = messages.getMessage("csv.import.extraction.ref.different", new Object[] {currentLine, extractionImport.getRefExtraction()}, locale);
                    importResult.addErrorLine(message);
                }

                // Be sure Masse à extraire is the same
                if (extraction.getMasseDepart() == null) {
                    extraction.setMasseDepart(extractionImport.getMasseAextraire());
                } else if (!extraction.getMasseDepart().equals(extractionImport.getMasseAextraire())) {
                    String message = messages.getMessage("csv.import.extraction.masseDepart.different", new Object[] {currentLine, extractionImport.getRefExtraction()}, locale);
                    importResult.addErrorLine(message);
                }
            }

            Extrait extrait = new Extrait();
            String refExtrait = extractionImport.getRefExtrait();
            if(refExtrait != null) {
                // Make sure the extract is not already added
                boolean alreadyLoaded = extraction.getExtraits().stream().anyMatch(extrait1 -> extrait1.getRef().equals(refExtrait));
                if (alreadyLoaded) {
                    String message = messages.getMessage("csv.import.extrait.existing", new Object[]{currentLine, refExtrait}, locale);
                    importResult.addErrorLine(message);
                } else if (produitByRef.containsKey(refExtrait.toUpperCase())){
                    String message = messages.getMessage("csv.import.extrait.existing", new Object[]{currentLine, refExtrait}, locale);
                    importResult.addErrorLine(message);
                } else {
                    extrait.setRef(extractionImport.getRefExtrait());

                    String typeExtraitInitials = extractionImport.getTypeExtrait();
                    TypeExtrait typeExtrait = null;
                    if (StringUtils.isNotBlank(typeExtraitInitials)) {
                        typeExtrait = typesExtrait.stream().filter(typeExtrait1 -> typeExtrait1.getInitiales().equals(typeExtraitInitials)).findFirst().orElse(null);
                        if (typeExtrait == null) {
                            String message = messages.getMessage("csv.import.typeExtrait.unknown", new Object[]{currentLine, typeExtraitInitials}, locale);
                            importResult.addErrorLine(message);
                        }
                    }
                    if (typeExtrait != null) {
                        // Check that an extract with the same type does not already exists
                        TypeExtrait finalTypeExtrait = typeExtrait;
                        boolean sameType = extraction.getExtraits().stream().anyMatch(extrait1 -> extrait1.getTypeExtrait().equals(finalTypeExtrait));
                        if (sameType) {
                            String message = messages.getMessage("csv.import.extrait.type.existing", new Object[]{currentLine, typeExtrait, refExtraction}, locale);
                            importResult.addErrorLine(message);
                        } else {
                            extrait.setTypeExtrait(typeExtrait);
                        }
                    }

                    extrait.setMasseObtenue(extractionImport.getMasseExtrait());
                    extrait.setExtraction(extraction);

                    extraction.getExtraits().add(extrait);
                }
            }

            Normalizer.normalize(ExtractionNormalizer.class, extraction);
            extractionParRef.put(extractionImport.getRefExtraction(), extraction);
        }


        if (importResult.isValid()) {
            int nbImported = 0;
            for (Extraction extraction : extractionParRef.values()) {
                extractionService.createExtraction(extraction);
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        ExtractionImportModel model = new ExtractionImportModel();

        File headers = File.createTempFile("extractions", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
