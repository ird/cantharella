package nc.ird.cantharella.service.model;

public class IngredientRemedeImport {

    String campagne;

    String station;

    String prenomPrescripteur;

    String nomPrescripteur;

    String nomRemede;

    String codeLangue;

    String referentiel;

    String referentielCode;

    String indicationBiomedicale;

    String indicationVernaculaire;

    Integer nbIngredients;

    String ingredientRefSpecimen;

    String ingredientNomVernaculaire;

    String partie;

    String provenance;

    String periode;

    String disponibilite;

    String preparation;

    String conservation;

    String administration;

    String posologie;

    Boolean recommandations;

    String recommandationsText;

    String effetsIndesirables;

    Integer nbPatients;

    String complement;

    String reference;

    String prenomEnqueteur;

    String nomEnqueteur;

    String date;

    public String getCampagne() {
        return campagne;
    }

    public void setCampagne(String campagne) {
        this.campagne = campagne;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getPrenomPrescripteur() {
        return prenomPrescripteur;
    }

    public void setPrenomPrescripteur(String prenomPrescripteur) {
        this.prenomPrescripteur = prenomPrescripteur;
    }

    public String getNomPrescripteur() {
        return nomPrescripteur;
    }

    public void setNomPrescripteur(String nomPrescripteur) {
        this.nomPrescripteur = nomPrescripteur;
    }

    public String getNomRemede() {
        return nomRemede;
    }

    public void setNomRemede(String nomRemede) {
        this.nomRemede = nomRemede;
    }

    public String getCodeLangue() {
        return codeLangue;
    }

    public void setCodeLangue(String codeLangue) {
        this.codeLangue = codeLangue;
    }

    public String getReferentiel() {
        return referentiel;
    }

    public void setReferentiel(String referentiel) {
        this.referentiel = referentiel;
    }

    public String getReferentielCode() {
        return referentielCode;
    }

    public void setReferentielCode(String referentielCode) {
        this.referentielCode = referentielCode;
    }

    public String getIndicationBiomedicale() {
        return indicationBiomedicale;
    }

    public void setIndicationBiomedicale(String indicationBiomedicale) {
        this.indicationBiomedicale = indicationBiomedicale;
    }

    public String getIndicationVernaculaire() {
        return indicationVernaculaire;
    }

    public void setIndicationVernaculaire(String indicationVernaculaire) {
        this.indicationVernaculaire = indicationVernaculaire;
    }

    public Integer getNbIngredients() {
        return nbIngredients;
    }

    public void setNbIngredients(Integer nbIngredients) {
        this.nbIngredients = nbIngredients;
    }

    public String getIngredientRefSpecimen() {
        return ingredientRefSpecimen;
    }

    public void setIngredientRefSpecimen(String ingredientRefSpecimen) {
        this.ingredientRefSpecimen = ingredientRefSpecimen;
    }

    public String getIngredientNomVernaculaire() {
        return ingredientNomVernaculaire;
    }

    public void setIngredientNomVernaculaire(String ingredientNomVernaculaire) {
        this.ingredientNomVernaculaire = ingredientNomVernaculaire;
    }

    public String getPartie() {
        return partie;
    }

    public void setPartie(String partie) {
        this.partie = partie;
    }

    public String getProvenance() {
        return provenance;
    }

    public void setProvenance(String provenance) {
        this.provenance = provenance;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(String disponibilite) {
        this.disponibilite = disponibilite;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public String getConservation() {
        return conservation;
    }

    public void setConservation(String conservation) {
        this.conservation = conservation;
    }

    public String getAdministration() {
        return administration;
    }

    public void setAdministration(String administration) {
        this.administration = administration;
    }

    public String getPosologie() {
        return posologie;
    }

    public void setPosologie(String posologie) {
        this.posologie = posologie;
    }

    public Boolean getRecommandations() {
        return recommandations;
    }

    public void setRecommandations(Boolean recommandations) {
        this.recommandations = recommandations;
    }

    public String getRecommandationsText() {
        return recommandationsText;
    }

    public void setRecommandationsText(String recommandationsText) {
        this.recommandationsText = recommandationsText;
    }

    public String getEffetsIndesirables() {
        return effetsIndesirables;
    }

    public void setEffetsIndesirables(String effetsIndesirables) {
        this.effetsIndesirables = effetsIndesirables;
    }

    public Integer getNbPatients() {
        return nbPatients;
    }

    public void setNbPatients(Integer nbPatients) {
        this.nbPatients = nbPatients;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getPrenomEnqueteur() {
        return prenomEnqueteur;
    }

    public void setPrenomEnqueteur(String prenomEnqueteur) {
        this.prenomEnqueteur = prenomEnqueteur;
    }

    public String getNomEnqueteur() {
        return nomEnqueteur;
    }

    public void setNomEnqueteur(String nomEnqueteur) {
        this.nomEnqueteur = nomEnqueteur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
