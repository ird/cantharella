package nc.ird.cantharella.service.services;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.*;
import nc.ird.cantharella.service.utils.normalizers.DisponibiliteNormalizer;
import nc.ird.cantharella.service.utils.normalizers.RemedeNormalizer;
import nc.ird.cantharella.service.utils.normalizers.UniqueFieldNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalize;

import java.util.List;
import java.util.SortedSet;

public interface RemedeService {

    /**
     * Créée un remède
     *
     * @param remede Le remède
     * @throws DataConstraintException Si le remède existe déjà
     */
    void createRemede(@Normalize(RemedeNormalizer.class) Remede remede) throws DataConstraintException;

    /**
     * Supprime un remède
     *
     * @param remede Le remède
     * @throws DataConstraintException En cas de données liées
     */
    void deleteRemede(Remede remede) throws DataConstraintException;

    /**
     * Charge un remède
     *
     * @param idRemede ID du remède
     * @return Le remède
     * @throws DataNotFoundException Si non trouvé
     */
    Remede loadRemede(Integer idRemede) throws DataNotFoundException;

    /**
     * Charge un remède
     *
     * @param idRemede ID du remède
     * @return Le remède
     * @throws DataNotFoundException Si non trouvé
     */
    Remede loadRemede(@Normalize(UniqueFieldNormalizer.class) String idRemede) throws DataNotFoundException;

    /**
     * Met à jour un remède
     *
     * @param remede Le remède
     * @throws DataConstraintException En cas de doublons (champs uniques)
     */
    void updateRemede(@Normalize(RemedeNormalizer.class) Remede remede) throws DataConstraintException;

    boolean updateOrdeleteRemedeEnabled(Remede remede, Utilisateur utilisateur);

    /**
     * Rafraichit un remède (pour éviter des LazyLoadingException)
     *
     * @param remede le remède
     */
    void refreshRemede(Remede remede);

    List<Remede> listRemede(Utilisateur utilisateur);

    SortedSet<Remede> listRemedeForUser(Utilisateur utilisateur);

    List<IngredientRemede> listIngredients(Utilisateur utilisateur);

    List<IngredientRemede> listIngredientsForUser(Utilisateur utilisateur);

    List<String> listNomRemede();

    List<String> listNomVernaculaireIngredients();

    /**
     * Créée une disponibilite
     *
     * @param erreurTest L'erreur
     * @throws DataConstraintException Si l'erreur (nom) existe déjà
     */
    void createDisponibilite(@Normalize(DisponibiliteNormalizer.class) Disponibilite erreurTest)
            throws DataConstraintException;

    /**
     * Supprime une disponibilite
     *
     * @param erreurTest L'erreur
     * @throws DataConstraintException En cas de données liées
     */
    void deleteDisponibilite(Disponibilite erreurTest) throws DataConstraintException;

    /**
     * Liste les disponibilités existantes
     *
     * @return la liste des disponibilites
     */
    List<Disponibilite> listDisponibilites();

    /**
     * Charge une erreur pour un test biologique
     *
     * @param idErreurTest ID de l'erreur
     * @return L'erreur
     * @throws DataNotFoundException Si non trouvée
     */
    Disponibilite loadDisponibilite(Integer idErreurTest) throws DataNotFoundException;
    /**
     * Met à jour une erreur pour un test biologique
     *
     * @param erreurTest L'erreur
     * @throws DataConstraintException En cas de doublons (champs uniques)
     */
    void updateDisponibilite(@Normalize(DisponibiliteNormalizer.class) Disponibilite erreurTest)
            throws DataConstraintException;

    List<EntreeReferentiel> listEntreeReferentiels(EntreeReferentiel.Referentiel referentiel);

    boolean canDeleteEntreeReferentiel(EntreeReferentiel entree);

    void deleteEntreeReferentiel(EntreeReferentiel entree) throws DataConstraintException;

    long countRemedes();

    long countPersonnesInterrogees();

    long countPrescripteurOccurence(IngredientRemede ingredient);

    long countCodeLangueOccurence(IngredientRemede ingredient);

    long countDateOccurence(IngredientRemede ingredient);

    long countNomRemedeOccurence(IngredientRemede ingredient);

    long countIndicationsVernaculairesOccurence(IngredientRemede ingredient);

    long countSpecimenRefOccurence(IngredientRemede ingredient);

    long countSpecimenFamilleOccurence(IngredientRemede ingredient);

    long countSpecimenGenreOccurence(IngredientRemede ingredient);

    long countSpecimenEspeceOccurence(IngredientRemede ingredient);

    long countNomVernaculaireOccurence(IngredientRemede ingredient);

    long countCodePaysOccurence(IngredientRemede ingredient);

    long countCampagneOccurence(IngredientRemede ingredient);

    /**
     * Détermine si un utilisateur peut accéder à un remède
     *
     * @param remede Remede
     * @param utilisateur Utilisateur
     * @return TRUE si il a le droit
     */
    boolean isRemedeAccessibleByUser(Remede remede, Utilisateur utilisateur);
    boolean isRemedePersonneInterrogeeAccessibleByUser(Remede remede, PersonneInterrogee personneInterrogee, Utilisateur utilisateur);
}
