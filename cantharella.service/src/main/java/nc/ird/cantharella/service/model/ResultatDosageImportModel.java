package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

/**
 * @author jcouteau (couteau@codelutin.com)
 */
public class ResultatDosageImportModel extends AbstractImportExportModel<ResultatDosageImport> {

    public static final char CSV_SEPARATOR = ';';

    public ResultatDosageImportModel() {
        super(CSV_SEPARATOR);

        // "Cible";
        newMandatoryColumn("Méthode", "methode");
        // "Conc";
        newMandatoryColumn("Conc", "concMasse", ImportParser.BIG_DECIMAL_PARSER);
        // "Unité";
        newMandatoryColumn("Unité", "uniteConcMasse");
        // "Réf. produit";
        newMandatoryColumn("Échantillon", "echantillon");
        // "Prod. témoin";
        newMandatoryColumn("Composé dosé", "composeDose");
        // "Prod. témoin";
        newMandatoryColumn("Molécule dosée", "moleculeDosee");
        // "Valeur";
        newMandatoryColumn("Valeur", "valeur", ImportParser.BIG_DECIMAL_PARSER);
        // "SD";
        newMandatoryColumn("SD", "SD", ImportParser.BIG_DECIMAL_PARSER);
        // "Actif";
        newMandatoryColumn("Sup. Seuil", "supSeuil", ImportParser.BOOLEAN_PARSER);
        // "Erreur"
        newMandatoryColumn("Erreur", "nomErreur");
        // "Repère";
        newMandatoryColumn("Nb. analyse", "analyseNb", ImportParser.INTEGER_PARSER);
        // "Réf. test"
        newMandatoryColumn("Réf. dosage", "refDosage");
        // "Organisme testeur
        newMandatoryColumn("Organisme", "organisme");
        // "Nom manipulateur"
        newMandatoryColumn("Nom manipulateur", "nomManipulateur");
        // "Prénom manipulateur"
        newMandatoryColumn("Prénom manipulateur", "prenomManipulateur");
        // "Date"
        newMandatoryColumn("Date", "date");
        // "Complément"
        newOptionalColumn("Complément", "complement");

        // Column for export

        // "Cible";
        newColumnForExport("Méthode", "methode");
        // "Conc";
        newColumnForExport("Conc", "concMasse");
        // "Unité";
        newColumnForExport("Unité", "uniteConcMasse");
        // "Réf. produit";
        newColumnForExport("Échantillon", "echantillon");
        // "Réf. produit";
        newColumnForExport("Composé dosé", "composeDose");
        // "Réf. produit";
        newColumnForExport("Molécule dosée", "moleculeDosee");
        // "Valeur";
        newColumnForExport("Valeur", "valeur");
        // "SD";
        newColumnForExport("SD", "SD");
        // "Actif";
        newColumnForExport("Sup. Seuil", "supSeuil");
        // "Erreur"
        newColumnForExport("Erreur", "nomErreur");
        // "Repère";
        newColumnForExport("Nb. analyse", "analyseNb");
        // "Réf. test"
        newColumnForExport("Réf. dosage", "refDosage");
        // "Organisme testeur
        newColumnForExport("Organisme", "organisme");
        // "Nom manipulateur"
        newColumnForExport("Nom manipulateur", "nomManipulateur");
        // "Prénom manipulateur"
        newColumnForExport("Prénom manipulateur", "prenomManipulateur");
        // "Date"
        newColumnForExport("Date", "date");
        // "Complément"
        newColumnForExport("Complément", "complement");
    }

    @Override
    public ResultatDosageImport newEmptyInstance() {
        return new ResultatDosageImport();
    }

}
