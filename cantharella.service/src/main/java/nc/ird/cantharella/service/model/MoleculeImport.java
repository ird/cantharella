package nc.ird.cantharella.service.model;

import java.math.BigDecimal;

public class MoleculeImport {

    protected Integer idMolecule;

    protected String nomCommun;

    protected String familleChimique;

    protected String formuleDevMol;

    protected String nomIupca;

    protected String formuleBrute;

    protected BigDecimal masseMolaire;

    protected Boolean nouvMolecul;

    protected String campagne;

    protected String identifieePar;

    protected String publiOrigine;

    protected String complement;

    protected String produit;

    protected BigDecimal pourcentage;

    public Integer getIdMolecule() {
        return idMolecule;
    }

    public void setIdMolecule(Integer idMolecule) {
        this.idMolecule = idMolecule;
    }

    public String getNomCommun() {
        return nomCommun;
    }

    public void setNomCommun(String nomCommun) {
        this.nomCommun = nomCommun;
    }

    public String getFamilleChimique() {
        return familleChimique;
    }

    public void setFamilleChimique(String familleChimique) {
        this.familleChimique = familleChimique;
    }

    public String getFormuleDevMol() {
        return formuleDevMol;
    }

    public void setFormuleDevMol(String formuleDevMol) {
        this.formuleDevMol = formuleDevMol;
    }

    public String getNomIupca() {
        return nomIupca;
    }

    public void setNomIupca(String nomIupca) {
        this.nomIupca = nomIupca;
    }

    public String getFormuleBrute() {
        return formuleBrute;
    }

    public void setFormuleBrute(String formuleBrute) {
        this.formuleBrute = formuleBrute;
    }

    public BigDecimal getMasseMolaire() {
        return masseMolaire;
    }

    public void setMasseMolaire(BigDecimal masseMolaire) {
        this.masseMolaire = masseMolaire;
    }

    public Boolean getNouvMolecul() {
        return nouvMolecul;
    }

    public void setNouvMolecul(Boolean nouvMolecul) {
        this.nouvMolecul = nouvMolecul;
    }

    public String getCampagne() {
        return campagne;
    }

    public void setCampagne(String campagne) {
        this.campagne = campagne;
    }

    public String getIdentifieePar() {
        return identifieePar;
    }

    public void setIdentifieePar(String identifieePar) {
        this.identifieePar = identifieePar;
    }

    public String getPubliOrigine() {
        return publiOrigine;
    }

    public void setPubliOrigine(String publiOrigine) {
        this.publiOrigine = publiOrigine;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public BigDecimal getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(BigDecimal pourcentage) {
        this.pourcentage = pourcentage;
    }
}
