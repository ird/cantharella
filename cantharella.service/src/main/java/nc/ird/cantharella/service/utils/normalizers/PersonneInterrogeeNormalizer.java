package nc.ird.cantharella.service.utils.normalizers;

import nc.ird.cantharella.data.model.PersonneInterrogee;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.AssertTools;

public class PersonneInterrogeeNormalizer  extends Normalizer<PersonneInterrogee> {

    /** {@inheritDoc} */
    @Override
    protected PersonneInterrogee normalize(PersonneInterrogee personneInterrogee) {
        AssertTools.assertNotNull(personneInterrogee);

        return personneInterrogee;
    }

}
