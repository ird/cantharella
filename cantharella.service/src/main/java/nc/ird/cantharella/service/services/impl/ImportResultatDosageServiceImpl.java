package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.PersonneDao;
import nc.ird.cantharella.data.dao.impl.UniteConcMasseDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.ErreurDosage;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.MethodeDosage;
import nc.ird.cantharella.data.model.Molecule;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.ResultatDosageImport;
import nc.ird.cantharella.service.model.ResultatDosageImportModel;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.service.services.MoleculeService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.ProduitService;
import nc.ird.cantharella.service.utils.normalizers.DosageNormalizer;
import nc.ird.cantharella.service.utils.normalizers.ErreurDosageNormalizer;
import nc.ird.cantharella.service.utils.normalizers.MoleculeNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author jcouteau (couteau@codelutin.com)
 */
@Service
@Qualifier("importResultatDosageService")
public class ImportResultatDosageServiceImpl extends AbstractImportService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ImportResultatDosageServiceImpl.class);
    protected final String DATE_FORMAT = "d/MM/uuuu";

    /** Messages d'internationalisation */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /** DAO */
    @Autowired
    private GenericDao dao;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private ProduitService produitService;

    @Autowired
    private LotService lotService;

    @Autowired
    private DosageService dosageService;

    @Autowired
    private MoleculeService moleculeService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

        // Chargement de certaines données en amont
        List<Produit> produits = produitService.listProduits(loadedUtilisateur);
        Map<String, Produit> produitByRef = Maps.uniqueIndex(produits, Produit::getRef);

        List<Lot> lots = lotService.listLots(loadedUtilisateur);
        Map<String, Lot> lotByRef = Maps.uniqueIndex(lots, Lot::getRef);

        List<Molecule> molecules = moleculeService.listMolecules();
        Map<String, Molecule> moleculesByNomCommun = new HashMap<>();
        for (Molecule mol: molecules) {
            if (mol.getNomCommun() == null) {
                continue;
            }
            if (!moleculesByNomCommun.containsKey(mol.getNomCommun())) {
                moleculesByNomCommun.put(mol.getNomCommun(), mol);
            } else {
                Molecule oldMol = moleculesByNomCommun.get(mol.getNomCommun());
                if (mol.getIdMolecule()< oldMol.getIdMolecule()) {
                    moleculesByNomCommun.replace(mol.getNomCommun(), oldMol, mol);
                }
            }
        }

        List<MethodeDosage> methodeDosages = dosageService.listMethodesDosage();
        Map<String, MethodeDosage> methodeDosageByAcronyme = Maps.uniqueIndex(methodeDosages, MethodeDosage::getAcronyme);

        List<ErreurDosage> erreurDosages = dosageService.listErreursDosage();
        Map<String, ErreurDosage> erreurDosageByName = Maps.uniqueIndex(erreurDosages, ErreurDosage::getNom);


        Map<String, Dosage> dosagesParRef = new HashMap<>();
        List<String> existingDosagesRefs = new ArrayList<>();

        int currentLine = 0;
        for (ImportRow<T> resultatDosageImportRow : importData) {
            currentLine++;

            boolean valid = resultatDosageImportRow.isValid();
            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = resultatDosageImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("Sup. Seuil")) {
                        String message = messages.getMessage("csv.import.boolean.invalid", new Object[] {currentLine, headerName, Lists.newArrayList("oui", "non", "o", "n")}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("conc") || headerName.equalsIgnoreCase("valeur") || headerName.equalsIgnoreCase("SD")) {
                        String message = messages.getMessage("csv.import.numeric.invalid", new Object[] {currentLine, headerName}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("unité")) {
                        String message = messages.getMessage("csv.import.conc.unity.invalid", new Object[] {currentLine, Lists.newArrayList("μg", "ng", "mg/ml", "μg/ml", "ng/ml")}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }
            ResultatDosageImport resultatDosageImport = (ResultatDosageImport) resultatDosageImportRow.getBean();
            String refDosage = resultatDosageImport.getRefDosage();

            // Mandatory fields/relation
            if (refDosage == null || refDosage.isBlank()) {
                String message = messages.getMessage("csv.import.refdosage.empty", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);

            } else if (!dosagesParRef.containsKey(refDosage) && existingDosagesRefs.contains(refDosage)) {
                // Already loaded
                String message = messages.getMessage("csv.import.refdosage.existing", new Object[] {currentLine, refDosage}, locale);
                importResult.addErrorLine(message);

            } else {
                // Load
                boolean exists = false;
                try {
                    exists = dosageService.loadDosage(refDosage) != null;
                } catch (DataNotFoundException dce) {
                    //do nothing exists already false
                }
                if (exists) {
                    String message = messages.getMessage("csv.import.refdosage.existing", new Object[] {currentLine, refDosage}, locale);
                    importResult.addErrorLine(message);
                    existingDosagesRefs.add(refDosage);
                }
            }

            // Check date of each line
            Date parsedDate = null;
            try {
                LocalDate date = LocalDate.parse(resultatDosageImport.getDate(), DateTimeFormatter.ofPattern(DATE_FORMAT).withResolverStyle(ResolverStyle.STRICT));
                parsedDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                if (date.isAfter(LocalDate.now())) {
                    String message = messages.getMessage("csv.import.date.future", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } catch (DateTimeParseException e) {
                String message = messages.getMessage("csv.import.date.invalid", new Object[] {currentLine, resultatDosageImport.getDate()}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve methode
            String methodeAcronyme = resultatDosageImport.getMethode();
            MethodeDosage methode = null;
            if (StringUtils.isBlank(methodeAcronyme)) {
                String message = messages.getMessage("csv.import.methoddosage.empty", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                methode = methodeDosageByAcronyme.get(methodeAcronyme);
                if (methode == null) {
                    String message = messages.getMessage("csv.import.methoddosage.unknown", new Object[] {currentLine, methodeAcronyme}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Need to find the personne
            String nomManipulateur = resultatDosageImport.getNomManipulateur();
            String prenomManipulateur = resultatDosageImport.getPrenomManipulateur();
            Map<String, Object> manipulateurParameters = ImmutableMap.of("nom", nomManipulateur, "prenom", prenomManipulateur);
            List<Personne> personnes = (List<Personne>) dao.list(PersonneDao.HQL_FIND_PERSONNE_PAR_NOM_PRENOM, manipulateurParameters);
            Personne technician = null;
            if (personnes.isEmpty()) {
                String message = messages.getMessage("csv.import.technician.unknown", new Object[] {currentLine, nomManipulateur, prenomManipulateur}, locale);
                importResult.addErrorLine(message);
            } else {
                technician = personnes.get(0);
            }

            Dosage dosage = dosagesParRef.get(refDosage);
            if (dosage == null) {
                dosage = new Dosage();
                dosage.setRef(refDosage);
                dosage.setCreateur(loadedUtilisateur);
                dosage.setOrganisme(resultatDosageImport.getOrganisme());

                dosage.setMethode(methode);

                dosage.setManipulateur(technician);
                dosage.setDate(parsedDate);

            } else {
                // Be sure dosage data are same
                if (dosage.getDate() == null) {
                    dosage.setDate(parsedDate);
                } else if (!dosage.getDate().equals(parsedDate)) {
                        String message = messages.getMessage("csv.import.date.different", new Object[] {currentLine, refDosage}, locale);
                        importResult.addErrorLine(message);
                }

                if (dosage.getManipulateur() == null) {
                    dosage.setManipulateur(technician);
                } else if (!dosage.getManipulateur().equals(technician)) {
                    String message = messages.getMessage("csv.import.technician.different", new Object[] {currentLine, refDosage}, locale);
                    importResult.addErrorLine(message);
                }

                if (dosage.getMethode() == null) {
                    dosage.setMethode(methode);
                } else if (!dosage.getMethode().equals(methode)) {
                    String message = messages.getMessage("csv.import.method.different", new Object[] {currentLine, refDosage, dosage.getMethode().getAcronyme()}, locale);
                    importResult.addErrorLine(message);
                }

            }

            ResultatDosage resultatDosage = new ResultatDosage();
            Integer analyse = resultatDosageImport.getAnalyseNb();
            if (analyse == null) {
                String message = messages.getMessage("csv.import.analyseNb.empty", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            }
            try {
                resultatDosage.setAnalyseNb(analyse);
            } catch (NumberFormatException e) {
                String message = messages.getMessage("csv.import.analyseNb.invalid", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            }
            resultatDosage.setConcMasse(resultatDosageImport.getConcMasse());

            // Need to find the unit
            String unite = resultatDosageImport.getUniteConcMasse();
            Map<String, Object> uniteParameters = ImmutableMap.of("valeur", unite );
            List<UniteConcMasse> unites = (List<UniteConcMasse>) dao.list(UniteConcMasseDao.HQL_FIND_UNITE_PAR_VALEUR, uniteParameters);
            UniteConcMasse uniteConcMasse = null;
            if (unites.isEmpty()) {
                String message = messages.getMessage("csv.import.unite.unknown", new Object[] {currentLine, unite}, locale);
                importResult.addErrorLine(message);
            } else {
                uniteConcMasse = unites.get(0);
            }
            resultatDosage.setUniteConcMasse(uniteConcMasse);


            //Echantillon
            Produit produit = produitByRef.get(resultatDosageImport.getEchantillon().toUpperCase());
            Lot lot = lotByRef.get(resultatDosageImport.getEchantillon().toUpperCase());
            if (produit == null && lot == null) {
                String message = messages.getMessage("csv.import.echantillon.unknown", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                //check 1 échantillon = 1 analyseNb
                boolean echantillonMismatch = dosage.getResultats().stream().
                        anyMatch(resultat ->
                                resultat.getAnalyseNb() != null &&
                                        StringUtils.isNotBlank(resultat.getEchantillon()) &&
                                        Objects.equals(resultat.getAnalyseNb(), analyse) &&
                                        !Objects.equals(resultat.getEchantillon(), resultatDosageImport.getEchantillon()));
                boolean analyseMismatch = dosage.getResultats().stream().
                        anyMatch(resultat ->
                                resultat.getAnalyseNb() != null &&
                                        StringUtils.isNotBlank(resultat.getEchantillon()) &&
                                        !Objects.equals(resultat.getAnalyseNb(), analyse) &&
                                        Objects.equals(resultat.getEchantillon(), resultatDosageImport.getEchantillon()));
                if (echantillonMismatch) {
                    String message = messages.getMessage("csv.import.echantillon.mismatch", new Object[] {currentLine, analyse}, locale);
                    importResult.addErrorLine(message);
                } else if (analyseMismatch) {
                    String message = messages.getMessage("csv.import.analyse.mismatch", new Object[] {currentLine, resultatDosageImport.getEchantillon()}, locale);
                    importResult.addErrorLine(message);
                } else {
                    resultatDosage.setProduit(produit);
                    resultatDosage.setLot(lot);
                }
            }

            //molecule & compose
            Molecule molecule = moleculesByNomCommun.get(resultatDosageImport.getMoleculeDosee());
            String compose = resultatDosageImport.getComposeDose();

            if (StringUtils.isBlank(resultatDosageImport.getMoleculeDosee()) && StringUtils.isBlank(compose)) {
                    String message = messages.getMessage("csv.import.molecule.compose.missing", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);

            } else if (molecule == null && StringUtils.isBlank(compose)) {
                    String message = messages.getMessage("csv.import.molecule.unknown", new Object[] {currentLine, resultatDosageImport.getMoleculeDosee()}, locale);
                    importResult.addErrorLine(message);
            } else if (molecule != null){
                if (StringUtils.isNotBlank(compose)) {
                    String message = messages.getMessage("csv.import.molecule.compose.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                } else {
                    boolean analyseMoleculeExist = dosage.getResultats().stream().
                        anyMatch(resultat ->
                                Objects.equals(resultat.getAnalyseNb(), analyse) &&
                                        resultat.getMoleculeDosee() != null &&
                                        Objects.equals(resultat.getMoleculeDosee().getNomCommun(), resultatDosageImport.getMoleculeDosee()));
                    if (analyseMoleculeExist) {
                        String message = messages.getMessage("csv.import.molecule.exist", new Object[] {currentLine}, locale);
                        importResult.addErrorLine(message);
                    } else {
                        Normalizer.normalize(MoleculeNormalizer.class, molecule);
                        resultatDosage.setMoleculeDosee(molecule);
                    }
                }
            } else {
                    boolean analyseComposeExist = dosage.getResultats().stream().
                        anyMatch(resultat ->
                                Objects.equals(resultat.getAnalyseNb(), analyse) &&
                                        resultat.getComposeDose() != null  &&
                                        Objects.equals(resultat.getComposeDose(), resultatDosageImport.getComposeDose()));
                    if (analyseComposeExist) {
                        String message = messages.getMessage("csv.import.compose.exist", new Object[] {currentLine}, locale);
                        importResult.addErrorLine(message);
                    } else {
                        resultatDosage.setComposeDose(compose);
                    }
            }

            String nomErreur = resultatDosageImport.getNomErreur();
            if (StringUtils.isNotBlank(nomErreur)) {
                ErreurDosage erreur = erreurDosageByName.get(nomErreur);
                if (erreur == null) {
                    String message = messages.getMessage("csv.import.error.unknown", new Object[] {currentLine, nomErreur}, locale);
                    importResult.addErrorLine(message);
                } else {
                    Normalizer.normalize(ErreurDosageNormalizer.class, erreur);
                    resultatDosage.setErreur(erreur);
                }
                if (resultatDosageImport.getValeur() != null) {
                    String message = messages.getMessage("csv.import.error.value.conflict", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } else {
                if (resultatDosageImport.getValeur() == null) {
                    String message = messages.getMessage("csv.import.error.value.empty", new Object[] {currentLine}, locale);
                    importResult.addErrorLine(message);
                } else {
                    resultatDosage.setValeur(resultatDosageImport.getValeur());
                }
            }
            resultatDosage.setSD(resultatDosageImport.getSD());
            resultatDosage.setSupSeuil(resultatDosageImport.getSupSeuil());

            dosage.getResultats().add(resultatDosage);
            resultatDosage.setDosage(dosage);

            if (dosage.getComplement() != null && !dosage.getComplement().isEmpty() && !dosage.getComplement().equals(resultatDosageImport.getComplement())) {
                String message = messages.getMessage("csv.import.error.complement.conflict", new Object[] {currentLine, refDosage}, locale);
                importResult.addErrorLine(message);
            }
            dosage.setComplement(resultatDosageImport.getComplement());

            Normalizer.normalize(DosageNormalizer.class, dosage);
            dosagesParRef.put(refDosage, dosage);
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (Dosage dosage : dosagesParRef.values()) {
                try {
                    dao.create(dosage);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        ResultatDosageImportModel model = new ResultatDosageImportModel();

        File headers = File.createTempFile("resultatDosages", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
