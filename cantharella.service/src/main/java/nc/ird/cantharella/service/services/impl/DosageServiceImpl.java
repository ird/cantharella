package nc.ird.cantharella.service.services.impl;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.DosageDao;
import nc.ird.cantharella.data.dao.impl.TestBioDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.*;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.utils.AssertTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

@Service
public class DosageServiceImpl implements DosageService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(DosageServiceImpl.class);

    /** DAO */
    @Autowired
    private GenericDao dao;

    /** Service : lots */
    @Autowired
    private LotService lotService;


    /** {@inheritDoc} */
    @Override
    public void createDosage(Dosage dosage) throws DataConstraintException {
        LOG.info("createDosage: " + dosage.getRef());
        dao.create(dosage);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteDosage(Dosage dosage) throws DataConstraintException {
        AssertTools.assertNotNull(dosage);
        LOG.info("deleteDosage: " + dosage.getRef());
        try {
            dao.delete(dosage);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }

    }

    /** {@inheritDoc} */
    @Override
    public List<ResultatDosage> listResultatsDosage(Utilisateur utilisateur) {
        AssertTools.assertNotNull(utilisateur);
        if (utilisateur.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR) {
            // si admin, on ajoute tous les dosages de la base
            return dao.readList(ResultatDosage.class, "analyseNb");
        }
        // gestion des droits en plus pour les utilisateurs
        return new ArrayList<>(listResultatsDosageForUser(utilisateur));
    }

    /** {@inheritDoc} */
    @Override
    public SortedSet<ResultatDosage> listResultatsDosageForUser(Utilisateur utilisateur) {
        SortedSet<ResultatDosage> resultats = new TreeSet<>();

        // liste triée par produit afin d'optimiser
        List<ResultatDosage> allResultTests = dao.readList(ResultatDosage.class, "analyseNb");
        for (ResultatDosage curRes : allResultTests) {
            if (lotService.isLotAccessibleByUser(curRes.getLotSource(), utilisateur)) {
                resultats.add(curRes);
            }
        }

        return resultats;
    }



    /** {@inheritDoc} */
    @Override
    public boolean isResultatDosageAccessibleByUser(ResultatDosage resultatDosage, Utilisateur utilisateur) {
        return lotService.isLotAccessibleByUser(resultatDosage.getLotSource(), utilisateur);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isResultatDosageUniqueInList(final ResultatDosage resultatDosage, final List<ResultatDosage> list) {
        // as resultatDosage is already in list, detect if more of one elements satisfy these conditions
        int count = 0;
        for (ResultatDosage curRes : list) {
            if (resultatDosage.getAnalyseNb().equals(curRes.getAnalyseNb())) {
                if (resultatDosage.getComposeDose().equals(curRes.getComposeDose())) {
                    count++;
                } else if (resultatDosage.getMoleculeDosee().equals(curRes.getMoleculeDosee())) {
                    count++;
                }
            }
        }
        return count <= 1;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDosageUnique(Dosage dosage) {
        AssertTools.assertNotNull(dosage);

        // unique if it doesn't exist different value or if it exists but with the same id (so same row in the db)
        if (!dao.exists(Dosage.class, "ref", dosage.getRef())) {
            return true;
        }

        Dosage puriWithSameVal;
        try {
            puriWithSameVal = dao.read(Dosage.class, "ref", dosage.getRef());
            dao.evict(puriWithSameVal);
        } catch (DataNotFoundException e) {
            return true; // never call, cover by dao.exists...
        }
        // in case of new record, id is null
        return dosage.getIdDosage() != null && dosage.getIdDosage().equals(puriWithSameVal.getIdDosage());
    }

    /** {@inheritDoc} */
    @Override
    public Dosage loadDosage(Integer idDosage) throws DataNotFoundException {
        return dao.read(Dosage.class, idDosage);
    }

    /** {@inheritDoc} */
    @Override
    public Dosage loadDosage(String nom) throws DataNotFoundException {
        return dao.read(Dosage.class, "ref", nom);
    }

    /** {@inheritDoc} */
    @Override
    public void updateDosage(Dosage dosage) throws DataConstraintException {
        LOG.info("updateDosage: " + dosage.getRef());
        try {
            dao.update(dosage);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void refreshDosage(Dosage dosage) {
        dao.refresh(dosage);
    }

    /** {@inheritDoc} */
    @Override
    public boolean updateOrdeleteDosageEnabled(Dosage dosage, Utilisateur utilisateur) {

        List<ResultatDosage> resultats = dosage.getResultats();

        return utilisateur.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR
                || Objects.equals(utilisateur.getIdPersonne(), dosage.getCreateur().getIdPersonne())
                || resultats.stream()
                        .map(ResultatDosage::getLotSource)
                        .filter(Objects::nonNull)
                        .anyMatch(l -> l.getCampagne().getAdministrateur().getIdPersonne().equals(utilisateur.getIdPersonne()))
                || resultats.stream()
                        .map(ResultatDosage::getLotSource)
                        .filter(Objects::nonNull)
                        .anyMatch(l -> l.getCampagne().getCreateur().getIdPersonne().equals(utilisateur.getIdPersonne()));
    }

    /** {@inheritDoc} */
    @Override
    public void createMethodeDosage(MethodeDosage methode) throws DataConstraintException {
        LOG.info("createMethodeDosage: " + methode.getNom());
        dao.create(methode);

    }

    /** {@inheritDoc} */
    @Override
    public void deleteMethodeDosage(MethodeDosage methode) throws DataConstraintException {
        AssertTools.assertNotNull(methode);
        LOG.info("deleteMethodeDosage: " + methode.getNom());
        try {
            dao.delete(methode);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }

    }

    /** {@inheritDoc} */
    @Override
    public List<MethodeDosage> listMethodesDosage() {
        return dao.readList(MethodeDosage.class, "nom");
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> listDomainesMethodes() {
        return (List<String>) dao.list(DosageDao.CRITERIA_DISTINCT_DOMAINES_METHODES);
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> listUnitesResultatMethodes() {
        return (List<String>) dao.list(DosageDao.CRITERIA_DISTINCT_UNITES_METHODES);
    }

    /** {@inheritDoc} */
    @Override
    public MethodeDosage loadMethodeDosage(Integer idMethode) throws DataNotFoundException {
        return dao.read(MethodeDosage.class, idMethode);
    }

    /** {@inheritDoc} */
    @Override
    public MethodeDosage loadMethodeDosage(String nom) throws DataNotFoundException {
        return dao.read(MethodeDosage.class, "nom", nom);
    }

    /** {@inheritDoc} */
    @Override
    public void updateMethodeDosage(MethodeDosage methode) throws DataConstraintException {
        LOG.info("updateMethodeTest: " + methode.getNom());
        try {
            dao.update(methode);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }

    }

    /** {@inheritDoc} */
    @Override
    public void refreshMethodeDosage(MethodeDosage methode) {
        AssertTools.assertNotNull(methode);
        dao.refresh(methode);
    }

    /** {@inheritDoc} */
    @Override
    public void createErreurDosage(ErreurDosage erreurDosage) throws DataConstraintException {
        LOG.info("createErreurDosage: " + erreurDosage.getNom());
        dao.create(erreurDosage);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteErreurDosage(ErreurDosage erreurDosage) throws DataConstraintException {
        AssertTools.assertNotNull(erreurDosage);
        LOG.info("deleteErreurDosage: " + erreurDosage.getNom());
        try {
            dao.delete(erreurDosage);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    /** {@inheritDoc} */
    @Override
    public List<ErreurDosage> listErreursDosage() {
        return dao.readList(ErreurDosage.class, "nom");
    }

    /** {@inheritDoc} */
    @Override
    public ErreurDosage loadErreurDosage(Integer idErreurDosage) throws DataNotFoundException {
        return dao.read(ErreurDosage.class, idErreurDosage);
    }

    /** {@inheritDoc} */
    @Override
    public ErreurDosage loadErreurDosage(String nom) throws DataNotFoundException {
        return dao.read(ErreurDosage.class, "nom", nom);
    }

    /** {@inheritDoc} */
    @Override
    public void updateErreurDosage(ErreurDosage erreurDosage) throws DataConstraintException {
        LOG.info("updateErreurDosage: " + erreurDosage.getNom());
        try {
            dao.update(erreurDosage);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    @Override
    public long countResultatsDosage() {
        return dao.count(ResultatDosage.class);
    }
}
