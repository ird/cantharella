package nc.ird.cantharella.service.utils.normalizers;

import nc.ird.cantharella.data.model.Remede;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.AssertTools;

public class RemedeNormalizer extends Normalizer<Remede> {

    /** {@inheritDoc} */
    @Override
    protected Remede normalize(Remede remede) {
        AssertTools.assertNotNull(remede);
        // Unique fields
        remede.setRef(normalize(UniqueFieldNormalizer.class, remede.getRef()));
        return remede;
    }
}
