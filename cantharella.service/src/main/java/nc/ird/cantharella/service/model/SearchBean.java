/*
 * #%L
 * Cantharella :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.service.model;

import nc.ird.cantharella.data.model.TypeDocument;

import java.io.Serializable;

/**
 * Search bean.
 * 
 * @author Eric Chatellier
 */
public class SearchBean implements Serializable {

    /** Search query. */
    protected String query;

    /** Search country. */
    protected String country;

    protected String NElatitude;

    protected String NElongitude;

    protected String SOlatitude;

    protected String SOlongitude;

    protected TypeDocument documentType;

    protected int searchTab = 0;

    /**
     * Constructor.
     */
    public SearchBean() {

    }

    /**
     * Constructor.
     * 
     * @param query query
     */
    public SearchBean(String query) {
        this();
        this.query = query;
    }

    /**
     * Constructor.
     * 
     * @param query query
     */
    public SearchBean(String query, TypeDocument typeDocument) {
        this();
        this.query = query;
        this.documentType = typeDocument;
    }

    /**
     * Search query getter.
     * 
     * @return query
     */
    public String getQuery() {
        return query;
    }

    /**
     * Search query setter.
     * 
     * @param query query
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * Search country getter.
     * 
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Search country setter.
     * 
     * @param country country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    public String getNElatitude() {
        return NElatitude;
    }

    public void setNElatitude(String NElatitude) {
        this.NElatitude = NElatitude;
    }

    public String getNElongitude() {
        return NElongitude;
    }

    public void setNElongitude(String NElongitude) {
        this.NElongitude = NElongitude;
    }

    public String getSOlatitude() {
        return SOlatitude;
    }

    public void setSOlatitude(String SOlatitude) {
        this.SOlatitude = SOlatitude;
    }

    public String getSOlongitude() {
        return SOlongitude;
    }

    public void setSOlongitude(String SOlongitude) {
        this.SOlongitude = SOlongitude;
    }

    public TypeDocument getDocumentType() {
        return documentType;
    }

    public void setDocumentType(TypeDocument documentType) {
        this.documentType = documentType;
    }

    public int getSearchTab() {
        return searchTab;
    }

    public void setSearchTab(int searchTab) {
        this.searchTab = searchTab;
    }
}
