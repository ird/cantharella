package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.MethodePurificationDao;
import nc.ird.cantharella.data.dao.impl.PersonneDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Fraction;
import nc.ird.cantharella.data.model.MethodePurification;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.Purification;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.PurificationImport;
import nc.ird.cantharella.service.model.PurificationImportModel;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.ProduitService;
import nc.ird.cantharella.service.utils.normalizers.PurificationNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Service
@Qualifier("importPurificationService")
public class ImportPurificationServiceImpl extends AbstractImportService {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImportPurificationServiceImpl.class);
    protected final String DATE_FORMAT = "d/MM/uuuu";

    /**
     * Messages d'internationalisation
     */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /**
     * DAO
     */
    @Autowired
    private GenericDao dao;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private ProduitService produitService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

        // Chargement de certaines données en amont
        List<Produit> produits = produitService.listProduits(loadedUtilisateur);
        Map<String, Produit> produitByRef = Maps.uniqueIndex(produits, Produit::getRef);

        List<MethodePurification> methodesPurification = (List<MethodePurification>) dao.list(MethodePurificationDao.getCriteriaFindAllMethodePurification());
        Map<String, MethodePurification> methodePurificationByNom = Maps.uniqueIndex(methodesPurification, MethodePurification::getNom);

        Map<String, Purification> purificationParRef = new HashMap<>();
        List<String> existingPurificationRef = new ArrayList<>();

        int currentLine = 0;
        for (ImportRow<T> purificationImportRow : importData) {
            currentLine++;

            boolean valid = purificationImportRow.isValid();

            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = purificationImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("Masse à purifier") || headerName.equalsIgnoreCase("Masse fraction")) {
                        String message = messages.getMessage("csv.import.numeric.invalid", new Object[]{currentLine, headerName}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }

            PurificationImport purificationImport = (PurificationImport) purificationImportRow.getBean();
            String refPurification = purificationImport.getReferencePurification();

            // Mandatory fields/relation
            if (refPurification == null || refPurification.isBlank()) {
                String message = messages.getMessage("csv.import.purification.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);

            } else if (!purificationParRef.containsKey(refPurification) && existingPurificationRef.contains(refPurification)) {
                // Already loaded
                String message = messages.getMessage("csv.import.purification.existing", new Object[]{currentLine, refPurification}, locale);
                importResult.addErrorLine(message);

            } else {
                // Load
                boolean exists = dao.exists(Purification.class, "ref", refPurification);
                if (exists) {
                    String message = messages.getMessage("csv.import.purification.existing", new Object[]{currentLine, refPurification}, locale);
                    importResult.addErrorLine(message);
                    existingPurificationRef.add(refPurification);
                }
            }

            // Check date of each line
            Date parsedDate = null;
            try {
                LocalDate date = LocalDate.parse(purificationImport.getDate(), DateTimeFormatter.ofPattern(DATE_FORMAT).withResolverStyle(ResolverStyle.STRICT));
                parsedDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                if (date.isAfter(LocalDate.now())) {
                    String message = messages.getMessage("csv.import.date.future", new Object[]{currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } catch (DateTimeParseException e) {
                String message = messages.getMessage("csv.import.date.invalid", new Object[]{currentLine, purificationImport.getDate()}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve methode
            String methodeCible = purificationImport.getMethode();
            MethodePurification methode = null;
            if (!StringUtils.isBlank(methodeCible)) {
                methode = methodePurificationByNom.get(methodeCible);
                if (methode == null) {
                    String message = messages.getMessage("csv.import.method.unknown", new Object[]{currentLine, methodeCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Retrieve produit
            String refProduit = purificationImport.getReferenceProduit();
            Produit produit = produitByRef.get(refProduit);
            if (produit == null) {
                String message = messages.getMessage("csv.import.product.unknown", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            // Need to find the personne
            String nomManipulateur = purificationImport.getNomManipulateur();
            String prenomManipulateur = purificationImport.getPrenomManipulateur();
            Map<String, Object> manipulateurParameters = ImmutableMap.of("nom", nomManipulateur, "prenom", prenomManipulateur);
            List<Personne> personnes = (List<Personne>) dao.list(PersonneDao.HQL_FIND_PERSONNE_PAR_NOM_PRENOM, manipulateurParameters);
            Personne technician = null;
            if (personnes.isEmpty()) {
                String message = messages.getMessage("csv.import.technician.unknown", new Object[]{currentLine, nomManipulateur, prenomManipulateur}, locale);
                importResult.addErrorLine(message);
            } else {
                technician = personnes.get(0);
            }


            Purification purification = purificationParRef.get(refPurification);
            if (purification == null) {
                purification = new Purification();
                purification.setRef(refPurification);
                purification.setCreateur(loadedUtilisateur);
                if (produit != null) {
                    purification.setProduit(produit);
                }
                purification.setMasseDepart(purificationImport.getMasse());
                purification.setComplement(purificationImport.getComplement());
                purification.setMethode(methode);
                purification.setDate(parsedDate);
                purification.setManipulateur(technician);

            } else {
                // Be sure purification data are same
                if (purification.getDate() == null) {
                    purification.setDate(parsedDate);
                } else if (!purification.getDate().equals(parsedDate)) {
                    String message = messages.getMessage("csv.import.date.different", new Object[]{currentLine, refPurification}, locale);
                    importResult.addErrorLine(message);
                }

                if (purification.getManipulateur() == null) {
                    purification.setManipulateur(technician);
                } else if (!purification.getManipulateur().equals(technician)) {
                    String message = messages.getMessage("csv.import.technician.different", new Object[]{currentLine, refPurification}, locale);
                    importResult.addErrorLine(message);
                }

                if (purification.getMethode() == null) {
                    purification.setMethode(methode);
                } else if (!purification.getMethode().equals(methode)) {
                    String message = messages.getMessage("csv.import.method.different", new Object[]{currentLine, refPurification, purification.getMethode().getNom()}, locale);
                    importResult.addErrorLine(message);
                }

                if (produit != null) {
                    if (purification.getProduit() == null) {
                        purification.setProduit(produit);
                    } else if (!purification.getProduit().equals(produit)) {
                        String message = messages.getMessage("csv.import.product.different", new Object[]{currentLine, refPurification, purification.getProduit().getRef()}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }

            Fraction fraction = new Fraction();

            boolean exists = dao.exists(Fraction.class, "ref", purificationImport.getReferenceFraction());
            if (exists) {
                String message = messages.getMessage("csv.import.refFraction.existing", new Object[]{currentLine, purificationImport.getReferenceFraction()}, locale);
                importResult.addErrorLine(message);
            } else {
                fraction.setRef(purificationImport.getReferenceFraction());
            }

            String numeroFraction = purificationImport.getIndiceFraction();
            if (StringUtils.isBlank(numeroFraction)) {
                String message = messages.getMessage("csv.import.numeroFraction.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                // On ne peut pas avoir deux fois le même numéro de fraction pour une purification
                boolean hasFraction = purification.getFractions().stream().anyMatch(existingFraction -> existingFraction.getIndice().equalsIgnoreCase(numeroFraction));
                if (hasFraction) {
                    String message = messages.getMessage("csv.import.numeroFraction.existing", new Object[]{currentLine, numeroFraction}, locale);
                    importResult.addErrorLine(message);
                }
            }
            fraction.setIndice(numeroFraction);
            fraction.setMasseObtenue(purificationImport.getMasseFraction());

            purification.getFractions().add(fraction);

            Normalizer.normalize(PurificationNormalizer.class, purification);

            fraction.setPurification(purification);

            purificationParRef.put(refPurification, purification);

        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (Purification purification : purificationParRef.values()) {
                try {
                    dao.create(purification);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        PurificationImportModel model = new PurificationImportModel();

        File headers = File.createTempFile("purifications", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
