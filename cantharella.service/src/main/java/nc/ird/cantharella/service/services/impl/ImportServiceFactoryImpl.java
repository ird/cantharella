package nc.ird.cantharella.service.services.impl;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ExtractionImportModel;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.IngredientRemedeImportModel;
import nc.ird.cantharella.service.model.LotImportModel;
import nc.ird.cantharella.service.model.MoleculeImportModel;
import nc.ird.cantharella.service.model.PersonneImportModel;
import nc.ird.cantharella.service.model.PersonneInterrogeeImportModel;
import nc.ird.cantharella.service.model.PurificationImportModel;
import nc.ird.cantharella.service.model.ReferentielImportModel;
import nc.ird.cantharella.service.model.ResultatDosageImportModel;
import nc.ird.cantharella.service.model.ResultatTestBioImportModel;
import nc.ird.cantharella.service.model.SpecimenImportModel;
import nc.ird.cantharella.service.model.StationImportModel;
import nc.ird.cantharella.service.services.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.InputStream;
import java.util.Locale;

@Service
public class ImportServiceFactoryImpl implements nc.ird.cantharella.service.services.ImportServiceFactory {

    /** Service : imports */
    @Autowired
    @Qualifier("importPersonneService")
    private ImportService importPersonneService;

    @Autowired
    @Qualifier("importPurificationService")
    private ImportService importPurificationService;

    @Autowired
    @Qualifier("importResultatTestBioService")
    private ImportService importResultatTestBioService;

    @Autowired
    @Qualifier("importResultatDosageService")
    private ImportService importResultatDosageService;

    @Autowired
    @Qualifier("importSpecimenService")
    private ImportService importSpecimenService;

    @Autowired
    @Qualifier("importStationService")
    private ImportService importStationService;

    @Autowired
    @Qualifier("importLotService")
    private ImportService importLotService;

    @Autowired
    @Qualifier("importExtractionService")
    private ImportService importExtractionService;

    @Autowired
    @Qualifier("ImportMoleculeService")
    private ImportService importMoleculeService;

    @Autowired
    @Qualifier("importPersonneInterrogeeService")
    private ImportService importPersonneInterrogeeService;

    @Autowired
    @Qualifier("importReferentielService")
    private ImportService importReferentielService;

    @Autowired
    @Qualifier("importIngredientRemedeService")
    private ImportService importIngredientRemedeService;

    @Override
    public File exportHeader(String entityType) throws Exception {

        switch (entityType) {
            case "ListStationsPage":
                return importStationService.exportHeader();
            case "ListPersonnesPage":
                return importPersonneService.exportHeader();
            case "ListPurificationsPage":
                return importPurificationService.exportHeader();
            case "ListSpecimensPage":
                return importSpecimenService.exportHeader();
            case "ListTestsBioPage":
                return importResultatTestBioService.exportHeader();
            case "ListDosagePage":
                return importResultatDosageService.exportHeader();
            case "ListLotsPage":
                return importLotService.exportHeader();
            case "ListMoleculesPage":
                return importMoleculeService.exportHeader();
            case "ListExtractionsPage":
                return importExtractionService.exportHeader();
            case "ListPersonnesInterrogeesPage":
                return importPersonneInterrogeeService.exportHeader();
            case "ListRemedePage":
                return importIngredientRemedeService.exportHeader();
            case "ListReferentielPanel":
                return importReferentielService.exportHeader();
            default:
                return null;
        }
    }

    @Override
    public <T> ImportResult importData(String entityType, InputStream input, Utilisateur utilisateur, Locale locale) throws DataConstraintException {

        switch (entityType) {
            case "ListStationsPage":
                return importStationService.importData(input, utilisateur, locale, new StationImportModel());
            case "ListPersonnesPage":
                return importPersonneService.importData(input, utilisateur, locale, new PersonneImportModel());
            case "ListPurificationsPage":
                return importPurificationService.importData(input, utilisateur, locale, new PurificationImportModel());
            case "ListSpecimensPage":
                return importSpecimenService.importData(input, utilisateur, locale, new SpecimenImportModel());
            case "ListTestsBioPage":
                return importResultatTestBioService.importData(input, utilisateur, locale, new ResultatTestBioImportModel());
            case "ListDosagePage":
                return importResultatDosageService.importData(input, utilisateur, locale, new ResultatDosageImportModel());
            case "ListLotsPage":
                return importLotService.importData(input, utilisateur, locale, new LotImportModel());
            case "ListMoleculesPage":
                return importMoleculeService.importData(input, utilisateur, locale, new MoleculeImportModel());
            case "ListExtractionsPage":
                return importExtractionService.importData(input, utilisateur, locale, new ExtractionImportModel());
            case "ListPersonnesInterrogeesPage":
                return importPersonneInterrogeeService.importData(input, utilisateur, locale, new PersonneInterrogeeImportModel());
            case "ListReferentielPanel":
                return importReferentielService.importData(input, utilisateur, locale, new ReferentielImportModel());
            case "ListRemedePage":
                return importIngredientRemedeService.importData(input, utilisateur, locale, new IngredientRemedeImportModel());
            default:
                return null;
        }
    }
}
