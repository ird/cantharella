package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class ReferentielImportModel extends AbstractImportExportModel<ReferentielImport> {

    public static final char CSV_SEPARATOR = ';';

    public ReferentielImportModel() {

        super(CSV_SEPARATOR);

        // "Cible";
        newMandatoryColumn("Référentiel", "referentiel");
        // "Conc";
        newMandatoryColumn("Code", "code");
        // "Unité";
        newMandatoryColumn("Valeur", "valeur");

        // Column for export

        // "Cible";
        newColumnForExport("Référentiel", "referentiel");
        // "Conc";
        newColumnForExport("Code", "code");
        // "Unité";
        newColumnForExport("Valeur", "valeur");
    }

    @Override
    public ReferentielImport newEmptyInstance() {
        return new ReferentielImport();
    }

}
