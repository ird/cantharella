package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class PersonneImportModel extends AbstractImportExportModel<PersonneImport> {

    public static final char CSV_SEPARATOR = ';';

    public PersonneImportModel() {
        super(CSV_SEPARATOR);

        // "Prénom";
        newMandatoryColumn("Prénom", "prenom");
        // "Nom";
        newMandatoryColumn("Nom", "nom");
        // "Organisme";
        newMandatoryColumn("Organisme", "organisme");
        // "Fonction";
        newOptionalColumn("Fonction", "fonction");
        // "Téléphone";
        newOptionalColumn("Téléphone", "telephone");
        // "Fax";
        newOptionalColumn("Fax", "fax");
        // "Courriel";
        newMandatoryColumn("Courriel", "courriel");
        // "Adresse postale";
        newMandatoryColumn("Adresse postale", "adressePostale");
        // "Code postal";
        newMandatoryColumn("Code postal", "codePostal");
        // "Ville";
        newMandatoryColumn("Ville", "ville");
        // "Pays";
        newMandatoryColumn("Code pays", "pays");

        // Column for export

        // "Prénom";
        newColumnForExport("Prénom", "prenom");
        // "Nom";
        newColumnForExport("Nom", "nom");
        // "Organisme";
        newColumnForExport("Organisme", "organisme");
        // "Fonction";
        newColumnForExport("Fonction", "fonction");
        // "Téléphone";
        newColumnForExport("Téléphone", "telephone");
        // "Fax";
        newColumnForExport("Fax", "fax");
        // "Courriel";
        newColumnForExport("Courriel", "courriel");
        // "Adresse postale";
        newColumnForExport("Adresse postale", "adressePostale");
        // "Code postal";
        newColumnForExport("Code postal", "codePostal");
        // "Ville";
        newColumnForExport("Ville", "ville");
        // "Pays";
        newColumnForExport("Code pays", "pays");

    }

    @Override
    public PersonneImport newEmptyInstance() {
        return new PersonneImport();
    }


}
