package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class IngredientRemedeImportModel extends AbstractImportExportModel<IngredientRemedeImport> {

    public static final char CSV_SEPARATOR = ';';

    public IngredientRemedeImportModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Campagne", "campagne");
        newMandatoryColumn("Station", "station");
        newOptionalColumn("Prénom prescripteur", "prenomPrescripteur");
        newOptionalColumn("Nom prescripteur", "nomPrescripteur");
        newOptionalColumn("Nom du remède", "nomRemede");
        newOptionalColumn("Langue", "codeLangue");
        newOptionalColumn("Référentiel", "referentiel");
        newOptionalColumn("Valeur", "referentielCode");
        newOptionalColumn("Indications biomédicales", "indicationBiomedicale");
        newOptionalColumn("Indications vernaculaires", "indicationVernaculaire");
        newOptionalColumn("Nombre d'ingrédients", "nbIngredients", ImportParser.INTEGER_PARSER);
        newOptionalColumn("Ingrédient ref Specimen", "ingredientRefSpecimen");
        newMandatoryColumn("Nom vernaculaire ingrédient", "ingredientNomVernaculaire");
        newOptionalColumn("Partie", "partie");
        newOptionalColumn("Provenance", "provenance");
        newOptionalColumn("Période", "periode");
        newOptionalColumn("Disponibilité", "disponibilite");
        newOptionalColumn("Préparation", "preparation");
        newOptionalColumn("Conservation", "conservation");
        newOptionalColumn("Administration", "administration");
        newOptionalColumn("Posologie", "posologie");
        newOptionalColumn("Recommandations", "recommandations", ImportParser.BOOLEAN_PARSER);
        newOptionalColumn("Lesquelles", "recommandationsText");
        newOptionalColumn("Effets indésirables", "effetsIndesirables");
        newOptionalColumn("Nombre patients", "nbPatients", ImportParser.INTEGER_PARSER);
        newOptionalColumn("Complément", "complement");
        newMandatoryColumn("Référence", "reference");
        newMandatoryColumn("Prénom enquêteur", "prenomEnqueteur");
        newMandatoryColumn("Nom enquêteur", "nomEnqueteur");
        newMandatoryColumn("Date", "date");

        // Column for export
        newColumnForExport("Campagne", "campagne");
        newColumnForExport("Station", "station");
        newColumnForExport("Prénom prescripteur", "prenomPrescripteur");
        newColumnForExport("Nom prescripteur", "nomPrescripteur");
        newColumnForExport("Nom du remède", "nomRemede");
        newColumnForExport("Langue", "codeLangue");
        newColumnForExport("Référentiel", "referentiel");
        newColumnForExport("Valeur", "referentielCode");
        newColumnForExport("Indications biomédicales", "indicationBiomedicale");
        newColumnForExport("Indications vernaculaires", "indicationVernaculaire");
        newColumnForExport("Nombre d'ingrédients", "nbIngredients");
        newColumnForExport("Ingrédient ref Specimen", "ingredientRefSpecimen");
        newColumnForExport("Nom vernaculaire ingrédient", "ingredientNomVernaculaire");
        newColumnForExport("Partie", "partie");
        newColumnForExport("Provenance", "provenance");
        newColumnForExport("Période", "periode");
        newColumnForExport("Disponibilité", "disponibilite");
        newColumnForExport("Préparation", "preparation");
        newColumnForExport("Conservation", "conservation");
        newColumnForExport("Administration", "administration");
        newColumnForExport("Posologie", "posologie");
        newColumnForExport("Recommandations", "recommandations");
        newColumnForExport("Lesquelles", "recommandationsText");
        newColumnForExport("Effets indésirables", "effetsIndesirables");
        newColumnForExport("Nombre patients", "nbPatients");
        newColumnForExport("Complément", "complement");
        newColumnForExport("Référence", "reference");
        newColumnForExport("Prénom enquêteur", "prenomEnqueteur");
        newColumnForExport("Nom enquêteur", "nomEnqueteur");
        newColumnForExport("Date", "date");
    }

    @Override
    public IngredientRemedeImport newEmptyInstance() {
        return new IngredientRemedeImport();
    }
}
