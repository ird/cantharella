package nc.ird.cantharella.service.model;

import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.ResultatTestBio.Stade;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.AbstractImportExportModel;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class ResultatTestBioImportModel extends AbstractImportExportModel<ResultatTestBioImport> {

    public static final char CSV_SEPARATOR = ';';

    public ResultatTestBioImportModel() {
        super(CSV_SEPARATOR);

        // "Cible";
        newMandatoryColumn("Cible", "methodeCible");
        // "Conc";
        newMandatoryColumn("Conc", "concMasse", ImportParser.BIG_DECIMAL_PARSER);
        // "Unité";
        newMandatoryColumn("Unité", "uniteConcMasse");
        // "Réf. produit";
        newMandatoryColumn("Réf. produit", "nomProduit");
        // "Prod. témoin";
        newOptionalColumn("Prod. témoin", "produitTemoin");
        // "Valeur";
        newMandatoryColumn("Valeur", "valeur", ImportParser.BIG_DECIMAL_PARSER);
        // "Unité";
        //TODO Cette seconde colonne d'unité est une information de l'unité de résultat de la methode de test. Pas nécessaire .
        // "Type extrait";
        // Mis via le produit sur le resultatTestBio
        // "Actif";
        newMandatoryColumn("Actif", "estActif", ImportParser.BOOLEAN_PARSER);
        // "Famille";
        // "Genre";
        // "Espèce";
        // "Pays";
        //TODO utiles ? d'apres l'export, l'info provient du produit (via Lot source / Specimen)

        // "Stade
        newMandatoryColumn("Stade", "stade", ImportParser.STADE_PARSER);

        // "Type"
        newMandatoryColumn("Type", "type", ImportParser.TYPE_RESULTAT_PARSER);

        // "Erreur"
        newMandatoryColumn("Erreur", "nomErreur");

        // "Repère";
        newMandatoryColumn("Repère", "repere");
        // "Réf. test"
        newMandatoryColumn("Réf. test", "refTestBio");

        // "Organisme testeur
        newMandatoryColumn("Organisme testeur", "organismeTesteur");
        // "Nom manipulateur"
        newMandatoryColumn("Nom manipulateur", "nomManipulateur");
        // "Prénom manipulateur"
        newMandatoryColumn("Prénom manipulateur", "prenomManipulateur");

        // "Date"
        newMandatoryColumn("Date", "date");
        // "Complément"
        newMandatoryColumn("Complément", "complement");

        // Column for export

        // "Cible";
        newColumnForExport("Cible", "methodeCible");
        // "Conc";
        newColumnForExport("Conc", "concMasse");
        // "Unité";
        newColumnForExport("Unité", "uniteConcMasse");
        // "Réf. produit";
        newColumnForExport("Réf. produit", "nomProduit");
        // "Réf. produit";
        newColumnForExport("Prod. témoin", "produitTemoin");
        // "Valeur";
        newColumnForExport("Valeur", "valeur");
        // "Actif";
        newColumnForExport("Actif", "estActif");
        // "Stade
        newColumnForExport("Stade", "stade");

        // "Type"
        newColumnForExport("Type", "type");

        // "Erreur"
        newColumnForExport("Erreur", "nomErreur");

        // "Repère";
        newColumnForExport("Repère", "repere");
        // "Réf. test"
        newColumnForExport("Réf. test", "refTestBio");

        // "Organisme testeur
        newColumnForExport("Organisme testeur", "organismeTesteur");
        // "Nom manipulateur"
        newColumnForExport("Nom manipulateur", "nomManipulateur");
        // "Prénom manipulateur"
        newColumnForExport("Prénom manipulateur", "prenomManipulateur");

        // "Date"
        newColumnForExport("Date", "date");
        // "Complement"
        newColumnForExport("Complément", "complement");

    }

    @Override
    public ResultatTestBioImport newEmptyInstance() {
        return new ResultatTestBioImport();
    }

}
