package nc.ird.cantharella.service.services;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import org.nuiton.csv.ImportModel;

import java.io.File;
import java.io.InputStream;
import java.util.Locale;

public interface ImportService {

    /**
     * 
     * Import {@link nc.ird.cantharella.data.model.TestBio} (with {@link nc.ird.cantharella.data.model.ResultatTestBio})
     * from CSV file.
     * 
     * @param input : {@link InputStream} corresponding to a CSV file like this format :
     *            "Cible";"Conc";"Unité";"Réf. produit"
     *            ;"Valeur";"Actif";"Stade";"Type";"Erreur";"Repère";"Réf. test";"Organisme testeur"
     *            ;"Nom manipulateur";"Prénom manipulateur";"Date"
     *            "ABTS";"2,00";"mg/ml";"MAY13-011DM";"65,40";"non";"DETECTION"
     *            ;"PRODUIT";;"01";"EP-MAY01-TEST";"CL";"DOE";"John";16/09/14
     *            "ABTS";"0,06";"mg/ml";"MAY13-007DM";"1 901,50"
     *            ;"oui";"DETECTION";"temoin";;"02";"EP-MAY01-TEST";"CL";"DOE";"John";16/09/14
     *            "ABTS";"2,00";"mg/ml";"MAY13-011DM"
     *            ;"62,00";"non";"DETECTION";"BLANC";;"03";"EP-MAY01-TEST";"CL";"DOE";"John";16/09/14
     *            "AChE";"1,00";"mg/ml"
     *            ;"R3028B_DESS";;"non";"DETECTION";"PRODUIT";;"S1-C7";"DG-S1-TEST";"CL";"DOE";"Jane";31/12/00
     *            "AChE";"1,00"
     *            ;"mg/ml";"R3028C";;"non";"DETECTION";"BLANC";;"S1-C2";"DG-S1-TEST";"CL";"DOE";"Jane";31/12/00
     * 
     */
    <T> ImportResult importData(InputStream input, Utilisateur utilisateur, Locale locale, ImportModel<T> importModel) throws DataConstraintException;

    File exportHeader() throws Exception;

}
