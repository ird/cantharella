/*
 * #%L
 * Cantharella :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.service.utils.normalizers;

import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.ResultatDosage;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.TestBio;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.AssertTools;
import nc.ird.cantharella.utils.StringTransformer;

/**
 * Dosage normalizer
 * 
 * @author Jean Couteau Viney
 */
public final class DosageNormalizer extends Normalizer<Dosage> {

    /** {@inheritDoc} */
    @Override
    protected Dosage normalize(Dosage dosage) {
        AssertTools.assertNotNull(dosage);
        // Unique fields
        dosage.setRef(Normalizer.normalize(UniqueFieldNormalizer.class, dosage.getRef()));
        // Autocomplete field
        dosage.setOrganisme(new StringTransformer(dosage.getOrganisme())
                .replaceConsecutiveWhitespaces().trimToNull().capitalizeFully().toString());
        for (ResultatDosage curResult : dosage.getResultats()) {
            //TODO normalize molecule et compose as unique
            // Autocomplete field
            curResult.setComposeDose(new StringTransformer(curResult.getComposeDose())
                    .replaceConsecutiveWhitespaces().trimToNull().capitalizeFully().toString());
        }

        return dosage;
    }
}
