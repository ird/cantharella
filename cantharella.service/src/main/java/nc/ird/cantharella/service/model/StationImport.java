package nc.ird.cantharella.service.model;

import java.math.BigDecimal;

public class StationImport {

    protected String nom;

    protected String codePays;

    protected String localite;

    protected String complement;

    protected Integer latitudeDegres;

    protected BigDecimal latitudeMinutes;

    protected Character latitudeOrientation;

    protected Integer longitudeDegres;

    protected BigDecimal longitudeMinutes;

    protected Character longitudeOrientation;

    protected Integer referentiel;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodePays() {
        return codePays;
    }

    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    public String getLocalite() {
        return localite;
    }

    public void setLocalite(String localite) {
        this.localite = localite;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public Integer getLatitudeDegres() {
        return latitudeDegres;
    }

    public void setLatitudeDegres(Integer latitudeDegres) {
        this.latitudeDegres = latitudeDegres;
    }

    public BigDecimal getLatitudeMinutes() {
        return latitudeMinutes;
    }

    public void setLatitudeMinutes(BigDecimal latitudeMinutes) {
        this.latitudeMinutes = latitudeMinutes;
    }

    public Character getLatitudeOrientation() {
        return latitudeOrientation;
    }

    public void setLatitudeOrientation(Character latitudeOrientation) {
        this.latitudeOrientation = latitudeOrientation;
    }

    public Integer getLongitudeDegres() {
        return longitudeDegres;
    }

    public void setLongitudeDegres(Integer longitudeDegres) {
        this.longitudeDegres = longitudeDegres;
    }

    public BigDecimal getLongitudeMinutes() {
        return longitudeMinutes;
    }

    public void setLongitudeMinutes(BigDecimal longitudeMinutes) {
        this.longitudeMinutes = longitudeMinutes;
    }

    public Character getLongitudeOrientation() {
        return longitudeOrientation;
    }

    public void setLongitudeOrientation(Character longitudeOrientation) {
        this.longitudeOrientation = longitudeOrientation;
    }

    public Integer getReferentiel() {
        return referentiel;
    }

    public void setReferentiel(Integer referentiel) {
        this.referentiel = referentiel;
    }
}
