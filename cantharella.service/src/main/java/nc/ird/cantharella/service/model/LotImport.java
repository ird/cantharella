package nc.ird.cantharella.service.model;

import java.math.BigDecimal;

public class LotImport {

    String campagne;

    String station;

    String dateRecolte;

    String ref;

    String specimenRef;

    String partie;

    BigDecimal masseFraiche;

    BigDecimal masseSeche;

    Boolean echantillonColl;

    Boolean echantillonIdent;

    Boolean echantillonPhylo;

    String complement;

    public String getCampagne() {
        return campagne;
    }

    public void setCampagne(String campagne) {
        this.campagne = campagne;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getDateRecolte() {
        return dateRecolte;
    }

    public void setDateRecolte(String dateRecolte) {
        this.dateRecolte = dateRecolte;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getSpecimenRef() {
        return specimenRef;
    }

    public void setSpecimenRef(String specimenRef) {
        this.specimenRef = specimenRef;
    }

    public String getPartie() {
        return partie;
    }

    public void setPartie(String partie) {
        this.partie = partie;
    }

    public BigDecimal getMasseFraiche() {
        return masseFraiche;
    }

    public void setMasseFraiche(BigDecimal masseFraiche) {
        this.masseFraiche = masseFraiche;
    }

    public BigDecimal getMasseSeche() {
        return masseSeche;
    }

    public void setMasseSeche(BigDecimal masseSeche) {
        this.masseSeche = masseSeche;
    }

    public Boolean getEchantillonColl() {
        return echantillonColl;
    }

    public void setEchantillonColl(Boolean echantillonColl) {
        this.echantillonColl = echantillonColl;
    }

    public Boolean getEchantillonIdent() {
        return echantillonIdent;
    }

    public void setEchantillonIdent(Boolean echantillonIdent) {
        this.echantillonIdent = echantillonIdent;
    }

    public Boolean getEchantillonPhylo() {
        return echantillonPhylo;
    }

    public void setEchantillonPhylo(Boolean echantillonPhylo) {
        this.echantillonPhylo = echantillonPhylo;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }
}
