/*
 * #%L
 * Cantharella :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.service.utils.normalizers;

import nc.ird.cantharella.data.model.MethodeDosage;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.AssertTools;
import nc.ird.cantharella.utils.StringTransformer;

/**
 * MethodeTest normalizer
 * 
 * @author Jean Couteau Viney
 */
public final class MethodeDosageNormalizer extends Normalizer<MethodeDosage> {

    /** {@inheritDoc} */
    @Override
    protected MethodeDosage normalize(MethodeDosage methodeDosage) {
        AssertTools.assertNotNull(methodeDosage);
        // Unique field
        methodeDosage.setNom(Normalizer.normalize(ConfigNameNormalizer.class, methodeDosage.getNom()));
        methodeDosage.setAcronyme(new StringTransformer(methodeDosage.getAcronyme()).trimToNull()
                .replaceConsecutiveWhitespaces().toString());
        methodeDosage.setDomaine(new StringTransformer(methodeDosage.getDomaine()).trimToNull()
                .replaceConsecutiveWhitespaces().toString());
        methodeDosage.setValeurMesuree(new StringTransformer(methodeDosage.getValeurMesuree()).trimToNull()
                .replaceConsecutiveWhitespaces().toString());
        methodeDosage.setUnite(new StringTransformer(methodeDosage.getUnite()).trimToNull()
                .replaceConsecutiveWhitespaces().toString());
        methodeDosage.setSeuil(new StringTransformer(methodeDosage.getSeuil()).trimToNull()
                .replaceConsecutiveWhitespaces().toString());
        return methodeDosage;
    }
}
