package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.PersonneDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.SpecimenImport;
import nc.ird.cantharella.service.model.SpecimenImportModel;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.service.utils.normalizers.LotNormalizer;
import nc.ird.cantharella.service.utils.normalizers.SpecimenNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("importSpecimenService")
public class ImportSpecimenServiceImpl extends AbstractImportService {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImportSpecimenServiceImpl.class);
    protected final String DATE_FORMAT = "d/MM/uuuu";

    /**
     * Messages d'internationalisation
     */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /**
     * DAO
     */
    @Autowired
    private GenericDao dao;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private StationService stationService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {

        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

        // Chargement de certaines données en amont
        List<Station> stations = stationService.listStations(loadedUtilisateur);
        Map<String, Station> stationByNom = Maps.uniqueIndex(stations, Station::getNom);

        Map<String, Specimen> specimenParRef = new HashMap<>();
        List<String> existingSpecimenRef = new ArrayList<>();

        int currentLine = 0;
        for (ImportRow<T> specimenImportRow : importData) {
            currentLine++;

            boolean valid = specimenImportRow.isValid();
            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = specimenImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("Type")) {
                        String message = messages.getMessage("csv.import.type.unknown", new Object[]{currentLine, Stream.of(Specimen.TypeOrganisme.values()).collect(Collectors.toList())}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }

            SpecimenImport specimenImport = (SpecimenImport) specimenImportRow.getBean();
            String refSpecimen = specimenImport.getReference();

            // Mandatory fields/relation
            if (refSpecimen == null || refSpecimen.isBlank()) {
                String message = messages.getMessage("csv.import.specimen.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else if (!specimenParRef.containsKey(refSpecimen) && existingSpecimenRef.contains(refSpecimen)) {
                // Already loaded
                String message = messages.getMessage("csv.import.specimen.existing", new Object[]{currentLine, refSpecimen}, locale);
                importResult.addErrorLine(message);
            } else {
                // Load
                boolean exists = dao.exists(Specimen.class, "ref", refSpecimen);
                if (exists) {
                    String message = messages.getMessage("csv.import.specimen.existing", new Object[]{currentLine, refSpecimen}, locale);
                    importResult.addErrorLine(message);
                    existingSpecimenRef.add(refSpecimen);
                }
            }

            if (refSpecimen != null && refSpecimen.length() > 10) {
                    String message = messages.getMessage("csv.import.specimen.ref.toolong", new Object[]{currentLine}, locale);
                    importResult.addErrorLine(message);
            }

            if (specimenImport.getEmbranchement() == null || specimenImport.getEmbranchement().isBlank()) {
                String message = messages.getMessage("csv.import.embranchement.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            if (specimenImport.getNumDepot().length() > 10) {
                String message = messages.getMessage("csv.import.specimen.numDepot.invalid", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            // Check date of each line
            Date parsedDate = null;
            try {
                LocalDate date = LocalDate.parse(specimenImport.getDateDepot(), DateTimeFormatter.ofPattern(DATE_FORMAT).withResolverStyle(ResolverStyle.STRICT));
                parsedDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                if (date.isAfter(LocalDate.now())) {
                    String message = messages.getMessage("csv.import.date.future", new Object[]{currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } catch (DateTimeParseException e) {
                String message = messages.getMessage("csv.import.date.invalid", new Object[]{currentLine, specimenImport.getDateDepot()}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve station
            String refStation = specimenImport.getStation();
            Station station = stationByNom.get(refStation.toUpperCase());
            if (station == null) {
                String message = messages.getMessage("csv.import.station.unknown", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            // Need to find the personne
            String nomIdentificateur = specimenImport.getNomIdentificateur();
            String prenomIdentificateur = specimenImport.getPrenomIdentificateur();
            Personne technician = null;
            if (StringUtils.isNotBlank(nomIdentificateur) && StringUtils.isNotBlank(prenomIdentificateur)) {
                Map<String, Object> identificateurParameters = ImmutableMap.of("nom", nomIdentificateur, "prenom", prenomIdentificateur);
                List<Personne> personnes = (List<Personne>) dao.list(PersonneDao.HQL_FIND_PERSONNE_PAR_NOM_PRENOM, identificateurParameters);
                if (personnes.isEmpty()) {
                    String message = messages.getMessage("csv.import.technician.unknown", new Object[]{currentLine, prenomIdentificateur, identificateurParameters}, locale);
                    importResult.addErrorLine(message);
                } else {
                    technician = personnes.get(0);
                }
            }

            Specimen specimen = specimenParRef.get(refSpecimen);
            if (specimen == null) {
                specimen = new Specimen();
                specimen.setRef(specimenImport.getReference());
                specimen.setEmbranchement(specimenImport.getEmbranchement());
                specimen.setFamille(specimenImport.getFamille());
                specimen.setGenre(specimenImport.getGenre());
                specimen.setEspece(specimenImport.getEspece());
                specimen.setSousEspece(specimenImport.getSousEspece());
                specimen.setVariete(specimenImport.getVariete());
                specimen.setTypeOrganisme(specimenImport.getTypeOrganisme());
                specimen.setIdentificateur(technician);
                specimen.setStation(station);
                specimen.setComplement(specimenImport.getComplement());
                specimen.setNumDepot(specimenImport.getNumDepot());
                specimen.setDateDepot(parsedDate);
                specimen.setLieuDepot(specimenImport.getLieuDepot());
                specimen.setCreateur(loadedUtilisateur);
            } else {
                // Be sure name are same
                if (specimen.getRef() == null) {
                    specimen.setRef(specimenImport.getReference());
                } else if (!specimen.getRef().equals(specimenImport.getReference())) {
                    String message = messages.getMessage("csv.import.specimen.nom.different", new Object[]{currentLine, specimenImport.getReference()}, locale);
                    importResult.addErrorLine(message);
                }

                // Be sure specimen date are same
                if (specimen.getDateDepot() == null) {
                    specimen.setDateDepot(parsedDate);
                } else if (!specimen.getDateDepot().equals(parsedDate)) {
                    String message = messages.getMessage("csv.import.date.different", new Object[]{currentLine, refSpecimen}, locale);
                    importResult.addErrorLine(message);
                }

            }

            Normalizer.normalize(SpecimenNormalizer.class, specimen);
            specimenParRef.put(specimenImport.getReference(), specimen);
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (Specimen specimen : specimenParRef.values()) {
                try {
                    dao.create(specimen);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        SpecimenImportModel model = new SpecimenImportModel();

        File headers = File.createTempFile("specimens", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
