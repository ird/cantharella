package nc.ird.cantharella.service.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class ImportResult {

    protected int nbImported;

    protected List<String> errorLines = new ArrayList<>();

    public boolean isValid() {
        return errorLines.isEmpty();
    }

    public int getNbImported() {
        return nbImported;
    }

    public void setNbImported(int nbImported) {
        this.nbImported = nbImported;
    }

    public List<String> getErrorLines() {
        return errorLines;
    }

    public void setErrorLines(List<String> errorLines) {
        this.errorLines.clear();
        this.errorLines.addAll(errorLines);
    }

    public void addErrorLine(String errorLine) {
        this.errorLines.add(errorLine);
    }
}
