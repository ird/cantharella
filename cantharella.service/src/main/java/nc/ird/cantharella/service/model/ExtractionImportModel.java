package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class ExtractionImportModel extends AbstractImportExportModel<ExtractionImport> {

    public static final char CSV_SEPARATOR = ';';

    public ExtractionImportModel() {
        super(CSV_SEPARATOR);

        // "Réf. lot";
        newMandatoryColumn("Réf. lot", "refLot");
        // "Masse à extraire";
        newOptionalColumn("Masse à extraire", "masseAextraire", ImportParser.BIG_DECIMAL_PARSER);
        // "Complément";
        newOptionalColumn("Complément", "complement");
        // "Méthode extraction";
        newOptionalColumn("Méthode extraction", "methodeExtraction");
        // "Type extrait"
        newOptionalColumn("Type extrait", "typeExtrait");
        // "Réf. extrait"
        newOptionalColumn("Réf. extrait", "refExtrait");
        // "Masse extrait"
        newOptionalColumn("Masse extrait", "masseExtrait", ImportParser.BIG_DECIMAL_PARSER);
        // "Réf. Extraction"
        newMandatoryColumn("Réf. Extraction", "refExtraction");
        // "Nom manipulateur"
        newMandatoryColumn("Nom manipulateur", "nomManipulateur");
        // "Prénom manipulateur"
        newMandatoryColumn("Prénom manipulateur", "prenomManipulateur");
        // "Date";
        newMandatoryColumn("Date", "date");

        // Column for export

        // "Réf. lot";
        newColumnForExport("Réf. lot", "lot");
        // "Masse à extraire";
        newColumnForExport("Masse à extraire", "masseDepart");
        // "Complément";
        newColumnForExport("Complément", "complement");
        // "Méthode extraction";
        newColumnForExport("Méthode extraction", "methode");
        // "Type extrait"
        newColumnForExport("Type extrait", "typeExtrait");
        // "Réf. extrait"
        newColumnForExport("Réf. extrait", "ref");
        // "Masse extrait"
        newColumnForExport("Masse extrait", "masseObtenue");
        // "Réf. Extraction"
        newColumnForExport("Réf. Extraction", "ref");
        // "Nom manipulateur"
        newColumnForExport("Nom manipulateur", "nomManipulateur");
        // "Prénom manipulateur"
        newColumnForExport("Prénom manipulateur", "prenomManipulateur");
        // "Date";
        newColumnForExport("Date", "date");

    }

    @Override
    public ExtractionImport newEmptyInstance() {
        return new ExtractionImport();
    }

}
