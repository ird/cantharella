package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class MoleculeImportModel extends AbstractImportExportModel<MoleculeImport> {

    public static final char CSV_SEPARATOR = ';';

    public MoleculeImportModel() {
        super(CSV_SEPARATOR);

        // "Nom Commun";
        newMandatoryColumn("Nom Commun", "nomCommun");
        // "Famille chimique"
        newOptionalColumn("Famille chimique", "familleChimique");
        // "Formule développée"
        newOptionalColumn("Code SMILES", "formuleDevMol");
        // "Nom IUPAC"
        newOptionalColumn("Nom IUPAC", "nomIupca");
        // "Formule brute"
        newOptionalColumn("Formule brute", "formuleBrute");
        // "M"
        newOptionalColumn("M", "masseMolaire", ImportParser.BIG_DECIMAL_PARSER);
        // "Nouvelle molécule"
        newOptionalColumn("Nouvelle molécule", "nouvMolecul", ImportParser.BOOLEAN_PARSER);
        // "Campagne"
        newOptionalColumn("Campagne", "campagne");
        // "Identifée par"
        newOptionalColumn("Identifée par", "identifieePar");
        // "Publication origine"
        newOptionalColumn("Publication origine", "publiOrigine");
        // "Complément"
        newOptionalColumn("Complément", "complement");
        // "Produit provenance"
        newOptionalColumn("Produit provenance", "produit");
        // "Pourcentage"
        newOptionalColumn("Pourcentage", "pourcentage", ImportParser.BIG_DECIMAL_PARSER);

        // Column for export

        // "Nom Commun";
        newColumnForExport("Nom Commun", "nomCommun");
        // "Famille chimique"
        newColumnForExport("Famille chimique", "familleChimique");
        // "Formule développée"
        newColumnForExport("Code SMILES", "formuleDevMol");
        // "Nom IUPAC"
        newColumnForExport("Nom IUPAC", "nomIupca");
        // "Formule brute"
        newColumnForExport("Formule brute", "formuleBrute");
        // "M"
        newColumnForExport("M", "masseMolaire");
        // "Nouvelle molécule"
        newColumnForExport("Nouvelle molécule", "nouvMolecul");
        // "Campagne"
        newColumnForExport("Campagne", "campagne");
        // "Identifée par"
        newColumnForExport("Identifée par", "identifieePar");
        // "Publication origine"
        newColumnForExport("Publication origine", "publiOrigine");
        // "Complément"
        newColumnForExport("Complément", "complement");
        // "Produit provenance"
        newColumnForExport("Produit provenance", "produit");
        // "Pourcentage"
        newColumnForExport("Pourcentage", "pourcentage");

    }

    @Override
    public MoleculeImport newEmptyInstance() {
        return new MoleculeImport();
    }

}
