package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class StationImportModel extends AbstractImportExportModel<StationImport> {

    public static final char CSV_SEPARATOR = ';';

    public StationImportModel() {
        super(CSV_SEPARATOR);

        // "Nom";
        newMandatoryColumn("Nom", "nom");
        // "Code pays";
        newMandatoryColumn("Code pays", "codePays");
        // "Localité";
        newOptionalColumn("Localité", "localite");
        // "Complément";
        newOptionalColumn("Complément", "complement");
        // "Latitude (degrés)";
        newOptionalColumn("Latitude (degrés)", "latitudeDegres", ImportParser.INTEGER_PARSER);
        // "Latitude (minutes)";
        newOptionalColumn("Latitude (minutes)", "latitudeMinutes", ImportParser.BIG_DECIMAL_PARSER);
        // "Latitude (N/S)";
        newOptionalColumn("Latitude (N/S)", "latitudeOrientation", ImportParser.ORIENTATION_PARSER);
        // "Longitude (degrés)";
        newOptionalColumn("Longitude (degrés)", "longitudeDegres", ImportParser.INTEGER_PARSER);
        // "Longitude (minutes)";
        newOptionalColumn("Longitude (minutes)", "longitudeMinutes", ImportParser.BIG_DECIMAL_PARSER);
        // "Longitude (E/O)";
        newOptionalColumn("Longitude (E/W)", "longitudeOrientation", ImportParser.ORIENTATION_PARSER);

        // Column for export

        // "Nom";
        newColumnForExport("Nom", "nom");
        // "Code pays";
        newColumnForExport("Code pays", "codePays");
        // "Localité";
        newColumnForExport("Localité", "localite");
        // "Complément";
        newColumnForExport("Complément", "complement");
        // "Latitude (degrés)";
        newColumnForExport("Latitude (degrés)", "latitudeDegres");
        // "Latitude (minutes)";
        newColumnForExport("Latitude (minutes)", "latitudeMinutes");
        // "Latitude (N/S)";
        newColumnForExport("Latitude (N/S)", "latitudeNS");
        // "Longitude (degrés)";
        newColumnForExport("Longitude (degrés)", "longitudeDegres");
        // "Longitude (minutes)";
        newColumnForExport("Longitude (minutes)", "longitudeMinutes");
        // "Longitude (E/O)";
        newColumnForExport("Longitude (E/W)", "longitudeEO");

    }

    @Override
    public StationImport newEmptyInstance() {
        return new StationImport();
    }


}
