package nc.ird.cantharella.service.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CoordinatesUtil {

    //protected String SPACE_EXPRESSION = "/\s+/";
    protected static String LATITUDE_EXPRESSION = "(\\d{1,2})°(?:\\s*(\\d{1,2}.\\d{1,3})[′'])?\\s*(N|S|N\\/S)"; // 0- 90° 0-59′ N/S

    protected static String LONGITUDE_EXPRESSION = "(\\d{1,3})°(?:\\s*(\\d{1,2}.\\d{1,3})[′'])?\\s*(E|W|E\\/W)"; // 0-180° 0-59′ E/W

    static double parseCoordinate(String expression, int limit, String surfaces, String text) {

        Pattern p = Pattern.compile(expression);
        Matcher m = p.matcher(text.trim());

        if (m.matches()) {
            int degrees = Integer.parseInt(m.group(1)); // 0-90° or 0-180°
            if (degrees > limit) {
                throw new IllegalArgumentException("Incorrect degrees value (should be in range from 0 to " + limit
                        + ") : " + text);
            }

            double minutes = Double.parseDouble(m.group(2)); // 0-60′
            if (minutes > 60) {
                throw new IllegalArgumentException(
                        "Incorrect minutes value (should be in range from 0 to 60 (strict)) : " + text);
            }

            if (degrees == 0 && minutes == 0) {
                return 0;
            }

            char surface = m.group(3).charAt(0); // N/S or E/W
            if (surface == surfaces.charAt(0)) {
                return +(degrees + minutes / 60.0);
            }
            if (surface == surfaces.charAt(1)) {
                return -(degrees + minutes / 60.0);
            }
            throw new IllegalArgumentException("Incorrect surface value (should be " + surfaces.charAt(0) + " or "
                    + surfaces.charAt(1) + ") : " + text);
        }
        throw new IllegalArgumentException("Incorrect coordinate format : " + text);
    };

    public static double parseLatitude(String latitude) {
        return parseCoordinate(LATITUDE_EXPRESSION, 90, "NS", latitude); // N/S
    };

    public static double parseLongitude(String longitude) {
        return parseCoordinate(LONGITUDE_EXPRESSION, 180, "EW", longitude); // E/W
    };

}
