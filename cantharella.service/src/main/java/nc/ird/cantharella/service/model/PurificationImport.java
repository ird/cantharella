package nc.ird.cantharella.service.model;

import java.math.BigDecimal;

public class PurificationImport {

    protected String referenceProduit;

    protected BigDecimal masse;

    protected String complement;

    protected String methode;

    protected String indiceFraction;

    protected String referenceFraction;

    protected BigDecimal masseFraction;

    protected String referencePurification;

    protected String nomManipulateur;

    protected String prenomManipulateur;

    protected String date;

    public String getReferenceProduit() {
        return referenceProduit;
    }

    public void setReferenceProduit(String referenceProduit) {
        this.referenceProduit = referenceProduit;
    }

    public BigDecimal getMasse() {
        return masse;
    }

    public void setMasse(BigDecimal masse) {
        this.masse = masse;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getMethode() {
        return methode;
    }

    public void setMethode(String methode) {
        this.methode = methode;
    }

    public String getIndiceFraction() {
        return indiceFraction;
    }

    public void setIndiceFraction(String indiceFraction) {
        this.indiceFraction = indiceFraction;
    }

    public String getReferenceFraction() {
        return referenceFraction;
    }

    public void setReferenceFraction(String referenceFraction) {
        this.referenceFraction = referenceFraction;
    }

    public BigDecimal getMasseFraction() {
        return masseFraction;
    }

    public void setMasseFraction(BigDecimal masseFraction) {
        this.masseFraction = masseFraction;
    }

    public String getReferencePurification() {
        return referencePurification;
    }

    public void setReferencePurification(String referencePurification) {
        this.referencePurification = referencePurification;
    }

    public String getNomManipulateur() {
        return nomManipulateur;
    }

    public void setNomManipulateur(String nomManipulateur) {
        this.nomManipulateur = nomManipulateur;
    }

    public String getPrenomManipulateur() {
        return prenomManipulateur;
    }

    public void setPrenomManipulateur(String prenomManipulateur) {
        this.prenomManipulateur = prenomManipulateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
