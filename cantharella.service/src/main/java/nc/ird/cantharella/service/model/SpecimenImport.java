package nc.ird.cantharella.service.model;

import nc.ird.cantharella.data.model.Specimen;

public class SpecimenImport {

    protected String reference;

    protected String embranchement;

    protected String famille;

    protected String genre;

    protected String espece;

    protected String sousEspece;

    protected String variete;

    protected Specimen.TypeOrganisme typeOrganisme;

    protected String nomIdentificateur;

    protected String prenomIdentificateur;

    protected String station;

    protected String complement;

    protected String numDepot;

    protected String dateDepot;

    protected String lieuDepot;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getEmbranchement() {
        return embranchement;
    }

    public void setEmbranchement(String embranchement) {
        this.embranchement = embranchement;
    }

    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getEspece() {
        return espece;
    }

    public void setEspece(String espece) {
        this.espece = espece;
    }

    public String getSousEspece() {
        return sousEspece;
    }

    public void setSousEspece(String sousEspece) {
        this.sousEspece = sousEspece;
    }

    public String getVariete() {
        return variete;
    }

    public void setVariete(String variete) {
        this.variete = variete;
    }

    public Specimen.TypeOrganisme getTypeOrganisme() {
        return typeOrganisme;
    }

    public void setTypeOrganisme(Specimen.TypeOrganisme typeOrganisme) {
        this.typeOrganisme = typeOrganisme;
    }

    public String getNomIdentificateur() {
        return nomIdentificateur;
    }

    public void setNomIdentificateur(String nomIdentificateur) {
        this.nomIdentificateur = nomIdentificateur;
    }

    public String getPrenomIdentificateur() {
        return prenomIdentificateur;
    }

    public void setPrenomIdentificateur(String prenomIdentificateur) {
        this.prenomIdentificateur = prenomIdentificateur;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getNumDepot() {
        return numDepot;
    }

    public void setNumDepot(String numDepot) {
        this.numDepot = numDepot;
    }

    public String getDateDepot() {
        return dateDepot;
    }

    public void setDateDepot(String dateDepot) {
        this.dateDepot = dateDepot;
    }

    public String getLieuDepot() {
        return lieuDepot;
    }

    public void setLieuDepot(String lieuDepot) {
        this.lieuDepot = lieuDepot;
    }
}
