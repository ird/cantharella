package nc.ird.cantharella.service.model;

import java.math.BigDecimal;

/**
 * @author jcouteau (couteau@codelutin.com)
 */
public class ResultatDosageImport {

    protected String methode;

    protected BigDecimal concMasse;

    protected String uniteConcMasse;

    protected String echantillon;

    protected String composeDose;

    protected String moleculeDosee;

    protected BigDecimal valeur;

    protected BigDecimal SD;

    protected Boolean supSeuil;

    protected Integer analyseNb;

    protected String nomErreur;

    protected String refDosage;

    protected String organisme;

    protected String nomManipulateur;

    protected String prenomManipulateur;

    protected String date;

    protected String complement;

    public String getMethode() {
        return methode;
    }

    public void setMethode(String methode) {
        this.methode = methode;
    }

    public BigDecimal getConcMasse() {
        return concMasse;
    }

    public void setConcMasse(BigDecimal concMasse) {
        this.concMasse = concMasse;
    }

    public String getUniteConcMasse() {
        return uniteConcMasse;
    }

    public void setUniteConcMasse(String uniteConcMasse) {
        this.uniteConcMasse = uniteConcMasse;
    }

    public String getEchantillon() {
        return echantillon;
    }

    public void setEchantillon(String echantillon) {
        this.echantillon = echantillon;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }

    public BigDecimal getSD() {
        return SD;
    }

    public void setSD(BigDecimal SD) {
        this.SD = SD;
    }

    public Boolean getSupSeuil() {
        return supSeuil;
    }

    public void setSupSeuil(Boolean supSeuil) {
        this.supSeuil = supSeuil;
    }

    public Integer getAnalyseNb() {
        return analyseNb;
    }

    public void setAnalyseNb(Integer analyseNb) {
        this.analyseNb = analyseNb;
    }

    public String getRefDosage() {
        return refDosage;
    }

    public void setRefDosage(String refDosage) {
        this.refDosage = refDosage;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public String getNomErreur() {
        return nomErreur;
    }

    public void setNomErreur(String nomErreur) {
        this.nomErreur = nomErreur;
    }

    public String getNomManipulateur() {
        return nomManipulateur;
    }

    public void setNomManipulateur(String nomManipulateur) {
        this.nomManipulateur = nomManipulateur;
    }

    public String getPrenomManipulateur() {
        return prenomManipulateur;
    }

    public void setPrenomManipulateur(String prenomManipulateur) {
        this.prenomManipulateur = prenomManipulateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComposeDose() {
        return composeDose;
    }

    public void setComposeDose(String composeDose) {
        this.composeDose = composeDose;
    }

    public String getMoleculeDosee() {
        return moleculeDosee;
    }

    public void setMoleculeDosee(String moleculeDosee) {
        this.moleculeDosee = moleculeDosee;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }
}
