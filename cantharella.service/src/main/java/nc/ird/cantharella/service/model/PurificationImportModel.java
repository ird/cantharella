package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class PurificationImportModel extends AbstractImportExportModel<PurificationImport> {

    public static final char CSV_SEPARATOR = ';';

    public PurificationImportModel() {
        super(CSV_SEPARATOR);

        // "Réf. produit";
        newMandatoryColumn("Réf. produit", "referenceProduit");
        // "Masse à purifier";
        newOptionalColumn("Masse à purifier", "masse", ImportParser.BIG_DECIMAL_PARSER);
        // "Complément";
        newOptionalColumn("Complément", "complement");
        // "Méthode purification";
        newOptionalColumn("Méthode purification", "methode");
        // "Numéro fraction";
        newOptionalColumn("Numéro fraction", "indiceFraction");
        // "Réf. fraction";
        newOptionalColumn("Réf. fraction", "referenceFraction");
        // "Masse fraction";
        newOptionalColumn("Masse fraction", "masseFraction", ImportParser.BIG_DECIMAL_PARSER);
        // "Réf. purification"
        newMandatoryColumn("Réf. purification", "referencePurification");
        // "Nom manipulateur"
        newMandatoryColumn("Nom manipulateur", "nomManipulateur");
        // "Prénom manipulateur"
        newMandatoryColumn("Prénom manipulateur", "prenomManipulateur");
        // "Date";
        newMandatoryColumn("Date", "date");

        // Column for export

        // "Réf. produit";
        newColumnForExport("Réf. produit", "referenceProduit");
        // "Masse à purifier";
        newColumnForExport("Masse à purifier", "masse");
        // "Complément";
        newColumnForExport("Complément", "complement");
        // "Méthode purification";
        newColumnForExport("Méthode purification", "methode");
        // "Numéro fraction";
        newColumnForExport("Numéro fraction", "indiceFraction");
        // "Réf. fraction";
        newColumnForExport("Réf. fraction", "referenceFraction");
        // "Masse fraction";
        newColumnForExport("Masse fraction", "masseFraction");
        // "Réf. purification"
        newColumnForExport("Réf. purification", "referencePurification");
        // "Nom manipulateur"
        newColumnForExport("Nom manipulateur", "nomManipulateur");
        // "Prénom manipulateur"
        newColumnForExport("Prénom manipulateur", "prenomManipulateur");
        // "Date";
        newColumnForExport("Date", "date");

    }

    @Override
    public PurificationImport newEmptyInstance() {
        return new PurificationImport();
    }

}
