package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Molecule;
import nc.ird.cantharella.data.model.MoleculeProvenance;
import nc.ird.cantharella.data.model.Produit;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.MoleculeImport;
import nc.ird.cantharella.service.model.MoleculeImportModel;
import nc.ird.cantharella.service.services.CampagneService;
import nc.ird.cantharella.service.services.MoleculeService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.ProduitService;
import nc.ird.cantharella.service.utils.normalizers.MoleculeNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.IChemObjectWriter;
import org.openscience.cdk.io.MDLV2000Writer;
import org.openscience.cdk.io.listener.PropertiesListener;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.SmilesParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@Service
@Qualifier("ImportMoleculeService")
public class ImportMoleculeServiceImpl extends AbstractImportService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(ImportMoleculeServiceImpl.class);
    protected final String DATE_FORMAT = "d/MM/uuuu";

    /** Messages d'internationalisation */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /** DAO */
    @Autowired
    private GenericDao dao;

    /** Services */
    @Autowired
    private MoleculeService moleculeService;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private CampagneService campagneService;

    @Autowired
    private ProduitService produitService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

       // Chargement de certaines données en amont
        List<Campagne> campagnes = campagneService.listCampagnes(loadedUtilisateur);
        Map<String, Campagne> campagnesByNom = Maps.uniqueIndex(campagnes, Campagne::getNom);

        List<Produit> produits = produitService.listProduits(loadedUtilisateur);
        Map<String, Produit> produitByRef = Maps.uniqueIndex(produits, Produit::getRef);

        Map<String, Molecule> moleculeParNom = new HashMap<>();
        List<String> existingMoleculesName = new ArrayList<>();

        int currentLine = 0;
        for ( ImportRow<T> moleculeImportRow : importData) {
            currentLine++;

            boolean valid = moleculeImportRow.isValid();

            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = moleculeImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("M") || headerName.equalsIgnoreCase("Pourcentage")) {
                        String message = messages.getMessage("csv.import.numeric.invalid", new Object[] {currentLine, headerName}, locale);
                        importResult.addErrorLine(message);
                    }
                    if (headerName.equalsIgnoreCase("Nouvelle molécule")) {
                        String message = messages.getMessage("csv.import.boolean.invalid", new Object[] {currentLine, "Nouvelle molécule", Lists.newArrayList("oui", "non", "o", "n")}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }

            MoleculeImport moleculeImport = (MoleculeImport) moleculeImportRow.getBean();
            String nomMolecule = moleculeImport.getNomCommun().toUpperCase(Locale.ROOT);

            // Mandatory fields/relation
            if (nomMolecule == null) {
                String message = messages.getMessage("csv.import.molecule.empty", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                // Load
                boolean exists = dao.exists(Molecule.class, "nomCommun", nomMolecule);
                if (exists) {
                    existingMoleculesName.add(nomMolecule);
                }
            }

            // Retrieve campagne
            String nomCampagne = moleculeImport.getCampagne();
            Campagne campagne = campagnesByNom.get(nomCampagne.toUpperCase());
            if (campagne == null) {
                String message = messages.getMessage("csv.import.campagne.unknown", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve produit
            String refProduit = moleculeImport.getProduit();
            Produit produit = produitByRef.get(refProduit.toUpperCase());
            if (produit == null) {
                String message = messages.getMessage("csv.import.product.unknown", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            }

            //Check Nom iupca length
            if (moleculeImport.getNomIupca().length()>255) {
                String message = messages.getMessage("csv.import.molecule.nomIupca.tooLong", new Object[] {currentLine}, locale);
                importResult.addErrorLine(message);
            }

            Molecule molecule = moleculeParNom.get(nomMolecule);
            if(molecule == null) {
                molecule = new Molecule();
                molecule.setNomCommun(moleculeImport.getNomCommun());
                molecule.setFamilleChimique(moleculeImport.getFamilleChimique());

                // Formule développée au format SMILES
                if(moleculeImport.getFormuleDevMol() != null && !moleculeImport.getFormuleDevMol().isEmpty()) {
                    SmilesParser parser = new SmilesParser(SilentChemObjectBuilder.getInstance());
                    try {
                        IAtomContainer smileMolecule = parser.parseSmiles(moleculeImport.getFormuleDevMol());

                        StructureDiagramGenerator sdg = new StructureDiagramGenerator();
                        sdg.setMolecule(smileMolecule);
                        sdg.generateCoordinates();
                        smileMolecule = sdg.getMolecule();

                        StringWriter stringWriter = new StringWriter();
                        IChemObjectWriter writer = new MDLV2000Writer(stringWriter);

                        Properties prop = new Properties();
                        prop.setProperty("WriteAromaticBondTypes", "true");
                        PropertiesListener listener = new PropertiesListener(prop);
                        writer.addChemObjectIOListener(listener);
                        writer.write(smileMolecule);
                        writer.close();
                        molecule.setFormuleDevMol(stringWriter.toString());
                        stringWriter.close();
                    } catch (InvalidSmilesException e) {
                        String message = messages.getMessage("csv.import.molecule.invalidSmiles", new Object[] {currentLine}, locale);
                        importResult.addErrorLine(message);
                    } catch (CDKException | IOException e) {
                        String message = messages.getMessage("csv.import.molecule.smilesParsingException", new Object[] {currentLine}, locale);
                        importResult.addErrorLine(message);
                    }
                }
                molecule.setNomIupca(moleculeImport.getNomIupca());
                molecule.setFormuleBrute(moleculeImport.getFormuleBrute());
                molecule.setMasseMolaire(moleculeImport.getMasseMolaire());
                if(moleculeImport.getNouvMolecul() != null) {
                    molecule.setNouvMolecul(moleculeImport.getNouvMolecul());
                }
                molecule.setCampagne(campagne);
                molecule.setIdentifieePar(moleculeImport.getIdentifieePar());
                molecule.setPubliOrigine(moleculeImport.getPubliOrigine());
                molecule.setComplement(moleculeImport.getComplement());
                molecule.setCreateur(loadedUtilisateur);
                molecule.setProvenances(new ArrayList<>());
            } else {
                // Be sure molecule data are same
                if (molecule.getMasseMolaire() == null) {
                    molecule.setMasseMolaire(moleculeImport.getMasseMolaire());
                } else if (!molecule.getMasseMolaire().equals(moleculeImport.getMasseMolaire())) {
                    String message = messages.getMessage("csv.import.molecule.existing", new Object[] {currentLine, nomMolecule}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Provenances
            MoleculeProvenance provenance = new MoleculeProvenance();
            provenance.setMolecule(molecule);
            provenance.setProduit(produit);
            provenance.setPourcentage(moleculeImport.getPourcentage());

            molecule.getProvenances().add(provenance);


            Normalizer.normalize(MoleculeNormalizer.class, molecule);
            moleculeParNom.put(nomMolecule, molecule);

        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (Molecule molecule : moleculeParNom.values()) {
                moleculeService.createMolecule(molecule);
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        MoleculeImportModel model = new MoleculeImportModel();

        File headers = File.createTempFile("molecules", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
