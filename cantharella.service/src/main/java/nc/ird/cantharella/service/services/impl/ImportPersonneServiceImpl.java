package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.Lists;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.exceptions.ImportException;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.PersonneImport;
import nc.ird.cantharella.service.model.PersonneImportModel;
import nc.ird.cantharella.service.utils.normalizers.LotNormalizer;
import nc.ird.cantharella.service.utils.normalizers.PersonneNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
@Qualifier("importPersonneService")
public class ImportPersonneServiceImpl extends AbstractImportService {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImportPersonneServiceImpl.class);

    /**
     * Messages d'internationalisation
     */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /**
     * DAO
     */
    @Autowired
    private GenericDao dao;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        ImportResult importResult = new ImportResult();

        Map<String, Personne> personnesParRef = new HashMap<>();
        Map<String, Personne> personnesParNoms = new HashMap<>();
        List<String> existingPersonnesRef = new ArrayList<>();

        int currentLine = 0;
        for (ImportRow<T> personneImportRow : importData) {
            currentLine++;

            PersonneImport personneImport = (PersonneImport) personneImportRow.getBean();
            String refPersonne = personneImport.getCourriel();

            // Mandatory fields/relation
            if (refPersonne == null || refPersonne.isBlank()) {
                String message = messages.getMessage("csv.import.personne.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);

            }

            String nomPersonne = personneImport.getNom();
            if (nomPersonne == null || nomPersonne.isBlank()) {
                String message = messages.getMessage("csv.import.personne.nom.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            String prenomPersonne = personneImport.getPrenom();
            if (prenomPersonne == null || prenomPersonne.isBlank()) {
                String message = messages.getMessage("csv.import.personne.prenom.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            if (personnesParNoms.containsKey(nomPersonne + prenomPersonne)) {
                String message = messages.getMessage("csv.import.personne.nom.existing", new Object[]{currentLine, nomPersonne, prenomPersonne}, locale);
                importResult.addErrorLine(message);
            } else {
                DetachedCriteria CRITERIA_UNIQUE_NOM_PRENOM = DetachedCriteria
                        .forClass(Utilisateur.class)
                        .add(Restrictions.eq("nom", nomPersonne))
                        .add(Restrictions.eq("prenom", prenomPersonne));
                List<?> homonymes = dao.list(CRITERIA_UNIQUE_NOM_PRENOM);
                if (homonymes != null && !homonymes.isEmpty()) {
                    String message = messages.getMessage("csv.import.personne.nom.existing", new Object[]{currentLine, nomPersonne, prenomPersonne}, locale);
                    importResult.addErrorLine(message);
                }
            }
            String organisme = personneImport.getOrganisme();
            if (organisme == null || organisme.isBlank()) {
                String message = messages.getMessage("csv.import.organisme.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }

            String adresse = personneImport.getAdressePostale();
            if (adresse == null || adresse.isBlank()) {
                String message = messages.getMessage("csv.import.adresse.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            String codePostal = personneImport.getCodePostal();
            if (codePostal == null || codePostal.isBlank()) {
                String message = messages.getMessage("csv.import.codePostal.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            String ville = personneImport.getVille();
            if (ville == null || ville.isBlank()) {
                String message = messages.getMessage("csv.import.ville.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            String pays = personneImport.getPays();
            if (pays == null || pays.isBlank()) {
                String message = messages.getMessage("csv.import.pays.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }
            if (pays != null && pays.length() != 2) {
                String message = messages.getMessage("csv.import.pays.invalid", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            }


            try {
                Personne personne = personnesParRef.get(refPersonne);

                if (personne == null) {
                    personne = new Personne();
                    personne.setPrenom(personneImport.getPrenom());
                    personne.setNom(personneImport.getNom().toUpperCase(Locale.ROOT));
                    personne.setOrganisme(personneImport.getOrganisme());
                    personne.setFonction(personneImport.getFonction());
                    personne.setTel(personneImport.getTelephone());
                    personne.setFax(personneImport.getFax());
                    personne.setCourriel(personneImport.getCourriel());
                    personne.setAdressePostale(personneImport.getAdressePostale());
                    personne.setCodePostal(personneImport.getCodePostal());
                    personne.setVille(personneImport.getVille());
                    personne.setCodePays(personneImport.getPays());

                } else {

                    // Be sure name are same
                    if (personne.getNom() == null) {
                        personne.setNom(personneImport.getNom());
                    } else if (!personne.getNom().equals(personneImport.getNom())) {
                        String message = messages.getMessage("csv.import.personne.nom.different", new Object[]{currentLine, personneImport.getNom(), personne.getNom()}, locale);
                        throw new ImportException(message);
                    }
                    if (personne.getPrenom() == null) {
                        personne.setPrenom(personneImport.getPrenom());
                    } else if (!personne.getPrenom().equals(personneImport.getPrenom())) {
                        String message = messages.getMessage("csv.import.personne.prenom.different", new Object[]{currentLine, personneImport.getPrenom(), personne.getPrenom()}, locale);
                        throw new ImportException(message);
                    }
                }

                Normalizer.normalize(PersonneNormalizer.class, personne);
                personnesParRef.put(personne.getCourriel(), personne);
                personnesParNoms.put(nomPersonne + prenomPersonne, personne);

            } catch (ImportException e) {
                importResult.addErrorLine(e.getMessage());
            }
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (Personne personne : personnesParRef.values()) {
                try {
                    dao.create(personne);
                } catch (ConstraintViolationException eee) {
                    eee.getConstraintViolations().forEach(violation -> importResult.addErrorLine(violation.getPropertyPath() + " " + violation.getMessage()));
                }
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        PersonneImportModel model = new PersonneImportModel();

        File headers = File.createTempFile("personnes", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
