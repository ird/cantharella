package nc.ird.cantharella.service.services.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.PartieDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Partie;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.LotImport;
import nc.ird.cantharella.service.model.LotImportModel;
import nc.ird.cantharella.service.services.CampagneService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.SpecimenService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.service.utils.normalizers.LotNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Service
@Qualifier("importLotService")
public class ImportLotServiceImpl extends AbstractImportService {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImportLotServiceImpl.class);
    protected final String DATE_FORMAT = "d/MM/uuuu";

    /**
     * Messages d'internationalisation
     */
    @Resource(name = "serviceMessageSource")
    private MessageSourceAccessor messages;

    /**
     * DAO
     */
    @Autowired
    private GenericDao dao;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private CampagneService campagneService;

    @Autowired
    private StationService stationService;

    @Autowired
    private SpecimenService specimenService;

    @Override
    public <T> ImportResult importLines(Import2<T> importData, Utilisateur utilisateur, Locale locale) throws DataConstraintException {
        Utilisateur loadedUtilisateur;
        try {
            loadedUtilisateur = personneService.loadUtilisateur(utilisateur.getIdPersonne());
        } catch (DataNotFoundException e) {
            throw new RuntimeException("Trying to import from a not existing user, should never happen", e);
        }

        ImportResult importResult = new ImportResult();

        Map<String, Lot> lotsParRef = new HashMap<>();
        List<String> existingLotsRef = new ArrayList<>();

        List<Campagne> campagnes = campagneService.listCampagnes(loadedUtilisateur);
        Map<String, Campagne> campagnesByName = Maps.uniqueIndex(campagnes, Campagne::getNom);

        List<Station> stations = stationService.listStations(loadedUtilisateur);
        Map<String, Station> stationsByName = Maps.uniqueIndex(stations, Station::getNom);

        List<Specimen> specimens = specimenService.listSpecimens(loadedUtilisateur);
        Map<String, Specimen> specimensByRef = Maps.uniqueIndex(specimens, Specimen::getRef);

        List<Partie> parties = (List<Partie>) dao.list(PartieDao.getCriteriaFindAllParties());
        Map<String, Partie> partiesByName = Maps.uniqueIndex(parties, Partie::getNom);


        int currentLine = 0;
        for (ImportRow<T> lotImportRow : importData) {
            currentLine++;

            boolean valid = lotImportRow.isValid();
            if (!valid) {
                Set<AbstractImportErrorInfo<T>> errors = lotImportRow.getErrors();
                for (AbstractImportErrorInfo<T> error : errors) {

                    ImportableColumn<T, Object> field = error.getField();
                    String headerName = field.getHeaderName();
                    if (headerName.equalsIgnoreCase("Masse fraîche")
                            || headerName.equalsIgnoreCase("Masse sèche")) {
                        String message = messages.getMessage("csv.import.numeric.invalid", new Object[]{currentLine, headerName}, locale);
                        importResult.addErrorLine(message);
                    } else if (headerName.equalsIgnoreCase("Ech Collection")
                            || headerName.equalsIgnoreCase("Ech Identification")
                            || headerName.equalsIgnoreCase("Ech phylogénie")) {
                        String message = messages.getMessage("csv.import.boolean.invalid", new Object[]{currentLine, headerName, Lists.newArrayList("oui", "non", "o", "n")}, locale);
                        importResult.addErrorLine(message);
                    }
                }
            }

            LotImport lotImport = (LotImport) lotImportRow.getBean();
            String refLot = lotImport.getRef();

            // Mandatory fields/relation
            if (refLot == null || refLot.isBlank()) {
                String message = messages.getMessage("csv.import.lot.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else if (lotsParRef.containsKey(refLot) || existingLotsRef.contains(refLot)) {
                // Already loaded
                String message = messages.getMessage("csv.import.lot.existing", new Object[]{currentLine, refLot}, locale);
                importResult.addErrorLine(message);
            } else {
                // Load
                boolean exists = dao.exists(Lot.class, "ref", refLot);
                if (exists) {
                    String message = messages.getMessage("csv.import.lot.existing", new Object[]{currentLine, refLot}, locale);
                    importResult.addErrorLine(message);
                    existingLotsRef.add(refLot);
                }
            }

            // Parse date récolte
            Date parsedDate = null;
            try {
                LocalDate date = LocalDate.parse(lotImport.getDateRecolte(), DateTimeFormatter.ofPattern(DATE_FORMAT).withResolverStyle(ResolverStyle.STRICT));
                parsedDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
                if (date.isAfter(LocalDate.now())) {
                    String message = messages.getMessage("csv.import.date.future", new Object[]{currentLine}, locale);
                    importResult.addErrorLine(message);
                }
            } catch (DateTimeParseException e) {
                String message = messages.getMessage("csv.import.date.invalid", new Object[]{currentLine, lotImport.getDateRecolte()}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve campagne
            String campagneCible = lotImport.getCampagne();
            Campagne campagne = null;
            if (StringUtils.isBlank(campagneCible)) {
                String message = messages.getMessage("csv.import.campagne.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                campagne = campagnesByName.get(campagneCible.toUpperCase());
                if (campagne == null) {
                    String message = messages.getMessage("csv.import.campagne.unknown", new Object[]{currentLine, campagneCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Retrieve station
            String stationCible = lotImport.getStation();
            Station station = null;
            if (StringUtils.isBlank(stationCible)) {
                String message = messages.getMessage("csv.import.station.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                station = stationsByName.get(stationCible.toUpperCase());
                if (station == null) {
                    String message = messages.getMessage("csv.import.station.unknown", new Object[]{currentLine, stationCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            //check if station is in campagne or else no right to import
            if (campagne != null && station != null && !campagne.getStations().contains(station)) {
                String message = messages.getMessage("csv.import.station.not.in.campagne", new Object[]{currentLine, stationCible, campagneCible}, locale);
                importResult.addErrorLine(message);
            }

            // Retrieve specimen
            String specimenCible = lotImport.getSpecimenRef();
            Specimen specimen = null;
            if (StringUtils.isBlank(specimenCible)) {
                String message = messages.getMessage("csv.import.specimen.empty", new Object[]{currentLine}, locale);
                importResult.addErrorLine(message);
            } else {
                specimen = specimensByRef.get(specimenCible.toUpperCase());
                if (specimen == null) {
                    String message = messages.getMessage("csv.import.specimen.unknown", new Object[]{currentLine, specimenCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            // Retrieve partie
            String partieCible = lotImport.getPartie();
            Partie partie = null;
            if (!StringUtils.isBlank(partieCible)) {
                partie = partiesByName.get(partieCible);
                if (partie == null) {
                    String message = messages.getMessage("csv.import.partie.unknown", new Object[]{currentLine, partieCible}, locale);
                    importResult.addErrorLine(message);
                }
            }

            Lot lot = lotsParRef.get(refLot);
            if (lot == null) {
                lot = new Lot();
                lot.setCampagne(campagne);
                lot.setStation(station);
                lot.setDateRecolte(parsedDate);
                lot.setRef(lotImport.getRef());
                lot.setSpecimenRef(specimen);
                lot.setPartie(partie);
                lot.setMasseFraiche(lotImport.getMasseFraiche());
                lot.setMasseSeche(lotImport.getMasseSeche());
                if (lotImport.getEchantillonColl() != null) {
                    lot.setEchantillonColl(lotImport.getEchantillonColl());
                }
                if (lotImport.getEchantillonIdent() != null) {
                    lot.setEchantillonIdent(lotImport.getEchantillonIdent());
                }
                if (lotImport.getEchantillonPhylo() != null) {
                    lot.setEchantillonPhylo(lotImport.getEchantillonPhylo());
                }
                lot.setComplement(lotImport.getComplement());
                lot.setCreateur(loadedUtilisateur);
            } else {
                // Be sure name are same
                if (lot.getRef() == null) {
                    lot.setRef(lotImport.getRef());
                } else if (!lot.getRef().equals(lotImport.getRef())) {
                    String message = messages.getMessage("csv.import.lot.ref.different", new Object[]{currentLine, lotImport.getRef()}, locale);
                    importResult.addErrorLine(message);
                }
            }

            Normalizer.normalize(LotNormalizer.class, lot);
            lotsParRef.put(lotImport.getRef(), lot);
        }

        if (importResult.isValid()) {
            int nbImported = 0;
            for (Lot lot : lotsParRef.values()) {
                dao.create(lot);
                nbImported++;
            }
            importResult.setNbImported(nbImported);
        }

        return importResult;
    }

    public File exportHeader() throws Exception {
        LotImportModel model = new LotImportModel();

        File headers = File.createTempFile("lots", ".csv");
        Export.exportToFile(model, Lists.newArrayList(), headers);

        return headers;
    }

}
