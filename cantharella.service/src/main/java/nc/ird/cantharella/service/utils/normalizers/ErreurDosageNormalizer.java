package nc.ird.cantharella.service.utils.normalizers;

import nc.ird.cantharella.data.model.ErreurDosage;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.AssertTools;

public class ErreurDosageNormalizer  extends Normalizer<ErreurDosage> {

    /** {@inheritDoc} */
    @Override
    protected ErreurDosage normalize(ErreurDosage erreurDosage) {
        AssertTools.assertNotNull(erreurDosage);
        // Unique fields
        erreurDosage.setNom(Normalizer.normalize(ConfigNameNormalizer.class, erreurDosage.getNom()));
        return erreurDosage;
    }
}
