package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class SpecimenImportModel extends AbstractImportExportModel<SpecimenImport> {

    public static final char CSV_SEPARATOR = ';';

    public SpecimenImportModel() {
        super(CSV_SEPARATOR);

        // "Référence";
        newMandatoryColumn("Référence", "reference");
        // "Embranchement";
        newMandatoryColumn("Embranchement", "embranchement");
        // "Famille";
        newOptionalColumn("Famille", "famille");
        // "Genre";
        newOptionalColumn("Genre", "genre");
        // "Espèce";
        newOptionalColumn("Espèce", "espece");
        // "Sous-espèce";
        newOptionalColumn("Sous-espèce", "sousEspece");
        // "Variété";
        newOptionalColumn("Variété", "variete");
        // "Type";
        newOptionalColumn("Type", "typeOrganisme", ImportParser.TYPE_ORGANISME_PARSER);
        // "Nom identificateur"
        newOptionalColumn("Nom identificateur", "nomIdentificateur");
        // "Prénom identificateur"
        newOptionalColumn("Prénom identificateur", "prenomIdentificateur");
        // "Station provenance"
        newOptionalColumn("Station provenance", "station");
        // "Complément"
        newOptionalColumn("Complément", "complement");
        // "Depot Numero"
        newOptionalColumn("Depot Numéro", "numDepot");
        // "Depot date"
        newOptionalColumn("Depot date", "dateDepot");
        // "Depot lieu"
        newOptionalColumn("Depot lieu", "lieuDepot");

        // Column for export

        // "Référence";
        newColumnForExport("Référence", "reference");
        // "Embranchement";
        newColumnForExport("Embranchement", "embranchement");
        // "Famille";
        newColumnForExport("Famille", "famille");
        // "Genre";
        newColumnForExport("Genre", "genre");
        // "Espèce";
        newColumnForExport("Espèce", "espece");
        // "Sous-espèce";
        newColumnForExport("Sous-espèce", "sousEspece");
        // "Variété";
        newColumnForExport("Variété", "variete");
        // "Type";
        newColumnForExport("Type", "typeOrganisme");
        // "Nom identificateur"
        newColumnForExport("Nom identificateur", "nomIdentificateur");
        // "Prénom identificateur"
        newColumnForExport("Prénom identificateur", "prenomIdentificateur");
        // "Station provenance"
        newColumnForExport("Station provenance", "station");
        // "Complément"
        newColumnForExport("Complément", "complement");
        // "Depot Numero"
        newColumnForExport("Depot Numéro", "numDepot");
        // "Depot date"
        newColumnForExport("Depot date", "dateDepot");
        // "Depot lieu"
        newColumnForExport("Depot lieu", "lieuDepot");

    }

    @Override
    public SpecimenImport newEmptyInstance() {
        return new SpecimenImport();
    }


}
