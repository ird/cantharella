package nc.ird.cantharella.service.model;

import org.nuiton.csv.ext.AbstractImportExportModel;

public class LotImportModel extends AbstractImportExportModel<LotImport> {

    public static final char CSV_SEPARATOR = ';';

    public LotImportModel() {
        super(CSV_SEPARATOR);

        // "Campagne";
        newMandatoryColumn("Campagne", "campagne");
        // "Station";
        newMandatoryColumn("Station", "station");
        // "Date récolte";
        newMandatoryColumn("Date récolte", "dateRecolte");
        // "Référence";
        newMandatoryColumn("Référence", "ref");
        // "Spécimen de référence";
        newMandatoryColumn("Spécimen de référence", "specimenRef");
        // "Partie";
        newOptionalColumn("Partie", "partie");
        // "Masse fraîche";
        newOptionalColumn("Masse fraîche", "masseFraiche", ImportParser.BIG_DECIMAL_PARSER);
        // "Masse sèche";
        newOptionalColumn("Masse sèche", "masseSeche", ImportParser.BIG_DECIMAL_PARSER);
        // "Ech Collection";
        newOptionalColumn("Ech Collection", "echantillonColl", ImportParser.BOOLEAN_PARSER);
        // "Ech Identification";
        newOptionalColumn("Ech Identification", "echantillonIdent", ImportParser.BOOLEAN_PARSER);
        // "Ech phylogénie";
        newOptionalColumn("Ech phylogénie", "echantillonPhylo", ImportParser.BOOLEAN_PARSER);
        // "Complément";
        newOptionalColumn("Complément", "complement");

        // Column for export

        // "Campagne";
        newColumnForExport("Campagne", "campagne");
        // "Station";
        newColumnForExport("Station", "station");
        // "Date récolte";
        newColumnForExport("Date récolte", "dateRecolte");
        // "Référence";
        newColumnForExport("Référence", "ref");
        // "Spécimen de référence";
        newColumnForExport("Spécimen de référence", "specimenRef");
        // "Partie";
        newColumnForExport("Partie", "partie");
        // "Masse fraîche";
        newColumnForExport("Masse fraîche", "masseFraiche");
        // "Masse sèche";
        newColumnForExport("Masse sèche", "masseSeche");
        // "Ech Collection";
        newColumnForExport("Ech Collection", "echantillonColl");
        // "Ech Identification";
        newColumnForExport("Ech Identification", "echantillonIdent");
        // "Ech phylogénie";
        newColumnForExport("Ech phylogénie", "echantillonPhylo");
        // "Complément";
        newColumnForExport("Complément", "complement");



    }


    @Override
    public LotImport newEmptyInstance() {
        return new LotImport();
    }
}
