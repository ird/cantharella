package nc.ird.cantharella.service.services.impl;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.dao.impl.PersonneInterrogeeDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.exceptions.UnexpectedException;
import nc.ird.cantharella.data.model.*;
import nc.ird.cantharella.service.services.CampagneService;
import nc.ird.cantharella.service.services.PersonneInterrogeeService;
import nc.ird.cantharella.service.services.RemedeService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.service.utils.normalizers.PersonneInterrogeeNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalize;
import nc.ird.cantharella.utils.AssertTools;
import nc.ird.cantharella.utils.BeanTools;
import nc.ird.cantharella.utils.CollectionTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class PersonneInterrogeeServiceImpl implements PersonneInterrogeeService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(PersonneInterrogeeServiceImpl.class);

    /** DAO */
    @Autowired
    private GenericDao dao;

    @Autowired
    StationService stationService;

    @Autowired
    CampagneService campagneService;

    @Autowired
    RemedeService remedeService;

    /** {@inheritDoc} */
    @Override
    public void createPersonneInterrogee(@Normalize(PersonneInterrogeeNormalizer.class) PersonneInterrogee personneInterrogee) throws DataConstraintException {
        LOG.info("createPersonneInterrogee: " + personneInterrogee.getNom());
        dao.create(personneInterrogee);
    }

    /** {@inheritDoc} */
    @Override
    public void deletePersonneInterrogee(PersonneInterrogee personneInterrogee) throws DataConstraintException {
        AssertTools.assertNotNull(personneInterrogee);
        LOG.info("deletePersonneInterrogee: " + personneInterrogee.getIdPersonne());
        try {
            dao.delete(personneInterrogee);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    /** {@inheritDoc} */
    @Override
    public PersonneInterrogee loadPersonneInterrogee(Integer idPersonneInterrogee) throws DataNotFoundException {
        return dao.read(PersonneInterrogee.class, idPersonneInterrogee);
    }

    /** {@inheritDoc} */
    @Override
    public void updatePersonneInterrogee(@Normalize(PersonneInterrogeeNormalizer.class) PersonneInterrogee personneInterrogee) throws DataConstraintException {
        LOG.info("updatePersonneInterrogee: " + personneInterrogee.getIdPersonne());
        try {
            dao.update(personneInterrogee);
        } catch (DataNotFoundException e) {
            LOG.error(e.getMessage(), e);
            throw new UnexpectedException(e);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void refreshPersonneInterrogee(PersonneInterrogee personneInterrogee){
        AssertTools.assertNotNull(personneInterrogee);
        dao.refresh(personneInterrogee);
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public List<PersonneInterrogee> listPersonneInterrogee(Utilisateur utilisateur) {
        AssertTools.assertNotNull(utilisateur);
        if (utilisateur.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR) {
            // si admin, on ajoute toutes les testBios de la base
            return (List<PersonneInterrogee>) dao.list(PersonneInterrogeeDao.CRITERIA_LIST_RESULTATS);
        }
        // gestion des droits en plus pour les utilisateurs
        return new ArrayList<>(listPersonneInterrogeeForUser(utilisateur));
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public List<PersonneInterrogee> listPersonneInterrogeeForCampagne(Utilisateur utilisateur, Campagne campagne) {
        AssertTools.assertNotNull(utilisateur);
        if (utilisateur.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR) {
            return (List<PersonneInterrogee>) dao.list(PersonneInterrogeeDao.getCriteriaListPersonneInterrogeeForCampagne(campagne));
        } else {
            List<Campagne> userCampagnes = campagneService.listCampagnes(utilisateur);

            if (userCampagnes.contains(campagne)) {
                Set<PersonneInterrogee> personnesInterrogees = listPersonneInterrogeeForUser(utilisateur);
                List<PersonneInterrogee> resultats = new ArrayList<>();
                for (PersonneInterrogee curRes : personnesInterrogees) {
                    if (curRes.getCampagne().equals(campagne)) {
                        resultats.add(curRes);
                    }
                }
                return resultats;
            } else {
                return new ArrayList<>();
            }
        }
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public SortedSet<PersonneInterrogee> listPersonneInterrogeeForUser(Utilisateur utilisateur) {
        SortedSet<PersonneInterrogee> resultats = new TreeSet<>();

        // liste triée par produit afin d'optimiser
        List<PersonneInterrogee> allResultTests = (List<PersonneInterrogee>) dao
                .list(PersonneInterrogeeDao.CRITERIA_LIST_RESULTATS);

        Set<Remede> userRemedes = remedeService.listRemedeForUser(utilisateur);

        for (PersonneInterrogee curRes : allResultTests) {
            if( Objects.equals(utilisateur.getIdPersonne(), curRes.getCreateur().getIdPersonne())
                    || Objects.equals(curRes.getCampagne().getAdministrateur().getIdPersonne(), utilisateur.getIdPersonne())
                    || Objects.equals(curRes.getCampagne().getCreateur().getIdPersonne(), utilisateur.getIdPersonne())) {
                resultats.add(curRes);
            } else if (utilisateur.getStationsCrees().contains(curRes.getStation())) {
                resultats.add(curRes);
            } else {
                for (Remede remede : userRemedes) {
                    if (remede.getPrescripteur() != null && remede.getPrescripteur().equals(curRes)) {
                        resultats.add(curRes);
                    }
                }
            }
        }

        return resultats;
    }

    /** {@inheritDoc} */
    @Override
    public boolean updateOrdeletePersonneInterrogeeEnabled(PersonneInterrogee personneInterrogee, Utilisateur utilisateur) {
        return utilisateur.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR
                || Objects.equals(utilisateur.getIdPersonne(), personneInterrogee.getCreateur().getIdPersonne())
                || Objects.equals(personneInterrogee.getCampagne().getAdministrateur().getIdPersonne(),
                        utilisateur.getIdPersonne())
                || Objects.equals(personneInterrogee.getCampagne().getCreateur().getIdPersonne(),
                        utilisateur.getIdPersonne());
    }

    @Override
    public boolean isPersonneInterrogeeAccessibleByUser(PersonneInterrogee personneInterrogee, Utilisateur utilisateur) {

        Utilisateur loadedFromDbUser;
        try {
            loadedFromDbUser = dao.read(Utilisateur.class, utilisateur.getIdValue());
        } catch (DataNotFoundException dnfe) {
            LOG.error("User not present in db, should never happen");
            return false;
        }

        // si administrateur ou créateur, accès ok
        if (loadedFromDbUser.getTypeDroit() == Utilisateur.TypeDroit.ADMINISTRATEUR || loadedFromDbUser.equals(personneInterrogee.getCreateur())) {
            return true;
        }
        // accessible si l'utilisateur a créé la campagne
        if (loadedFromDbUser.getCampagnesCreees().contains(personneInterrogee.getCampagne())) {
            return true;
        }

        // accessible si l'utilisateur est administrateur de la campagne
        if (loadedFromDbUser.getCampagnesAdministrees().contains(personneInterrogee.getCampagne())) {
            return true;
        }

        // Accessible si a accès à la campagne
        if (CollectionTools.containsWithValue(loadedFromDbUser.getCampagnesDroits().keySet(), "idCampagne",
                BeanTools.AccessType.GETTER, personneInterrogee.getCampagne().getIdCampagne())) {
            return true;
        }

        AtomicBoolean returnValue = new AtomicBoolean(false);

        // Accessible si la personne interrogée est dans un remède sur lequel on a les droits
        loadedFromDbUser.getRemedesDroits().keySet().forEach(remede -> {
            if (remedeService.isRemedePersonneInterrogeeAccessibleByUser(remede, personneInterrogee, utilisateur)) {
                returnValue.set(true);
            }
        });

        // pas d'accès sinon
        return returnValue.get();
    }
}
