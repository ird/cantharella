package nc.ird.cantharella.service.utils;

import org.junit.Assert;
import org.junit.Test;

public class CoordinateUtilsTest {

    @Test
    public void testLatitudeConversion() {
        //One north
        Assert.assertEquals(25.5963833, CoordinatesUtil.parseLatitude("25°35.783'N"), 0.0000001);
        Assert.assertEquals(25.5963833, CoordinatesUtil.parseLatitude("25° 35.783'N"), 0.0000001);
        Assert.assertEquals(25.5963833, CoordinatesUtil.parseLatitude("25°35.783' N"), 0.0000001);
        Assert.assertEquals(25.5963833, CoordinatesUtil.parseLatitude("25° 35.783' N"), 0.0000001);

        //One south
        Assert.assertEquals(-25.5963833, CoordinatesUtil.parseLatitude("25°35.783'S"), 0.0000001);
        Assert.assertEquals(-25.5963833, CoordinatesUtil.parseLatitude("25° 35.783'S"), 0.0000001);
        Assert.assertEquals(-25.5963833, CoordinatesUtil.parseLatitude("25°35.783' S"), 0.0000001);
        Assert.assertEquals(-25.5963833, CoordinatesUtil.parseLatitude("25° 35.783' S"), 0.0000001);

        //Space before and after
        Assert.assertEquals( -9.2443667, CoordinatesUtil.parseLatitude(" 9°14.662'S"), 0.0000001);
        Assert.assertEquals( -9.2443667, CoordinatesUtil.parseLatitude("9°14.662'S "), 0.0000001);

        //59 minutes
        Assert.assertEquals( -8.9981667, CoordinatesUtil.parseLatitude("8°59.890'S"), 0.0000001);



        //One with negative deg value
        try {
            CoordinatesUtil.parseLatitude("-25°35.783'S");
            Assert.fail("Exception should have been thrown");
        } catch (IllegalArgumentException e) {
            //Exception correctly thrown
        }

        //One with too big deg value
        try {
            CoordinatesUtil.parseLatitude("99°35.783'S");
            Assert.fail("Exception should have been thrown");
        } catch (IllegalArgumentException e) {
            //Exception correctly thrown
        }

        //One with too big min value
        try {
            CoordinatesUtil.parseLatitude("25°60.783'S");
            Assert.fail("Exception should have been thrown");
        } catch (IllegalArgumentException e) {
            //Exception correctly thrown
        }
    }

    @Test
    public void testLongitudeConversion() {
        //One east
        Assert.assertEquals(160.0266667, CoordinatesUtil.parseLongitude("160°01.600'E"), 0.0000001);
        Assert.assertEquals(160.0266667, CoordinatesUtil.parseLongitude("160° 01.600'E"), 0.0000001);
        Assert.assertEquals(160.0266667, CoordinatesUtil.parseLongitude("160°01.600' E"), 0.0000001);
        Assert.assertEquals(160.0266667, CoordinatesUtil.parseLongitude("160° 01.600' E"), 0.0000001);

        //One south
        Assert.assertEquals(-160.0266667, CoordinatesUtil.parseLongitude("160°01.600'W"), 0.0000001);
        Assert.assertEquals(-160.0266667, CoordinatesUtil.parseLongitude("160° 01.600'W"), 0.0000001);
        Assert.assertEquals(-160.0266667, CoordinatesUtil.parseLongitude("160°01.600' W"), 0.0000001);
        Assert.assertEquals(-160.0266667, CoordinatesUtil.parseLongitude("160° 01.600' W"), 0.0000001);

        //Space before and after
        Assert.assertEquals(160.0266667, CoordinatesUtil.parseLongitude(" 160°01.600'E"), 0.0000001);
        Assert.assertEquals(160.0266667, CoordinatesUtil.parseLongitude("160°01.600'E "), 0.0000001);

        //59 minutes
        Assert.assertEquals( 160.9933333, CoordinatesUtil.parseLongitude("160°59.600'E"), 0.0000001);



        //One with negative deg value
        try {
            CoordinatesUtil.parseLongitude("-160°59.600'E");
            Assert.fail("Exception should have been thrown");
        } catch (IllegalArgumentException e) {
            //Exception correctly thrown
        }

        //One with too big deg value
        try {
            CoordinatesUtil.parseLongitude("181°59.600'E");
            Assert.fail("Exception should have been thrown");
        } catch (IllegalArgumentException e) {
            //Exception correctly thrown
        }

        //One with too big min value
        try {
            CoordinatesUtil.parseLongitude("160°60.600'E");
            Assert.fail("Exception should have been thrown");
        } catch (IllegalArgumentException e) {
            //Exception correctly thrown
        }

    }
}
