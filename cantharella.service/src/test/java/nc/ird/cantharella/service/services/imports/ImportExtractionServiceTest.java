package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ExtractionImportModel;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.services.ExtractionService;
import nc.ird.cantharella.service.services.ImportService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;

public class ImportExtractionServiceTest extends AbstractServiceTest {

    @Autowired
    private ExtractionService extractionService;

    @Autowired
    private ImportDataInitService importDataInitService;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("importExtractionService")
    private ImportService importExtractionService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importExtraction() {

        long nbInitExtractions = extractionService.countExtractions();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/extractions/extractionsOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importExtractionService.importData(fileStream, utilisateur, Locale.FRENCH, new ExtractionImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new extractions are present
        long nbExtractionsAfterImport = extractionService.countExtractions();
        Assert.assertNotEquals(nbInitExtractions, nbExtractionsAfterImport);
        Assert.assertTrue(nbExtractionsAfterImport == nbInitExtractions + 1);

        // Clean data
        try {
            Extraction extraction = extractionService.loadExtraction("DDD-EEE");
            extractionService.deleteExtraction(extraction);
        } catch (DataNotFoundException e) {
            Assert.fail("Extraction not found");
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }
    }

    // Import extractions avec plusieurs extraits
    @Test
    @Transactional
    public void importMultipleExtracts() {

        long nbInitExtractions = extractionService.countExtractions();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/extractions/extractionsMultiplesOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importExtractionService.importData(fileStream, utilisateur, Locale.FRENCH, new ExtractionImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new extractions are present
        long nbExtractionsAfterImport = extractionService.countExtractions();
        Assert.assertNotEquals(nbInitExtractions, nbExtractionsAfterImport);
        Assert.assertTrue(nbExtractionsAfterImport == nbInitExtractions + 1);

        List<Extraction> extractions = extractionService.listExtractions(utilisateur);
        for (Extraction extraction : extractions) {
            if ("EXTRACT1".equals(extraction.getRef())) {
                // Check that all extracts are present
                Assert.assertNotNull(extraction.getExtraits());
                Assert.assertFalse(extraction.getExtraits().isEmpty());
                Assert.assertEquals(2, extraction.getExtraits().size());

            }
        }
    }

    // Extractions avec deux fois le même type d'extrait (erreur)
    @Test
    @Transactional
    public void importMultipleExtractsWithSameType() {

        long nbInitExtractions = extractionService.countExtractions();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/extractions/extractionsMultiplesKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importExtractionService.importData(fileStream, utilisateur, Locale.FRENCH, new ExtractionImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertNotEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new extractions are present
        long nbExtractionsAfterImport = extractionService.countExtractions();
        Assert.assertEquals(nbInitExtractions, nbExtractionsAfterImport);
    }

    // Extractions avec deux masses à extraire différentes (erreur)
    @Test
    @Transactional
    public void importMultipleExtractsWithDifferentMass() {

        long nbInitExtractions = extractionService.countExtractions();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/extractions/extractionsMultiplesMasseKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importExtractionService.importData(fileStream, utilisateur, Locale.FRENCH, new ExtractionImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getErrorLines().size());
            Assert.assertNotEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new extractions are present
        long nbExtractionsAfterImport = extractionService.countExtractions();
        Assert.assertEquals(nbInitExtractions, nbExtractionsAfterImport);
    }

    // Données invalides
    // - ref lot absent
    // - masse à extraire invalide
    // - méthode d'extraction inconnue
    // - type d'extrait inconnu
    // - masse extrait invalide
    // - manipulateur inconnu
    // - date invalide
    @Test
    @Transactional
    public void importExtractionsWithErrors() {

        long nbInitExtractions = extractionService.countExtractions();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/extractions/extractionsKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importExtractionService.importData(fileStream, utilisateur, Locale.FRENCH, new ExtractionImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(7, result.getErrorLines().size());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new extractions are present
        long nbExtractionsAfterImport = extractionService.countExtractions();
        Assert.assertEquals(nbInitExtractions, nbExtractionsAfterImport);
    }
}
