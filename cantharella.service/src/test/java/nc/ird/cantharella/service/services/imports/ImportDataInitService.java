package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.Extrait;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.MethodeDosage;
import nc.ird.cantharella.data.model.MethodeExtraction;
import nc.ird.cantharella.data.model.MethodePurification;
import nc.ird.cantharella.data.model.MethodeTestBio;
import nc.ird.cantharella.data.model.Partie;
import nc.ird.cantharella.data.model.Purification;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.TypeExtrait;
import nc.ird.cantharella.data.model.UniteConcMasse;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.services.CampagneService;
import nc.ird.cantharella.service.services.ExtractionService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.service.services.MoleculeService;
import nc.ird.cantharella.service.services.PersonneService;
import nc.ird.cantharella.service.services.ProduitService;
import nc.ird.cantharella.service.services.SpecimenService;
import nc.ird.cantharella.service.services.StationService;
import nc.ird.cantharella.service.utils.normalizers.PersonneNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.PasswordTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ImportDataInitService {

    @Autowired
    private PersonneService personneService;

    @Autowired
    private MoleculeService moleculeService;

    @Autowired
    private CampagneService campagneService;

    @Autowired
    private ProduitService produitService;

    @Autowired
    private StationService stationService;

    @Autowired
    private SpecimenService specimenService;

    @Autowired
    private ExtractionService extractionService;

    @Autowired
    private LotService lotService;

    @Autowired
    private GenericDao dao;

    @Transactional
    public Utilisateur initData() throws DataConstraintException {
        Utilisateur utilisateur = null;
        try {
            utilisateur = personneService.loadUtilisateur("testimport@test.com");
        } catch (DataNotFoundException e) {
            utilisateur = new Utilisateur();
            utilisateur.setTypeDroit(Utilisateur.TypeDroit.ADMINISTRATEUR);
            utilisateur.setAdressePostale("BP A5");
            utilisateur.setCodePostal("44000");
            utilisateur.setCourriel("testimport@test.com");
            utilisateur.setNom("ADMIN_IMPORT");
            utilisateur.setOrganisme("IRD");
            utilisateur.setPasswordHash(PasswordTools.sha1("test"));
            utilisateur.setCodePays("FR");
            utilisateur.setPrenom("Admin");
            utilisateur.setVille("Nantes");
            Normalizer.normalize(PersonneNormalizer.class, utilisateur);

            // fix user creation for test
            utilisateur.setValide(Boolean.TRUE);
            personneService.createPersonne(utilisateur);
        }

        // station
        Station station = new Station();
        station.setNom("STATION_IMPORT");
        station.setCodePays("FR");
        station.setCreateur(utilisateur);
        stationService.createStation(station);

        // campagne
        Campagne campagne = new Campagne();
        campagne.setNom("CAMPAGNE_IMPORT");
        campagne.setDateDeb(new Date());
        campagne.setDateFin(new Date());
        campagne.setCreateur(utilisateur);
        campagne.setCodePays("FR");
        campagne.setStations(List.of(station));
        campagneService.createCampagne(campagne);

        // create some specimens
        Specimen specimen1 = new Specimen();
        specimen1.setRef("IMPORT");
        specimen1.setEmbranchement("Porifera");
        specimen1.setTypeOrganisme(Specimen.TypeOrganisme.PLANTE);
        specimen1.setCreateur(utilisateur);
        specimenService.createSpecimen(specimen1);

        // create some lots
        Lot lot1 = new Lot();
        lot1.setRef("LOT_IMPORT");
        lot1.setStation(station);
        lot1.setEchantillonPhylo(false);
        lot1.setEchantillonColl(false);
        lot1.setEchantillonIdent(true);
        lot1.setCampagne(campagne);
        lot1.setDateRecolte(new Date());
        lot1.setSpecimenRef(specimen1);
        lot1.setCreateur(utilisateur);
        lotService.createLot(lot1);

        MethodeExtraction methode = new MethodeExtraction();
        methode.setNom("METHODE_IMPORT");
        methode.setDescription("Blabla");
        List<TypeExtrait> typesExtrait = new ArrayList<>();
        TypeExtrait typeExtrait = new TypeExtrait();
        typeExtrait.setInitiales("TYPE1");
        typeExtrait.setMethodeExtraction(methode);
        typeExtrait.setDescription("Blabla");
        typesExtrait.add(typeExtrait);
        TypeExtrait typeExtrait2 = new TypeExtrait();
        typeExtrait2.setInitiales("TYPE2");
        typeExtrait2.setMethodeExtraction(methode);
        typeExtrait2.setDescription("Blabla");
        typesExtrait.add(typeExtrait2);
        methode.setTypesEnSortie(typesExtrait);
        extractionService.createMethodeExtraction(methode);

        Extraction extraction = new Extraction();
        extraction.setRef("IMPORT");
        extraction.setDate(new Date());
        extraction.setLot(lot1);
        extraction.setManipulateur(utilisateur);
        extraction.setCreateur(utilisateur);
        extraction.setMethode(methode);

        Extrait extrait = new Extrait();
        extrait.setExtraction(extraction);
        extrait.setRef("EXTRAIT_IMPORT_1");
        extrait.setTypeExtrait(typeExtrait);
        List<Extrait> listExtraits = new ArrayList<>();
        listExtraits.add(extrait);

        Extrait extrait2 = new Extrait();
        extrait2.setExtraction(extraction);
        extrait2.setRef("EXTRAIT_IMPORT_2");
        extrait2.setTypeExtrait(typeExtrait);
        listExtraits.add(extrait2);

        Extrait extrait3 = new Extrait();
        extrait3.setExtraction(extraction);
        extrait3.setRef("EXTRAIT_IMPORT_3");
        extrait3.setTypeExtrait(typeExtrait);
        listExtraits.add(extrait3);

        extraction.setExtraits(listExtraits);
        extractionService.createExtraction(extraction);

        Purification purification = new Purification();
        purification.setDate(new Date());
        purification.setRef("PURIFICATION_IMPORT");
        purification.setProduit(extrait);

        Partie partie = new Partie();
        partie.setNom("PARTIE_IMPORT");
        dao.create(partie);

        MethodePurification methodePurification = new MethodePurification();
        methodePurification.setNom("METHODE_IMPORT");
        methodePurification.setDescription("Blabla");
        dao.create(methodePurification);

        MethodeTestBio methodeTestBio = new MethodeTestBio();
        methodeTestBio.setNom("METHODE_IMPORT");
        methodeTestBio.setDescription("Blabla");
        methodeTestBio.setCible("METHODE_IMPORT");
        methodeTestBio.setDomaine("Domaine import");
        methodeTestBio.setUniteResultat("mm");
        methodeTestBio.setValeurMesuree("un truc");
        dao.create(methodeTestBio);

        MethodeDosage methodeDosage = new MethodeDosage();
        methodeDosage.setNom("METHODE_IMPORT");
        methodeDosage.setDescription("Blabla");
        methodeDosage.setAcronyme("METHODE_IMPORT");
        methodeDosage.setDomaine("Domaine import");
        methodeDosage.setUnite("mm");
        methodeDosage.setValeurMesuree("un truc");
        dao.create(methodeDosage);

        UniteConcMasse unite = new UniteConcMasse();
        unite.setIdUnite(4);
        unite.setCode("MICROG_ML");
        unite.setValeur("µg/ml");
        dao.create(unite);

        return utilisateur;
    }

    public FileInputStream loadFile(String filePath) throws FileNotFoundException {
        URL url = this.getClass().getResource(filePath);
        File file = new File(url.getFile());

        FileInputStream fileStream = new FileInputStream(file);
        return fileStream;
    }

    public void cleanData() {

        try {
            Extraction extraction = extractionService.loadExtraction("IMPORT");
            extractionService.deleteExtraction(extraction);
        } catch (DataNotFoundException | DataConstraintException e) {
            //Do nothing
        }

        try {
            Lot lot = lotService.loadLot("LOT_IMPORT");
            lotService.deleteLot(lot);
        } catch (DataNotFoundException | DataConstraintException e) {
            //Do nothing
        }

        try {
            Specimen specimen = specimenService.loadSpecimen("IMPORT");
            specimenService.deleteSpecimen(specimen);
        } catch (DataNotFoundException | DataConstraintException e) {
            //Do nothing
        }

        try {
            Campagne campagne = campagneService.loadCampagne("CAMPAGNE_IMPORT");
            campagneService.deleteCampagne(campagne);
        } catch (DataNotFoundException | DataConstraintException e) {
            //Do nothing
        }

        try {
            Station station = stationService.loadStation("STATION_IMPORT");
            stationService.deleteStation(station);
        } catch (DataNotFoundException | DataConstraintException e) {
            //Do nothing
        }
    }
}
