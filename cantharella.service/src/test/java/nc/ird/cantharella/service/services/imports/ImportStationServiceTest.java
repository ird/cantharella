package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.StationImportModel;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.service.services.StationService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;

public class ImportStationServiceTest extends AbstractServiceTest {

    @Autowired
    private StationService stationService;

    @Autowired
    private ImportDataInitService importDataInitService;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("importStationService")
    private ImportService importStationService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importStation() {

        long nbInitStations = stationService.countStations();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/stations/stationsOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importStationService.importData(fileStream, utilisateur, Locale.FRENCH, new StationImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new stations are present
        long nbStationsAfterImport = stationService.countStations();
        Assert.assertNotEquals(nbInitStations, nbStationsAfterImport);
        Assert.assertTrue(nbStationsAfterImport == nbInitStations + 1);

        // Clean data
        try {
            Station station = stationService.loadStation("TEST_IMPORT");
            stationService.deleteStation(station);
        } catch (DataNotFoundException e) {
            Assert.fail("Station not found");
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }
    }

    // Données invalides
    // - Nom absent
    // - Pays absent
    // - Latitude invalide (degres, minutes, N/S, orientation vide)
    // - Longitude invalide (degres, minutes, E/W, orientation vide)
    // - Référentiel invalide
    // - Nom existant
    // - Latitude complètement vide = ok
    // - Longitude complètement vide = ok
    @Test
    @Transactional
    public void importStationsWithErrors() {

        long nbInitStations = stationService.countStations();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/stations/stationsKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importStationService.importData(fileStream, utilisateur, Locale.FRENCH, new StationImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(15, result.getErrorLines().size());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new stations are present
        long nbStationsAfterImport = stationService.countStations();
        Assert.assertEquals(nbInitStations, nbStationsAfterImport);
    }
}
