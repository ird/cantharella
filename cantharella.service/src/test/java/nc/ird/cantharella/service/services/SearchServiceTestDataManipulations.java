package nc.ird.cantharella.service.services;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Campagne;
import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.DocumentContent;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.Extrait;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.MethodeExtraction;
import nc.ird.cantharella.data.model.MethodeTestBio;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.TestBio;
import nc.ird.cantharella.data.model.TypeDocument;
import nc.ird.cantharella.data.model.TypeExtrait;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.utils.normalizers.PersonneNormalizer;
import nc.ird.cantharella.service.utils.normalizers.utils.Normalizer;
import nc.ird.cantharella.utils.PasswordTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;

@Service
public class SearchServiceTestDataManipulations {

    @Autowired
    private PersonneService personneService;

    @Autowired
    private CampagneService campagneService;

    @Autowired
    private StationService stationService;

    @Autowired
    private SpecimenService specimenService;

    @Autowired
    private LotService lotService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private TestBioService testBioService;

    @Autowired
    private ExtractionService extractionService;

    @Autowired
    private PersonneInterrogeeService personneInterrogeeService;

    @Autowired
    private DocumentService documentService;

    private TypeDocument typeDocument;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void clearIndex() {
        personneService.listPersonnes().forEach(personne -> {
            try {

                testBioService.listResultatsTestBio((Utilisateur) personne).forEach(resultatTestBio -> {
                    try {
                        testBioService.deleteTestBio(resultatTestBio.getTestBio());
                    } catch (DataConstraintException e) {
                        e.printStackTrace();
                    }
                });

                testBioService.listMethodesTestBio().forEach(methodeTestBio -> {
                    try {
                        testBioService.deleteMethodeTestBio(methodeTestBio);
                    } catch (DataConstraintException e) {
                        e.printStackTrace();
                    }
                });

                extractionService.listExtractions((Utilisateur) personne).forEach(extraction -> {
                    try {
                        extractionService.deleteExtraction(extraction);
                    } catch (DataConstraintException e) {
                        e.printStackTrace();
                    }
                });

                extractionService.listMethodesExtraction().forEach(methodeExtraction -> {
                    try {
                        extractionService.deleteMethodeExtraction(methodeExtraction);
                    } catch (DataConstraintException e) {
                        e.printStackTrace();
                    }
                });

                lotService.listLots((Utilisateur) personne).forEach(lot -> {
                    try {
                        lotService.deleteLot(lot);
                    } catch (DataConstraintException e) {
                        e.printStackTrace();
                    }
                });

                specimenService.listSpecimens((Utilisateur) personne).forEach(specimen -> {
                    try {
                        specimenService.deleteSpecimen(specimen);
                    } catch (DataConstraintException e) {
                        e.printStackTrace();
                    }
                });

                //Delete campagne for personne
                campagneService.listCampagnes((Utilisateur) personne).forEach(campagne -> {
                    try {
                        campagneService.deleteCampagne(campagne);
                    } catch (DataConstraintException e) {
                        e.printStackTrace();
                    }
                });

                stationService.listStations((Utilisateur) personne).forEach(station -> {
                    try {
                        stationService.deleteStation(station);
                    } catch (DataConstraintException e) {
                        e.printStackTrace();
                    }
                });

                //Delete personne in the end
                personneService.deletePersonne(personne);
            } catch (DataConstraintException e) {
                e.printStackTrace();
            }
        });

        searchService.reIndex();
    }
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Utilisateur createAdmin() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = new Utilisateur();
        admin.setTypeDroit(Utilisateur.TypeDroit.ADMINISTRATEUR);
        admin.setAdressePostale("BP A5");
        admin.setCodePostal("98848");
        admin.setCourriel("test@test.com");
        admin.setNom("ADMIN");
        admin.setOrganisme("IRD");
        admin.setPasswordHash(PasswordTools.sha1("test"));
        admin.setCodePays("FR");
        admin.setPrenom("Admin");
        admin.setVille("Nouméa");
        Normalizer.normalize(PersonneNormalizer.class, admin);

        // fix user creation for test
        admin.setValide(Boolean.TRUE);
        personneService.createPersonne(admin);

        admin = personneService.loadUtilisateur("test@test.com");

        return admin;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Utilisateur initSearchLotTest() throws DataConstraintException, DataNotFoundException {

        // fix user creation for test
        Utilisateur admin = createAdmin();

        DocumentContent content = new DocumentContent();
        byte[] contentBytes = "Any String you want".getBytes();
        content.setFileContent(contentBytes);

        //create one doc
        Document doc = new Document();
        doc.setTitre("titre");
        doc.setCreateur(admin);
        doc.setAjoutePar(admin);
        doc.setEditeur("editeur");
        doc.setTypeDocument(getTypeDocument());
        doc.setDateCreation(new Date());
        doc.setFileName("test.pdf");
        doc.setFileContent(content);
        doc.setFileMimetype("mime");

        DocumentContent content2 = new DocumentContent();
        byte[] contentBytes2 = "Any String you want".getBytes();
        content2.setFileContent(contentBytes2);

        Document doc2 = new Document();
        doc2.setTitre("titre");
        doc2.setCreateur(admin);
        doc2.setAjoutePar(admin);
        doc2.setEditeur("editeur");
        doc2.setTypeDocument(getTypeDocument());
        doc2.setDateCreation(new Date());
        doc2.setFileName("test2.pdf");
        doc2.setFileContent(content2);
        doc2.setFileMimetype("mime");

        // station
        Station station = new Station();
        station.setNom("MT1");
        station.setCodePays("FR");
        station.setCreateur(admin);
        stationService.createStation(station);

        // campagne
        Campagne campagne = new Campagne();
        campagne.setNom("CORALSPOT_(GAMBIER)");
        campagne.setDateDeb(new Date());
        campagne.setDateFin(new Date());
        campagne.setCreateur(admin);
        campagne.setCodePays("FR");
        campagne.setDocuments(Collections.singletonList(doc2));
        campagneService.createCampagne(campagne);

        // create some specimens
        Specimen specimen1 = new Specimen();
        specimen1.setRef("P175");
        specimen1.setEmbranchement("Porifera");
        specimen1.setTypeOrganisme(Specimen.TypeOrganisme.PLANTE);
        specimen1.setCreateur(admin);
        specimenService.createSpecimen(specimen1);

        Specimen specimen2 = new Specimen();
        specimen2.setRef("P176");
        specimen2.setEmbranchement("Embranchement Factice");
        specimen2.setTypeOrganisme(Specimen.TypeOrganisme.PLANTE);
        specimen2.setCreateur(admin);
        specimenService.createSpecimen(specimen2);

        // create some lots
        Lot lot1 = new Lot();
        lot1.setRef("P164-MHO4");
        lot1.setStation(station);
        lot1.setEchantillonPhylo(false);
        lot1.setEchantillonColl(false);
        lot1.setEchantillonIdent(true);
        lot1.setCampagne(campagne);
        lot1.setDateRecolte(new Date());
        lot1.setSpecimenRef(specimen1);
        lot1.setCreateur(admin);
        lot1.setDocuments(Collections.singletonList(doc));
        lotService.createLot(lot1);

        Lot lot2 = new Lot();
        lot2.setRef("P164-MH15");
        lot2.setStation(station);
        lot2.setEchantillonPhylo(false);
        lot2.setEchantillonColl(false);
        lot2.setEchantillonIdent(true);
        lot2.setCampagne(campagne);
        lot2.setDateRecolte(new Date());
        lot2.setSpecimenRef(specimen2);
        lot2.setCreateur(admin);
        lotService.createLot(lot2);

        return admin;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Utilisateur initDysideaTest() throws DataConstraintException, DataNotFoundException {

        // fix user creation for test
        Utilisateur admin = createAdmin();

        // station
        Station station = new Station();
        station.setNom("MT1");
        station.setCodePays("FR");
        station.setCreateur(admin);
        stationService.createStation(station);

        // campagne
        Campagne campagne = new Campagne();
        campagne.setNom("CORALSPOT_(GAMBIER)");
        campagne.setDateDeb(new Date());
        campagne.setDateFin(new Date());
        campagne.setCreateur(admin);
        campagne.setCodePays("FR");
        campagneService.createCampagne(campagne);

        // create some specimens
        Specimen specimen1 = new Specimen();
        specimen1.setRef("P175");
        specimen1.setEmbranchement("Porifera");
        specimen1.setFamille("Dysideidae");
        specimen1.setGenre("Dysidea");
        specimen1.setEspece("4866");
        specimen1.setTypeOrganisme(Specimen.TypeOrganisme.PLANTE);
        specimen1.setCreateur(admin);
        specimenService.createSpecimen(specimen1);

        Specimen specimen2 = new Specimen();
        specimen2.setRef("P176");
        specimen2.setEmbranchement("Porifera");
        specimen2.setFamille("Dysideidae");
        specimen2.setGenre("Dysidea");
        specimen2.setEspece("2669");
        specimen2.setTypeOrganisme(Specimen.TypeOrganisme.PLANTE);
        specimen2.setCreateur(admin);
        specimenService.createSpecimen(specimen2);

        // create some lots
        /*Lot lot1 = new Lot();
        lot1.setRef("P164-MHO4");
        lot1.setStation(station);
        lot1.setEchantillonPhylo(false);
        lot1.setEchantillonColl(false);
        lot1.setEchantillonIdent(true);
        lot1.setCampagne(campagne);
        lot1.setDateRecolte(new Date());
        lot1.setSpecimenRef(specimen1);
        lot1.setCreateur(admin);
        lotService.createLot(lot1);

        Lot lot2 = new Lot();
        lot2.setRef("P164-MH15");
        lot2.setStation(station);
        lot2.setEchantillonPhylo(false);
        lot2.setEchantillonColl(false);
        lot2.setEchantillonIdent(true);
        lot2.setCampagne(campagne);
        lot2.setDateRecolte(new Date());
        lot2.setSpecimenRef(specimen2);
        lot2.setCreateur(admin);
        lotService.createLot(lot2);*/

        return admin;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAssociatedPropertySearchLotTest() throws DataConstraintException, DataNotFoundException {
        Specimen specimen1 = specimenService.loadSpecimen("P175");

        specimen1.setEmbranchement("Niphatidae");
        specimenService.updateSpecimen(specimen1);
    }


    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateRefSearchLotTest() throws DataConstraintException, DataNotFoundException {
        Lot lot1 = lotService.loadLot("P164-MHO4");

        lot1.setRef("P175-MT4+5");
        lotService.updateLot(lot1);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Utilisateur createSpecimensForSearchSpecimenTest() throws DataConstraintException, DataNotFoundException {

        // fix user creation for test
        Utilisateur admin = createAdmin();

        DocumentContent content1 = new DocumentContent();
        byte[] contentBytes = "Any String you want".getBytes();
        content1.setFileContent(contentBytes);

        //create one doc
        Document doc = new Document();
        doc.setTitre("titre");
        doc.setCreateur(admin);
        doc.setAjoutePar(admin);
        doc.setEditeur("editeur");
        doc.setTypeDocument(getTypeDocument());
        doc.setDateCreation(new Date());
        doc.setFileName("testSpecimen1.pdf");
        doc.setFileContent(content1);
        doc.setFileMimetype("mime");

        DocumentContent content2 = new DocumentContent();
        byte[] contentBytes2 = "Any String you want".getBytes();
        content2.setFileContent(contentBytes2);

        //create one doc
        Document doc2 = new Document();
        doc2.setTitre("titre");
        doc2.setCreateur(admin);
        doc2.setAjoutePar(admin);
        doc2.setEditeur("editeur");
        doc2.setTypeDocument(getTypeDocument());
        doc2.setDateCreation(new Date());
        doc2.setFileName("testSpecimen2.pdf");
        doc2.setFileContent(content2);
        doc2.setFileMimetype("mime");

        // create some specimens
        Specimen specimen1 = new Specimen();
        specimen1.setRef("P175");
        specimen1.setEmbranchement("embranchement 175");
        specimen1.setTypeOrganisme(Specimen.TypeOrganisme.PLANTE);
        specimen1.setCreateur(admin);
        specimen1.setDocuments(Collections.singletonList(doc));
        specimenService.createSpecimen(specimen1);

        Specimen specimen2 = new Specimen();
        specimen2.setRef("P174");
        specimen2.setEmbranchement("embranchement 174");
        specimen2.setTypeOrganisme(Specimen.TypeOrganisme.CHAMPIGNON);
        specimen2.setCreateur(admin);
        specimen2.setDocuments(Collections.singletonList(doc2));
        specimenService.createSpecimen(specimen2);

        return admin;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Utilisateur initTestSearchResultatTestBio() throws DataConstraintException, DataNotFoundException {
        // fix user creation for test
        Utilisateur admin = createAdmin();

        // station
        Station station = new Station();
        station.setNom("MT1");
        station.setCodePays("FR");
        station.setCreateur(admin);
        stationService.createStation(station);

        // campagne
        Campagne campagne = new Campagne();
        campagne.setNom("BSM-PF1_(MARQUISES)");
        campagne.setDateDeb(new Date());
        campagne.setDateFin(new Date());
        campagne.setCreateur(admin);
        campagne.setCodePays("FR");
        campagneService.createCampagne(campagne);

        // create some specimens
        Specimen specimen1 = new Specimen();
        specimen1.setRef("P175");
        specimen1.setEmbranchement("Porifera");
        specimen1.setTypeOrganisme(Specimen.TypeOrganisme.PLANTE);
        specimen1.setCreateur(admin);
        specimenService.createSpecimen(specimen1);

        // create some lots
        Lot lot1 = new Lot();
        lot1.setRef("P164-MHO4");
        lot1.setStation(station);
        lot1.setEchantillonPhylo(false);
        lot1.setEchantillonColl(false);
        lot1.setEchantillonIdent(true);
        lot1.setCampagne(campagne);
        lot1.setDateRecolte(new Date());
        lot1.setSpecimenRef(specimen1);
        lot1.setCreateur(admin);
        lotService.createLot(lot1);

        // type extrait
        TypeExtrait typeExtrait1 = new TypeExtrait();
        typeExtrait1.setDescription("Type extrait");
        typeExtrait1.setInitiales("TE");

        // methode extraction
        MethodeExtraction methodeExtraction1 = new MethodeExtraction();
        typeExtrait1.setMethodeExtraction(methodeExtraction1);
        methodeExtraction1.setNom("MET-01");
        methodeExtraction1.setDescription("Desc");
        methodeExtraction1.setTypesEnSortie(Collections.singletonList(typeExtrait1));
        extractionService.createMethodeExtraction(methodeExtraction1);

        // produit
        Extrait extrait1 = new Extrait();
        extrait1.setRef("P175-MT1D");
        extrait1.setTypeExtrait(typeExtrait1);
        Extraction extraction1 = new Extraction();
        extraction1.setRef("EXT-01");
        extraction1.setExtraits(Collections.singletonList(extrait1));
        extrait1.setExtraction(extraction1);
        extraction1.setDate(new Date());
        extraction1.setCreateur(admin);
        extraction1.setManipulateur(admin);
        extraction1.setLot(lot1);
        extraction1.setMethode(methodeExtraction1);
        extractionService.createExtraction(extraction1);

        // methode
        MethodeTestBio methodeTestBio1 = new MethodeTestBio();
        methodeTestBio1.setNom("Test method");
        methodeTestBio1.setCible("KB");
        methodeTestBio1.setValeurMesuree("Temperature");
        methodeTestBio1.setDomaine("domain");
        methodeTestBio1.setDescription("Temperature effective");
        methodeTestBio1.setUniteResultat("°C");
        testBioService.createMethodeTestBio(methodeTestBio1);

        // test bio
        TestBio testBio1 = new TestBio();
        testBio1.setRef("TC-T2");
        testBio1.setMethode(methodeTestBio1);
        testBio1.setCreateur(admin);
        testBio1.setManipulateur(admin);
        testBio1.setDate(new Date());
        testBio1.setOrganismeTesteur("IRD");

        DocumentContent content = new DocumentContent();
        byte[] contentBytes = "Any String you want".getBytes();
        content.setFileContent(contentBytes);

        //create one doc
        Document doc = new Document();
        doc.setTitre("titre");
        doc.setCreateur(admin);
        doc.setAjoutePar(admin);
        doc.setEditeur("editeur");
        doc.setTypeDocument(getTypeDocument());
        doc.setDateCreation(new Date());
        doc.setFileName("testResultatTestBio.pdf");
        doc.setFileContent(content);
        doc.setFileMimetype("mime");

        // resultatTestBio
        ResultatTestBio resultatTestBio1 = new ResultatTestBio();
        resultatTestBio1.setRepere("T2-E2");
        resultatTestBio1.setTypeResultat(ResultatTestBio.TypeResultat.PRODUIT);
        resultatTestBio1.setActif(true);
        resultatTestBio1.setTestBio(testBio1);
        resultatTestBio1.setProduit(extrait1);
        testBio1.setResultats(Collections.singletonList(resultatTestBio1));
        testBio1.setDocuments(Collections.singletonList(doc));
        testBioService.createTestBio(testBio1);

        return admin;
    }

    public TypeDocument getTypeDocument() {
        if (typeDocument == null) {
            //create type document
            typeDocument = new TypeDocument();
            typeDocument.setDescription("desc");
            typeDocument.setNom("monSuperTypeDeDoc");
            typeDocument.setDomaine("domaine");

            try {
                documentService.createTypeDocument(typeDocument);
            } catch (DataConstraintException e) {
                System.out.println("Could not create type document" + e);
            }
        }

        return typeDocument;
    }

    public void setTypeDocument(TypeDocument typeDocument) {
        this.typeDocument = typeDocument;
    }
}
