package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.MoleculeImportModel;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.service.services.MoleculeService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;

public class ImportMoleculeServiceTest extends AbstractServiceTest {

    @Autowired
    private MoleculeService moleculeService;

    @Autowired
    private ImportDataInitService importDataInitService;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("ImportMoleculeService")
    private ImportService importMoleculeService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importMolecule() {

        long nbInitMolecules = moleculeService.countMolecules();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/molecules/moleculesOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importMoleculeService.importData(fileStream, utilisateur, Locale.FRENCH, new MoleculeImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new molecules are present
        long nbMoleculesAfterImport = moleculeService.countMolecules();
        Assert.assertNotEquals(nbInitMolecules, nbMoleculesAfterImport);
        Assert.assertTrue(nbMoleculesAfterImport == nbInitMolecules + 1);

    }

    // Import molécules avec plusieurs produits
    @Test
    @Transactional
    public void importMultipleMolecules() {

        long nbInitMolecules = moleculeService.countMolecules();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/molecules/moleculesMultiplesOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importMoleculeService.importData(fileStream, utilisateur, Locale.FRENCH, new MoleculeImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(3, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new molecules are present
        long nbMoleculesAfterImport = moleculeService.countMolecules();
        Assert.assertNotEquals(nbInitMolecules, nbMoleculesAfterImport);
        Assert.assertTrue(nbMoleculesAfterImport == nbInitMolecules + 3);
    }

    // Import molécules avec plusieurs erreurs
    // Campagne inconnue
    // Produit inconnu
    // Format des données incorrect (pourcentage, Masse molaire, nouvelle molécule)
    @Test
    @Transactional
    public void importMoleculesWithErrors() {

        long nbInitMolecules = moleculeService.countMolecules();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/molecules/moleculesWithErrors.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importMoleculeService.importData(fileStream, utilisateur, Locale.FRENCH, new MoleculeImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(0, result.getNbImported());
            Assert.assertEquals(5, result.getErrorLines().size());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new molecules are present
        long nbMoleculesAfterImport = moleculeService.countMolecules();
        Assert.assertEquals(nbInitMolecules, nbMoleculesAfterImport);
    }
}
