package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Extraction;
import nc.ird.cantharella.data.model.Personne;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ExtractionImportModel;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.PersonneImportModel;
import nc.ird.cantharella.service.services.ExtractionService;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.service.services.PersonneService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;

public class ImportPersonneServiceTest extends AbstractServiceTest {

    @Autowired
    private PersonneService personneService;

    @Autowired
    private ImportDataInitService importDataInitService;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("importPersonneService")
    private ImportService importPersonneService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importPerson() {

        long nbInitPersons = personneService.countPersonnes();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/personnes/personnesOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importPersonneService.importData(fileStream, utilisateur, Locale.FRENCH, new PersonneImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new persons are present
        long nbPersonsAfterImport = personneService.countPersonnes();
        Assert.assertNotEquals(nbInitPersons, nbPersonsAfterImport);
        Assert.assertTrue(nbPersonsAfterImport == nbInitPersons + 1);

        // Clean data
        try {
            Personne personne = personneService.loadPersonne("testimport@import.com");
            personneService.deletePersonne(personne);
        } catch (DataNotFoundException e) {
            Assert.fail("Person not found");
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }
    }

    // Données invalides
    // - Prénom absent
    // - Nom absent
    // - Organisme absent
    // - Courriel absent
    // - Adresse postale absentes
    // - Code postal absent
    // - Ville absente
    // - Pays absent
    // - Nom déjà donné
    // - Adresse mail déjà donnée
    @Test
    @Transactional
    public void importPersonsWithErrors() {

        long nbInitPersons = personneService.countPersonnes();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/personnes/personnesKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importPersonneService.importData(fileStream, utilisateur, Locale.FRENCH, new PersonneImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(11, result.getErrorLines().size());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new extractions are present
        long nbPersonsAfterImport = personneService.countPersonnes();
        Assert.assertEquals(nbInitPersons, nbPersonsAfterImport);
    }

    // Données invalides
    // - Code pays invalide
    @Test
    @Transactional
    public void importPersonsWithErrorsOnCodePays() {

        long nbInitPersons = personneService.countPersonnes();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/personnes/personnesCodePaysKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importPersonneService.importData(fileStream, utilisateur, Locale.FRENCH, new PersonneImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getErrorLines().size());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new personnes are present
        long nbPersonsAfterImport = personneService.countPersonnes();
        Assert.assertEquals(nbInitPersons, nbPersonsAfterImport);
    }
}
