package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.MethodeTestBio;
import nc.ird.cantharella.data.model.Purification;
import nc.ird.cantharella.data.model.TestBio;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.ResultatTestBioImportModel;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.service.services.TestBioService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;

public class ImportResultatTestBioTest  extends AbstractServiceTest {

    @Autowired
    private TestBioService testBioService;

    @Autowired
    private ImportDataInitService importDataInitService;

    @Autowired
    private GenericDao dao;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("importResultatTestBioService")
    private ImportService importResultatTestBioService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importResultatTestBio() {

        long nbInitResultatTestBio = testBioService.countResultatsTestsBio();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/resultatsTestBios/resultatTestBioOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importResultatTestBioService.importData(fileStream, utilisateur, Locale.FRENCH, new ResultatTestBioImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new purifications are present
        long nbResultatTestBioAfterImport = testBioService.countResultatsTestsBio();
        Assert.assertNotEquals(nbInitResultatTestBio, nbResultatTestBioAfterImport);
        Assert.assertEquals(nbInitResultatTestBio + 1, nbResultatTestBioAfterImport);

        //Reimporting should fail because TestBio already exists
        try {
            fileStream = importDataInitService.loadFile("/importFiles/resultatsTestBios/resultatTestBioOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }
        try {
            ImportResult result = importResultatTestBioService.importData(fileStream, utilisateur, Locale.FRENCH, new ResultatTestBioImportModel());

            Assert.assertFalse(result.isValid());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Clean data
        try {
            TestBio purification = testBioService.loadTestBio("TEST");
            testBioService.deleteTestBio(purification);
        } catch (DataNotFoundException e) {
            Assert.fail("Purification not found");
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }
    }
}
