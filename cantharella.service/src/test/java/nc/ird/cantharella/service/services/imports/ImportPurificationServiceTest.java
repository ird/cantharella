package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Purification;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.LotImportModel;
import nc.ird.cantharella.service.model.PurificationImportModel;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.service.services.PurificationService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;

public class ImportPurificationServiceTest extends AbstractServiceTest {

    @Autowired
    private PurificationService purificationService;

    @Autowired
    private ImportDataInitService importDataInitService;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("importPurificationService")
    private ImportService importPurificationService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importPurification() {

        long nbInitPurifications = purificationService.countPurifications();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/purifications/purificationOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importPurificationService.importData(fileStream, utilisateur, Locale.FRENCH, new PurificationImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new purifications are present
        long nbPurificationsAfterImport = purificationService.countPurifications();
        Assert.assertNotEquals(nbInitPurifications, nbPurificationsAfterImport);
        Assert.assertTrue(nbPurificationsAfterImport == nbInitPurifications + 1);

        // Clean data
        try {
            Purification purification = purificationService.loadPurification("TEST_IMPORT");
            purificationService.deletePurification(purification);
        } catch (DataNotFoundException e) {
            Assert.fail("Purification not found");
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }
    }

    // Données invalides
    // - Produit absent
    // - Produit inconnu
    // - Masse à purifier invalide
    // - Méthode purification inconnue
    // - Masse fraction invalide
    // - Ref purification absente
    // - Nom manipulateur absent
    // - Prénom manipulateur absent
    // - Manipulateur inconnu
    // - Date absente
    // - Date invalide
    // - 2 fractions avec le même numéro pour une purification
    @Test
    @Transactional
    public void importPurificationsWithErrors() {

        long nbInitPurifications = purificationService.countPurifications();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/purifications/purificationsKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importPurificationService.importData(fileStream, utilisateur, Locale.FRENCH, new PurificationImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(12, result.getErrorLines().size());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new purifications are present
        long nbPurificationsAfterImport = purificationService.countPurifications();
        Assert.assertEquals(nbInitPurifications, nbPurificationsAfterImport);
    }
}
