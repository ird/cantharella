package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.dao.GenericDao;
import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Dosage;
import nc.ird.cantharella.data.model.TestBio;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.ResultatDosageImportModel;
import nc.ird.cantharella.service.model.ResultatTestBioImportModel;
import nc.ird.cantharella.service.services.DosageService;
import nc.ird.cantharella.service.services.ImportService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;

public class ImportResultatDosageTest extends AbstractServiceTest {

    @Autowired
    private DosageService dosageService;

    @Autowired
    private ImportDataInitService importDataInitService;

    @Autowired
    private GenericDao dao;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("importResultatDosageService")
    private ImportService importResultatDosageService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importResultatDosage() {

        long nbInitResultatDosage = dosageService.countResultatsDosage();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/dosages/resultatDosageOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importResultatDosageService.importData(fileStream, utilisateur, Locale.FRENCH, new ResultatDosageImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new dosage are present
        long nbResultatDosageAfterImport = dosageService.countResultatsDosage();
        Assert.assertNotEquals(nbInitResultatDosage, nbResultatDosageAfterImport);
        Assert.assertEquals(nbInitResultatDosage + 2, nbResultatDosageAfterImport);

        //Reimporting should fail because Dosage already exists
        try {
            fileStream = importDataInitService.loadFile("/importFiles/dosages/resultatDosageOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }
        try {
            ImportResult result = importResultatDosageService.importData(fileStream, utilisateur, Locale.FRENCH, new ResultatDosageImportModel());

            Assert.assertFalse(result.isValid());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Clean data
        try {
            Dosage dosage = dosageService.loadDosage("TEST");
            dosageService.deleteDosage(dosage);
        } catch (DataNotFoundException e) {
            Assert.fail("Dosage not found");
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }
    }
}
