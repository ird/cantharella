package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.Station;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.LotImportModel;
import nc.ird.cantharella.service.model.StationImportModel;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.service.services.LotService;
import nc.ird.cantharella.service.services.StationService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;

public class ImportLotServiceTest extends AbstractServiceTest {

    @Autowired
    private LotService lotService;

    @Autowired
    private ImportDataInitService importDataInitService;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("importLotService")
    private ImportService importLotService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importLot() {

        long nbInitLots = lotService.countLots();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/lots/lotsOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importLotService.importData(fileStream, utilisateur, Locale.FRENCH, new LotImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new lots are present
        long nbLotsAfterImport = lotService.countLots();
        Assert.assertNotEquals(nbInitLots, nbLotsAfterImport);
        Assert.assertTrue(nbLotsAfterImport == nbInitLots + 1);

        // Clean data
        try {
            Lot lot = lotService.loadLot("TEST_IMPORT");
            lotService.deleteLot(lot);
        } catch (DataNotFoundException e) {
            Assert.fail("Lot not found");
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }
    }

    // Données invalides
    // - Campagne absente
    // - Campagne inconnue
    // - Station absente
    // - Station inconnue
    // - Date récolte absente
    // - Date récolte invalide
    // - Référence absente
    // - Specimen de référence absent
    // - Specimen de référence inconnu
    // - Partie inconnue
    // - Masse fraîche invalide
    // - Masse sèche invalide
    // - Ech collection invalide
    // - Ech identification invalide
    // - Ech phylogénie invalide
    // - Lot existe déjà
    @Test
    @Transactional
    public void importLotsWithErrors() {

        long nbInitLots = lotService.countLots();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/lots/lotsKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importLotService.importData(fileStream, utilisateur, Locale.FRENCH, new LotImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(16, result.getErrorLines().size());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new lots are present
        long nbLotsAfterImport = lotService.countLots();
        Assert.assertEquals(nbInitLots, nbLotsAfterImport);
    }
}
