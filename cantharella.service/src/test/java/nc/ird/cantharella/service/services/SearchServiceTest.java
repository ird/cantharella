/*
 * #%L
 * Cantharella :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package nc.ird.cantharella.service.services;

import java.util.List;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.exceptions.DataNotFoundException;
import nc.ird.cantharella.data.model.Document;
import nc.ird.cantharella.data.model.Lot;
import nc.ird.cantharella.data.model.ResultatTestBio;
import nc.ird.cantharella.data.model.Specimen;
import nc.ird.cantharella.data.model.TypeDocument;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.DocumentSearchResult;
import nc.ird.cantharella.service.model.SearchBean;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Search service tests. Toutes les opérations de manipulation de la base sont à réaliser dans la classe
 * {@link SearchServiceTestDataManipulations} avec un @Transactional(propagation = Propagation.REQUIRES_NEW) pour que
 * l'index soit mis à jour et que les recherches puissent être effectuées.
 * 
 * @author echatellier
 */
public class SearchServiceTest extends AbstractServiceTest {

    /** Administrateur par défaut */
    @Autowired
    private Utilisateur defaultAdmin;

    @Autowired
    private SpecimenService specimenService;

    @Autowired
    private SearchService searchService;

    @Autowired
    SearchServiceTestDataManipulations manips;

    /**
     * Clean everything between each test (index is not transactional so not rollbacked and  transactions are commited
     * so index is updated)
     */
    @Before
    public void clearIndex() {
        manips.clearIndex();
    }

    /**
     * Test to create some specimen and search for it.
     * 
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchSpecimen() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.createSpecimensForSearchSpecimenTest();

        // test to create some entity
        Assert.assertEquals(2, specimenService.countSpecimens());

        // search into lucene
        List<Specimen> specimens = searchService.search(new SearchBean("P175"), admin).getSpecimens();
        Assert.assertEquals("P175", specimens.get(0).getRef());
        specimens = searchService.search(new SearchBean("P174"), admin).getSpecimens();
        Assert.assertEquals("P174", specimens.get(0).getRef());
        specimens = searchService.search(new SearchBean("P17*"), admin).getSpecimens();
        Assert.assertEquals(2, specimens.size());
    }

    /**
     * Test to create some specimen doc and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchCampagneDocuments() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initSearchLotTest();

        // search into lucene
        DocumentSearchResult searchResult = searchService.searchDocs(new SearchBean("CORALSPOT_(GAMBIER)"), admin);
        List<Document> docs = searchResult.getCampagneDocs();
        Assert.assertEquals("test2.pdf", docs.get(0).getFileName());
        Assert.assertEquals("CORALSPOT_(GAMBIER)",searchResult.getCampagnes().get(docs.get(0).getCampagneId()).getNom());
        docs = searchService.searchDocs(new SearchBean("Foo"), admin).getCampagneDocs();
        Assert.assertTrue(docs.isEmpty());
    }

    /**
     * Test to create some campagne doc and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchSpecimenDocuments() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.createSpecimensForSearchSpecimenTest();

        // test to create some entity
        Assert.assertEquals(2, specimenService.countSpecimens());

        // search into lucene
        DocumentSearchResult searchResult = searchService.searchDocs(new SearchBean("P175"), admin);
        List<Document> docs = searchResult.getSpecimenDocs();
        Assert.assertEquals("testSpecimen1.pdf", docs.get(0).getFileName());
        Assert.assertEquals("P175",searchResult.getSpecimens().get(docs.get(0).getSpecimenId()).getRef());
        docs = searchService.searchDocs(new SearchBean("P174"), admin).getSpecimenDocs();
        Assert.assertEquals("testSpecimen2.pdf", docs.get(0).getFileName());
        docs = searchService.searchDocs(new SearchBean("Foo"), admin).getSpecimenDocs();
        Assert.assertTrue(docs.isEmpty());
    }

    /**
     * Test to create some specimen doc and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchSpecimenDocumentsWithFilter() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.createSpecimensForSearchSpecimenTest();

        // test to create some entity
        Assert.assertEquals(2, specimenService.countSpecimens());

        // search into lucene, ok filter
        List<Document> docs = searchService.searchDocs(new SearchBean("P175", manips.getTypeDocument()), admin).getSpecimenDocs();
        Assert.assertEquals("testSpecimen1.pdf", docs.get(0).getFileName());
        docs = searchService.searchDocs(new SearchBean("P174"), admin).getSpecimenDocs();
        Assert.assertEquals("testSpecimen2.pdf", docs.get(0).getFileName());
        docs = searchService.searchDocs(new SearchBean("Foo"), admin).getSpecimenDocs();
        Assert.assertTrue(docs.isEmpty());

        // search into lucene, ko filter
        docs = searchService.searchDocs(new SearchBean("P175", new TypeDocument()), admin).getSpecimenDocs();
        Assert.assertTrue(docs.isEmpty());
        docs = searchService.searchDocs(new SearchBean("P174", new TypeDocument()), admin).getSpecimenDocs();
        Assert.assertTrue(docs.isEmpty());
        docs = searchService.searchDocs(new SearchBean("Foo", new TypeDocument()), admin).getSpecimenDocs();
        Assert.assertTrue(docs.isEmpty());
    }

    /**
     * Test to create some lot and search for it.
     * 
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchLot() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initSearchLotTest();

        // search into lucene
        List<Lot> lots = searchService.search(new SearchBean("P164-MHO4"), admin).getLots();
        Assert.assertEquals("P164-MHO4", lots.get(0).getRef());
        lots = searchService.search(new SearchBean("*"), admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals(2, lots.size());

        lots = searchService.search(new SearchBean("P164-MHO*"), admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals("P164-MHO4", lots.get(0).getRef());

        lots = searchService.search(new SearchBean("Foo"), admin).getLots();
        Assert.assertTrue(lots.isEmpty());

        lots = searchService.search(new SearchBean("CORALSPOT*"), admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals(2, lots.size());

        lots = searchService.search(new SearchBean("CORALSPOT_(GAMBIER)"), admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals(2, lots.size());

        //test de recherche avec filtre pays
        SearchBean search = new SearchBean("*");
        search.setCountry("FR");
        lots = searchService.search(search, admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals(2, lots.size());
        search.setCountry("PF");
        lots = searchService.search(search, admin).getLots();
        Assert.assertTrue(lots.isEmpty());


        // test de recherche sur les entités liées
        lots = searchService.search(new SearchBean("porifera"), admin).getLots();
        Assert.assertEquals("P164-MHO4", lots.get(0).getRef());

        // update associated property
        manips.updateAssociatedPropertySearchLotTest();
        lots = searchService.search(new SearchBean("porifera"), admin).getLots();
        Assert.assertTrue(lots.isEmpty());
        lots = searchService.search(new SearchBean("niphatidae"), admin).getLots();
        Assert.assertEquals("P164-MHO4", lots.get(0).getRef());

        // update direct entity
        manips.updateRefSearchLotTest();
        lots = searchService.search(new SearchBean("porifera"), admin).getLots();
        Assert.assertTrue(lots.isEmpty());
        lots = searchService.search(new SearchBean("niphatidae"), admin).getLots();
        Assert.assertEquals("P175-MT4+5", lots.get(0).getRef());
    }

    @Test
    public void testSearchDysidea() throws Exception {

        Utilisateur admin = manips.initDysideaTest();

        // search into lucene
        List<Specimen> specimen = searchService.search(new SearchBean("Dysidea"), admin).getSpecimens();
        Assert.assertEquals(2, specimen.size());

        specimen = searchService.search(new SearchBean("+Dysidea +4866"), admin).getSpecimens();
        Assert.assertEquals(1, specimen.size());
        Assert.assertEquals("4866", specimen.get(0).getEspece());

        specimen = searchService.search(new SearchBean("Dysidea AND 4866"), admin).getSpecimens();
        Assert.assertEquals(1, specimen.size());
        Assert.assertEquals("4866", specimen.get(0).getEspece());

        specimen = searchService.search(new SearchBean("Dysidea OR 4866"), admin).getSpecimens();
        Assert.assertEquals(2, specimen.size());

        specimen = searchService.search(new SearchBean("+Dysidea -2669"), admin).getSpecimens();
        Assert.assertEquals(1, specimen.size());
        Assert.assertEquals("4866", specimen.get(0).getEspece());

        specimen = searchService.search(new SearchBean("Dysidea NOT 2669"), admin).getSpecimens();
        Assert.assertEquals(1, specimen.size());
        Assert.assertEquals("4866", specimen.get(0).getEspece());
    }

    /**
     * Test to create some lot and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchLotOr() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initSearchLotTest();

        // search into lucene
        List<Lot> lots = searchService.search(new SearchBean("P164-MHO4"), admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals("P164-MHO4", lots.get(0).getRef());
        lots = searchService.search(new SearchBean("P164-MH15"), admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals("P164-MH15", lots.get(0).getRef());
        //Test dans ou avec le mot clé
        lots = searchService.search(new SearchBean("P164-MHO4 or P164-MH15"), admin).getLots();
        Assert.assertEquals(2, lots.size());
    }

    /**
     * Test to create some lot and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchLowercase() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initSearchLotTest();

        // search into lucene
        List<Lot> lots = searchService.search(new SearchBean("P164-mhO4"), admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals("P164-MHO4", lots.get(0).getRef());
        lots = searchService.search(new SearchBean("P164-Mh15"), admin).getLots();
        Assert.assertFalse(lots.isEmpty());
        Assert.assertEquals("P164-MH15", lots.get(0).getRef());
    }

    /**
     * Test to create some lot and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchLotDocuments() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initSearchLotTest();

        // search into lucene
        DocumentSearchResult searchResult = searchService.searchDocs(new SearchBean("P164-MHO4", manips.getTypeDocument()), admin);
        List<Document> docs = searchResult.getLotDocs();
        Assert.assertEquals("test.pdf", docs.get(0).getFileName());
        Assert.assertEquals("P164-MHO4", searchResult.getLots().get(docs.get(0).getLotId()).getRef());
        docs = searchService.searchDocs(new SearchBean("Foo"), admin).getLotDocs();
        Assert.assertTrue(docs.isEmpty());
    }

    /**
     * Test to create some lot and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchLotDocumentsWithFilter() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initSearchLotTest();

        // search into lucene ok filter
        DocumentSearchResult searchResult = searchService.searchDocs(new SearchBean("P164-MHO4", manips.getTypeDocument()), admin);
        List<Document> docs = searchResult.getLotDocs();
        Assert.assertEquals("test.pdf", docs.get(0).getFileName());
        Assert.assertEquals("P164-MHO4", searchResult.getLots().get(docs.get(0).getLotId()).getRef());
        docs = searchService.searchDocs(new SearchBean("Foo"), admin).getLotDocs();
        Assert.assertTrue(docs.isEmpty());

        // search into lucene, ko filter
        docs = searchService.searchDocs(new SearchBean("P164-MHO4", new TypeDocument()), admin).getLotDocs();
        Assert.assertTrue(docs.isEmpty());
        docs = searchService.searchDocs(new SearchBean("Foo"), admin).getLotDocs();
        Assert.assertTrue(docs.isEmpty());
    }

    /**
     * Test to create some resultat test bio and search for it.
     * 
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchResultatTestBio() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initTestSearchResultatTestBio();

        // test search resultatbio
        List<ResultatTestBio> resultatTestBios = searchService.search(new SearchBean("BSM-PF1_(MARQUISES)"), admin)
                .getResultatTestBios();
        Assert.assertFalse(resultatTestBios.isEmpty());
        resultatTestBios = searchService.search(new SearchBean("BSM-PF1"), admin)
                .getResultatTestBios();
        Assert.assertTrue(resultatTestBios.isEmpty());
    }

    /**
     * Test to create some resultat test bio and search for it with filter on pays.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchResultatTestBioFiltrePays() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initTestSearchResultatTestBio();

        /* Recherche nom et filtre sur pays */
        SearchBean searchBean = new SearchBean("BSM-PF1_(MARQUISES)");
        searchBean.setCountry("FR");
        List<ResultatTestBio> resultatTestBios = searchService.search(searchBean, admin)
                .getResultatTestBios();
        Assert.assertFalse(resultatTestBios.isEmpty());

        /* Recherche * et filtre sur pays */
        searchBean = new SearchBean("*");
        searchBean.setCountry("FR");
        resultatTestBios = searchService.search(searchBean, admin).getResultatTestBios();
        Assert.assertFalse(resultatTestBios.isEmpty());
    }

    /**
     * Test to create some resultat test bio documents and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchResultatTestBioDocuments() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initTestSearchResultatTestBio();

        // test search resultatbio
        DocumentSearchResult searchResult = searchService.searchDocs(new SearchBean("BSM-PF1_(MARQUISES)"), admin);
        List<Document> docs = searchResult.getResultatTestBioDocs();
        Assert.assertEquals("testResultatTestBio.pdf", docs.get(0).getFileName());
        Assert.assertEquals("TC-T2", searchResult.getTestBios().get(docs.get(0).getResultatTestBioId()).getRef());
    }

    /**
     * Test to create some resultat test bio documents and search for it.
     *
     * @throws DataConstraintException never thrown in test
     */
    @Test
    public void testSearchResultatTestBioDocumentsWithFilter() throws DataConstraintException, DataNotFoundException {

        Utilisateur admin = manips.initTestSearchResultatTestBio();

        // test search resultatbio ok filter
        List<Document> docs = searchService.searchDocs(new SearchBean("BSM-PF1_(MARQUISES)", manips.getTypeDocument()), admin).getResultatTestBioDocs();
        Assert.assertEquals("testResultatTestBio.pdf", docs.get(0).getFileName());

        // test search resultatbio ko filter
        docs = searchService.searchDocs(new SearchBean("BSM-PF1_(MARQUISES)", new TypeDocument()), admin).getResultatTestBioDocs();
        Assert.assertTrue(docs.isEmpty());
    }
}
