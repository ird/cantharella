package nc.ird.cantharella.service.services.imports;

import nc.ird.cantharella.data.exceptions.DataConstraintException;
import nc.ird.cantharella.data.model.Utilisateur;
import nc.ird.cantharella.service.AbstractServiceTest;
import nc.ird.cantharella.service.model.ImportResult;
import nc.ird.cantharella.service.model.SpecimenImportModel;
import nc.ird.cantharella.service.services.ImportService;
import nc.ird.cantharella.service.services.SpecimenService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;

public class ImportSpecimenServiceTest extends AbstractServiceTest {

    @Autowired
    private SpecimenService specimenService;

    @Autowired
    private ImportDataInitService importDataInitService;

    /**
     * Import service
     */
    @Autowired
    @Qualifier("importSpecimenService")
    private ImportService importSpecimenService;

    private Utilisateur utilisateur;

    @Before
    public void initData() throws DataConstraintException {
        utilisateur = importDataInitService.initData();
    }

    @After
    public void cleanData() {
        importDataInitService.cleanData();
    }

    @Test
    @Transactional
    public void importSpecimen() {

        long nbInitSpecimens = specimenService.countSpecimens();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/specimens/specimensOK.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importSpecimenService.importData(fileStream, utilisateur, Locale.FRENCH, new SpecimenImportModel());

            Assert.assertTrue(result.isValid());
            Assert.assertTrue(result.getErrorLines().isEmpty());
            Assert.assertEquals(1, result.getNbImported());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that the new specimens are present
        long nbSpecimensAfterImport = specimenService.countSpecimens();
        Assert.assertNotEquals(nbInitSpecimens, nbSpecimensAfterImport);
        Assert.assertTrue(nbSpecimensAfterImport == nbInitSpecimens + 1);


    }

    // Données invalides
    // - Référence absente
    // - Référence trop longue
    // - Embranchement absent
    // - Type invalide
    // - Manipulateur inconnu
    // - Station inconnue
    // - Date dépôt invalide
    @Test
    @Transactional
    public void importSpecimensWithErrors() {

        long nbInitSpecimens = specimenService.countSpecimens();

        FileInputStream fileStream = null;
        try {
            fileStream = importDataInitService.loadFile("/importFiles/specimens/specimensKO.csv");
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }

        try {
            ImportResult result = importSpecimenService.importData(fileStream, utilisateur, Locale.FRENCH, new SpecimenImportModel());

            Assert.assertFalse(result.isValid());
            Assert.assertFalse(result.getErrorLines().isEmpty());
            Assert.assertEquals(7, result.getErrorLines().size());
        } catch (DataConstraintException e) {
            Assert.fail(e.getMessage());
        }

        // Check that no new specimens are present
        long nbSpecimensAfterImport = specimenService.countSpecimens();
        Assert.assertEquals(nbInitSpecimens, nbSpecimensAfterImport);
    }
}
