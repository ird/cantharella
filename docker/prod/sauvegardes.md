# Mise en place

* Adapter le script `backup.sh` à votre architecture, puis l'installer: `sudo install -v backup.sh /usr/local/bin`
* Ajouter une entrée du type `0 22 * * * backup.sh` dans la crontab du serveur (avec `crontab -e` par exemple)

# Exemple de vérification

* Lancer un conteneur Postgresql en arrière plan: `sudo docker run -d --name pg_test -e POSTGRES_USER=cantharella -e POSTGRES_DB=cantharella -e POSTGRES_PASSWORD=xxxxxxxx postgres:13-alpine`
* Copier un dump dans le conteneur: `sudo docker cp /backups/2022-12-09.dump pg_test:/tmp`
* Lancer la restauration: `sudo docker exec pg_test pg_restore -U cantharella -d cantharella /tmp/2022-12-09.dump`
* Vérifier si les tables sont bien présentes
  * Ouvrir un shell psql avec `sudo docker exec -it pg_test psql -U cantharella`
  * Afficher les tables avec `\dt`
  * On devrait obtenir un résultat similaire à:
    ```
    psql (13.9)
    Type "help" for help.

    cantharella=# \dt
                         List of relations
     Schema |            Name             | Type  |    Owner
    --------+-----------------------------+-------+-------------
     public | campagne                    | table | cantharella
     public | campagne_station            | table | cantharella
     public | campagnegroupedroits        | table | cantharella
     public | campagnepersonnedroits      | table | cantharella
     public | campagnepersonneparticipant | table | cantharella
     public | document                    | table | cantharella
     public | documentcontent             | table | cantharella
     public | erreurtestbio               | table | cantharella
     public | extraction                  | table | cantharella
     public | extrait                     | table | cantharella
     public | fraction                    | table | cantharella
     public | groupe                      | table | cantharella
     public | lot                         | table | cantharella
     public | lotgroupedroits             | table | cantharella
     public | lotpersonnedroits           | table | cantharella
     public | methodeextraction           | table | cantharella
     public | methodepurification         | table | cantharella
     public | methodetestbio              | table | cantharella
     public | molecule                    | table | cantharella
     public | moleculeprovenance          | table | cantharella
     public | parammethopuri              | table | cantharella
     public | parammethopurieffectif      | table | cantharella
     public | partie                      | table | cantharella
     public | personne                    | table | cantharella
     public | produit                     | table | cantharella
     public | purification                | table | cantharella
     public | resultattestbio             | table | cantharella
     public | specimen                    | table | cantharella
     public | station                     | table | cantharella
     public | testbio                     | table | cantharella
     public | typedocument                | table | cantharella
     public | typeextrait                 | table | cantharella
     public | utilisateur                 | table | cantharella
    (33 rows)
    ```
* Sortir du conteneur avec Ctrl + D
* Effacer le conteneur: `sudo docker rm -f pg_test`
