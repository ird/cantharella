#!/bin/sh

set -e

DOSSIER_BACKUPS="/backups"
SAUVEGARDE_JOUR_J="$(date +%Y-%m-%d).dump"
SAUVEGARDE_JOUR_J_moins_1="$(date -d "1 day ago" +%Y-%m-%d).dump"

# Sauvegarde de la base de donées
echo "Sauvegarde de la base Postgres..."
sudo docker exec cantharella-db pg_dump -Fc -U cantharella cantharella > "$DOSSIER_BACKUPS/$SAUVEGARDE_JOUR_J"
echo "Sauvegarde terminée. MD5: $(md5sum "$DOSSIER_BACKUPS/$SAUVEGARDE_JOUR_J" | cut -f1)"

if [ -f "$DOSSIER_BACKUPS/$SAUVEGARDE_JOUR_J" -a -f "$DOSSIER_BACKUPS/$SAUVEGARDE_JOUR_J_moins_1" ]; then
  echo "Des sauvegardes ont été retrouvées pour les deux derniers jours. On efface les autres fichiers:"
  find "$DOSSIER_BACKUPS" -maxdepth 1 -type f ! -name "$SAUVEGARDE_JOUR_J" ! -name "$SAUVEGARDE_JOUR_J_moins_1" -print -delete
fi
