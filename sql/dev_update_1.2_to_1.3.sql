---
-- #%L
-- Cantharella :: Data
-- $Id$
-- $HeadURL$
-- %%
-- Copyright (C) 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Mise à jour à appliquer à la base de données
-- pour les modifications effectuées pendant le développement


BEGIN;
-- Documents (16/11/2012) - update (25/03/2013)
    alter table Campagne
        add column administrateur_idPersonne int4;

    alter table Campagne
        add constraint FK30AF64AF81B953A7
        foreign key (administrateur_idPersonne)
        references Personne;

    update Campagne set administrateur_idPersonne = createur_idPersonne;

    ALTER TABLE Campagne ALTER COLUMN administrateur_idpersonne SET NOT NULL;


COMMIT;