---
-- #%L
-- Cantharella :: Data
-- $Id$
-- $HeadURL$
-- %%
-- Copyright (C) 2013 IRD (Institut de Recherche pour le Developpement) and by respective authors (see below)
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Mise à jour à appliquer à la base de données
-- pour les modifications effectuées pendant le développement


BEGIN;
-- Dosages (19/03/2024)

    create table MethodeDosage (
        idMethodeDosage int4 not null,
        description text not null,
        domaine varchar(255) not null,
        nom varchar(255) unique not null,
        acronyme varchar(255) unique not null,
        unite varchar(255) not null,
        valeurMesuree varchar(255) not null,
        seuil varchar(255),
        primary key (idMethodeDosage)
    );

    create table ErreurDosage (
        idErreurDosage int4 not null,
        description text not null,
        nom varchar(255) unique not null,
        primary key (idErreurDosage)
    );

    create table Dosage (
        idDosage int4 not null,
        complement text,
        concMasseDefaut numeric(9, 4),
        date date not null,
        organisme varchar(255) not null,
        ref varchar(255) unique not null,
        uniteConcMasseDefaut_idUnite int4,
        createur_idPersonne int4 not null,
        manipulateur_idPersonne int4 not null,
        methode_idMethodeDosage int4 not null,
        primary key (idDosage)
    );

    create table ResultatDosage (
        id int4 not null,
        analyseNb int4 not null,
        concMasse numeric(9, 4),
        supSeuil bool,
        uniteConcMasse_idUnite int4,
        composeDose varchar(255),
        moleculeDosee_idMolecule int4,
        valeur numeric(9, 4),
        SD numeric(9, 4),
        erreur_idErreurDosage int4,
        dosage_idDosage int4 not null,
        produit_id int4,
        lot_idLot int4,
        primary key (id)
    );

    alter table Document
        add column dosage int4;

    alter table Document
        add constraint FK_dosage
        foreign key (dosage)
        references Dosage;

-- Configurations (19/03/2024)
    create table UniteConcMasse (
        idUnite int4 not null,
        code varchar(255) unique not null,
        valeur varchar(255) unique not null,
        primary key (idUnite)
    );

    INSERT INTO UniteConcMasse (idUnite, code, valeur)
        VALUES
        (0, 'MG', 'mg'),
        (1, 'MICROG', 'μg'),
        (2, 'NG', 'ng'),
        (3, 'MG_ML', 'mg/ml'),
        (4, 'MICROG_ML', 'μg/ml'),
        (5, 'NG_ML', 'ng/ml');

    alter table ResultatTestBio add column uniteConcMasse_idUnite int4;

    alter table TestBio add column uniteConcMasseDefaut_idUnite int4;

    update ResultatTestBio set uniteConcMasse_idUnite = uniteConcMasse;

    update TestBio set uniteConcMasseDefaut_idUnite = uniteConcMasseDefaut;

    alter table TestBio drop column uniteConcMasseDefaut;

    alter table ResultatTestBio drop column uniteConcMasse;

    alter table Dosage
        add constraint FK_uniteConcMasseDefaut
        foreign key (uniteConcMasseDefaut_idUnite)
        references UniteConcMasse;

    alter table ResultatDosage
        add constraint FK_uniteConcMasse
        foreign key (uniteConcMasse_idUnite)
        references UniteConcMasse;

    alter table TestBio
        add constraint FK_uniteConcMasseDefaut
        foreign key (uniteConcMasseDefaut_idUnite)
        references UniteConcMasse;

    alter table ResultatTestBio
        add constraint FK_uniteConcMasse
        foreign key (uniteConcMasse_idUnite)
        references UniteConcMasse;

    create table Configuration (
      idConfiguration int4 not null,
      maxFileSize int4,
      messageAccueil varchar,
      droitAcces varchar,
      editeur varchar,
      hebergement varchar,
      droitsReproduction varchar,
      logo bytea
    );

    INSERT INTO Configuration (idConfiguration, maxFileSize, messageAccueil)
        VALUES
        (1, 5, 'Super message d''accueil');


    -- PersonnesInterrogees/Remedes (14/06/2024)
    create table PersonneInterrogee (
        idPersonne int4 not null,
        campagne_idCampagne int4 not null,
        station_idStation int4 not null,
        nom varchar(255) not null,
        prenom varchar(255) not null,
        genre  int4 not null,
        anneeNaissance int4,
        experience int4,
        profession varchar(255),
        labellisation varchar(255),
        telephone varchar(255),
        courriel varchar(255),
        adressePostale text,
        ville varchar(255) not null,
        codePostal varchar(10),
        codePays varchar(10) not null,
        createur_idPersonne int4 not null,
        complement text,
        primary key (idPersonne)
    );

    alter table Document
        add column personneInterrogee int4;

    alter table Document
        add constraint FK_personneInterrogee
        foreign key (personneInterrogee)
        references PersonneInterrogee;

    create table Disponibilite (
        idDisponibilite int4 not null,
        nom varchar(255) unique not null,
        primary key (idDisponibilite)
    );

    insert into Disponibilite (idDisponibilite, nom)
        values
        (0, 'très commune'),
        (1, 'commune'),
        (2, 'rare'),
        (3, 'très rare');

    CREATE TABLE EntreeReferentiel (
      idEntree int4 NOT NULL,
      referentiel int4 NOT NULL,
      code varchar(10) NOT NULL,
      valeur varchar(255) NULL,
      PRIMARY KEY (identree),
      UNIQUE (referentiel, code)
    );

    create table Remede (
        idRemede int4 not null,
        campagne_idCampagne int4 not null,
        createur_idPersonne int4 not null,
        station_idStation int4 not null,
        date date not null,
        enqueteur_idPersonne int4 not null,
        reference varchar(255) not null,
        prescripteur_idPersonne int4,
        consentement_idDocument int4,
        nom varchar(255),
        codeLangue varchar(10),
        indicationBiomedicale text,
        indicationVernaculaire text,
        nbIngredients int4,
        preparation text,
        conservation text,
        administration text,
        posologie text,
        recommandations bool,
        recommandationsText text,
        effetsIndesirables text,
        patientsParAn int4,
        dispoAnalyse bool,
        dispoAnalyseLabo varchar(255),
        complement text,
        primary key (idRemede)
        FOREIGN KEY (createur_idPersonne) REFERENCES Personne (idPersonne)
    )

    create table Remede_EntreeReferentiel (
      idEntree int4 NOT NULL,
      idRemede int4 NOT NULL,
      PRIMARY KEY (idEntree, idRemede),
      FOREIGN KEY (idEntree) REFERENCES EntreeReferentiel (idEntree),
      FOREIGN KEY (idRemede) REFERENCES Remede (idRemede)
    )

    create table IngredientRemede (
        idIngredient int4 not null,
        specimen_idSpecimen int4 not null,
        nomVernaculaire varchar(255),
        partie_idPartie int4,
        provenance_idStation int4,
        periode varchar(255),
        disponibilite varchar(255),
        remede_idRemede int4 not null,
        primary key (idIngredient),
        foreign key (remede_idRemede) references remede(idRemede)
    )

    alter table Document
        add column remede int4;

    alter table Document
        add constraint FK_remede
        foreign key (remede)
        references Remede;

    insert into TypeDocument (idTypeDocument, description, domaine, nom)
        select max(idTypeDocument) + 1, 'Formulaire de consentement', 'Autorisation', 'Consentement' from TypeDocument;

    create table RemedePersonneDroits (
        droitExtrait bool,
        droitPuri bool,
        droitRecolte bool,
        droitTestBio bool,
        pk2_idPersonne int4 not null,
        pk1_idRemede int4 not null,
        primary key (pk1_idRemede, pk2_idPersonne)
    );

    alter table RemedePersonneDroits
        add constraint FKremedePersonneDroit_Remede
        foreign key (pk1_idRemede)
        references Remede;

    alter table RemedePersonneDroits
        add constraint FKremedePersonneDroit_Personne
        foreign key (pk2_idPersonne)
        references Personne;

    alter table Document
        alter column fileMimetype type VARCHAR(100);


COMMIT;